/*
 * cocos2d for iPhone: http://www.cocos2d-iphone.org
 *
 * Copyright (c) 2011 Ricardo Quesada
 * Copyright (c) 2012 Zynga Inc.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

const char* ccHigame3D_Skin_vert = STRINGIFY(

attribute vec3 a_position;
attribute vec2 a_texCoord;
attribute vec4 a_color;
attribute vec4 a_blendIndex;
attribute vec4 a_blendWeight;
uniform mat4 u_bones[50];
uniform vec4 u_color;
uniform vec2 u_uvtrans;

\n#ifdef GL_ES\n
varying lowp vec4 v_fragmentColor;
varying mediump vec2 v_texCoord;
\n#else\n
varying vec4 v_fragmentColor;
varying vec2 v_texCoord;
\n#endif\n

void main()
{
    vec4 pos;
    int index1 = int(a_blendIndex[0]);
    int index2 = int(a_blendIndex[1]);
    if( index1 < 50 && index2 < 50)
    {
        
        float fWeight1 = a_blendWeight[0];
        if(fWeight1 > 0.0)
        {
            mat4 matrix = u_bones[index1] * fWeight1;
            float fWeight2 = a_blendWeight[1];
            if(fWeight2 > 0.0)
            {
                matrix += u_bones[index2] * fWeight2;
            }
            
            pos = matrix * vec4(a_position, 1.0); 
        }
           
    }
    else
    {
        pos = vec4(a_position, 1.0); 
    }

    gl_Position = CC_MVPMatrix * pos;
    vec4 color = a_color * u_color;
    v_fragmentColor = color * vec4(color.w, color.w, color.w, 1);
    v_texCoord = a_texCoord + u_uvtrans;
}
);
