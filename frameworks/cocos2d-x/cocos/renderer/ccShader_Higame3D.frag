/*
 * cocos2d for iPhone: http://www.cocos2d-iphone.org
 *
 * Copyright (c) 2011 Ricardo Quesada
 * Copyright (c) 2012 Zynga Inc.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

const char* ccHigame3D_frag = R"(
#ifdef GL_ES
precision mediump float;
#endif

#ifdef GL_ES
uniform lowp int u_has_colorkey;
#else
uniform int u_has_colorkey;
#endif

#ifdef HIGAME_FLOW_LIGHT

uniform	vec4 u_flow_light_color;
uniform	vec2 u_light_speed;
uniform	float u_light_width;
uniform	float u_light_time;


#endif


varying vec4 v_fragmentColor;
varying vec2 v_texCoord;



#ifdef HIGAME_NORMAL_SPECULAR

uniform float u_specular;

varying float v_fresnel;
varying vec3 v_normal;

#endif


void main()									
{											
	vec4 resultColor = v_fragmentColor * texture2D(CC_Texture0, v_texCoord);
	//vec4 resultColor = texture2D(CC_Texture0, v_texCoord);

#ifdef HIGAME_NORMAL_SPECULAR

	vec4 secuperColor = v_fragmentColor * texture2D(CC_Texture1, v_texCoord);
	secuperColor.rgb *= vec3(v_fresnel*u_specular, v_fresnel*u_specular, v_fresnel*u_specular);
	resultColor.rgb += secuperColor.rgb;
	
#endif

#ifdef HIGAME_FLOW_LIGHT

	vec2 uv = v_texCoord / u_light_width;
	float time = u_light_time;
	vec2 speed = u_light_speed;
	uv.y += time * speed.y;
	uv.x += time * speed.x;

	vec3 light = texture2D(CC_Texture2, uv).rgb;
	float mask = texture2D(CC_Texture2, v_texCoord).a;
	//vec3 lightColor = u_flow_light_color.rgb * mask;

	resultColor.rgb += light * u_flow_light_color.rgb *  u_flow_light_color.a * mask;

#endif


	gl_FragColor = resultColor; 
	if(u_has_colorkey>0)
	{
		if(gl_FragColor.a <= 0.5)				
			discard;
	}
}
)";