
--------------------------------
-- @module AbstractCheckButton
-- @extend Widget
-- @parent_module ccui

--------------------------------
-- Query the button title content.<br>
-- return Get the button's title content.
-- @function [parent=#AbstractCheckButton] getTitleText 
-- @param self
-- @return string#string ret (return value: string)
        
--------------------------------
-- Change CheckBox state.<br>
-- Set to true will cause the CheckBox's state to "selected", false otherwise.<br>
-- param selected Set to true will change CheckBox to selected state, false otherwise.
-- @function [parent=#AbstractCheckButton] setSelected 
-- @param self
-- @param #bool selected
-- @return AbstractCheckButton#AbstractCheckButton self (return value: ccui.AbstractCheckButton)
        
--------------------------------
--  replaces the current Label node with a new one 
-- @function [parent=#AbstractCheckButton] setTitleLabel 
-- @param self
-- @param #cc.Label label
-- @return AbstractCheckButton#AbstractCheckButton self (return value: ccui.AbstractCheckButton)
        
--------------------------------
-- Change the font size of button's title<br>
-- param size Title font size in float.
-- @function [parent=#AbstractCheckButton] setTitleFontSize 
-- @param self
-- @param #float size
-- @return AbstractCheckButton#AbstractCheckButton self (return value: ccui.AbstractCheckButton)
        
--------------------------------
-- brief Return the sprite instance of background when selected<br>
-- return the sprite instance of background when selected
-- @function [parent=#AbstractCheckButton] getRendererBackgroundSelected 
-- @param self
-- @return Sprite#Sprite ret (return value: cc.Sprite)
        
--------------------------------
-- 
-- @function [parent=#AbstractCheckButton] getBackDisabledFile 
-- @param self
-- @return ResourceData#ResourceData ret (return value: cc.ResourceData)
        
--------------------------------
-- Return the inner title renderer of Button.<br>
-- return The button title.<br>
-- since v3.3
-- @function [parent=#AbstractCheckButton] getTitleRenderer 
-- @param self
-- @return Label#Label ret (return value: cc.Label)
        
--------------------------------
-- brief Return a zoom scale<br>
-- return A zoom scale of Checkbox.<br>
-- since v3.3
-- @function [parent=#AbstractCheckButton] getZoomScale 
-- @param self
-- @return float#float ret (return value: float)
        
--------------------------------
-- brief Return the sprite instance of front cross<br>
-- return the sprite instance of front cross
-- @function [parent=#AbstractCheckButton] getRendererFrontCross 
-- @param self
-- @return Sprite#Sprite ret (return value: cc.Sprite)
        
--------------------------------
-- Change the color of button's title.<br>
-- param color The title color in Color3B.
-- @function [parent=#AbstractCheckButton] setTitleColor 
-- @param self
-- @param #color3b_table color
-- @return AbstractCheckButton#AbstractCheckButton self (return value: ccui.AbstractCheckButton)
        
--------------------------------
-- Query the font name of button's title<br>
-- return font name in std::string
-- @function [parent=#AbstractCheckButton] getTitleFontName 
-- @param self
-- @return string#string ret (return value: string)
        
--------------------------------
-- Load background disabled state texture for checkbox.<br>
-- param backGroundDisabled    The background disabled state texture name.<br>
-- param texType    @see `Widget::TextureResType`
-- @function [parent=#AbstractCheckButton] loadTextureBackGroundDisabled 
-- @param self
-- @param #string backGroundDisabled
-- @param #int texType
-- @return AbstractCheckButton#AbstractCheckButton self (return value: ccui.AbstractCheckButton)
        
--------------------------------
-- Load cross texture for check button.<br>
-- param crossTextureName    The cross texture name.<br>
-- param texType    @see `Widget::TextureResType`
-- @function [parent=#AbstractCheckButton] loadTextureFrontCross 
-- @param self
-- @param #string crossTextureName
-- @param #int texType
-- @return AbstractCheckButton#AbstractCheckButton self (return value: ccui.AbstractCheckButton)
        
--------------------------------
-- Query whether CheckBox is selected or not.<br>
-- return true means "selected", false otherwise.
-- @function [parent=#AbstractCheckButton] isSelected 
-- @param self
-- @return bool#bool ret (return value: bool)
        
--------------------------------
-- 
-- @function [parent=#AbstractCheckButton] init 
-- @param self
-- @param #string backGround
-- @param #string backGroundSelected
-- @param #string cross
-- @param #string backGroundDisabled
-- @param #string frontCrossDisabled
-- @param #int texType
-- @return bool#bool ret (return value: bool)
        
--------------------------------
-- 
-- @function [parent=#AbstractCheckButton] getCrossDisabledFile 
-- @param self
-- @return ResourceData#ResourceData ret (return value: cc.ResourceData)
        
--------------------------------
-- Change the content of button's title.<br>
-- param text The title in std::string.
-- @function [parent=#AbstractCheckButton] setTitleText 
-- @param self
-- @param #string text
-- @return AbstractCheckButton#AbstractCheckButton self (return value: ccui.AbstractCheckButton)
        
--------------------------------
-- Load background texture for check button.<br>
-- param backGround   The background image name.<br>
-- param type    @see `Widget::TextureResType`
-- @function [parent=#AbstractCheckButton] loadTextureBackGround 
-- @param self
-- @param #string backGround
-- @param #int type
-- @return AbstractCheckButton#AbstractCheckButton self (return value: ccui.AbstractCheckButton)
        
--------------------------------
-- 
-- @function [parent=#AbstractCheckButton] setSelectedHideBackGround 
-- @param self
-- @param #bool isSelectedHideBackGround
-- @return AbstractCheckButton#AbstractCheckButton self (return value: ccui.AbstractCheckButton)
        
--------------------------------
-- brief Return the sprite instance of background<br>
-- return the sprite instance of background.
-- @function [parent=#AbstractCheckButton] getRendererBackground 
-- @param self
-- @return Sprite#Sprite ret (return value: cc.Sprite)
        
--------------------------------
-- Change the font name of button's title<br>
-- param fontName a font name string.
-- @function [parent=#AbstractCheckButton] setTitleFontName 
-- @param self
-- @param #string fontName
-- @return AbstractCheckButton#AbstractCheckButton self (return value: ccui.AbstractCheckButton)
        
--------------------------------
-- 
-- @function [parent=#AbstractCheckButton] getCrossNormalFile 
-- @param self
-- @return ResourceData#ResourceData ret (return value: cc.ResourceData)
        
--------------------------------
-- Load all textures for initializing a check button.<br>
-- param background    The background image name.<br>
-- param backgroundSelected    The background selected image name.<br>
-- param cross    The cross image name.<br>
-- param backgroundDisabled    The background disabled state texture.<br>
-- param frontCrossDisabled    The front cross disabled state image name.<br>
-- param texType    @see `Widget::TextureResType`
-- @function [parent=#AbstractCheckButton] loadTextures 
-- @param self
-- @param #string background
-- @param #string backgroundSelected
-- @param #string cross
-- @param #string backgroundDisabled
-- @param #string frontCrossDisabled
-- @param #int texType
-- @return AbstractCheckButton#AbstractCheckButton self (return value: ccui.AbstractCheckButton)
        
--------------------------------
-- Load frontcross disabled texture for checkbox.<br>
-- param frontCrossDisabled    The front cross disabled state texture name.<br>
-- param texType    @see `Widget::TextureResType`
-- @function [parent=#AbstractCheckButton] loadTextureFrontCrossDisabled 
-- @param self
-- @param #string frontCrossDisabled
-- @param #int texType
-- @return AbstractCheckButton#AbstractCheckButton self (return value: ccui.AbstractCheckButton)
        
--------------------------------
-- Load background selected state texture for check button.<br>
-- param backGroundSelected    The background selected state image name.<br>
-- param texType    @see `Widget::TextureResType`
-- @function [parent=#AbstractCheckButton] loadTextureBackGroundSelected 
-- @param self
-- @param #string backGroundSelected
-- @param #int texType
-- @return AbstractCheckButton#AbstractCheckButton self (return value: ccui.AbstractCheckButton)
        
--------------------------------
-- 
-- @function [parent=#AbstractCheckButton] getBackPressedFile 
-- @param self
-- @return ResourceData#ResourceData ret (return value: cc.ResourceData)
        
--------------------------------
-- brief Return the sprite instance of front cross when disabled<br>
-- return the sprite instance of front cross when disabled
-- @function [parent=#AbstractCheckButton] getRendererFrontCrossDisabled 
-- @param self
-- @return Sprite#Sprite ret (return value: cc.Sprite)
        
--------------------------------
--  returns the current Label being used 
-- @function [parent=#AbstractCheckButton] getTitleLabel 
-- @param self
-- @return Label#Label ret (return value: cc.Label)
        
--------------------------------
-- Query the font size of button title<br>
-- return font size in float.
-- @function [parent=#AbstractCheckButton] getTitleFontSize 
-- @param self
-- @return float#float ret (return value: float)
        
--------------------------------
-- brief Return the sprite instance of background when disabled<br>
-- return the sprite instance of background when disabled
-- @function [parent=#AbstractCheckButton] getRendererBackgroundDisabled 
-- @param self
-- @return Sprite#Sprite ret (return value: cc.Sprite)
        
--------------------------------
-- Query the button title color.<br>
-- return Color3B of button title.
-- @function [parent=#AbstractCheckButton] getTitleColor 
-- @param self
-- @return color3b_table#color3b_table ret (return value: color3b_table)
        
--------------------------------
-- 
-- @function [parent=#AbstractCheckButton] getBackNormalFile 
-- @param self
-- @return ResourceData#ResourceData ret (return value: cc.ResourceData)
        
--------------------------------
-- @overload self, int, int         
-- @overload self, int         
-- @function [parent=#AbstractCheckButton] setTitleAlignment
-- @param self
-- @param #int hAlignment
-- @param #int vAlignment
-- @return AbstractCheckButton#AbstractCheckButton self (return value: ccui.AbstractCheckButton)

--------------------------------
--  When user pressed the CheckBox, the button will zoom to a scale.<br>
-- The final scale of the CheckBox  equals (CheckBox original scale + _zoomScale)<br>
-- since v3.3
-- @function [parent=#AbstractCheckButton] setZoomScale 
-- @param self
-- @param #float scale
-- @return AbstractCheckButton#AbstractCheckButton self (return value: ccui.AbstractCheckButton)
        
--------------------------------
-- 
-- @function [parent=#AbstractCheckButton] getVirtualRenderer 
-- @param self
-- @return Node#Node ret (return value: cc.Node)
        
--------------------------------
-- 
-- @function [parent=#AbstractCheckButton] init 
-- @param self
-- @return bool#bool ret (return value: bool)
        
--------------------------------
-- 
-- @function [parent=#AbstractCheckButton] getVirtualRendererSize 
-- @param self
-- @return size_table#size_table ret (return value: size_table)
        
return nil
