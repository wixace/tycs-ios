package org.cocos2dx.utils;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

import org.cocos2dx.lib.Cocos2dxHelper;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.BatteryManager;
import android.os.Vibrator;
import android.provider.MediaStore;
import android.provider.Settings;
import android.util.Log;
import android.view.Window;
import android.view.WindowManager;
import android.preference.PreferenceManager.OnActivityResultListener;

public class SystemTool {
	
	private static int LOCAl_PHOTO = 3080;
	private static OnActivityResultListener _local_photo;
	private static Vibrator _vibrator = (Vibrator)Cocos2dxHelper.getActivity().getSystemService(Context.VIBRATOR_SERVICE);  
	private static Intent _batteryIntent = Cocos2dxHelper.getActivity().registerReceiver(null,    
	        new IntentFilter(Intent.ACTION_BATTERY_CHANGED));
	
	private static native void nativeSavePhotoImage(final String pApkPath);
	private static native void nativeLocationInfo(final String pApkPath);
	
    public static void startVibrate() {
    	
		_vibrator = (Vibrator)Cocos2dxHelper.getActivity().getSystemService(Context.VIBRATOR_SERVICE);  
        long [] pattern = {100,400,100,400};   // 停止 开启 停止 开启   
        _vibrator.vibrate(pattern,2);  	
	}
    
    public static void stopVibrate() {
    	_vibrator.cancel();
	}
	
    // 获取电量
    public static float getBatteryQuantity(){
    
		int currLevel = _batteryIntent.getIntExtra(BatteryManager.EXTRA_LEVEL, 0);    
		int total = _batteryIntent.getIntExtra(BatteryManager.EXTRA_SCALE, 1);    
		float percent = currLevel / total;    
		return percent;
    }
    // 获取电池状态
    public static int getBatteryStauts(){
    	return _batteryIntent.getIntExtra("status", BatteryManager.BATTERY_STATUS_UNKNOWN);
    }
    // 获取屏幕亮度
    public static float getScreenBrightness(){
        float systemBrightness = 0;
        try {
        systemBrightness = Settings.System.getInt(Cocos2dxHelper.getActivity().getContentResolver(), Settings.System.SCREEN_BRIGHTNESS);
        } catch (Settings.SettingNotFoundException e) {
         e.printStackTrace();
        }
        return systemBrightness / 255;
    }
    
    // 设置屏幕亮度(0.0-1.0)
    public static void setScreenBrightness(float brightness){

        Window window = Cocos2dxHelper.getActivity().getWindow();
        WindowManager.LayoutParams lp = window.getAttributes();
         if (brightness == -1) {
             lp.screenBrightness = WindowManager.LayoutParams.BRIGHTNESS_OVERRIDE_NONE;
         } else {
             lp.screenBrightness = (brightness <= 0 ? 1 : brightness) / 255f;
         }
         window.setAttributes(lp);
    }
    
    public static void localPhoto(int callback){
    	
        Intent intent = new Intent(Intent.ACTION_PICK,
                android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        Cocos2dxHelper.getActivity().startActivityForResult(intent, LOCAl_PHOTO);
        
        if(_local_photo == null){
        	

        	_local_photo = new OnActivityResultListener(){

    			@Override
    			public boolean onActivityResult(int requestCode, int resultCode,
    					Intent data) {
    				
    				 if (requestCode == LOCAl_PHOTO && resultCode == Activity.RESULT_OK) {
    				      Uri uri = data.getData();
    				      Cursor cursor = Cocos2dxHelper.getActivity().getContentResolver().query(uri, null, null, null,null);
    				      if (cursor != null && cursor.moveToFirst()) {
    				          String path = cursor.getString(cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA));
    				          Bitmap bitmap = BitmapFactory.decodeFile(path);
    				          
    				          File destDir = new File(Cocos2dxHelper.getCocos2dxWritablePath() + "/tmpimage/");
    				          if (!destDir.exists()) {
    				        	  destDir.mkdirs();
    				          }
    				          
    				          File file = new File(Cocos2dxHelper.getCocos2dxWritablePath() + "/tmpimage/tmp.png");  
    				          FileOutputStream out;
							try {
								out = new FileOutputStream(file);
								bitmap.compress(Bitmap.CompressFormat.PNG, 100, out);  
								out.flush();  
								out.close();
								
							} catch (FileNotFoundException e) {
								// TODO Auto-generated catch block
								e.printStackTrace();
							} catch (IOException e) {
								// TODO Auto-generated catch block
								e.printStackTrace();
							}  

    				      };
    				    }
    				
    				return true;
    			}
        	};
        	
        	Cocos2dxHelper.addOnActivityResultListener(_local_photo);
        }
        
    }
    public static void takePhoto(int callback){
    	
    	
    	
    }
    
    public static void startLocation(int callback){
    	
    }
    
    public static void saveImageToPhotoWithPath(String path, int callback){
    	
    }

    @SuppressLint("NewApi") 
    public static void copyToClipboard(final String text){
        try
        {
        	
        	Cocos2dxHelper.getActivity().runOnUiThread(new Runnable(){

				@Override
				public void run() {

					ClipboardManager clipboard = (ClipboardManager) Cocos2dxHelper.getActivity().getSystemService(Activity.CLIPBOARD_SERVICE);
                    ClipData clip = ClipData.newPlainText("text", text);
                    clipboard.setPrimaryClip(clip);					
				}
        		
        	});

 
        }catch(Exception e){
           // Log.d("cocos2dx","copyToClipboard error");
            e.printStackTrace();
        }
    }
    
}
