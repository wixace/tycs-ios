package org.cocos2dx.utils;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

import org.cocos2dx.lib.Cocos2dxHelper;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.BatteryManager;
import android.os.Environment;
import android.os.Vibrator;
import android.provider.MediaStore;
import android.provider.Settings;
import android.util.Log;
import android.view.Window;
import android.view.WindowManager;
import android.preference.PreferenceManager.OnActivityResultListener;

public class SystemUtils {
	
	private static int LOCAl_PHOTO = 3080;
	private static OnActivityResultListener _local_photo;
	private static Vibrator _vibrator = (Vibrator)Cocos2dxHelper.getActivity().getSystemService(Context.VIBRATOR_SERVICE);  
	
	private static native void nativeSavePhotoImage(final String pApkPath);
	private static native void nativeLocationInfo(final String pApkPath);

    private static final String EXP_PATH = "/Android/obb/";

    public static void startVibrate() {
    	
		_vibrator = (Vibrator)Cocos2dxHelper.getActivity().getSystemService(Context.VIBRATOR_SERVICE);  
        long [] pattern = {100,400,100,400};  
        _vibrator.vibrate(pattern,2);  	
	}
    
    public static void stopVibrate() {
    	_vibrator.cancel();
	}
	
    // 鑾峰彇鐢甸噺
    public static float getBatteryQuantity(){
    
		Intent batteryIntent = Cocos2dxHelper.getActivity().registerReceiver(null,    
		        new IntentFilter(Intent.ACTION_BATTERY_CHANGED));  
		
		int currLevel = batteryIntent.getIntExtra(BatteryManager.EXTRA_LEVEL, 0);    
		int total = batteryIntent.getIntExtra(BatteryManager.EXTRA_SCALE, 1);    
		Log.d("getBatteryQuantity", "currLevel:"+currLevel+" total"+total);
		float percent = (float)currLevel / (float)total;    
		return percent;
    }


    public static synchronized int getVersionCode(Context context) {
        try {
            PackageManager packageManager = context.getPackageManager();
            PackageInfo packageInfo = packageManager.getPackageInfo(
                    context.getPackageName(), 0);
            return packageInfo.versionCode;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return 0;
    }


    // 获取obb文件位置
    public static String getAPKExpansionZipFilePath(){
        Log.d("getAPKExpansionZipFilePath", "aaaaa1"); 
        String strMainPath = new String("");
        String packageName = Cocos2dxHelper.getActivity().getPackageName();
        if (Environment.getExternalStorageState().equals(
                Environment.MEDIA_MOUNTED)) {
            // Build the full path to the app's expansion files
            File root = Environment.getExternalStorageDirectory();
            File expPath = new File(root.toString() + EXP_PATH + packageName);
            // Check that expansion file path exists
            int mainVersion = getVersionCode(Cocos2dxHelper.getActivity());
            Log.d("getAPKExpansionZipFilePath", "aaaaa4" + root.toString() + EXP_PATH + packageName); 
            if (expPath.exists()) {
                if ( mainVersion > 0 ) {
                    strMainPath = expPath + File.separator + "main." + mainVersion + "." + packageName + ".obb";
                }
            }
        }
        Log.i("getAPKExpansionZipFilePath", strMainPath); 
        return strMainPath;
    }

    // 鑾峰彇鐢垫睜鐘舵��
    public static int getBatteryStauts(){
    	
		Intent batteryIntent = Cocos2dxHelper.getActivity().registerReceiver(null,    
		        new IntentFilter(Intent.ACTION_BATTERY_CHANGED));    
    	
    	return batteryIntent.getIntExtra("status", BatteryManager.BATTERY_STATUS_UNKNOWN);
    }
    // 鑾峰彇灞忓箷浜害
    public static float getScreenBrightness(){
        float systemBrightness = 0;
        Window window = Cocos2dxHelper.getActivity().getWindow();
        WindowManager.LayoutParams lp = window.getAttributes();
        systemBrightness = lp.screenBrightness;
        return systemBrightness;
    }
    
    // 璁剧疆灞忓箷浜害(0.0-1.0)
    public static void setScreenBrightness(final float brightness){

        Cocos2dxHelper.getActivity().runOnUiThread(new Runnable(){

            @Override
            public void run() {

                Window window = Cocos2dxHelper.getActivity().getWindow();
                WindowManager.LayoutParams lp = window.getAttributes();
                 if (brightness == -1) {
                     lp.screenBrightness = WindowManager.LayoutParams.BRIGHTNESS_OVERRIDE_NONE;
                 } else {
                     lp.screenBrightness = ((brightness < 0 || brightness > 1)? 1 : brightness);
                 }
                 window.setAttributes(lp);          
            }
            
        });
        
    }
    
    public static void localPhoto(int callback){
    	
        Intent intent = new Intent(Intent.ACTION_PICK,
                android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        Cocos2dxHelper.getActivity().startActivityForResult(intent, LOCAl_PHOTO);
        
        if(_local_photo == null){
        	

        	_local_photo = new OnActivityResultListener(){

    			@Override
    			public boolean onActivityResult(int requestCode, int resultCode,
    					Intent data) {
    				
    				 if (requestCode == LOCAl_PHOTO && resultCode == Activity.RESULT_OK) {
    				     
    				      Cursor cursor = Cocos2dxHelper.getActivity().getContentResolver().query(data.getData(), null, null, null,null);
    				      if (cursor != null && cursor.moveToFirst()) {
    				          String path = cursor.getString(cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA));
    				          Bitmap bitmap = BitmapFactory.decodeFile(path);
    				          
    				          File destDir = new File(Cocos2dxHelper.getCocos2dxWritablePath() + "/tmpimage/");
    				          if (!destDir.exists()) {
    				        	  destDir.mkdirs();
    				          }
    				          
    				          File file = new File(Cocos2dxHelper.getCocos2dxWritablePath() + "/tmpimage/tmp.png");  
    				          FileOutputStream out;
							try {
								out = new FileOutputStream(file);
								bitmap.compress(Bitmap.CompressFormat.PNG, 100, out);  
								out.flush();  
								out.close();
								
							} catch (FileNotFoundException e) {
								// TODO Auto-generated catch block
								e.printStackTrace();
							} catch (IOException e) {
								// TODO Auto-generated catch block
								e.printStackTrace();
							}  

    				      };
    				    }
    				
    				return true;
    			}
        	};
        	
        	Cocos2dxHelper.addOnActivityResultListener(_local_photo);
        }
        
    }
    public static void takePhoto(int callback){
    	
    	
    	
    }
    
    public static void startLocation(int callback){
    	
    }
    
    public static void saveImageToPhotoWithPath(String path, int callback){
    	
    }

    @SuppressLint("NewApi") 
    public static void copyToClipboard(final String text){
        try
        {
        	
        	Cocos2dxHelper.getActivity().runOnUiThread(new Runnable(){

				@Override
				public void run() {

					ClipboardManager clipboard = (ClipboardManager) Cocos2dxHelper.getActivity().getSystemService(Activity.CLIPBOARD_SERVICE);
                    ClipData clip = ClipData.newPlainText("text", text);
                    clipboard.setPrimaryClip(clip);					
				}
        		
        	});

 
        }catch(Exception e){
           // Log.d("cocos2dx","copyToClipboard error");
            e.printStackTrace();
        }
    }
    
}
