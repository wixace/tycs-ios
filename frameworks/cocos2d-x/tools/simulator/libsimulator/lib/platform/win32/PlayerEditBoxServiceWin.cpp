
#include <string>

#include "stdafx.h"
#include "cocos2d.h"
#include "PlayerEditBoxServiceWin.h"
#include "scripting/lua-bindings/manual/CCLuaEngine.h"
PLAYER_NS_BEGIN

PlayerEditBoxServiceWin::PlayerEditBoxServiceWin(HWND hwnd)
: _hfont(NULL),
_logIndex(0)
{
    _hwnd = hwnd;
    HINSTANCE instance = (HINSTANCE)GetWindowLong(_hwnd, GWL_HINSTANCE);
    DWORD style = WS_CHILD | ES_LEFT | ES_AUTOHSCROLL;
    _hwndSingle = CreateWindowEx(WS_EX_CLIENTEDGE, L"Edit", L"", style, 0, 0, 0, 0, _hwnd, NULL, instance, NULL);
    style = WS_CHILD | ES_MULTILINE | ES_LEFT | ES_AUTOVSCROLL | ES_WANTRETURN | WS_VSCROLL;
    _hwndMulti = CreateWindowEx(WS_EX_CLIENTEDGE, L"Edit", L"", style, 0, 0, 0, 0, _hwnd, NULL, instance, NULL);


	SetWindowLongPtr(_hwndSingle, GWL_USERDATA, (LONG_PTR)this);
	_prevWndProc = (WNDPROC)SetWindowLongPtr(_hwndSingle, GWL_WNDPROC, (LONG_PTR)WindowProc);

}

PlayerEditBoxServiceWin::~PlayerEditBoxServiceWin()
{
    removeFont();
    DestroyWindow(_hwndSingle);
    DestroyWindow(_hwndMulti);
}

void PlayerEditBoxServiceWin::showSingleLineEditBox(const cocos2d::Rect &rect)
{
    MoveWindow(_hwndSingle, rect.origin.x, rect.origin.y, rect.size.width, rect.size.height, TRUE);
    ShowWindow(_hwndSingle, SW_SHOW);
    SetFocus(_hwndSingle);
}

void PlayerEditBoxServiceWin::showMultiLineEditBox(const cocos2d::Rect &rect)
{
    MoveWindow(_hwndMulti, rect.origin.x, rect.origin.y, rect.size.width, rect.size.height, TRUE);
    ShowWindow(_hwndMulti, SW_SHOW);
    SetFocus(_hwndMulti);
}

void PlayerEditBoxServiceWin::hide()
{
	ShowWindow(_hwndSingle, SW_HIDE);
	ShowWindow(_hwndMulti, SW_HIDE);
}

void PlayerEditBoxServiceWin::setText(const std::string &text)
{
	std::u16string utf16Result;
	cocos2d::StringUtils::UTF8ToUTF16(text, utf16Result);
	::SetWindowTextW(_hwndSingle, (LPCWSTR)utf16Result.c_str());
	int textLen = text.size();
	SendMessage(_hwndSingle, EM_SETSEL, textLen, textLen);
	SendMessage(_hwndSingle, EM_SCROLLCARET, 0, 0);
}

void PlayerEditBoxServiceWin::setFont(const std::string &name, int size)
{
    removeFont();

    std::u16string u16name;
    cocos2d::StringUtils::UTF8ToUTF16(name, u16name);

    HDC hdc = GetDC(_hwnd);
    size = -MulDiv(size, GetDeviceCaps(hdc, LOGPIXELSY), 72);
    ReleaseDC(_hwnd, hdc);

    _hfont = CreateFont(size, 0, 0, 0, FW_NORMAL, FALSE, FALSE, FALSE,
                        0, OUT_DEFAULT_PRECIS, FW_NORMAL, DEFAULT_QUALITY, DEFAULT_PITCH,
                        (LPCTSTR)u16name.c_str());
    if (!_hfont)
    {
        DWORD err = GetLastError();
        CCLOG("PlayerEditBoxServiceWin::setFont() - create HFONT for font \"%s\" failed, error code = 0x%08x",
              name.c_str(), err);
    }
    else
    {
        SendMessage(_hwndSingle, WM_SETFONT, (WPARAM)_hfont, NULL);
        SendMessage(_hwndMulti, WM_SETFONT, (WPARAM)_hfont, NULL);
    }
}

void PlayerEditBoxServiceWin::setFontColor(const cocos2d::Color3B &color)
{

}

void PlayerEditBoxServiceWin::removeFont()
{
    if (_hfont)
    {
        SendMessage(_hwndSingle, WM_SETFONT, NULL, NULL);
        SendMessage(_hwndMulti, WM_SETFONT, NULL, NULL);
        DeleteObject(_hfont);
    }
    _hfont = NULL;
}

void PlayerEditBoxServiceWin::setFormator(int /*formator*/ )
{

}


void PlayerEditBoxServiceWin::_WindowProc(HWND hwnd, UINT uMsg, WPARAM wParam, LPARAM lParam)
{
	switch (uMsg)
	{
		case WM_CHAR:
		{
			switch (wParam)
			{
				case VK_RETURN:
				{
					std::u16string wstrResult;
					std::string utf8Result;

					int inputLength = ::GetWindowTextLengthW(_hwndSingle);
					wstrResult.resize(inputLength);

					::GetWindowTextW(_hwndSingle, (LPWSTR) const_cast<char16_t*>(wstrResult.c_str()), inputLength + 1);
					bool conversionResult = cocos2d::StringUtils::UTF16ToUTF8(wstrResult, utf8Result);
					if (!conversionResult)
					{
						CCLOG("warning, editbox input text convertion error.");
					}
					std::string execute = std::move(utf8Result).c_str();

					cocos2d::Scheduler *sched = cocos2d::Director::getInstance()->getScheduler();
					sched->performFunctionInCocosThread([=]() {

						auto engine = cocos2d::LuaEngine::getInstance();
						cocos2d::LuaStack* stack = engine->getLuaStack();
						stack->executeString(execute.c_str());

						setText(std::string(""));

					});
					if (execute != "") {
						_logIndex = _log.size() + 1;
						_log.push_back(execute);
						if (_log.size() > 30) {
							_log.erase(_log.begin());
							_logIndex -= 1;
						}
					}
					hide();
					break;
				}
			}
			break;
		}
		case WM_KEYDOWN:
		{
			switch (wParam)
			{
				case VK_UP:
				{
					_logIndex -= 1;
					if (_logIndex <= 0)
					{
						_logIndex = 0;
					}

					std::u16string utf16Result;
					if (_log.size() > 0)
					{
						std::string text = _log.at(_logIndex);
						setText(text);
					}
					break;
				}
				case VK_DOWN:
				{
					_logIndex += 1;
					if (_logIndex >= _log.size())
					{
						_logIndex = _log.size();
					}
					std::u16string utf16Result;
					std::string text;
					if (_logIndex < _log.size())
					{
						text = _log.at(_logIndex);
					}
					else
					{
						text = std::string("");
					}
					setText(text);
					break;
				}
			}
		}
	}
}


LRESULT PlayerEditBoxServiceWin::WindowProc(HWND hwnd, UINT uMsg, WPARAM wParam, LPARAM lParam)
{
	PlayerEditBoxServiceWin* pThis = (PlayerEditBoxServiceWin*)GetWindowLongPtr(hwnd, GWLP_USERDATA);
	if (pThis)
	{
		pThis->_WindowProc(hwnd, uMsg, wParam, lParam);
	}

	return CallWindowProc(pThis->_prevWndProc, hwnd, uMsg, wParam, lParam);

}


PLAYER_NS_END
