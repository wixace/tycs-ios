#include "hwCommMapData.h"

using namespace std;

//构造函数
ChwCommMapData::ChwCommMapData():
	m_mapData(nullptr),
	m_mapBound(0)
{
 
}

// 析构函数
ChwCommMapData::~ChwCommMapData()
{
	if (m_mapData!=nullptr)
	{
		delete[] m_mapData;
	}
	m_mapData = nullptr;
	if (m_mapID!=nullptr)
	{
		delete[] m_mapID;
	}
	m_mapID = nullptr;
}

void ChwCommMapData::createMap(unsigned int bound)
{
	m_mapBound = bound;
	m_realBound = 2*bound+1;
	m_dataSize = m_realBound*m_realBound;
	m_mapData = new unsigned char[m_dataSize];
	m_mapID   = new unsigned char[m_dataSize];
}


// 初始化地图边界
void ChwCommMapData::initMap(unsigned int bound)
{
	createMap(bound);
	memset(m_mapData, 0, m_dataSize);
}
//设置点的类型,点的类型应该扩展为按位来做，比如第一位确定是否障碍，第二位确定是否功能
void ChwCommMapData::setElementType(int x, int y, int elementType)
{
	x = x + m_mapBound;
	y = y + m_mapBound;

	if(y<0){
		y = 0;
	}
	if(x<0){
		x = 0;
	}

	unsigned int pos = x * m_realBound + y;
	if(pos > m_dataSize)
	{
		pos = m_dataSize;
	}
	m_mapData[pos] = elementType;
}

	// 获取地图元素类型
short ChwCommMapData::getElementType(int x, int y)
{
	x = x + m_mapBound;
	y = y + m_mapBound;

	if(y<0){
		y = 0;
	}
	if(x<0){
		x = 0;
	}

	unsigned int pos = x * m_realBound + y;
	if(pos > m_dataSize)
	{
		return 0;
	}
	return m_mapData[pos];
}

//设置点的该类型中的id
void ChwCommMapData::setElementID(int x, int y, int elementID)
{
	x = x + m_mapBound;
	y = y + m_mapBound;

	if(y<0){
		y = 0;
	}
	if(x<0){
		x = 0;
	}

	unsigned int pos = x * m_realBound + y;
	if(pos > m_dataSize)
	{
		pos = m_dataSize;
	}
	m_mapID[pos] = elementID;
}

// 获取地图元素在类型中的id
short ChwCommMapData::getElementID(int x, int y)
{
	x = x + m_mapBound;
	y = y + m_mapBound;

	if(y<0){
		y = 0;
	}
	if(x<0){
		x = 0;
	}

	unsigned int pos = x * m_realBound + y;
	if(pos > m_dataSize)
	{
		return 0;
	}
	return m_mapID[pos];
}


// 读取地图信息
bool ChwCommMapData::loadMapDataWithFile(const std::string& filename)
{
	if(FileUtils::getInstance()->isFileExist(filename.c_str()) == true)
	{
		auto Data = FileUtils::getInstance()->getDataFromFile(filename.c_str());
		unsigned char* buffer = Data.getBytes();
		unsigned char bound = *buffer;

		createMap(bound);

		memcpy(m_mapData, buffer+1, m_dataSize);
		if (Data.getSize() == (m_dataSize * 2 + 1))
			memcpy(m_mapID, buffer + 1 + m_dataSize, m_dataSize);
		else
			memset(m_mapID, 1, m_dataSize);
		return true;
	}
	else
	{
		return false;
	}
}

unsigned int ChwCommMapData::getMapBound()
{
	return m_mapBound;
}

// 读取地图信息
bool ChwCommMapData::saveMapDataToFile(const std::string& filename)
{

	std::string fullpath = FileUtils::getInstance()->fullPathForFilename(filename);
	FILE *out = fopen(fullpath.c_str(), "wb");
	if (!out)
	{
		//CCLOG("can not open destination file %s", outfilepath);
		return false;
	}
	fwrite(&m_mapBound, sizeof(unsigned char), 1, out);
	fwrite(m_mapData, m_dataSize, 1, out);
	fwrite(m_mapID, m_dataSize, 1, out);
	fclose(out);

	return true;
}
