#pragma once

#include <iostream>
#include "cocos2d.h"

USING_NS_CC;

class ChwCommByteBuff: public Ref
{

public:

	unsigned int        m_dwSize;
	unsigned int		m_dwCapacity;
	unsigned int		m_dwReadIndex;
	unsigned int		m_dwWriteIndex;
	char*               m_pBuffer;

public:

	ChwCommByteBuff(const ChwCommByteBuff& buf);
	ChwCommByteBuff(unsigned int size);
	ChwCommByteBuff(const char* pBuffer, unsigned int dwLength);
	ChwCommByteBuff();
	~ChwCommByteBuff();

public:

	void			ReadIndex(unsigned int index);
	void			WriteIndex(unsigned int index);
	unsigned int 	GetSize();
	void			ReSize(unsigned int size);

public:
	unsigned int			Read(void* pBuf, unsigned int dwLength);
	void					ReadBuf(ChwCommByteBuff* pBuf);

	unsigned char	ReadBYTE(void);
	char            ReadChar(void);
	unsigned short	ReadWORD(void);
	unsigned int	ReadDWORD(void);
	unsigned long	ReadLong(void);
	int				ReadInt(void);
	float           ReadFloat(void);
	double			ReadDouble(void);
	std::string     ReadString(void);
	std::string     ReadText(void);

public:

	void Write(const char* pBuf, unsigned int dwLength);
	void WriteBuf(ChwCommByteBuff* pBuf);

	void WriteBYTE(unsigned char byValue);
	void WriteChar(char cValue);
	void WriteWORD(unsigned short wValue);
	void WriteDWORD(unsigned int dwValue);
	void WriteLong(unsigned long);
	void WriteInt(int nValue);
	void WriteFloat(float fValue);
	void WriteDouble(double dValue);
	void WriteString(const char* pString);
	void WriteText(const char* pString);
};




