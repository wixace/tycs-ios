//
//  CommonTools.m
//  SwiftSDKOCDemo
//
//  Created by Toby on 2019/11/19.
//  Copyright © 2019 Muzi. All rights reserved.
//
#import "ResearcherSurveyUtils.h"



@implementation ResearcherSurveyUtils


//根据日期算出周几

+ (NSString*)weekdayStringFromDate:(NSDate*)inputDate

{
    NSArray *weekdays = [NSArray arrayWithObjects: [NSNull null], @"周日", @"周一", @"周二", @"周三", @"周四", @"周五", @"周六", nil];
    
    NSCalendar *calendar = [[NSCalendar alloc] initWithCalendarIdentifier:NSCalendarIdentifierGregorian];
    
    NSTimeZone *timeZone = [[NSTimeZone alloc] initWithName:@"Asia/Shanghai"];
    
    [calendar setTimeZone: timeZone];
    
    NSCalendarUnit calendarUnit = NSCalendarUnitWeekday;
    
    NSDateComponents *theComponents = [calendar components:calendarUnit fromDate:inputDate];
        
    return [weekdays objectAtIndex:theComponents.weekday];
}



//转化为时间字符串

+ (NSString *)dateStringFromNumberTimer:(NSString *)timerStr {
    
    //转化为Double
    
    double t = [timerStr doubleValue];
    
    //计算出距离1970的NSDate
    
    NSDate *date = [NSDate dateWithTimeIntervalSince1970:t];
    
    //转化为时间格式化字符串
    
    //NSDateFormatter *df = [[[NSDateFormatter alloc] init] autorelease];
    
    
    
    NSDateFormatter *df = [[NSDateFormatter alloc] init];
    
    df.dateFormat = @"yyyy-MM-dd HH:mm:ss";
    
    //转化为时间字符串
    
    return [df stringFromDate:date];
    
}

//动态计算行高

//根据字符串的实际内容的多少在固定的宽度和字体的大小，动态的计算出实际的高度

+ (CGFloat)textHeightFromTextString:(NSString *)text width:(CGFloat)textWidth fontSize:(CGFloat)size{
    
    if ([[[UIDevice currentDevice] systemVersion] doubleValue] >= 7.0) {
        
        //iOS7之后
        
        /*
         
         第一个参数: 预设空间宽度固定  高度预设一个最大值
         
         第二个参数: 行间距如果超出范围是否截断
         
         第三个参数: 属性字典可以设置字体大小
         
         */
        
        NSDictionary *dict = @{NSFontAttributeName:[UIFont systemFontOfSize:size]};
        
        CGRect rect = [text boundingRectWithSize:CGSizeMake(textWidth, MAXFLOAT) options:NSStringDrawingTruncatesLastVisibleLine|NSStringDrawingUsesFontLeading|NSStringDrawingUsesLineFragmentOrigin attributes:dict context:nil];
        
        //返回计算出的行高
        
        return rect.size.height;
        
        
        
    }else {
        
        //iOS7之前
        
        /*
         
         1.第一个参数 设置的字体固定大小
         
         2.预设宽度和高度 宽度是固定的高度一般写成最大值
         
         3.换行模式字符换行
         
         */
        
        CGSize textSize = [text sizeWithFont:[UIFont systemFontOfSize:size] constrainedToSize:CGSizeMake(textWidth, MAXFLOAT) lineBreakMode:NSLineBreakByCharWrapping];
        
        return textSize.height;//返回计算出得行高
        
    }
    
}

//获取iOS版本号

+ (double)getCurrentIOS {
    
    return [[[UIDevice currentDevice] systemVersion] doubleValue];
    
}

+ (CGSize)getScreenSize {
    
    return [[UIScreen mainScreen] bounds].size;
    
}

//获得到指定时间的时间差字符串,格式在此方法内返回前自己根据需要格式化

+(NSString*)stringNowToDate:(NSDate*)toDate

{
    
    //创建日期 NSCalendar对象
    
    NSCalendar *cal = [NSCalendar currentCalendar];
    
    //得到当前时间
    
    NSDate *today = [NSDate date];
    
    
    
    //用来得到具体的时差,位运算
    
    unsigned int unitFlags = NSCalendarUnitYear | NSCalendarUnitMonth | NSCalendarUnitDay | NSCalendarUnitHour | NSCalendarUnitMinute | NSCalendarUnitSecond ;
    
    
    
    if (toDate && today) {//不为nil进行转化
        
        NSDateComponents *d = [cal components:unitFlags fromDate:today toDate:toDate options:0 ];
        
        
        
        //NSString *dateStr=[NSString stringWithFormat:@"%d年%d月%d日%d时%d分%d秒",[d year],[d month], [d day], [d hour], [d minute], [d second]];
        
        NSString *dateStr=[NSString stringWithFormat:@"%02ld:%02ld:%02ld",[d hour], [d minute], [d second]];
        
        return dateStr;
        
    }
    
    return @"";
    
}

//MD5加密字符串

//获取一个文件 在沙盒Library/Caches/ 目录下的路径

+ (NSString *)getFullPathWithFile:(NSString *)urlName {
    
    
    
    //先获取沙盒中的Library/Caches/路径
    
    NSString *docPath = [NSSearchPathForDirectoriesInDomains(NSCachesDirectory, NSUserDomainMask, YES) lastObject];
    
    NSString *myCacheDirectory = [docPath stringByAppendingPathComponent:@"MyCaches"];
    
    //检测MyCaches文件夹是否存在
    
    if (![[NSFileManager defaultManager] fileExistsAtPath:myCacheDirectory]) {
        
        //不存在那么创建
        
        [[NSFileManager defaultManager] createDirectoryAtPath:myCacheDirectory withIntermediateDirectories:YES attributes:nil error:nil];
        
    }
    
    //用md5进行加密 转化为一串十六进制数字 (md5加密可以把一个字符串转化为一串唯一的用十六进制表示的串)
    
    
    
    //拼接路径
    
    return [myCacheDirectory stringByAppendingPathComponent:urlName];
    
}

//检测缓存文件 是否超时

+ (BOOL)isTimeOutWithFile:(NSString *)filePath timeOut:(double)timeOut {
    
    //获取文件的属性
    
    NSDictionary *fileDict = [[NSFileManager defaultManager] attributesOfItemAtPath:filePath error:nil];
    
    //获取文件的上次的修改时间
    
    NSDate *lastModfyDate = fileDict.fileModificationDate;
    
    //算出时间差获取当前系统时间 和 lastModfyDate时间差
    
    NSTimeInterval sub = [[NSDate date] timeIntervalSinceDate:lastModfyDate];
    
    if (sub < 0) {
        
        sub = -sub;
        
    }
    
    //比较是否超时
    
    if (sub > timeOut) {
        
        //如果时间差大于 设置的超时时间那么就表示超时
        
        return YES;
        
    }
    
    return NO;
    
}



//缓存目录

+ (NSString*) cacheDirectory {
    
    NSArray* paths = NSSearchPathForDirectoriesInDomains(NSCachesDirectory, NSUserDomainMask, YES);
    
    NSString *cacheDirectory = [paths objectAtIndex:0];
    
    cacheDirectory = [cacheDirectory stringByAppendingPathComponent:@"MZLCaches"];
    
    return cacheDirectory;
    
}

//删除指定的缓存

+ (NSData*) cacObjectForKey:(NSString*)key {
    
    NSArray* paths = NSSearchPathForDirectoriesInDomains(NSCachesDirectory, NSUserDomainMask, YES);
    
    NSString *cacheDirectory = [paths objectAtIndex:0];
    
    cacheDirectory = [cacheDirectory stringByAppendingPathComponent:@"MZLCaches"];
    
    NSFileManager *fileManager = [NSFileManager defaultManager];
    
    NSString *filename = [cacheDirectory stringByAppendingPathComponent:key];
    
    
    
    if ([fileManager fileExistsAtPath:filename])
        
    {
        
        NSDate *modificationDate = [[fileManager attributesOfItemAtPath:filename error:nil] objectForKey:NSFileModificationDate];
        
        if ([modificationDate timeIntervalSinceNow] > 30) {
            
            [fileManager removeItemAtPath:filename error:nil];
            
        } else {
            
            NSData *data = [NSData dataWithContentsOfFile:filename];
            
            return data;
            
        }
        
    }
    
    return nil;
    
}

//创建指定缓存

+ (void) setCaceObject:(NSData*)data forKey:(NSString*)key {
    
    NSArray* paths = NSSearchPathForDirectoriesInDomains(NSCachesDirectory, NSUserDomainMask, YES);
    
    NSString *cacheDirectory = [paths objectAtIndex:0];
    
    cacheDirectory = [cacheDirectory stringByAppendingPathComponent:@"MZLCaches"];
    NSFileManager *fileManager = [NSFileManager defaultManager];
    
    NSString *filename = [cacheDirectory stringByAppendingPathComponent:key];
    
    
    
    BOOL isDir = YES;
    
    if (![fileManager fileExistsAtPath:cacheDirectory isDirectory:&isDir]) {
        
        [fileManager createDirectoryAtPath:cacheDirectory withIntermediateDirectories:NO attributes:nil error:nil];
        
    }
    
    
    
    NSError *error;
    
    @try {
        
        [data writeToFile:filename options:NSDataWritingAtomic error:&error];
        
    }
    
    @catch (NSException * e) {
        
        //TODO: error handling maybe
        
    }
    
}

//获取某个路径下文件大小

+ (long) fileSizeAtPath:(NSString*) filePath{
    
    
    
    NSFileManager* manager = [NSFileManager defaultManager];
    
    if ([manager fileExistsAtPath:filePath]){
        
        
        
        return [[manager attributesOfItemAtPath:filePath error:nil] fileSize];
        
    }
    
    return 0;
    
    
    
}

//获取缓存大小

+ (float ) folderSizeAtPath:(NSString*) folderPath{
    
    NSFileManager* manager = [NSFileManager defaultManager];
    
    if (![manager fileExistsAtPath:folderPath])
        
        return 0;
    
    NSEnumerator *childFilesEnumerator = [[manager subpathsAtPath:folderPath] objectEnumerator];
    
    NSString* fileName;
    
    long long folderSize = 0;
    
    while ((fileName = [childFilesEnumerator nextObject]) != nil){
        
        NSString* fileAbsolutePath = [folderPath stringByAppendingPathComponent:fileName];
        
        folderSize += 2;
        
    }
    
    return folderSize/(1024.0*1024.0);
    
}

// 判断字符是否为空

+(BOOL)isNull:(NSString*)str

{
    
    if(str == nil)
        
    {
        
        return YES;
        
    }
    
    
    
    if([str isEqualToString:@""])
        
    {
        
        return YES;
        
    }
    
    
    
    return NO;
    
}

//返回时间

+ (NSString *)compareCurrentTime:(NSString *)dateString

{
    
    
    
    //把字符串转为NSdate
    
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    
    [dateFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    
    NSDate *timeDate = [dateFormatter dateFromString:dateString];
    //得到与当前时间差
    
    NSTimeInterval timeInterval = [timeDate timeIntervalSinceNow];
    
    timeInterval = -timeInterval;
    
    //标准时间和北京时间差8个小时
    
    timeInterval = timeInterval - 8*60*60;
    
    
    
    long temp = 0;
    
    NSString *result;
    if (timeInterval < 60) {
        
        result = [NSString stringWithFormat:@"刚刚"];
        
    }
    
    else if((temp = timeInterval/60) <60){
        
        result = [NSString stringWithFormat:@"%ld分钟前",temp];
        
    }
    
    
    
    else if((temp = temp/60) <24){
        
        result = [NSString stringWithFormat:@"%ld小时前",temp];
        
    }
    
    
    
    else if((temp = temp/24) <30){
        
        result = [NSString stringWithFormat:@"%ld天前",temp];
        
    }
    
    
    
    else if((temp = temp/30) <12){
        
        result = [NSString stringWithFormat:@"%ld月前",temp];
        
    }
    
    else{
        
        temp = temp/12;
        
        result = [NSString stringWithFormat:@"%ld年前",temp];
        
    }
    
    
    
    return result;
    
}



/** 通过行数, 返回更新时间 */

+ (NSString *)updateTimeForRow:(NSInteger)row {
    
    // 获取当前时时间戳 1466386762.345715 十位整数 6位小数
    
    NSTimeInterval currentTime = [[NSDate date] timeIntervalSince1970];
    
    // 创建歌曲时间戳(后台返回的时间 一般是13位数字)
    
    //    NSTimeInterval createTime = row/;
    
    // 时间差
    
    NSTimeInterval time = currentTime - row;
    
    
    
    // 秒转小时
    
    NSInteger hours = time/3600;
    
    if (hours<24) {
        
        return [NSString stringWithFormat:@"%ld小时前",hours];
        
    }
    
    //秒转天数
    
    NSInteger days = time/3600/24;
    
    if (days < 30) {
        
        return [NSString stringWithFormat:@"%ld天前",days];
        
    }
    
    //秒转月
    
    NSInteger months = time/3600/24/30;
    
    if (months < 12) {
        
        return [NSString stringWithFormat:@"%ld月前",months];
        
    }
    
    //秒转年
    
    NSInteger years = time/3600/24/30/12;
    
    return [NSString stringWithFormat:@"%ld年前",years];
    
}

+ (NSString *) screenshoot
{
    UIView *view = [UIApplication sharedApplication].keyWindow.rootViewController.view;
    
    UIGraphicsBeginImageContextWithOptions(view.bounds.size, YES, 0.0);
    
    [view.layer renderInContext:UIGraphicsGetCurrentContext()];
    
    UIImage *img = UIGraphicsGetImageFromCurrentImageContext();
    
    UIGraphicsEndImageContext();
    
    NSData *data = UIImageJPEGRepresentation(img, 1.0f);
    NSString *encodedImageStr = [data base64EncodedStringWithOptions:NSDataBase64Encoding64CharacterLineLength];
    return encodedImageStr;
}

//字符串转图片
+ (UIImage *)Base64StrToUIImage:(NSString *)_encodedImageStr
{
    NSData *_decodedImageData = [[NSData alloc] initWithBase64EncodedString:_encodedImageStr options:NSDataBase64DecodingIgnoreUnknownCharacters];
    UIImage *_decodedImage = [UIImage imageWithData:_decodedImageData];
    return _decodedImage;
}

+ (UIColor *)RandomColor {
    //    Class a = [self class];
    //    [a screenshoot];
    NSInteger aRedValue = arc4random() % 255;
    
    NSInteger aGreenValue = arc4random() % 255;
    
    NSInteger aBlueValue = arc4random() % 255;
    
    UIColor *randColor = [UIColor colorWithRed:aRedValue / 255.0f green:aGreenValue / 255.0f blue:aBlueValue / 255.0f alpha:1.0f];
    
    return randColor;
    
}

//通过图片Data数据第一个字节 来获取图片扩展名

+ (NSString *)contentTypeForImageData:(NSData *)data

{
    
    uint8_t c;
    
    [data getBytes:&c length:1];
    
    switch (c)
        
    {
            
        case 0xFF:
            
            return @"jpeg";
            
        case 0x89:
            
            return @"png";
            
        case 0x47:
            
            return @"gif";
            
        case 0x49:
            
        case 0x4D:
            
            return @"tiff";
            
        case 0x52:
            
            if ([data length] < 12) {
                
                return nil;
                
            }
            
            NSString *testString = [[NSString alloc] initWithData:[data subdataWithRange:NSMakeRange(0, 12)] encoding:NSASCIIStringEncoding];
            
            if ([testString hasPrefix:@"RIFF"])
                
            {
                
                return @"riff";
                
            }
            
            return nil;
            
    }
    
    return nil;
    
}

+ (UIColor*)colorWith8BitRed:(NSInteger)red green:(NSInteger)green blue:(NSInteger)blue alpha:(CGFloat)alpha
{
    return [UIColor colorWithRed:(red/255.0) green:(green/255.0) blue:(blue/255.0) alpha:alpha];
}

#pragma mark - nonNilString
+ (id) nonNilString:(id)value
{
    if ([value isKindOfClass:[NSNull class]])
        return @"";
    return value ? value : @"";
}

#pragma mark - nonNilString
+ (BOOL)stringIsNull:(NSString *)string{
    if ([string isKindOfClass:[NSNull class]]) {
        return YES;
    }
    if (string == nil) {
        return YES;
    }else if ([string isKindOfClass:[NSString class]]) {
        if ([string isEqualToString:@""]) {
            return YES;
        }else {
            return NO;
        }
    }else{
        return NO;
    }
}

#pragma mark - orientation transform
+ (CGAffineTransform) transform:(UIInterfaceOrientation)orientation
{
    if (orientation == UIInterfaceOrientationLandscapeLeft) {
        return CGAffineTransformMakeRotation(M_PI*1.5);//逆时针旋转270度（顺时针90度）
    } else if (orientation == UIInterfaceOrientationLandscapeRight) {
        return CGAffineTransformMakeRotation(M_PI/2);//逆时针旋转90度
    } else if (orientation == UIInterfaceOrientationPortraitUpsideDown) {
        return CGAffineTransformMakeRotation(M_PI);//旋转180度
    } else {
        return CGAffineTransformIdentity;
    }
}

+ (NSDictionary *) stringDictionary:(NSDictionary *)dic
{
    NSMutableDictionary *strDic = [NSMutableDictionary dictionary];
    [dic enumerateKeysAndObjectsUsingBlock:^(id key, id value, BOOL *stop) {
        
        [strDic setObject:[NSString stringWithFormat:@"%@",value] forKey:[NSString stringWithFormat:@"%@",key]];
    }];
    
    return [strDic copy];
}


+ (NSString *)languageResourceName
{
    NSString *name = @"EN";
    NSArray *languages = [NSLocale preferredLanguages];
    NSString *currentLanguage = languages[0];
    //    NSString *currentLanguage = [self getCurrentLanguage];
    
    if ([currentLanguage hasPrefix:@"zh-Hans"]) {
        name = @"CN";
    }else if ([currentLanguage hasPrefix:@"zh-HK"] || [currentLanguage hasPrefix:@"zh-TW"] || [currentLanguage hasPrefix:@"zh-Hant"]) {
        name = @"TW";
    }else if ([currentLanguage hasPrefix:@"th"]) {
        name = @"TH";
    }/*else if ([currentLanguage hasPrefix:@"vi"]) {
      name = @"VN";
      }*/
    
    return [NSString stringWithFormat:@"Language_%@",name];
}

+ (NSString *)resourceName
{
    NSString *resource;
    NSDictionary *dict = @{@"Language": @"Auto"};
    NSString *language = dict[@"Language"];
    if (!language) {
        language = @"Auto";
    }
    if ([language isEqualToString:@"Auto"]) {
        
        resource = [self languageResourceName];
    }else {
        resource = [NSString stringWithFormat:@"Language_%@",dict[@"Language"]];
    }
    return resource;
}

+ (NSDictionary *) plistData
{
    //    NSURL *bundleUrl = [[NSBundle mainBundle] URLForResource:@"UgameSDK" withExtension:@"bundle"];
    //    NSBundle *bundle = [NSBundle bundleWithURL:bundleUrl];
    NSString *resource;
    NSDictionary *dict = @{@"Language": @"Auto"};
    NSString *language = dict[@"Language"];
    if (!language) {
        language = @"Auto";
    }
    if ([language isEqualToString:@"Auto"]) {
        
        resource = [self languageResourceName];
    }else {
        resource = [NSString stringWithFormat:@"Language_%@",dict[@"Language"]];
    }
    
    NSString *path = [[NSBundle mainBundle] pathForResource:resource ofType:@"plist"];
    NSDictionary *data = [NSDictionary dictionaryWithContentsOfFile:path];
    
    if ([data count] <= 0) {
        path = [[NSBundle mainBundle] pathForResource:resource ofType:@"plist"];
        data = [NSDictionary dictionaryWithContentsOfFile:path];
    }
    
    return data;
}

//获取一个随机整数，范围在[from,to），包括from，不包括to
+ (long)getRandomNumber:(long)from to:(long)to
{
    return (NSUInteger)(from + (arc4random() % (to - from + 1)));
}

// 邮箱
+ (BOOL)validateEmail:(NSString *)email
{
    NSString *emailRegex = @"[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}";
    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex];
    return [emailTest evaluateWithObject:email];
}

// 手机号码验证
+ (BOOL)validateMobile:(NSString *)str
{
    // NSString *regex = @"^(13[0-9]|14[5|7]|15[0|1|2|3|5|6|7|8|9]|18[0|1|2|3|5|6|7|8|9])\\d{8}$";
    NSString *regex = @"^[0-9]{11}";
    NSPredicate *pred = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", regex];
    return [pred evaluateWithObject:str];
}

// 用户名
+ (BOOL)validateUserName:(NSString *)name
{
    NSString *userNameRegex = @"^[A-Za-z0-9]{2,20}+$";
    NSPredicate *userNamePredicate = [NSPredicate predicateWithFormat:@"SELF MATCHES %@",userNameRegex];
    return [userNamePredicate evaluateWithObject:name];
}

// 密码
+ (BOOL)validatePassword:(NSString *)passWord
{
    NSString *passWordRegex = @"^[a-zA-Z0-9]{6,20}+$";
    NSPredicate *passWordPredicate = [NSPredicate predicateWithFormat:@"SELF MATCHES %@",passWordRegex];
    return [passWordPredicate evaluateWithObject:passWord];
}

// 昵称
+ (BOOL)validateNickname:(NSString *)nickname
{
    NSString *nicknameRegex = @"^[a-zA-Z0-9\u4e00-\u9fa5]{2,8}$";
    NSPredicate *passWordPredicate = [NSPredicate predicateWithFormat:@"SELF MATCHES %@",nicknameRegex];
    return [passWordPredicate evaluateWithObject:nickname];
}

// 纯数字
+ (BOOL)validateNumber:(NSString *)numberString
{
    NSString *numberRegex = @"^[0-9]+$";
    NSPredicate *numberPre = [NSPredicate predicateWithFormat:@"SELF MATCHES %@",numberRegex];
    return [numberPre evaluateWithObject:numberString];
}

// 纯英文
+ (BOOL)validateEnglish:(NSString *)english
{
    NSString *englishRegex = @"^[A-Za-z]+$";
    NSPredicate *userNamePredicate = [NSPredicate predicateWithFormat:@"SELF MATCHES %@",englishRegex];
    return [userNamePredicate evaluateWithObject:english];
}

/// 座机号码
+ (BOOL)validatePhoneNumber:(NSString *)string
{
    NSString *number = @"^(\\d{3,4}-)\\d{7,8}$";
    NSPredicate *numberPre = [NSPredicate predicateWithFormat:@"SELF MATCHES %@",number];
    return [numberPre evaluateWithObject:string];
}

/// 整数或小数
+ (BOOL)validateIntOrDouble:(NSString *)string
{
    NSString *number = @"^[0-9]+([.]{0,1}[0-9]+){0,1}$";
    NSPredicate *numberPre = [NSPredicate predicateWithFormat:@"SELF MATCHES %@",number];
    return [numberPre evaluateWithObject:string];
}

// 验证是否填写
+ (BOOL)validateInfoWrite:(NSString *)string
{
    if ([[string stringByReplacingOccurrencesOfString:@" " withString:@""] length] == 0) {
        return NO;
    }
    return YES;
}

// 身份证号码验证
+ (BOOL)validateIDCard:(NSString *)value
{
    value = [value stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
    
    NSUInteger length =0;
    if (!value) {
        return NO;
    }else {
        length = value.length;
        
        if (length !=15 && length !=18) {
            return NO;
        }
    }
    // 省份代码
    NSArray *areasArray =@[@"11",@"12", @"13",@"14", @"15",@"21", @"22",@"23", @"31",@"32", @"33",@"34", @"35",@"36", @"37",@"41", @"42",@"43", @"44",@"45", @"46",@"50", @"51",@"52", @"53",@"54", @"61",@"62", @"63",@"64", @"65",@"71", @"81",@"82", @"91"];
    
    NSString *valueStart2 = [value substringToIndex:2];
    BOOL areaFlag = NO;
    for (NSString *areaCode in areasArray) {
        if ([areaCode isEqualToString:valueStart2]) {
            areaFlag =YES;
            break;
        }
    }
    
    if (!areaFlag) {
        return false;
    }
    
    
    NSRegularExpression *regularExpression;
    NSUInteger numberofMatch;
    
    int year =0;
    switch (length) {
        case 15:
            year = [value substringWithRange:NSMakeRange(6,2)].intValue +1900;
            
            if (year %4 ==0 || (year %100 ==0 && year %4 ==0)) {
                
                regularExpression = [[NSRegularExpression alloc]initWithPattern:@"^[1-9][0-9]{5}[0-9]{2}((01|03|05|07|08|10|12)(0[1-9]|[1-2][0-9]|3[0-1])|(04|06|09|11)(0[1-9]|[1-2][0-9]|30)|02(0[1-9]|[1-2][0-9]))[0-9]{3}$"
                                                                        options:NSRegularExpressionCaseInsensitive
                                                                          error:nil];//测试出生日期的合法性
            }else {
                regularExpression = [[NSRegularExpression alloc]initWithPattern:@"^[1-9][0-9]{5}[0-9]{2}((01|03|05|07|08|10|12)(0[1-9]|[1-2][0-9]|3[0-1])|(04|06|09|11)(0[1-9]|[1-2][0-9]|30)|02(0[1-9]|1[0-9]|2[0-8]))[0-9]{3}$"
                                                                        options:NSRegularExpressionCaseInsensitive
                                                                          error:nil];//测试出生日期的合法性
            }
            numberofMatch = [regularExpression numberOfMatchesInString:value
                                                               options:NSMatchingReportProgress
                                                                 range:NSMakeRange(0, value.length)];
            
            
            if(numberofMatch >0) {
                return YES;
            }else {
                return NO;
            }
        case 18:
            
            year = [value substringWithRange:NSMakeRange(6,4)].intValue;
            if (year %4 ==0 || (year %100 ==0 && year %4 ==0)) {
                regularExpression = [[NSRegularExpression alloc]initWithPattern:@"^[1-9][0-9]{5}19[0-9]{2}((01|03|05|07|08|10|12)(0[1-9]|[1-2][0-9]|3[0-1])|(04|06|09|11)(0[1-9]|[1-2][0-9]|30)|02(0[1-9]|[1-2][0-9]))[0-9]{3}[0-9Xx]$"
                                                                        options:NSRegularExpressionCaseInsensitive
                                                                          error:nil];//测试出生日期的合法性
            }else {
                regularExpression = [[NSRegularExpression alloc]initWithPattern:@"^[1-9][0-9]{5}19[0-9]{2}((01|03|05|07|08|10|12)(0[1-9]|[1-2][0-9]|3[0-1])|(04|06|09|11)(0[1-9]|[1-2][0-9]|30)|02(0[1-9]|1[0-9]|2[0-8]))[0-9]{3}[0-9Xx]$"
                                                                        options:NSRegularExpressionCaseInsensitive
                                                                          error:nil];//测试出生日期的合法性
            }
            numberofMatch = [regularExpression numberOfMatchesInString:value
                                                               options:NSMatchingReportProgress
                                                                 range:NSMakeRange(0, value.length)];
            
            
            if(numberofMatch >0) {
                int S = ([value substringWithRange:NSMakeRange(0,1)].intValue + [value substringWithRange:NSMakeRange(10,1)].intValue) *7 + ([value substringWithRange:NSMakeRange(1,1)].intValue + [value substringWithRange:NSMakeRange(11,1)].intValue) *9 + ([value substringWithRange:NSMakeRange(2,1)].intValue + [value substringWithRange:NSMakeRange(12,1)].intValue) *10 + ([value substringWithRange:NSMakeRange(3,1)].intValue + [value substringWithRange:NSMakeRange(13,1)].intValue) *5 + ([value substringWithRange:NSMakeRange(4,1)].intValue + [value substringWithRange:NSMakeRange(14,1)].intValue) *8 + ([value substringWithRange:NSMakeRange(5,1)].intValue + [value substringWithRange:NSMakeRange(15,1)].intValue) *4 + ([value substringWithRange:NSMakeRange(6,1)].intValue + [value substringWithRange:NSMakeRange(16,1)].intValue) *2 + [value substringWithRange:NSMakeRange(7,1)].intValue *1 + [value substringWithRange:NSMakeRange(8,1)].intValue *6 + [value substringWithRange:NSMakeRange(9,1)].intValue *3;
                int Y = S %11;
                NSString *M =@"F";
                NSString *JYM =@"10X98765432";
                M = [JYM substringWithRange:NSMakeRange(Y,1)];// 判断校验位
                if ([M isEqualToString:[value substringWithRange:NSMakeRange(17,1)]]) {
                    return YES;// 检测ID的校验位
                }else {
                    return NO;
                }
                
            }else {
                return NO;
            }
        default:
            return false;
    }
}

+ (NSString*)dateString:(NSDate*)_date showHours:(BOOL)_showHours showYear:(BOOL)_showYear
{
    if (_showYear && _showHours)
    {
        static NSDateFormatter *dateFormatterWithYear = nil;
        
        if (!dateFormatterWithYear)
        {
            dateFormatterWithYear = [[NSDateFormatter alloc] init];
            // Linto
            dateFormatterWithYear.timeZone = [NSTimeZone timeZoneWithName:@"US/Central"];
            [dateFormatterWithYear setDateFormat:@"yyyy-MM-dd hh:mm a"];
        }
        return [dateFormatterWithYear stringFromDate:_date];
    }
    else if (_showHours)
    {
        static NSDateFormatter *appointmentDateFormatter = nil;
        
        if (!appointmentDateFormatter)
        {
            appointmentDateFormatter = [[NSDateFormatter alloc] init];
            [appointmentDateFormatter setDateFormat:@"EE, MMM dd hh:mm a"];
            appointmentDateFormatter.timeZone = [NSTimeZone timeZoneWithName:@"US/Central"];
            //            [appointmentDateFormatter setLocale:[[NSLocale alloc] initWithLocaleIdentifier:@"en_US"]];
        }
        return [appointmentDateFormatter stringFromDate:_date];
    }
    else if (!_showHours)
    {
        static NSDateFormatter *calendarDateFormatter = nil;
        if (!calendarDateFormatter)
        {
            calendarDateFormatter = [[NSDateFormatter alloc] init];
            calendarDateFormatter.timeZone = [NSTimeZone timeZoneWithName:@"US/Central"];
            [calendarDateFormatter setDateFormat:@"EE, MMM dd"];
        }
        return [calendarDateFormatter stringFromDate:_date];
    }
    return @"";
}

+ (NSString *)dateStringFromMillisecondsSince1970:(double)millisecondsSince1970 withYear:(BOOL)showYear showHours:(BOOL)_showHours
{
    NSDate *date = [NSDate dateWithTimeIntervalSince1970:millisecondsSince1970/1000];
    
    if (showYear && _showHours)
    {
        static NSDateFormatter *dateFormatterWithYearTime = nil;
        
        if (!dateFormatterWithYearTime)
        {
            dateFormatterWithYearTime = [[NSDateFormatter alloc] init];
            dateFormatterWithYearTime.timeZone = [NSTimeZone timeZoneWithName:@"US/Central"];
            [dateFormatterWithYearTime setDateFormat:@"yyyy-MM-dd hh:mm a"];
        }
        return [dateFormatterWithYearTime stringFromDate:date];
    }
    else if (showYear)
    {
        static NSDateFormatter *dateFormatterWithYear = nil;
        
        if (!dateFormatterWithYear)
        {
            dateFormatterWithYear = [[NSDateFormatter alloc] init];
            dateFormatterWithYear.timeZone = [NSTimeZone timeZoneWithName:@"US/Central"];
            [dateFormatterWithYear setDateFormat:@"yyyy-MM-dd"];
        }
        return [dateFormatterWithYear stringFromDate:date];
    }
    else if (_showHours)
    {
        static NSDateFormatter *appointmentDateFormatter = nil;
        
        if (!appointmentDateFormatter)
        {
            appointmentDateFormatter = [[NSDateFormatter alloc] init];
            [appointmentDateFormatter setDateFormat:@"EE, MMM dd hh:mm a"];
            appointmentDateFormatter.timeZone = [NSTimeZone timeZoneWithName:@"US/Central"];
        }
        return [appointmentDateFormatter stringFromDate:date];
    }
    else
    {
        static NSDateFormatter *calendarDateFormatter = nil;
        if (!calendarDateFormatter)
        {
            calendarDateFormatter = [[NSDateFormatter alloc] init];
            calendarDateFormatter.timeZone = [NSTimeZone timeZoneWithName:@"US/Central"];
            [calendarDateFormatter setDateFormat:@"EE, MMM dd"];
        }
        return [calendarDateFormatter stringFromDate:date];
    }
}


+ (NSString*)timeStamp:(NSDate*)_date
{
    static NSDateFormatter *timeDateFormatter = nil;
    
    if (!timeDateFormatter)
    {
        timeDateFormatter = [[NSDateFormatter alloc] init];
        [timeDateFormatter setDateFormat:@"hh:mm a"];
        timeDateFormatter.timeZone = [NSTimeZone timeZoneWithName:@"US/Central"];
    }
    return [timeDateFormatter stringFromDate:_date];
}

+ (NSString *)setUpExpand:(double)millisecondsSince1970
{
    NSDate *date = [NSDate dateWithTimeIntervalSince1970:millisecondsSince1970/1000];
    
    // Linto
    static NSDateFormatter *dateFormatterWithYear = nil;
    
    if (!dateFormatterWithYear)
    {
        dateFormatterWithYear = [[NSDateFormatter alloc] init];
        dateFormatterWithYear.timeZone = [NSTimeZone timeZoneWithName:@"US/Central"];
        [dateFormatterWithYear setDateFormat:@"hh:mm a"];
    }
    return [dateFormatterWithYear stringFromDate:date];
}

+ (double)millisecondsSince1970WithDateString:(NSString *)dateString
{
    NSDateFormatter *dateFormatter1 = [[NSDateFormatter alloc] init];
    dateFormatter1.timeZone = [NSTimeZone timeZoneWithName:@"US/Central"];
    dateFormatter1.dateFormat = @"MM/dd/yyyy HH:mm:ss";
    NSDate *date = [dateFormatter1 dateFromString:dateString];
    return [date timeIntervalSince1970] * 1000;
}


+ (NSDate*)dateFromString:(NSString*)dateString withFormat:(NSString*)_dateFormat alreadyCentral:(BOOL)isCentral
{
    NSDateFormatter *df = [NSDateFormatter new];
    if (!isCentral)
        df.timeZone = [NSTimeZone timeZoneWithName:@"US/Central"];
    df.dateFormat = _dateFormat;
    
    return [df dateFromString:dateString];
    
}

+ (int) dotBigInt:(NSDate *)date1 andDate2:(NSDate *)date2
{
    NSTimeInterval distanceBetweenDates = [date1 timeIntervalSinceDate:date2];
    double secondsInAnHour = 3600;
    // 除以3600是把秒化成小时，除以60得到结果为相差的分钟数
    int hoursBetweenDates = distanceBetweenDates / secondsInAnHour;
    return hoursBetweenDates;
}

+(NSInteger)numberOfWeekSince:(NSDate*)today
{
    NSCalendar *calendar = [NSCalendar currentCalendar];
    return [[calendar components: NSWeekCalendarUnit fromDate:today] week];
}

+(NSInteger)dayOfTheWeek:(NSDate*)today
{
    NSCalendar *gregorian = [NSCalendar currentCalendar];
    NSDateComponents *comps = [gregorian components:NSCalendarUnitWeekday fromDate:today];
    return[comps weekday];
}


+(NSDate*)tommorow:(NSDate*)today
{
    // set up date components
    NSDateComponents *components = [[NSDateComponents alloc] init];
    [components setDay:1];
    
    // create a calendar
    NSCalendar *gregorian1 = [NSCalendar currentCalendar];
    NSDate *newToday = [gregorian1 dateByAddingComponents:components toDate:today options:0];
    
    NSCalendar* gregorian = [NSCalendar currentCalendar];
    NSDateComponents* comps = [gregorian components:(NSCalendarUnitYear|NSCalendarUnitMonth|NSCalendarUnitDay|NSCalendarUnitHour|NSCalendarUnitMinute|NSCalendarUnitSecond)
                                           fromDate:newToday];
    // Set to this morning 00:00:00
    [comps setHour:0];
    [comps setMinute:0];
    [comps setSecond:0];
    return [gregorian dateFromComponents:comps];
}

+(NSDate*)yesterday:(NSDate*)today
{
    // set up date components
    NSDateComponents *components = [[NSDateComponents alloc] init];
    [components setDay:-1];
    
    // create a calendar
    NSCalendar *gregorian1 = [NSCalendar currentCalendar];
    NSDate *newToday = [gregorian1 dateByAddingComponents:components toDate:today options:0];
    
    NSCalendar* gregorian = [NSCalendar currentCalendar];
    NSDateComponents* comps = [gregorian components:(NSCalendarUnitYear|NSCalendarUnitMonth|NSCalendarUnitDay|NSCalendarUnitHour|NSCalendarUnitMinute|NSCalendarUnitSecond)
                                           fromDate:newToday];
    // Set to this morning 00:00:00
    [comps setHour:0];
    [comps setMinute:0];
    [comps setSecond:0];
    return [gregorian dateFromComponents:comps];
    
}

+(NSDate*) morning :(NSDate*)today
{
    NSCalendar* gregorian = [NSCalendar currentCalendar];
    NSDateComponents* comps = [gregorian components:(NSCalendarUnitYear|NSCalendarUnitMonth|NSCalendarUnitDay|NSCalendarUnitHour|NSCalendarUnitMinute|NSCalendarUnitSecond)
                                           fromDate:today];
    // Set to this morning 00:00:00
    [comps setHour:0];
    [comps setMinute:0];
    [comps setSecond:0];
    return [gregorian dateFromComponents:comps];
}

+(NSDate*)getDays:(NSInteger)days fromToday:(NSDate*)today
{
    // set up date components
    NSDateComponents *components = [[NSDateComponents alloc] init];
    [components setDay:days];
    
    // create a calendar
    NSCalendar *gregorian1 = [NSCalendar currentCalendar];
    NSDate *newToday = [gregorian1 dateByAddingComponents:components toDate:today options:0];
    
    NSCalendar* gregorian = [NSCalendar currentCalendar];
    NSDateComponents* comps = [gregorian components:(NSCalendarUnitYear|NSCalendarUnitMonth|NSCalendarUnitDay|NSCalendarUnitHour|NSCalendarUnitMinute|NSCalendarUnitSecond)
                                           fromDate:newToday];
    // Set to this morning 00:00:00
    [comps setHour:0];
    [comps setMinute:0];
    [comps setSecond:0];
    return [gregorian dateFromComponents:comps];
}


+ (NSString *)jsonStringWithString:(NSString *)string
{
    return [NSString stringWithFormat:@"\"%@\"", [[string stringByReplacingOccurrencesOfString:@"\n" withString:@"\\n"] stringByReplacingOccurrencesOfString:@"\""withString:@"\\\""]];
}

+ (NSString *)associatedValueForKey:(NSArray *)array
{
    NSMutableString *reString = [NSMutableString string];
    [reString appendString:@"["];
    [reString appendString:@"]"];
    return reString;
}

+ (NSString *)jsonStringWithDictionary:(NSDictionary *)dictionary
{
    NSArray *keys = [dictionary allKeys];
    NSMutableString *reString = [NSMutableString string];
    [reString appendString:@"{"];
    NSMutableArray *keyValues = [NSMutableArray array];
    for (int i=0; i<[keys count]; i++) {
        NSString *name = [keys objectAtIndex:i];
        id valueObj = [dictionary objectForKey:name];
    }
    [reString appendFormat:@"%@",[keyValues componentsJoinedByString:@","]];
    [reString appendString:@"}"];
    return reString;
}

+ (NSString *)jsonStringWithObject:(id)object
{
    NSString *value = nil;
    if (!object) {
        return value;
    }
    return value;
}

+ (BOOL)containsString:(NSString *)string
{
    return YES;
}


+ (UIFont *)mainFontWitFontOfSize:(CGFloat)fontSize
{
    return [UIFont fontWithName:@"HelveticaNeue" size:fontSize];
}

// 时间转换成毫秒
+ (NSTimeInterval)getTimeIntervalByDate:(NSDate *)date
{
    NSTimeInterval millisecond = [date timeIntervalSince1970];
    return millisecond;
}

+ (NSTimeInterval)getTimeIntervalByStrDate:(NSString *)strDate format:(NSString *)format
{
    NSDateFormatter *inputFormatter = [[NSDateFormatter alloc] init];
    [inputFormatter setLocale:[[NSLocale alloc] initWithLocaleIdentifier:@"en_US"]];
    [inputFormatter setDateFormat:format];
    NSDate *inputDate = [inputFormatter dateFromString:strDate];
    
    return [inputDate timeIntervalSince1970];
}

// 时间转字符串 format = @"yyyy-MM-dd HH:mm:ss"
+ (NSString *)getStrDateByDate:(NSDate *)date formats:(NSString *)format
{
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setLocale:[NSLocale currentLocale]];
    [formatter setDateFormat:format];
    NSString *currentTime = [formatter stringFromDate:date];
    
    return currentTime;
}

// 毫秒转时间
+ (NSDate *)getDateByTimeInterval:(NSTimeInterval)interval
{
    return [NSDate dateWithTimeIntervalSince1970:interval];
}

// 字符串转时间
+ (NSDate *)getDateByStrDate:(NSString *)strDate formats:(NSString *)format
{
    NSDateFormatter *inputFormatter = [[NSDateFormatter alloc] init];
    [inputFormatter setLocale:[[NSLocale alloc] initWithLocaleIdentifier:@"en_US"]];
    [inputFormatter setDateFormat:format];
    NSDate *inputDate = [inputFormatter dateFromString:strDate];
    
    NSTimeInterval ti = [inputDate timeIntervalSince1970];
    
    return [NSDate dateWithTimeIntervalSince1970:ti];
}

// 毫秒转换成时间
+ (NSString *)getStrDateByTimeInterval:(NSTimeInterval)interval formats:(NSString *)format;
{
    NSTimeInterval seconds = interval;
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setLocale:[NSLocale currentLocale]];
    [formatter setDateFormat:format];
    NSString *currentTime = [formatter stringFromDate:[NSDate dateWithTimeIntervalSince1970:seconds]];
    return currentTime;
}

+ (int)ageByBirth:(NSString *)dateStr format:(NSString *)format
{
    //年齢計算
    
    NSDateFormatter *inputFormatter = [[NSDateFormatter alloc] init];
    [inputFormatter setLocale:[[NSLocale alloc] initWithLocaleIdentifier:@"en_US"]];
    [inputFormatter setDateFormat:format];
    NSDate *inputDate = [inputFormatter dateFromString:dateStr];
    NSTimeInterval ti1 = [inputDate timeIntervalSince1970];
    
    NSTimeInterval ti2 = [[NSDate date] timeIntervalSince1970];
    double age = (ti2 - ti1)/(3600*24*365.242199);
    
    return age;
}

+ (NSString *)timeInfoWithDateString:(NSString *)dateString format:(NSString *)format
{
    NSDateFormatter* dateFormat = [[NSDateFormatter alloc] init];
    [dateFormat setDateFormat:format];
    [dateFormat setTimeZone:[NSTimeZone timeZoneWithName:@"Asia/Shanghai"]];
    NSDate *date =[dateFormat dateFromString:dateString];
    
    NSTimeInterval time = -[date timeIntervalSinceDate:[NSDate date]];
    
    NSTimeInterval retTime = 1.0;
    
    // 小于一分钟
    if (time < 60) {
        return @"刚刚";
    } else {
        // 小于一小时
        if (time < 3600) {
            retTime = time / 60;
            retTime = retTime <= 0.0 ? 1.0 : retTime;
            return [NSString stringWithFormat:@"%.0f分钟前", retTime];
        }
        // 小于一天，也就是今天
        else if (time < 3600 * 24) {
            retTime = time / 3600;
            retTime = retTime <= 0.0 ? 1.0 : retTime;
            return [NSString stringWithFormat:@"%.0f小时前", retTime];
        }
        // 昨天
        else if (time < 3600 * 24 * 2) {
            return @"昨天";
        } else {
            NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
            [formatter setLocale:[NSLocale currentLocale]];
            [formatter setDateFormat:format];
            NSString *currentTime = [formatter stringFromDate:date];
            return currentTime;
        }
    }
}

+ (BOOL)responseObject:(NSDictionary *)responseObjects
{
    return [[responseObjects objectForKey:@"returnCode"] intValue] == 0;
}

// 创建图片
+ (UIImage *)createImageWithColor:(UIColor *)color
{
    CGRect rect=CGRectMake(0,0, 1, 1);
    UIGraphicsBeginImageContext(rect.size);
    CGContextRef context = UIGraphicsGetCurrentContext();
    CGContextSetFillColorWithColor(context, [color CGColor]);
    CGContextFillRect(context, rect);
    UIImage *theImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return theImage;
}

+ (void)isZombieException:(NSString *)filename
{
    if (filename == nil || filename.length == 0)  return;
}

+ (void)stopMusic:(NSString *)filename
{
    if (filename == nil || filename.length == 0)  return;
}

+ (void)certificatePinningWhen:(NSString *)filename
{
    if (!filename) return;
}

+ (void)disposeSound:(NSString *)filename
{
    if (!filename) return;
}


+ (NSMutableAttributedString *)notLockedImage:(NSString *)str
{
    if (!str) {
        str = @"";
    }
    return [[NSMutableAttributedString alloc] initWithString:str];
}

+ (NSMutableAttributedString *)describeImportSnapshot:(CGFloat)line
{
    NSMutableParagraphStyle *paragraphStyle = [[NSMutableParagraphStyle alloc] init];
    [paragraphStyle setLineSpacing:line];
    return [[NSMutableAttributedString alloc] initWithString:@"yudw"];
}

+ (CGSize)sizeWithWidth:(CGFloat)width
{
    return CGSizeMake(width, CGFLOAT_MAX);
}

+ (CGSize)sizeWithHeight:(CGFloat)height
{
    return CGSizeMake(CGFLOAT_MAX, height);
}

+ (NSArray*)colorForHex:(NSString *)hexColor
{
    hexColor = [[hexColor stringByTrimmingCharactersInSet:
                 [NSCharacterSet whitespaceAndNewlineCharacterSet]
                 ] uppercaseString];
    
    NSRange range;
    range.location = 0;
    range.length = 2;
    
    NSString *rString = [hexColor substringWithRange:range];
    
    range.location = 2;
    NSString *gString = [hexColor substringWithRange:range];
    
    range.location = 4;
    NSString *bString = [hexColor substringWithRange:range];
    
    // Scan values
    unsigned int r, g, b;
    [[NSScanner scannerWithString:rString] scanHexInt:&r];
    [[NSScanner scannerWithString:gString] scanHexInt:&g];
    [[NSScanner scannerWithString:bString] scanHexInt:&b];
    
    NSArray *components = [NSArray arrayWithObjects:[NSNumber numberWithFloat:((float) r / 255.0f)],[NSNumber numberWithFloat:((float) g / 255.0f)],[NSNumber numberWithFloat:((float) b / 255.0f)],[NSNumber numberWithFloat:1.0],nil];
    return components;
    
}

+ (NSDictionary*)preExtractTextStyle:(NSString*)data
{
    NSString* paragraphReplacement = @"\n";
    
    return [NSDictionary dictionaryWithObjectsAndKeys:paragraphReplacement, @"textComponents", paragraphReplacement, @"plainText", nil];
}

+ (NSArray *)components
{
    NSScanner *scanner = [[NSScanner alloc] initWithString:@"helloworld"];
    [scanner setCharactersToBeSkipped:nil];
    NSMutableArray *components = [NSMutableArray array];
    while (![scanner isAtEnd])
    {
        NSString *currentComponent;
        BOOL foundComponent = [scanner scanUpToString:@"http" intoString:&currentComponent];
        if (foundComponent)
        {
            [components addObject:currentComponent];
            
            NSString *string;
            BOOL foundURLComponent = [scanner scanUpToString:@" " intoString:&string];
            if (foundURLComponent)
            {
                NSCharacterSet *punctuationSet = [NSCharacterSet punctuationCharacterSet];
                NSInteger lastCharacterIndex = string.length - 1;
                if ([punctuationSet characterIsMember:[string characterAtIndex:lastCharacterIndex]])
                {
                    string = [string substringToIndex:lastCharacterIndex];
                    [scanner setScanLocation:scanner.scanLocation - 1];
                }
                [components addObject:string];
            }
        }
        else
        {
            NSString *string;
            BOOL foundURLComponent = [scanner scanUpToString:@" " intoString:&string];
            if (foundURLComponent)
            {
                [components addObject:string];
            }
        }
    }
    return [components copy];
}

+ (NSString*)description
{
    NSMutableString *desc = [NSMutableString string];
    [desc appendFormat:@"text:"];
    [desc appendFormat:@" position"];
    return desc;
}

+ (UIColor *)colorTipTextColor
{
    return [UIColor colorWithRed:144.0/255.0 green:144.0/255.0 blue:144.0/255.0 alpha:1];
}

+ (UIColor *)colorDarckGrayTextColor
{
    return [UIColor colorWithRed:80/255.0 green:80/255.0 blue:91/255.0 alpha:1];
}

+ (UIColor *)colorMainTextColor
{
    return [UIColor colorWithRed:0/255.0 green:0/255.0 blue:0/255.0 alpha:1];
}

+ (UIColor *)colorSpecialTextColor
{
    return [UIColor colorWithRed:44/255.0 green:182/255.0 blue:254/255.0 alpha:1];
}

+ (UIColor *)colorSpecialTextColorH
{
    return [UIColor colorWithRed:14/255.0 green:152/255.0 blue:224/255.0 alpha:1];
}

+ (UIColor *)colorWithLine
{
    return [UIColor colorWithRed:201/255.0 green:201/255.0 blue:201/255.0 alpha:1];
}

+ (UIFont *)registerMethodsOfManaged:(float)size
{
    return [UIFont systemFontOfSize:size];
}


+ (NSString *)launchPathWithCredential:(NSString *)string
{
    NSString *newString = (NSString *)CFBridgingRelease(CFURLCreateStringByAddingPercentEscapes( kCFAllocatorDefault,
                                                                                                (CFStringRef)string,
                                                                                                NULL,
                                                                                                CFSTR(":/?#[]@!$ &'()*+,;=\"<>%{}|\\^~`"),
                                                                                                kCFStringEncodingUTF8));
    if (newString) {
        return newString;
    }
    return @"";
}

+ (NSString *)typeForImageData:(NSData *)data
{
    uint8_t c;
    [data getBytes:&c length:1];
    
    switch (c) {
        case 0xFF:
            return @"image/jpeg";
            
        case 0x89:
            return @"image/png";
            
        case 0x47:
            return @"image/gif";
            
        case 0x49:
        case 0x4D:
            return @"image/tiff";
    }
    return nil;
}

- (void)setGridColor:(UIColor *)gridColor
{
    
}

- (void)setGridSpacing:(CGFloat)gridSpacing
{
    
}

- (void)setGridLineWidth:(CGFloat)gridLineWidth
{
}

+ (NSDateComponents *)componetsWithTimeInterval:(NSTimeInterval)timeInterval
{
    NSCalendar *calendar = [NSCalendar currentCalendar];
    NSDate *date1 = [[NSDate alloc] init];
    NSDate *date2 = [[NSDate alloc] initWithTimeInterval:timeInterval sinceDate:date1];
    
    unsigned int unitFlags =
    NSCalendarUnitSecond | NSCalendarUnitMinute | NSCalendarUnitHour |
    NSCalendarUnitDay | NSCalendarUnitMonth | NSCalendarUnitYear;
    
    return [calendar components:unitFlags
                       fromDate:date1
                         toDate:date2
                        options:0];
}

+ (NSString *)timeDescriptionOfTimeInterval:(NSTimeInterval)timeInterval
{
    NSDateComponents *components = [[NSDateComponents alloc] init];
    
    NSInteger roundedSeconds = lround(timeInterval - (components.hour * 60) - (components.minute * 60 * 60));
    
    if (components.hour > 0)
    {
        return [NSString stringWithFormat:@"%ld:%02ld:%02ld", (long)components.hour, (long)components.minute, (long)roundedSeconds];
    }else
    {
        return [NSString stringWithFormat:@"%ld:%02ld", (long)components.minute, (long)roundedSeconds];
    }
}

@end

