//
//  SystemUtils.hpp
//  quick_libs
//
//  Created by tthw on 17/10/26.
//  Copyright © 2017年 chukong. All rights reserved.
//

#ifndef SystemUtils_hpp
#define SystemUtils_hpp

#include <stdio.h>
#include <cocos2d.h>


class SystemUtilsCocoCallback {
    
    
public:
    
    typedef std::function<void(int, int, std::string)> SystemUtilsCallback;
    
    SystemUtilsCocoCallback();
    void onResult(int callbackType, int code, const char *data);
    
    void setCallback(const SystemUtilsCallback& callback);
    
private:
    SystemUtilsCallback m_callback;
};

class SystemUtils
{
public:

    ~SystemUtils();

    static SystemUtils* getInstance();
    std::string getSystemLanguage();
	std::string getSystemVersion();
    std::string getSystemModel();
    std::string getDeviceBrand();
    std::string getIMEI();
    std::string getIDFA();
    std::string getPhoneUDID();
	void startVibrate();
    void stopVibrate();
    float getBatteryQuantity();
    int getBatteryStauts();
    float getScreenBrightness();
    void setScreenBrightness(float brightness);
    void resquestLocalPhoto();
    void resquestTakePhoto();
	void resquestStartLocation();
    void resquestSaveImageToPhotoWithPath(std::string& path);
    void copyToClipboard(std::string &text);
    void setOpCallback(const SystemUtilsCocoCallback::SystemUtilsCallback& callback);
	std::string getAPKExpansionZipFilePath();
protected:

    SystemUtils();
    
    std::string m_busyErrorMsg;
    
    //SystemUtilsCocoCallback::SystemUtilsCallback* m_cocoCallback;
    static SystemUtils *s_instance;
};

#endif /* SystemUtils_hpp */
