//
//  SystemUtils.cpp
//  quick_libs
//
//  Created by tthw on 17/10/26.
//  Copyright © 2017年 chukong. All rights reserved.
//

#include "SystemUtils.h"
#include "CCLuaEngine.h"
#if (CC_TARGET_PLATFORM == CC_PLATFORM_IOS)
    #include "SystemUtilsIOS.h"
#elif (CC_TARGET_PLATFORM == CC_PLATFORM_ANDROID)
    #include "SystemUtilsAndroid.h"
#else

#endif

SystemUtilsCocoCallback::SystemUtilsCocoCallback():
m_callback(nullptr)
{
    
}

void SystemUtilsCocoCallback::setCallback(const SystemUtilsCallback& callback)
{
    m_callback = callback;
}

void SystemUtilsCocoCallback::onResult(int callbackType, int code, const char *data)
{
    if(m_callback)
    {
        m_callback(callbackType, code, data);
    }
}


SystemUtils* SystemUtils::s_instance = nullptr;
SystemUtils* SystemUtils::getInstance()
{
    if (nullptr == s_instance)
    {
        s_instance = new(std::nothrow) SystemUtils();
    }
    return s_instance;
}

SystemUtils::SystemUtils()
:m_busyErrorMsg("{'resutl':'failed', 'msg':'execute function busy'}")
{
#if (CC_TARGET_PLATFORM == CC_PLATFORM_IOS)
    

#elif (CC_TARGET_PLATFORM == CC_PLATFORM_ANDROID)
    
#else

#endif
}

SystemUtils::~SystemUtils()
{

}

std::string SystemUtils::getSystemLanguage()
{
#if (CC_TARGET_PLATFORM == CC_PLATFORM_IOS)
    
    return SystemUtilsIOS::getSystemLanguage();
    
#elif (CC_TARGET_PLATFORM == CC_PLATFORM_ANDROID)

#else
    CCLOG("[waring]SystemUtils::getSystemLanguage no supper this platform");
    return std::string("");
#endif
}

std::string SystemUtils::getSystemVersion()
{
#if (CC_TARGET_PLATFORM == CC_PLATFORM_IOS)
    
    return SystemUtilsIOS::getSystemVersion();
    
#elif (CC_TARGET_PLATFORM == CC_PLATFORM_ANDROID)

#else
    CCLOG("[waring]SystemUtils::getSystemVersion no supper this platform");
    return std::string("");  
#endif
}

std::string SystemUtils::getAPKExpansionZipFilePath()
{
#if (CC_TARGET_PLATFORM == CC_PLATFORM_IOS)
    return std::string("");
#elif (CC_TARGET_PLATFORM == CC_PLATFORM_ANDROID)

	return SystemUtilsAndroid::getInstance()->getAPKExpansionZipFilePath();
#else
	CCLOG("[waring]SystemUtils::getAPKExpansionZipFilePath no supper this platform");
	return std::string("");
#endif
}


std::string SystemUtils::getSystemModel()
{
#if (CC_TARGET_PLATFORM == CC_PLATFORM_IOS)
    
    return SystemUtilsIOS::getSystemModel();
    
#elif (CC_TARGET_PLATFORM == CC_PLATFORM_ANDROID)

#else
    CCLOG("[waring]SystemUtils::getSystemModel no supper this platform");
    return std::string("");      
#endif
} 

std::string SystemUtils::getDeviceBrand()
{
#if (CC_TARGET_PLATFORM == CC_PLATFORM_IOS)
    
    return SystemUtilsIOS::getDeviceBrand();
    
#elif (CC_TARGET_PLATFORM == CC_PLATFORM_ANDROID)

#else
    CCLOG("[waring]SystemUtils::getDeviceBrand no supper this platform");
    return std::string("");  
#endif
} 

std::string SystemUtils::getIMEI()
{
#if (CC_TARGET_PLATFORM == CC_PLATFORM_IOS)
    
    return SystemUtilsIOS::getIDFA();
    
#elif (CC_TARGET_PLATFORM == CC_PLATFORM_ANDROID)

#else
    CCLOG("[waring]SystemUtils::getIMEI no supper this platform");
    return std::string("");  
#endif
}

std::string SystemUtils::getIDFA()
{
#if (CC_TARGET_PLATFORM == CC_PLATFORM_IOS)
    
    return SystemUtilsIOS::getIDFA();
    
#elif (CC_TARGET_PLATFORM == CC_PLATFORM_ANDROID)

#else
    CCLOG("[waring]SystemUtils::getIDFA no supper this platform");
    return std::string("");  
#endif
}

std::string SystemUtils::getPhoneUDID()
{
#if (CC_TARGET_PLATFORM == CC_PLATFORM_IOS)
    
    return getIMEI();
    
#elif (CC_TARGET_PLATFORM == CC_PLATFORM_ANDROID)

    return getIDFA();

#else
    CCLOG("[waring]SystemUtils::getPhoneUDID no supper this platform");
    return std::string("");
#endif
}

void SystemUtils::stopVibrate()
{
#if (CC_TARGET_PLATFORM == CC_PLATFORM_IOS)

    SystemUtilsIOS::stopVibrate();

#elif (CC_TARGET_PLATFORM == CC_PLATFORM_ANDROID)

#else
    CCLOG("[waring]SystemUtils::stopVibrate no supper this platform");
#endif
}

void SystemUtils::startVibrate()
{
#if (CC_TARGET_PLATFORM == CC_PLATFORM_IOS)
    
    SystemUtilsIOS::startVibrate();
    
#elif (CC_TARGET_PLATFORM == CC_PLATFORM_ANDROID)

#else
	CCLOG("[waring]SystemUtils::startVibrate no supper this platform");
#endif
}

float SystemUtils::getBatteryQuantity()
{
#if (CC_TARGET_PLATFORM == CC_PLATFORM_IOS)
    
    return SystemUtilsIOS::getBatteryQuantity();
    
#elif (CC_TARGET_PLATFORM == CC_PLATFORM_ANDROID)

    return SystemUtilsAndroid::getInstance()->getBatteryQuantity();

#else
    CCLOG("[waring]SystemUtils::getBatteryQuantity no supper this platform");
    return 0.0;
#endif
 
}


int SystemUtils::getBatteryStauts()
{
#if (CC_TARGET_PLATFORM == CC_PLATFORM_IOS)
    
    return SystemUtilsIOS::getBatteryStauts();
    
#elif (CC_TARGET_PLATFORM == CC_PLATFORM_ANDROID)
    
    
#else
    CCLOG("[waring]SystemUtils::getBatteryStauts no supper this platform");
    return 0;
#endif
}


float SystemUtils::getScreenBrightness()
{
#if (CC_TARGET_PLATFORM == CC_PLATFORM_IOS)
    
    return SystemUtilsIOS::getScreenBrightness();
    
#elif (CC_TARGET_PLATFORM == CC_PLATFORM_ANDROID)
    
    
#else
    CCLOG("[waring]SystemUtils::getScreenBrightness no supper this platform");
    return 0.0;
#endif
}

void SystemUtils::setScreenBrightness(float brightness)
{
#if (CC_TARGET_PLATFORM == CC_PLATFORM_IOS)
    
    SystemUtilsIOS::setScreenBrightness(brightness);
    
#elif (CC_TARGET_PLATFORM == CC_PLATFORM_ANDROID)
    
    
#else

    CCLOG("[waring]SystemUtils::setScreenBrightness no supper this platform");
#endif
}

void SystemUtils::resquestLocalPhoto()
{
#if (CC_TARGET_PLATFORM == CC_PLATFORM_IOS)
    
    SystemUtilsIOS::resquestLocalPhoto();
    
#elif (CC_TARGET_PLATFORM == CC_PLATFORM_ANDROID)
    
    
#else
    CCLOG("[waring]SystemUtils::resquestLocalPhoto no supper this platform");
#endif
}

void SystemUtils::resquestTakePhoto()
{
  
#if (CC_TARGET_PLATFORM == CC_PLATFORM_IOS)
    
    SystemUtilsIOS::resquestTakePhoto();
    
#elif (CC_TARGET_PLATFORM == CC_PLATFORM_ANDROID)
    
    
#else
    CCLOG("[waring]SystemUtils::resquestTakePhoto no supper this platform");
#endif
}

void SystemUtils::resquestStartLocation()
{  
#if (CC_TARGET_PLATFORM == CC_PLATFORM_IOS)
    
    SystemUtilsIOS::getSystemLanguage();
    
#elif (CC_TARGET_PLATFORM == CC_PLATFORM_ANDROID)
    
    
#else
    CCLOG("[waring]SystemUtils::resquestStartLocation no supper this platform");
#endif
}

void SystemUtils::resquestSaveImageToPhotoWithPath(std::string& path)
{

#if (CC_TARGET_PLATFORM == CC_PLATFORM_IOS)
    
    SystemUtilsIOS::resquestSaveImageToPhotoWithPath(path.c_str());
    
#elif (CC_TARGET_PLATFORM == CC_PLATFORM_ANDROID)
    
    
#else
	CCLOG("[waring]SystemUtils::resquestSaveImageToPhotoWithPath no supper this platform");
#endif
}


void SystemUtils::copyToClipboard(std::string &text)
{
    
#if (CC_TARGET_PLATFORM == CC_PLATFORM_IOS)
    
    SystemUtilsIOS::copyToClipboard(text.c_str());
    
#elif (CC_TARGET_PLATFORM == CC_PLATFORM_ANDROID)
    
    
#elif(CC_TARGET_PLATFORM == CC_PLATFORM_WIN32)

	OpenClipboard(cocos2d::Director::getInstance()->getOpenGLView()->getWin32Window());
	//清空剪贴板     
	EmptyClipboard();
	//向剪贴板中放东西     

	//分配内存     
	HGLOBAL hgl = GlobalAlloc(GMEM_MOVEABLE, text.length() * sizeof(WCHAR));
	LPWSTR lpstrcpy = (LPWSTR)GlobalLock(hgl);
	memcpy(lpstrcpy, text.c_str(), text.length() * sizeof(WCHAR));

	GlobalUnlock(hgl);
	SetClipboardData(CF_TEXT, lpstrcpy);
	//关闭剪贴板     
	CloseClipboard();
	//CCLOG("[waring]SystemUtils::copyToClipboard no supper this platform");
#endif
    
}

void SystemUtils::setOpCallback(const SystemUtilsCocoCallback::SystemUtilsCallback& callback)
{
    
}

