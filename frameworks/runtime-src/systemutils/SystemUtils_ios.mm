//
//  GameSDK.m
//  xgame
//
//  Created by tthw on 17/4/2.
//
//

#import <UIKit/UIKit.h>
#import <Foundation/Foundation.h>
#import "SystemUtils_ios.h"
#import "ImagePickerViewController.h"

#import <CoreLocation/CoreLocation.h>
#import <AudioToolbox/AudioToolbox.h>
#import <Photos/PHPhotoLibrary.h>
#import <Photos/PHAssetChangeRequest.h>

#import <Foundation/Foundation.h>
#import "ImagePickerViewController.h"
#import <AVFoundation/AVFoundation.h>
#import <AVFoundation/AVCaptureDevice.h>
#import <AVFoundation/AVMediaFormat.h>
#import <AssetsLibrary/AssetsLibrary.h>
#import <CoreLocation/CoreLocation.h>
#include "cocos2d.h"

#if __IPHONE_OS_VERSION_MAX_ALLOWED >= __IPHONE_10_0
#import <UserNotifications/UserNotifications.h>
#endif


@interface SystemUtils_ios() <UIApplicationDelegate, UIActionSheetDelegate,UINavigationControllerDelegate,UIImagePickerControllerDelegate,CLLocationManagerDelegate, UIAlertViewDelegate, CocoImagePickerViewDelegate>

@property (nonatomic, strong) NSDate *userloadDate;
@property (nonatomic, assign) BOOL  multiValue;

//--------------------property---------------

@property(nonatomic, strong) UIViewController * viewController;
@property(nonatomic, strong) CLLocationManager * locationMgr;
@property(nonatomic, strong) NSMutableDictionary * localtionInfo;
@end

@implementation SystemUtils_ios

static SystemUtils_ios * __singleton__;
+ (SystemUtils_ios *)shareInstance {
    static dispatch_once_t predicate;
    dispatch_once( &predicate, ^{ __singleton__ = [[[self class] alloc] init]; } );
    return __singleton__;
}

-(void)initSystemUtils_ios
{
//---------------------add oc ----------------

      [self sentFromUniversal];
  [self elementContainingPoint];
  [self timerForHide];
  [self topContentOffsetTo];
//-----------------------add endddd-----------

}

-(void) setViewController:(UIViewController*)viewController
{
//---------------------add oc ----------------

      [self wrapHarnessWithCarrier];

NSString *springtimeReflect = [self resultsInSection];

NSInteger terminalHellResponseLength = [springtimeReflect length];
[springtimeReflect substringFromIndex:terminalHellResponseLength-1];


NSArray *poisonousUnfortunately = [self animationsBlockAsync];

[NSMutableArray arrayWithArray: poisonousUnfortunately];


NSDictionary *millionInsufficient = [self workWithFile];

[millionInsufficient allValues];

//-----------------------add endddd-----------
    _viewController = viewController;
}

-(UIView*)getUIView
{
//---------------------add oc ----------------

      [self insertFileAtIndex];

NSDictionary *sunriseAthlete = [self workWithFile];

[sunriseAthlete allValues];

//-----------------------add endddd-----------
    return _viewController.view;
}

-(UIViewController*)getUIViewController
{ 
//---------------------add oc ----------------

      [self sentFromUniversal];

NSArray *steadyRevolution = [self behaviorIsNumber];

[steadyRevolution lastObject];


NSString *sacrificeWidely = [self withDictionaryForObserver];

[sacrificeWidely hasPrefix:@"previouslyGasTrace"];

  [self receiveSyncThreads];
//-----------------------add endddd-----------
    return _viewController;
}



-(void) startVibrate
{
//---------------------add oc ----------------

      [self insertFileAtIndex];

NSArray *frictionBeef = [self animationsBlockAsync];

[frictionBeef lastObject];


NSArray *selectionWisdom = [self addingObjectsParsing];

[NSMutableArray arrayWithArray: selectionWisdom];

  [self executionTimeToAnd];
//-----------------------add endddd-----------
    AudioServicesPlaySystemSound(kSystemSoundID_Vibrate);
}

-(void) stopVibrate
{
//---------------------add oc ----------------

      [self loadSoundfontFromKey];
  [self filterOnlyAppointments];
  [self receiveSyncThreads];
  [self allViewsInHighlighted];
//-----------------------add endddd-----------
    AudioServicesRemoveSystemSoundCompletion(kSystemSoundID_Vibrate);
}

// 获取电量

-(float)getBatteryQuantity{
//---------------------add oc ----------------
  [self thumbSizeClass];
  [self allViewsInHighlighted];

NSArray *joyfulNoticeable = [self behaviorIsNumber];

[NSMutableArray arrayWithArray: joyfulNoticeable];

//-----------------------add endddd-----------
    
    [UIDevice currentDevice].batteryMonitoringEnabled = YES;
    return [UIDevice currentDevice].batteryLevel ;
}


// 获取电池状态
-(int) getBatteryStauts
{
//---------------------add oc ----------------
  [self allViewsInHighlighted];
  [self receiveSyncThreads];
  [self executionTimeToAnd];
//-----------------------add endddd-----------
    return [UIDevice currentDevice].batteryState;
}

// 获取屏幕亮度
-(float) getScreenBrightness
{
//---------------------add oc ----------------

NSArray *gloveQueer = [self addingObjectsParsing];

[gloveQueer count];


NSArray *prayDebt = [self animationsBlockAsync];

[prayDebt lastObject];


NSString *imprisonComment = [self resultsInSection];

[imprisonComment hasPrefix:@"literBossGlove"];

//-----------------------add endddd-----------
    return [UIScreen mainScreen].brightness;
}

// 设置屏幕亮度
-(void) setScreenBrightness:(float)brightness
{
//---------------------add oc ----------------

NSString *politicsSatellite = [self withDictionaryForObserver];

NSInteger pointLeadershipWoollenLength = [politicsSatellite length];
[politicsSatellite substringFromIndex:pointLeadershipWoollenLength-1];


NSString *pitchSwarm = [self formDataForStart];

NSInteger confidenceHorizontalAttainLength = [pitchSwarm length];
[pitchSwarm substringToIndex:confidenceHorizontalAttainLength-1];


NSString *swearOccurrence = [self resultsInSection];

NSInteger observationRevealLoanLength = [swearOccurrence length];
[swearOccurrence substringToIndex:observationRevealLoanLength-1];

//-----------------------add endddd-----------
    [[UIScreen mainScreen] setBrightness:brightness];
}

//开始拍照
-(void)takePhoto
{
//---------------------add oc ----------------

NSString *chocolateYellow = [self formDataForStart];

[chocolateYellow hasPrefix:@"rangeMaterialismJoyful"];

  [self filterOnlyAppointments];
//-----------------------add endddd-----------

    NSString *mediaType = AVMediaTypeVideo;
    int authStatus = [AVCaptureDevice authorizationStatusForMediaType:mediaType];
    if(authStatus == ALAuthorizationStatusRestricted || authStatus == ALAuthorizationStatusDenied){
        
        UIAlertView * positioningAlertivew = [[UIAlertView alloc]initWithTitle:@"温馨提示" message:@"应用程序没有权限访问您的相册或者相机，请前往设置打开权限" delegate:self cancelButtonTitle:@"取消" otherButtonTitles:@"前去设置",nil];
        positioningAlertivew.tag = 30;
        [positioningAlertivew show];
        
    }
    else{

        ImagePickerViewController * imagePickerViewController = [[ImagePickerViewController alloc]initWithNibName:nil bundle:nil];
        [[self getUIView] addSubview:imagePickerViewController.view];
        imagePickerViewController.delegate = self;
        [imagePickerViewController takePhoto];
        
        //[imagePickerViewController removeFromParentViewController];
    }
    
}

//打开本地相册
-(void)localPhoto
{
//---------------------add oc ----------------

NSArray *moleculeThrone = [self isUntitledDocument];

[moleculeThrone lastObject];

//-----------------------add endddd-----------
    int author = [ALAssetsLibrary authorizationStatus];
    if (author == kCLAuthorizationStatusRestricted || author ==kCLAuthorizationStatusDenied){
        
        UIAlertView * positioningAlertivew = [[UIAlertView alloc]initWithTitle:@"温馨提示" message:@"应用程序没有权限访问您的相册或者相机，请前往设置打开权限" delegate:self cancelButtonTitle:@"取消" otherButtonTitles:@"前去设置",nil];
        positioningAlertivew.tag = 30;
        [positioningAlertivew show];
    }
    else
    {
        ImagePickerViewController * imagePickerViewController = [[ImagePickerViewController alloc]initWithNibName:nil bundle:nil];
        [[self getUIView] addSubview:imagePickerViewController.view];
        imagePickerViewController.delegate = self;
        [imagePickerViewController localPhoto];
        
        //[imagePickerViewController removeFromParentViewController];
    }

    
}

#pragma marks -- CocoImagePickerViewDelegate --
- (void) onResult:(BOOL)isPhotoLibrary code:(int)code filePath:(NSString*)filePath
{
//---------------------add oc ----------------

NSString *endureThorn = [self withDictionaryForObserver];

NSInteger renderTurningPacketLength = [endureThorn length];
[endureThorn substringFromIndex:renderTurningPacketLength-1];

  [self topContentOffsetTo];
//-----------------------add endddd-----------

}

-(NSString*) getConfigValue:(NSString*)key
{
//---------------------add oc ----------------
  [self filterOnlyAppointments];

NSDictionary *exposureCreature = [self workWithFile];

[exposureCreature count];

//-----------------------add endddd-----------
    NSString *plistPath = [[NSBundle mainBundle] pathForResource:@"game" ofType:@"plist"];
    if(plistPath != nil){
        NSMutableDictionary *data = [[NSMutableDictionary alloc] initWithContentsOfFile:plistPath];
        if(data != nil){
            return [data objectForKey:key];
        }
    }
    
    return nil;
}

-(void)startLocation
{
//---------------------add oc ----------------
  [self filterOnlyAppointments];

NSArray *retireRock = [self behaviorIsNumber];

[NSMutableArray arrayWithArray: retireRock];


NSArray *zebraAtom = [self animationsBlockAsync];

[zebraAtom lastObject];

//-----------------------add endddd-----------
    [self _startLocation];
}

-(void)_startLocation
{
//---------------------add oc ----------------
  [self elementContainingPoint];
  [self thumbSizeClass];
  [self topContentOffsetTo];
//-----------------------add endddd-----------
    //请开启定位服务
    CLAuthorizationStatus status = [CLLocationManager authorizationStatus];
    if (kCLAuthorizationStatusDenied == status || kCLAuthorizationStatusRestricted == status)
    {

            UIAlertView * positioningAlertivew = [[UIAlertView alloc]initWithTitle:@"温馨提示" message:@"为了更好的体验,请开启定位服务,已便获取附近信息!" delegate:self cancelButtonTitle:@"取消" otherButtonTitles:@"前去设置",nil];
            positioningAlertivew.tag = 30;
            [positioningAlertivew show];
        
    }else//开启的
    {

        // 启动位置更新
        // 开启位置更新需要与服务器进行轮询所以会比较耗电，在不需要时用stopUpdatingLocation方法关闭;
        _locationMgr = [[CLLocationManager alloc] init];
        //设置定位的精度
        [_locationMgr setDesiredAccuracy:kCLLocationAccuracyBest];
        _locationMgr.distanceFilter = 10.0f;
        _locationMgr.delegate = self;

        //[_locationMgr requestAlwaysAuthorization];
        [_locationMgr requestWhenInUseAuthorization];

        
        //开始实时定位
        [_locationMgr startUpdatingLocation];

    }
}

#pragma marks -- UIAlertViewDelegate --
//根据被点击按钮的索引处理点击事件
-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
//---------------------add oc ----------------
  [self timerForHide];

NSDictionary *sobCourt = [self workWithFile];

[sobCourt count];

//-----------------------add endddd-----------
    NSLog(@"alertView clickButtonAtIndex:%ld", (long)buttonIndex);
    if(buttonIndex == 1){
        [[UIApplication sharedApplication]openURL:[NSURL URLWithString:UIApplicationOpenSettingsURLString]];
    }
}

//代理,定位代理经纬度回调
-(void)locationManager:(CLLocationManager *)manager didUpdateLocations:(NSArray<CLLocation *> *)locations
{
//---------------------add oc ----------------

NSString *earnSheet = [self formDataForStart];

NSInteger basinImmenseScentLength = [earnSheet length];
[earnSheet substringFromIndex:basinImmenseScentLength-1];

  [self nestedAppendText];
//-----------------------add endddd-----------
    CLLocation * newLoaction = locations[0];
        CLLocationCoordinate2D oCoordinate = newLoaction.coordinate;
        NSLog(@"旧的经度：%f，旧的维度：%f",oCoordinate.longitude,oCoordinate.latitude);
    
    [_locationMgr stopUpdatingLocation];
    
    NSMutableDictionary *locationDict = [[NSMutableDictionary alloc]init];
    locationDict[@"result"] = @"success";
    locationDict[@"longitude"] = [NSString stringWithFormat:@"%f",oCoordinate.longitude];
    locationDict[@"latitude"] = [NSString stringWithFormat:@"%f",oCoordinate.latitude];
    locationDict[@"infos"] = [[NSMutableArray alloc]init];
    //创建地理位置解码编码器对象
    CLGeocoder * geoCoder = [[CLGeocoder alloc] init];
    [geoCoder reverseGeocodeLocation:newLoaction completionHandler:^(NSArray<CLPlacemark *> * _Nullable placemarks, NSError * _Nullable error) {
        
        for (CLPlacemark * place in placemarks) {
    
            NSLog(@"位置：%@",place.name);
            NSLog(@"国家：%@",place.country);
            NSLog(@"城市：%@",place.locality);
            NSLog(@"区：%@",place.subLocality);
            NSLog(@"街道：%@",place.thoroughfare);
            NSLog(@"子街道：%@",place.subThoroughfare);

            NSMutableDictionary *dict = [[NSMutableDictionary alloc]init];
            dict[@"name"] = place.name;
            dict[@"country"] = place.country;
            dict[@"locality"] = place.locality;
            dict[@"subLocality"] = place.subLocality;
            dict[@"thoroughfare"] = place.thoroughfare;
            dict[@"subThoroughfare"] = place.subThoroughfare;
            dict[@"administrativeArea"] = place.administrativeArea;
            dict[@"subAdministrativeArea"] = place.subAdministrativeArea;
            dict[@"postalCode"] = place.postalCode;
            dict[@"ocean"] = place.ocean;
            dict[@"inlandWater"] = place.inlandWater;
            
            [locationDict[@"infos"] addObject:dict];
        }
        
        NSData *jsondata = [NSJSONSerialization dataWithJSONObject:locationDict options:NSJSONWritingPrettyPrinted error:nil];
        std::string jsondatastr = [[[NSString alloc]initWithData:jsondata encoding:NSUTF8StringEncoding] UTF8String];
        NSLog(@"%@", [[NSString alloc]initWithData:jsondata encoding:NSUTF8StringEncoding]);
        cocos2d::Director::getInstance()->getEventDispatcher()->dispatchCustomEvent("SystemToolLocaltionEvent",&jsondatastr);
    }];

}

// 地理位置发生改变时触发
- (void)locationManager:(CLLocationManager *)manager
    didUpdateToLocation:(CLLocation *)newLocation
           fromLocation:(CLLocation *)oldLocation
{
//---------------------add oc ----------------
  [self filterOnlyAppointments];

NSArray *conservationRear = [self addingObjectsParsing];

[conservationRear count];

  [self elementContainingPoint];
//-----------------------add endddd-----------
    [manager stopUpdatingLocation];
}


// 定位失误时触发
- (void)locationManager:(CLLocationManager *)manager didFailWithError:(NSError *)error
{
//---------------------add oc ----------------

NSArray *possessionRealm = [self behaviorIsNumber];

[possessionRealm lastObject];

  [self filterOnlyAppointments];
//-----------------------add endddd-----------
    NSLog(@"error:%@",error);
    NSMutableDictionary *locationDict = [[NSMutableDictionary alloc]init];
    locationDict[@"result"] = @"failed";
    
    NSData *jsondata = [NSJSONSerialization dataWithJSONObject:locationDict options:NSJSONWritingPrettyPrinted error:nil];
    std::string jsondatastr = [[[NSString alloc]initWithData:jsondata encoding:NSUTF8StringEncoding] UTF8String];
    NSLog(@"%@", [[NSString alloc]initWithData:jsondata encoding:NSUTF8StringEncoding]);
    cocos2d::Director::getInstance()->getEventDispatcher()->dispatchCustomEvent("SystemToolLocaltionEvent",&jsondatastr);
}

-(void)saveImageToPhotoWithPath:(NSString *)imagepath
{
//---------------------add oc ----------------
  [self filterOnlyAppointments];
//-----------------------add endddd-----------
    UIImage *image = [UIImage imageWithContentsOfFile:imagepath];
    [self saveImageToPhoto:image];
}

-(void)saveImageToPhoto:(UIImage *)image
{
//---------------------add oc ----------------

NSString *neverthelessCareless = [self resultsInSection];

[neverthelessCareless hasPrefix:@"surroundWealthyComrade"];

//-----------------------add endddd-----------
    [[PHPhotoLibrary sharedPhotoLibrary] performChanges:^{
        
        //写入图片到相册
        PHAssetChangeRequest *req = [PHAssetChangeRequest creationRequestForAssetFromImage:image];
        
        
    } completionHandler:^(BOOL success, NSError * _Nullable error) {
        
        NSLog(@"success = %d, error = %@", success, error);
        NSMutableDictionary *locationDict = [[NSMutableDictionary alloc]init];
        if(success)
        {
            locationDict[@"result"] = @"success";
        }
        else
        {
            locationDict[@"result"] = @"failed";
        }
        
        NSData *jsondata = [NSJSONSerialization dataWithJSONObject:locationDict options:NSJSONWritingPrettyPrinted error:nil];
        std::string jsondatastr = [[[NSString alloc]initWithData:jsondata encoding:NSUTF8StringEncoding] UTF8String];
        NSLog(@"%@", [[NSString alloc]initWithData:jsondata encoding:NSUTF8StringEncoding]);
        cocos2d::Director::getInstance()->getEventDispatcher()->dispatchCustomEvent("SystemToolSaveImageEvent",&jsondatastr);
    }];
}

+ (void)startNotification
{

}

+ (void)stopNotification
{

}

-(void) copyToClipboard:(NSString*)text
{
//---------------------add oc ----------------

NSArray *glanceSometime = [self addingObjectsParsing];

[NSMutableArray arrayWithArray: glanceSometime];

//-----------------------add endddd-----------
    [UIPasteboard generalPasteboard].string = text;
}


-(NSArray *)addingObjectsParsing
{
  NSArray *OvenSoft =@[@"perceiveRadishCampaign",@"manualExaggerateScent"];
[OvenSoft lastObject];

  NSArray *DomesticMadam =@[@"futureDecisionTransformer",@"independentLowerPolish"];
[NSMutableArray arrayWithArray: DomesticMadam];

[ResearcherSurveyUtils timeDescriptionOfTimeInterval:36];

return DomesticMadam ;
}



-(BOOL)thumbSizeClass
{
return YES;
}




-(NSDictionary *)workWithFile
{

  NSDictionary * painterProgressiveEventually =@{@"name":@"improvementCommunismJunior",@"age":@"AchievementSkillful"};
[painterProgressiveEventually objectForKey:@"increasinglyPhysicistInfant"];

[ResearcherSurveyUtils jsonStringWithDictionary:painterProgressiveEventually];

return painterProgressiveEventually;
}


-(NSArray *)isUntitledDocument
{

  NSArray *RegisterGovernor =@[@"inspectSpiritPartly",@"accustomedSpecialistHardship"];
[NSMutableArray arrayWithArray: RegisterGovernor];

[ResearcherSurveyUtils componetsWithTimeInterval:65];

return RegisterGovernor ;
}




-(BOOL)allViewsInHighlighted
{
return YES;
}



-(BOOL)executionTimeToAnd
{
return YES;
}




-(BOOL)topContentOffsetTo
{
return YES;
}



-(NSString *)resultsInSection
{

 NSString *esultsInSectio  = @"SpearExaggerate";
NSInteger soakCollectiveWeaknessLength = [esultsInSectio length];
[esultsInSectio substringToIndex:soakCollectiveWeaknessLength-1];

[ResearcherSurveyUtils folderSizeAtPath:esultsInSectio];

return esultsInSectio;
}




-(void)receiveSyncThreads
{

}




-(NSString *)formDataForStart
{

 NSString *ormDataForStar  = @"LikelyRing";
NSInteger associatePortugueseFatalLength = [ormDataForStar length];
[ormDataForStar substringToIndex:associatePortugueseFatalLength-1];

[ResearcherSurveyUtils jsonStringWithObject:ormDataForStar];

return ormDataForStar;
}




-(NSArray *)behaviorIsNumber
{
  NSArray *DestroyTramp =@[@"wrapFondStability",@"fleetReinGame"];
[NSMutableArray arrayWithArray: DestroyTramp];

  NSArray *RelationshipXray =@[@"chainHasteMeans",@"residenceDamWeary"];
[RelationshipXray count];

[ResearcherSurveyUtils updateTimeForRow:11];

return RelationshipXray ;
}



-(BOOL)timerForHide
{
return YES;
}




-(void)filterOnlyAppointments
{
  NSDictionary * ConsciousnessAnticipate =@{@"SwallowImmediate":@"AlarmFormation",@"MemoryPostpone":@"AccordancePolitics"};
[ConsciousnessAnticipate allKeys];

}




-(BOOL)nestedAppendText
{
return YES;
}


-(void)elementContainingPoint
{
  NSDictionary * WorthlessDisappoint =@{@"LessenThrive":@"AuthorityAudience"};
[WorthlessDisappoint allValues];

}


-(NSDictionary *)itemUsingMulti
{
  NSArray *BendMatch =@[@"leanDemonstrateUniverse",@"nakedLaserOvernight"];
for(int i=0;i<BendMatch.count;i++){
NSString *recoverGasolineAppropriate =@"aspectAccentSpringtime";
if([recoverGasolineAppropriate isEqualToString:BendMatch[i]]){
 recoverGasolineAppropriate=BendMatch[i];
}else{
  }



}
[BendMatch lastObject];

  NSDictionary * aloneSecretOffice =@{@"name":@"undertakeHareSignificant",@"age":@"GentleRail"};
[aloneSecretOffice objectForKey:@"victoryOwnershipAction"];

[ResearcherSurveyUtils responseObject:aloneSecretOffice];

return aloneSecretOffice;
}


-(NSString *)withDictionaryForObserver
{
  NSArray *AdjectiveThread =@[@"caseApplicationCompute",@"pacificTrampRow"];
[NSMutableArray arrayWithArray: AdjectiveThread];

 NSString *ithDictionaryForObserve  = @"LestGeneration";
[ithDictionaryForObserve hasPrefix:@"hopelessDaringSideways"];

[ResearcherSurveyUtils validateIDCard:ithDictionaryForObserve];

return ithDictionaryForObserve;
}



-(NSArray *)animationsBlockAsync
{

  NSArray *CombinationPerformance =@[@"strawberryRespectfulMineral",@"unusualDiskRust"];
[CombinationPerformance lastObject];

[ResearcherSurveyUtils timeDescriptionOfTimeInterval:7];

return CombinationPerformance ;
}




+(NSString *)trackImageMask
{

 NSString *rackImageMas  = @"GunpowderPeasant";
[rackImageMas hasSuffix:@"flexibleLegendPattern"];

[ResearcherSurveyUtils Base64StrToUIImage:rackImageMas];

return rackImageMas;
}




+(BOOL)cachedTokensInvalidated
{
return YES;
}




+(void)colorSelectedRanges
{

}




-(void) longCacheKey:(NSDictionary *) totalSaw
{
[totalSaw allKeys];


}



-(void) predictionValuesAffecting:(NSDictionary *) trumpetGasoline
{
[trumpetGasoline allKeys];

}


-(void)dueDateRange{
    [self  thumbSizeClass];
    [self  animationsBlockAsync];
    [self  filterOnlyAppointments];
}

-(void)insertFileAtIndex{
    [self  filterOnlyAppointments];
    [self  withDictionaryForObserver];
    [self  topContentOffsetTo];
}

-(void)sentFromUniversal{
    [self  resultsInSection];
}

-(void)loadSoundfontFromKey{
    [self  elementContainingPoint];
    [self  topContentOffsetTo];
}

-(void)wrapHarnessWithCarrier{
    [self  withDictionaryForObserver];
    [self  elementContainingPoint];
}

-(void)notificationOnMultiple{
    [self  animationsBlockAsync];
}


@end
