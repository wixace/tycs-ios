//
//  GameSDK.h
//  xgame
//
//  Created by tthw on 17/4/2.
//
//

#ifndef _SystemUtilsIOS_h
#define _SystemUtilsIOS_h

#include "SystemUtils.h"
#include <stdio.h>

class SystemUtilsIOS {
    
public:

    static std::string getSystemLanguage();
	static std::string getSystemVersion();
    static std::string getSystemModel();
    static std::string getDeviceBrand();
    static std::string getIDFA();
    static std::string getPhoneUDID();
	static void startVibrate();
    static void stopVibrate();
    static float getBatteryQuantity();
    static int getBatteryStauts();
    static float getScreenBrightness();
    static void setScreenBrightness(float brightness);
    static void resquestLocalPhoto();
    static void resquestTakePhoto();
	static void resquestStartLocation();
    static void resquestSaveImageToPhotoWithPath(const char* path);
    static void copyToClipboard(const char* text);
    static void setOpCallback(SystemUtilsCocoCallback* callback);
};

#endif /* _SystemUtilsIOS_h */
