//
//  ImageViewController.h
//  Puzzles
//
//  Created by xujw on 15/12/9.
//
//

#ifndef ImagePickerViewController_h
#define ImagePickerViewController_h

#import <UIKit/UIKit.h>


@protocol CocoImagePickerViewDelegate <NSObject>

@optional
- (void) onResult:(BOOL)isPhotoLibrary code:(int)code filePath:(NSString*)filePath;
@end


@interface ImagePickerViewController:UIViewController<UINavigationControllerDelegate,UIImagePickerControllerDelegate, UIApplicationDelegate>
{
    NSString *filePath;
    BOOL isPhotoLibrary;
}

@property (nonatomic,weak)id<CocoImagePickerViewDelegate>delegate;

//-----------------property-----------

//打开本地相册
-(void)localPhoto;

//打开相机
-(void)takePhoto;

@end

#endif /* ImageViewController_h */
