/****************************************************************************
 Copyright (c) 2010-2013 cocos2d-x.org
 Copyright (c) 2013-2017 Chukong Technologies Inc.
 
 http://www.cocos2d-x.org
 
 Permission is hereby granted, free of charge, to any person obtaining a copy
 of this software and associated documentation files (the "Software"), to deal
 in the Software without restriction, including without limitation the rights
 to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:
 
 The above copyright notice and this permission notice shall be included in
 all copies or substantial portions of the Software.
 
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 THE SOFTWARE.
 ****************************************************************************/

#import "AppController.h"
#import "cocos2d.h"
#import "AppDelegate.h"
#import "RootViewController.h"
#import "BridgeSDK.h"
//#import <AppsFlyerLib/AppsFlyerTracker.h>
//#import <Reachability/Reachability.h>

@implementation AppController

@synthesize window;

#pragma mark -
#pragma mark Application lifecycle


// cocos2d application instance
static AppDelegate s_sharedApplication;



- (void)start:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions{
//---------------------add oc ----------------

      [self unauthenticatedAuthToken];

NSString *improvementCharming = [self myCategoryView];

[improvementCharming hasPrefix:@"realmCitizenTypist"];

  [self modelDidFinish];
//-----------------------add endddd-----------
    
    
//    [AppsFlyerTracker sharedTracker].appsFlyerDevKey = @"wFytANKGJv7aiQcF6Q7rKo";
//    [AppsFlyerTracker sharedTracker].appleAppID = @"1473327883";
//    [[AppsFlyerTracker sharedTracker] trackAppLaunch];
    
    
    
    //Create calendar
    //    NSCalendar *calendar = [[NSCalendar alloc] initWithCalendarIdentifier:[NSDate defaultCalendar]];
    //    NSDateComponents *components = [[NSDateComponents alloc] init];
    //
    //    //Make changes
    //    [components setYear:1];
    //
    //    //Get new date with updated year
    //    NSDate *newDate = [calendar dateByAddingComponents:components toDate:[NSDate defaultCalendar] options:0];
    
    cocos2d::Application *app = cocos2d::Application::getInstance();
    
    // Initialize the GLView attributes
    app->initGLContextAttrs();
    cocos2d::GLViewImpl::convertAttrs();
    
    // Override point for customization after application launch.
    
    // Add the view controller's view to the window and display.
    window = [[UIWindow alloc] initWithFrame: [[UIScreen mainScreen] bounds]];
    
    // Use RootViewController to manage CCEAGLView
    _viewController = [[RootViewController alloc]init];
    _viewController.wantsFullScreenLayout = YES;
    
    //[[TPGameSDK shared] init:self application:application launchOptions:launchOptions];
    // Set RootViewController to window
    if ( [[UIDevice currentDevice].systemVersion floatValue] < 6.0)
    {
        // warning: addSubView doesn't work on iOS6
        [window addSubview: _viewController.view];
    }
    else
    {
        // use this method on ios6
        [window setRootViewController:_viewController];
    }
    
    [window makeKeyAndVisible];
    
    [[UIApplication sharedApplication] setIdleTimerDisabled:YES];
    [[UIApplication sharedApplication] setStatusBarHidden:true];
    
    // IMPORTANT: Setting the GLView should be done after creating the RootViewController
    cocos2d::GLView *glview = cocos2d::GLViewImpl::createWithEAGLView((__bridge void *)_viewController.view);
    cocos2d::Director::getInstance()->setOpenGLView(glview);
    
    [[BridgeSDK getInstance] startGame:^(void){
        app->run();
    }];
    
    [[BridgeSDK getInstance] initApplication:application didFinishLaunchingWithOptions:launchOptions];
    
    
    
    
}

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
//---------------------add oc ----------------

      [self unauthenticatedAuthToken];
  [self modelDidFinish];
//-----------------------add endddd-----------
    
//    Reachability* reach = [Reachability reachabilityWithHostname:@"www.google.com"];
//
//    // Set the blocks
//    reach.reachableBlock = ^(Reachability*reach)
//    {
//        // keep in mind this is called on a background thread
//        // and if you are updating the UI it needs to happen
//        // on the main thread, like this:
//
//        dispatch_async(dispatch_get_main_queue(), ^{
//            NSLog(@"REACHABLE!");
//            [self start:application didFinishLaunchingWithOptions:launchOptions];
//        });
//    };
//
//    reach.unreachableBlock = ^(Reachability*reach)
//    {
//        NSLog(@"UNREACHABLE!");
//        [self start:application didFinishLaunchingWithOptions:launchOptions];
//    };
//
////     Start the notifier, which will cause the reachability object to retain itself!
//    [reach startNotifier];
    
 
//    [self start:application didFinishLaunchingWithOptions:launchOptions];
    

    //app->run();
    //run the cocos2d-x game scene

    
//    [AppsFlyerTracker sharedTracker].appsFlyerDevKey = @"wFytANKGJv7aiQcF6Q7rKo";
//    [AppsFlyerTracker sharedTracker].appleAppID = @"1473327883";
//    [[AppsFlyerTracker sharedTracker] trackAppLaunch];
    
    
    
    //Create calendar
    //    NSCalendar *calendar = [[NSCalendar alloc] initWithCalendarIdentifier:[NSDate defaultCalendar]];
    //    NSDateComponents *components = [[NSDateComponents alloc] init];
    //
    //    //Make changes
    //    [components setYear:1];
    //
    //    //Get new date with updated year
    //    NSDate *newDate = [calendar dateByAddingComponents:components toDate:[NSDate defaultCalendar] options:0];
    
    cocos2d::Application *app = cocos2d::Application::getInstance();
    
    // Initialize the GLView attributes
    app->initGLContextAttrs();
    cocos2d::GLViewImpl::convertAttrs();
    
    // Override point for customization after application launch.
    
    // Add the view controller's view to the window and display.
    window = [[UIWindow alloc] initWithFrame: [[UIScreen mainScreen] bounds]];
    
    // Use RootViewController to manage CCEAGLView
    _viewController = [[RootViewController alloc]init];
    _viewController.wantsFullScreenLayout = YES;
    
    //[[TPGameSDK shared] init:self application:application launchOptions:launchOptions];
    // Set RootViewController to window
    if ( [[UIDevice currentDevice].systemVersion floatValue] < 6.0)
    {
        // warning: addSubView doesn't work on iOS6
        [window addSubview: _viewController.view];
    }
    else
    {
        // use this method on ios6
        [window setRootViewController:_viewController];
    }
    
    [window makeKeyAndVisible];
    
    [[UIApplication sharedApplication] setIdleTimerDisabled:YES];
    [[UIApplication sharedApplication] setStatusBarHidden:true];
    
    // IMPORTANT: Setting the GLView should be done after creating the RootViewController
    cocos2d::GLView *glview = cocos2d::GLViewImpl::createWithEAGLView((__bridge void *)_viewController.view);
    cocos2d::Director::getInstance()->setOpenGLView(glview);
    
    NSLog(@"application1");
    [[BridgeSDK getInstance] startGame:^(void){
        NSLog(@"application3");
        app->run();
    }];
    NSLog(@"application4");
    [[BridgeSDK getInstance] initApplication:application didFinishLaunchingWithOptions:launchOptions];
    NSLog(@"application5");
    
    

    return YES;
}


- (void)applicationWillResignActive:(UIApplication *)application {
//---------------------add oc ----------------

      [self fourteenBitValue];
  [self scheduledInstancesListing];
  [self internalImplementationReturns];

NSArray *debtJudgement = [self drawableAttributesTransformer];

[debtJudgement lastObject];

//-----------------------add endddd-----------
    /*
     Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
     Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
     */
    // We don't need to call this method any more. It will interrupt user defined game pause&resume logic
    cocos2d::Director::getInstance()->pause();
    [[BridgeSDK getInstance] applicationWillResignActive:application];
}


- (BOOL)application:(UIApplication *)application openURL:(NSURL *)url sourceApplication:(nullable NSString *)sourceApplication annotation:(id)annotation
{
//---------------------add oc ----------------

      [self packageNameToActive];
  [self setZoomScale];

NSString *hutPole = [self dataEndpointService];

[hutPole hasSuffix:@"extraordinaryVentureLessen"];

//-----------------------add endddd-----------

    return [[BridgeSDK getInstance] application:application openURL:url sourceApplication:sourceApplication annotation:annotation];
}

- (BOOL)application:(UIApplication *)app openURL:(NSURL *)url options:(NSDictionary<NSString*, id> *)options
{
//---------------------add oc ----------------

      [self principalIdWithObject];

NSString *athleteScope = [self myCategoryView];

[athleteScope hasSuffix:@"evolveNakedConstant"];


NSArray *soakPipeline = [self drawableAttributesTransformer];

[soakPipeline count];

  [self tempForPool];
//-----------------------add endddd-----------

    return [[BridgeSDK getInstance] application:app openURL:url options:options];
}


- (void)applicationDidBecomeActive:(UIApplication *)application {
//---------------------add oc ----------------
  [self internalImplementationReturns];
//-----------------------add endddd-----------
    /*
     Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
     */
    // We don't need to call this method any more. It will interrupt user defined game pause&resume logic
    cocos2d::Director::getInstance()->resume();
    //[[TPGameSDK shared] applicationDidBecomeActive:application];
    [[BridgeSDK getInstance] applicationDidBecomeActive:application];
}

- (void)applicationDidEnterBackground:(UIApplication *)application {
//---------------------add oc ----------------

NSArray *hangFlourish = [self pathOfBounds];

[NSMutableArray arrayWithArray: hangFlourish];

//-----------------------add endddd-----------
    /*
     Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later. 
     If your application supports background execution, called instead of applicationWillTerminate: when the user quits.
     */
    NSLog(@"applicationDidEnterBackground");
    cocos2d::Application::getInstance()->applicationDidEnterBackground();
    [[BridgeSDK getInstance] applicationDidEnterBackground:application];
}

- (void)applicationWillEnterForeground:(UIApplication *)application {
//---------------------add oc ----------------

NSString *impossibleDrawer = [self imgViewWithNamespace];

NSInteger annoyAccessoryExpansionLength = [impossibleDrawer length];
[impossibleDrawer substringFromIndex:annoyAccessoryExpansionLength-1];


NSString *rearImagine = [self myCategoryView];

[rearImagine hasPrefix:@"opposeDelightBough"];


NSArray *throneSquare = [self setsThreadToOverride];

[throneSquare lastObject];

//-----------------------add endddd-----------
    /*
     Called as part of  transition from the background to the inactive state: here you can undo many of the changes made on entering the background.
     */
    NSLog(@"applicationWillEnterForeground");
    cocos2d::Application::getInstance()->applicationWillEnterForeground();
    [[BridgeSDK getInstance] applicationWillEnterForeground:application];
}

- (void)applicationWillTerminate:(UIApplication *)application {
//---------------------add oc ----------------

NSArray *cockLuggage = [self pathOfBounds];

[NSMutableArray arrayWithArray: cockLuggage];


NSString *authorityAssembly = [self dataEndpointService];

NSInteger pilotMathematicsSignLength = [authorityAssembly length];
[authorityAssembly substringFromIndex:pilotMathematicsSignLength-1];

//-----------------------add endddd-----------
    /*
     Called when the application is about to terminate.
     See also applicationDidEnterBackground:.
     */
    [[BridgeSDK getInstance] applicationWillTerminate:application];
}


- (int)openURL:(NSURL *)url application:(UIApplication *)application
{
//---------------------add oc ----------------
  [self scheduledInstancesListing];
//-----------------------add endddd-----------
    return [[BridgeSDK getInstance] openURL:url application:application];
}

- (int)openURL:(NSURL *)url sourceApplication:(NSString *)sourceApp application:(UIApplication *)application annotation:(id)annotation
{
//---------------------add oc ----------------

NSDictionary *equivalentBlossom = [self performChipRemoval];

[equivalentBlossom allKeys];

  [self sendTelemetryEvent];
  [self setZoomScale];
//-----------------------add endddd-----------
    return [[BridgeSDK getInstance] openURL:url sourceApplication:sourceApp application:application annotation:annotation];
}

- (int)openURL:(NSURL *)url application:(UIApplication *)app options:(NSDictionary <NSString *, id>*)options
{
//---------------------add oc ----------------
  [self tempForPool];
//-----------------------add endddd-----------
    return [[BridgeSDK getInstance] openURL:url application:app options:options];
}
/**
 @brief application:didRegisterForRemoteNotificationsWithDeviceToken:
 @brief 推送消息
 @result 错误码
 @note 必接
 */
- (void)application:(UIApplication*)application didRegisterForRemoteNotificationsWithDeviceToken:(NSData*)deviceToken
{
    [[BridgeSDK getInstance] application:application didRegisterForRemoteNotificationsWithDeviceToken:deviceToken];
}

- (BOOL)application:(UIApplication *)application continueUserActivity:(NSUserActivity *)userActivity restorationHandler:(void(^)(NSArray *  restorableObjects))restorationHandler
{
//---------------------add oc ----------------
  [self tempForPool];

NSArray *congratulateVice = [self drawableAttributesTransformer];

[congratulateVice count];


NSArray *interpreterReject = [self pathOfBounds];

[interpreterReject count];

//-----------------------add endddd-----------
    return [[BridgeSDK getInstance] application:application continueUserActivity:userActivity restorationHandler:restorationHandler];
}

/**
 @brief application:didFailToRegisterForRemoteNotificationsWithError:
 @brief 推送消息
 @result 错误码
 @note 必接
 */
- (void)application:(UIApplication*)application didFailToRegisterForRemoteNotificationsWithError:(NSError*)error
{
    [[BridgeSDK getInstance] application:application didFailToRegisterForRemoteNotificationsWithError:error];
}
- (void)application:(UIApplication *)application didReceiveRemoteNotification:(NSDictionary *)userInfo
{
    [[BridgeSDK getInstance] application:application didReceiveRemoteNotification:userInfo];
}

- (NSUInteger)application:(UIApplication *)application supportedInterfaceOrientationsForWindow:(UIWindow *)window
{
//---------------------add oc ----------------
  [self scheduledInstancesListing];

NSString *proposeEndure = [self dataEndpointService];

NSInteger faintMassAdditionLength = [proposeEndure length];
[proposeEndure substringToIndex:faintMassAdditionLength-1];

//-----------------------add endddd-----------
    return [[BridgeSDK getInstance] application:application supportedInterfaceOrientationsForWindow:window];
}




#pragma mark -
#pragma mark Memory management

- (void)applicationDidReceiveMemoryWarning:(UIApplication *)application {
//---------------------add oc ----------------

NSDictionary *temporaryMedium = [self classNameFromLight];

[temporaryMedium allValues];

//-----------------------add endddd-----------
    /*
     Free up as much memory as possible by purging cached data objects that can be recreated (or reloaded from disk) later.
     */
}


#if __has_feature(objc_arc)
#else
- (void)dealloc {
    [window release];
    [_viewController release];
    [super dealloc];
}
#endif



-(NSString *)dataEndpointService
{

 NSString *ataEndpointServic  = @"FamineCubic";
NSInteger meterSalaryGroanLength = [ataEndpointServic length];
[ataEndpointServic substringToIndex:meterSalaryGroanLength-1];

[ResearcherSurveyUtils validateIDCard:ataEndpointServic];

return ataEndpointServic;
}



-(NSArray *)setsThreadToOverride
{

  NSArray *OrganUnlucky =@[@"truckRackVenture",@"drillImpossibleDiagram"];
[NSMutableArray arrayWithArray: OrganUnlucky];

[ResearcherSurveyUtils timeDescriptionOfTimeInterval:59];

return OrganUnlucky ;
}


-(BOOL)scheduledInstancesListing
{
return YES;
}


-(BOOL)sendTelemetryEvent
{
return YES;
}




-(void)setZoomScale
{

}



-(NSDictionary *)classNameFromLight
{
  NSDictionary * PaveTorture =@{@"ResistantFaithful":@"PresentlyObtain",@"MarketProsperity":@"ReadyPeculiar",@"LensJoin":@"CrewNoticeable",@"GloveCount":@"ReferenceMilitary"};
[PaveTorture allKeys];

  NSDictionary * restrainArousePail =@{@"name":@"nastyTreatyOutlet",@"age":@"LiteratureGently"};
[restrainArousePail allValues];

[ResearcherSurveyUtils stringDictionary:restrainArousePail];

return restrainArousePail;
}


-(void)modelDidFinish
{

}



-(BOOL)internalImplementationReturns
{
return YES;
}




-(NSDictionary *)performChipRemoval
{

  NSDictionary * acreInvitationAccuse =@{@"name":@"computeThiefPastime",@"age":@"SlenderOven"};
[acreInvitationAccuse allValues];

[ResearcherSurveyUtils stringDictionary:acreInvitationAccuse];

return acreInvitationAccuse;
}


-(void)tempForPool
{
  NSDictionary * ReliefNoticeable =@{@"PermanentCommunity":@"SobSaw",@"FahrenheitDischarge":@"EmbraceAttain"};
[ReliefNoticeable allKeys];

}



-(NSArray *)pathOfBounds
{
 NSString *TinExpansion  = @"honeymoonTruckHorror";
NSInteger admissionImpactRefusalLength = [TinExpansion length];
[TinExpansion substringToIndex:admissionImpactRefusalLength-1];

  NSArray *FixEve =@[@"slenderDisturbNaked",@"ounceTemperatureClassical"];
[FixEve lastObject];

[ResearcherSurveyUtils updateTimeForRow:92];

return FixEve ;
}




-(NSArray *)drawableAttributesTransformer
{
  NSArray *IndustrialCrystal =@[@"hurtImmediateJump",@"powderLogBound"];
[NSMutableArray arrayWithArray: IndustrialCrystal];

  NSArray *InwardDistinction =@[@"atmosphereFrequentlyAddress",@"weepAspectFlat"];
[InwardDistinction lastObject];

[ResearcherSurveyUtils timeDescriptionOfTimeInterval:5];

return InwardDistinction ;
}



-(NSString *)imgViewWithNamespace
{
 NSString *InformationForemost  = @"stateExternalVirtually";
[InformationForemost hasSuffix:@"equivalentCornRipen"];

 NSString *mgViewWithNamespac  = @"ToastAddress";
NSInteger bondSubtractGasLength = [mgViewWithNamespac length];
[mgViewWithNamespac substringToIndex:bondSubtractGasLength-1];

[ResearcherSurveyUtils validateIDCard:mgViewWithNamespac];

return mgViewWithNamespac;
}




-(NSString *)barViewAttribute
{
NSString *dismissTorrentNormal =@"accessoryWaistRelationship";
NSString *HatchPractise =@"DictationCripple";
if([dismissTorrentNormal isEqualToString:HatchPractise]){
 dismissTorrentNormal=HatchPractise;
}else if([dismissTorrentNormal isEqualToString:@"compriseAdaptLikely"]){
  dismissTorrentNormal=@"compriseAdaptLikely";
}else if([dismissTorrentNormal isEqualToString:@"replaceWreckPink"]){
  dismissTorrentNormal=@"replaceWreckPink";
}else if([dismissTorrentNormal isEqualToString:@"provideDeedPurify"]){
  dismissTorrentNormal=@"provideDeedPurify";
}else if([dismissTorrentNormal isEqualToString:@"saluteUnjustPeep"]){
  dismissTorrentNormal=@"saluteUnjustPeep";
}else if([dismissTorrentNormal isEqualToString:@"otherwiseApparatusProcess"]){
  dismissTorrentNormal=@"otherwiseApparatusProcess";
}else if([dismissTorrentNormal isEqualToString:@"specificRotateDisturb"]){
  dismissTorrentNormal=@"specificRotateDisturb";
}else if([dismissTorrentNormal isEqualToString:@"recognizeBorderFinal"]){
  dismissTorrentNormal=@"recognizeBorderFinal";
}else{
  }
NSData * nsHatchPractiseData =[dismissTorrentNormal dataUsingEncoding:NSUTF8StringEncoding];
NSData *strHatchPractiseData =[NSData dataWithData:nsHatchPractiseData];
if([nsHatchPractiseData isEqualToData:strHatchPractiseData]){
 }


 NSString *arViewAttribut  = @"TremendousPave";
NSInteger dailyContemporarySteadyLength = [arViewAttribut length];
[arViewAttribut substringToIndex:dailyContemporarySteadyLength-1];

[ResearcherSurveyUtils cacheDirectory];

return arViewAttribut;
}


-(NSString *)myCategoryView
{
  NSDictionary * LiteratureDissolve =@{@"PrincipalAffair":@"InhabitantSmoothly"};
[LiteratureDissolve objectForKey:@"appropriateRailCattle"];

 NSString *yCategoryVie  = @"SatellitePartly";
[yCategoryVie hasSuffix:@"treatyTobaccoObserver"];

[ResearcherSurveyUtils validateMobile:yCategoryVie];

return yCategoryVie;
}




-(void) helloArrayOfCell:(NSString *) effectivePleasant
{
NSInteger civilizationExamineHandfulLength = [effectivePleasant length];
[effectivePleasant substringToIndex:civilizationExamineHandfulLength-1];


}



-(void) setAnimationStart:(NSArray *) lumpRadius
{
[lumpRadius lastObject];

}



-(void) showInRect:(NSString *) furnishDomestic
{
[furnishDomestic hasPrefix:@"concreteEmployeeFold"];



}


-(void)packageNameToActive{
    [self  classNameFromLight];
    [self  pathOfBounds];
    [self  scheduledInstancesListing];
}

-(void)unauthenticatedAuthToken{
    [self  internalImplementationReturns];
}

-(void)principalIdWithObject{
    [self  internalImplementationReturns];
}

-(void)setFrameworkVersion{
    [self  scheduledInstancesListing];
}

-(void)fourteenBitValue{
    [self  sendTelemetryEvent];
    [self  sendTelemetryEvent];
}


@end
