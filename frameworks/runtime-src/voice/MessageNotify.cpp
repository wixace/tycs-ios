
#include "MessageNotify.hpp"
#include "CCLuaEngine.h"

MessageNotify::MessageNotify()
{
    m_luaHandles["OnUploadFile"] = 0;
    m_luaHandles["OnDownloadFile"] = 0;
    m_luaHandles["OnPlayRecordedFile"] = 0;
    m_luaHandles["OnApplyMessageKey"] = 0;
    m_luaHandles["OnJoinRoom"] = 0;
    m_luaHandles["OnQuitRoom"] = 0;
    m_luaHandles["OnMemberVoice"] = 0;
    m_luaHandles["OnSpeechToText"] = 0;
}


void MessageNotify::RegisterHandler(std::string type, int luaHandle)
{
	m_luaHandles[type] = luaHandle;
}

void MessageNotify::OnUploadFile(gcloud_voice::GCloudVoiceCompleteCode code, const char *filePath, const char *fileID)
{
	std::string filePathString = "";
	std::string fileIDString = "";
	if (filePath != NULL)
	{
		std::string tranPath(filePath);
		filePathString = tranPath;
	}

	if (fileID != NULL)
	{
		std::string tranID(fileID);
		fileIDString = tranID;
	}

	Scheduler *sched = Director::getInstance()->getScheduler();
	sched->performFunctionInCocosThread([=](){

		// 回调LUA Close 通知
		LuaStack *stack = LuaEngine::getInstance()->getLuaStack();
		stack->pushInt(code);
		stack->pushString(filePathString.c_str(), filePathString.length());
		stack->pushString(fileIDString.c_str(), fileIDString.length());
		stack->executeFunctionByHandler(m_luaHandles["OnUploadFile"], 3);
	});
}

void MessageNotify::OnDownloadFile(gcloud_voice::GCloudVoiceCompleteCode code, const char *filePath, const char *fileID)
{
	Scheduler *sched = Director::getInstance()->getScheduler();

	std::string filePathString = "";
	std::string fileIDString = "";
	if (filePath != NULL)
	{
		std::string tranPath(filePath);
		filePathString = tranPath;
	}

	if (fileID != NULL)
	{
		std::string tranID(fileID);
		fileIDString = tranID;
	}

	sched->performFunctionInCocosThread([=](){

		// 回调LUA Close 通知
		LuaStack *stack = LuaEngine::getInstance()->getLuaStack();
		stack->pushInt(code);
		stack->pushString(filePathString.c_str(), filePathString.length());
		stack->pushString(fileIDString.c_str(), fileIDString.length());
		stack->executeFunctionByHandler(m_luaHandles["OnDownloadFile"], 3);
	});
}

void MessageNotify::OnPlayRecordedFile(gcloud_voice::GCloudVoiceCompleteCode code,const char *filePath)
{
	Scheduler *sched = Director::getInstance()->getScheduler();

	std::string filePathString = "";
	if (filePath != NULL)
	{
		std::string tranPath(filePath);
		filePathString = filePath;
	}

	sched->performFunctionInCocosThread([=](){

		// 回调LUA Close 通知
		LuaStack *stack = LuaEngine::getInstance()->getLuaStack();
		stack->pushInt(code);
		stack->pushString(filePathString.c_str(), filePathString.length());
		stack->executeFunctionByHandler(m_luaHandles["OnPlayRecordedFile"], 2);
	});
}

void MessageNotify::OnApplyMessageKey(gcloud_voice::GCloudVoiceCompleteCode code)
{
	Scheduler *sched = Director::getInstance()->getScheduler();
	sched->performFunctionInCocosThread([=](){

		// 回调LUA Close 通知
		LuaStack *stack = LuaEngine::getInstance()->getLuaStack();
		stack->pushInt(code);
		stack->executeFunctionByHandler(m_luaHandles["OnApplyMessageKey"], 1);

		// success return gcloud_voice::GV_ON_MESSAGE_KEY_APPLIED_SUCC
	});
}

void MessageNotify::OnSpeechToText(gcloud_voice::GCloudVoiceCompleteCode code, const char *fileID, const char *result)
{
	Scheduler *sched = Director::getInstance()->getScheduler();

	std::string fileIDString = "";
	std::string resultString = "";
	if (fileID != NULL)
	{
		std::string tranID(fileID);
		fileIDString = tranID;
	}

	if (result != NULL)
	{
		std::string tranRes(result);
		resultString = tranRes;
	}

	sched->performFunctionInCocosThread([=](){

		// 回调LUA Close 通知
		LuaStack *stack = LuaEngine::getInstance()->getLuaStack();
		stack->pushInt(code);
		stack->pushString(fileIDString.c_str(), fileIDString.length());
		stack->pushString(resultString.c_str(), resultString.length());
		stack->executeFunctionByHandler(m_luaHandles["OnSpeechToText"], 3);
			
		// success return gcloud_voice::GV_ON_STT_SUCC
	});
}

void MessageNotify::OnStreamSpeechToText(gcloud_voice::GCloudVoiceCompleteCode code, int error, const char *result, const char *voicePath)
{

}

void MessageNotify::OnRoleChanged(gcloud_voice::GCloudVoiceCompleteCode code, const char *roomName, int memberID, int role)
{


}

void MessageNotify::OnMemberVoice(const char *roomName, unsigned int member, int status)
{


}

void MessageNotify::OnRecording(const unsigned char* pAudioData, unsigned int nDataLength)
{
    
}

#if (CC_TARGET_PLATFORM != CC_PLATFORM_IOS)
void MessageNotify::OnEvent(gcloud_voice::GCloudVoiceEvent event, const char * info)
{

}


void MessageNotify::OnStatusUpdate(gcloud_voice::GCloudVoiceCompleteCode status, const char *roomName, int memberID)
{
    
}
#endif



