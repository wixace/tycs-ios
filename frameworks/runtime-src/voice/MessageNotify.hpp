//
//
//
//

#ifndef MessageNotify_hpp
#define MessageNotify_hpp

#include "cocos2d.h"
#include "GCloudVoice.h"
#include <thread>
#include <map>

USING_NS_CC;
//, public Node
class MessageNotify : public gcloud_voice::IGCloudVoiceNotify
{
public:
    MessageNotify();
    //~MessageNotify();
	void RegisterHandler(std::string type, int luaHandle);
public:
    // Voice Message Callback
    virtual void OnUploadFile(gcloud_voice::GCloudVoiceCompleteCode code, const char *filePath, const char *fileID) ;
    virtual void OnDownloadFile(gcloud_voice::GCloudVoiceCompleteCode code, const char *filePath, const char *fileID) ;
    virtual void OnPlayRecordedFile(gcloud_voice::GCloudVoiceCompleteCode code,const char *filePath) ;
    virtual void OnApplyMessageKey(gcloud_voice::GCloudVoiceCompleteCode code) ;
	virtual void OnJoinRoom(gcloud_voice::GCloudVoiceCompleteCode code, const char *roomName, int memberID) {}
	virtual void OnQuitRoom(gcloud_voice::GCloudVoiceCompleteCode code, const char *roomName) {}
	virtual void OnMemberVoice(const unsigned int *members, int count){}
	virtual void OnSpeechToText(gcloud_voice::GCloudVoiceCompleteCode code, const char *fileID, const char *result) ;
	virtual void OnStreamSpeechToText(gcloud_voice::GCloudVoiceCompleteCode code, int error, const char *result, const char *voicePath);
	virtual void OnRoleChanged(gcloud_voice::GCloudVoiceCompleteCode code, const char *roomName, int memberID, int role);
	virtual void OnMemberVoice(const char *roomName, unsigned int member, int status);
    virtual void OnRecording(const unsigned char* pAudioData, unsigned int nDataLength);

#if (CC_TARGET_PLATFORM != CC_PLATFORM_IOS)

    virtual void OnEvent(gcloud_voice::GCloudVoiceEvent event, const char * info);
    virtual void OnStatusUpdate(gcloud_voice::GCloudVoiceCompleteCode status, const char *roomName, int memberID);
#endif
private:
	std::map<std::string, int>	m_luaHandles;

};

#endif /* MessageNotify_hpp */
