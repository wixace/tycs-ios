
#include "VoiceMgr.hpp"
#if (CC_TARGET_PLATFORM == CC_PLATFORM_ANDROID)
#include "../andpermission/Permission.h"
#endif
VoiceMgr::VoiceMgr()
{
	notify = NULL;
}

VoiceMgr::~VoiceMgr()
{
	notify = NULL;
}

// 注册lua回调函数
void VoiceMgr::RegisterMessageNotify(std::string type, int luaHandle)
{
	// CCLOG("applyMessageKey not init %s", type);
	notify->RegisterHandler(type, luaHandle);
}

void VoiceMgr::setNotify()
{
    gcloud_voice::GetVoiceEngine()->SetNotify(notify);
}

int VoiceMgr::setAppInfo(std::string appID, std::string appKey, std::string uid)
{
	gcloud_voice::GCloudVoiceErrno setRet = gcloud_voice::GetVoiceEngine()->SetAppInfo(appID.c_str(), appKey.c_str(), uid.c_str());
	return setRet;
}

int VoiceMgr::initVoiceMgr()
{
	gcloud_voice::GCloudVoiceErrno initRet = gcloud_voice::GetVoiceEngine()->Init();
	return initRet;
}

// 初始化
void VoiceMgr::create()
{	
	notify = new MessageNotify();
}

// 设置
void VoiceMgr::setMode(gcloud_voice::GCloudVoiceMode mode)
{

	gcloud_voice::GetVoiceEngine()->SetMode(mode);
}

int VoiceMgr::applyMessageKey()
{

	gcloud_voice::GCloudVoiceErrno rst = gcloud_voice::GetVoiceEngine()->ApplyMessageKey();
	return rst;
}


// 设置消息长度
int VoiceMgr::setMaxMessageLength(int msgLength)
{
	gcloud_voice::GCloudVoiceErrno rst = gcloud_voice::GetVoiceEngine()->SetMaxMessageLength(msgLength);
	return rst;
}

// 录音
int VoiceMgr::startRecording(std::string wpath)
{
	gcloud_voice::GCloudVoiceErrno rst = gcloud_voice::GetVoiceEngine()->StartRecording(wpath.c_str());
	return rst;
}

// 结束录音
int VoiceMgr::stopRecording()
{
	gcloud_voice::GCloudVoiceErrno rst = gcloud_voice::GetVoiceEngine()->StopRecording();
	return rst;
}

// 上传录音
int VoiceMgr::uploadRecordedFile(std::string wpath, int msgTimeOut)
{
	gcloud_voice::GCloudVoiceErrno rst = gcloud_voice::GetVoiceEngine()->UploadRecordedFile(wpath.c_str(), msgTimeOut);
	return rst;
}

// 下载录音
int VoiceMgr::downloadRecordedFile(std::string fileID, std::string dpath)
{
	gcloud_voice::GCloudVoiceErrno rst = gcloud_voice::GetVoiceEngine()->DownloadRecordedFile(fileID.c_str(), dpath.c_str());
	return rst;
}

// 播放语音
int VoiceMgr::playRecordedFile(std::string dpath)
{
	gcloud_voice::GCloudVoiceErrno rst = gcloud_voice::GetVoiceEngine()->PlayRecordedFile(dpath.c_str());
	return rst;
}

// 停止播放下载的语音
int VoiceMgr::stopPlayFile()
{
	gcloud_voice::GCloudVoiceErrno rst = gcloud_voice::GetVoiceEngine()->StopPlayFile();
	return rst;
}

// 每帧都调用 处理回调
void VoiceMgr::poll()
{
	gcloud_voice::GetVoiceEngine()->Poll();
}

// 获取录音声音大小
int VoiceMgr::getMiclevel()
{
	return gcloud_voice::GetVoiceEngine()->GetMicLevel();
}

// 语音转文字
int VoiceMgr::speechToText(std::string fileID, gcloud_voice::GCloudLanguage language)
{
	gcloud_voice::GCloudVoiceErrno rst = gcloud_voice::GetVoiceEngine()->SpeechToText(fileID.c_str(), 6000, language);
	return rst;
}

void VoiceMgr::pause()
{
	gcloud_voice::GetVoiceEngine()->Pause();
}

void VoiceMgr::resume()
{
	gcloud_voice::GetVoiceEngine()->Resume();
}
