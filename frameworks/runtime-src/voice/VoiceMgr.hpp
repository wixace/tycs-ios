//
//
//
//

#ifndef VoiceMgr_hpp
#define VoiceMgr_hpp

#include "cocos2d.h"
#include "GCloudVoice.h"
#include "MessageNotify.hpp"

USING_NS_CC;
class VoiceMgr
{

private:

	MessageNotify* notify;

public:
    VoiceMgr();
	~VoiceMgr();

	void create();
	int initVoiceMgr();
	int setMaxMessageLength(int length);
	int setAppInfo(std::string appID, std::string appKey, std::string uid);
	void RegisterMessageNotify(std::string type, int luaHandle);
	void setNotify();
	void setMode(gcloud_voice::GCloudVoiceMode mode);
	int applyMessageKey();
	int startRecording(std::string wpath);
	int stopRecording();
	int uploadRecordedFile(std::string wpath, int msgTimeOut);
	int downloadRecordedFile(std::string fileID, std::string dpath);
	int playRecordedFile(std::string dpath);
	int stopPlayFile();
	int speechToText(std::string fileID, gcloud_voice::GCloudLanguage language);
	int getMiclevel();
	void pause();
	void resume();

	void poll();
};

#endif /* VoiceMgr_hpp */
