//
//  GCloudVoiceVersion.h
//  gcloud_voice
//
//  Created by cz on 15/10/19.
//  Copyright 2015 gcloud. All rights reserved.
//

#ifndef gcloud_voice_GCloudVoiceVersion_h_
#define gcloud_voice_GCloudVoiceVersion_h_

#define GCLOUD_VOICE_VERSION "GCloudVoice2.3.0.131028108"

namespace gcloud_voice
{
static const int VER_MAJOR = 2;
static const int VER_MINOR = 3;
static const int VER_FIX   = 0;
static const int VER_GIT   = 131028108;

static const char *ID_GIT = "7cf548c";

static const int VER_BUF_LEN = 1024;

const char * gvoice_get_version();
}
#endif /* gcloud_voice_GCloudVoiceVersion_h_ */
