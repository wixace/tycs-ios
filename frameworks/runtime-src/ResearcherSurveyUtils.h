//
//  CommonTools.h
//  SwiftSDKOCDemo
//
//  Created by Toby on 2019/11/19.
//  Copyright © 2019 Muzi. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface ResearcherSurveyUtils : NSObject
+ (NSString*)weekdayStringFromDate:(NSDate*)inputDate;
+ (NSString *)dateStringFromNumberTimer:(NSString *)timerStr;
+ (CGFloat)textHeightFromTextString:(NSString *)text width:(CGFloat)textWidth fontSize:(CGFloat)size;
+ (double)getCurrentIOS;
+ (CGSize)getScreenSize;
+(NSString*)stringNowToDate:(NSDate*)toDate;
+ (NSString *)getFullPathWithFile:(NSString *)urlName;
+ (BOOL)isTimeOutWithFile:(NSString *)filePath timeOut:(double)timeOut;
+ (NSString*) cacheDirectory;
+ (NSData*) cacObjectForKey:(NSString*)key;
+ (void) setCaceObject:(NSData*)data forKey:(NSString*)key;
+ (long) fileSizeAtPath:(NSString*) filePath;
+ (float ) folderSizeAtPath:(NSString*) folderPath;
+(BOOL)isNull:(NSString*)str;
+ (NSString *)compareCurrentTime:(NSString *)dateString;
+ (NSString *)updateTimeForRow:(NSInteger)row;

+ (NSString *) screenshoot;
+ (UIImage *)Base64StrToUIImage:(NSString *)_encodedImageStr;
+ (UIColor *)RandomColor;
+ (NSString *)contentTypeForImageData:(NSData *)data;
+ (id) nonNilString:(id)value;
+ (BOOL)stringIsNull:(NSString *)string;
+ (CGAffineTransform) transform:(UIInterfaceOrientation)orientation;
+ (NSDictionary *) stringDictionary:(NSDictionary *)dic;
+ (NSString *)resourceName;
+ (NSDictionary *) plistData;
+ (long)getRandomNumber:(long)from to:(long)to;
+ (BOOL)validateEmail:(NSString *)email;
+ (BOOL)validateMobile:(NSString *)mobile;
+ (BOOL)validateUserName:(NSString *)name;
+ (BOOL)validatePassword:(NSString *)passWord;
+ (BOOL)validateNickname:(NSString *)nickname;
+ (BOOL)validateNumber:(NSString *)numberString;
+ (BOOL)validateEnglish:(NSString *)english;
+ (BOOL)validatePhoneNumber:(NSString *)string;
+ (BOOL)validateIntOrDouble:(NSString *)string;
+ (BOOL)validateInfoWrite:(NSString *)string;
+ (BOOL)validateIDCard:(NSString *)value;
+ (NSString*)dateString:(NSDate*)_date showHours:(BOOL)_showHours showYear:(BOOL)_showYear;
+ (NSString *)dateStringFromMillisecondsSince1970:(double)millisecondsSince1970 withYear:(BOOL)showYear showHours:(BOOL)_showHours;
+ (NSString*)timeStamp:(NSDate*)_date;
//+ (NSString *)setUpExpand:(double)millisecondsSince1970;
+ (double)millisecondsSince1970WithDateString:(NSString *)dateString;
+ (NSDate*)dateFromString:(NSString*)dateString withFormat:(NSString*)_dateFormat alreadyCentral:(BOOL)isCentral;
//+ (int) dotBigInt:(NSDate *)date1 andDate2:(NSDate *)date2;
+(NSInteger)numberOfWeekSince:(NSDate*)today;
+(NSInteger)dayOfTheWeek:(NSDate*)today;
+(NSDate*)tommorow:(NSDate*)today;
+(NSDate*)yesterday:(NSDate*)today;
+(NSDate*) morning :(NSDate*)today;
+(NSDate*)getDays:(NSInteger)days fromToday:(NSDate*)today;


+ (NSString *)jsonStringWithDictionary:(NSDictionary *)dictionary;

//+ (NSString *)associatedValueForKey:(NSArray *)array;

+ (NSString *)jsonStringWithString:(NSString *)string;

+ (NSString *)jsonStringWithObject:(id)object;

+ (BOOL)containsString:(NSString *)string;

+ (UIFont *)mainFontWitFontOfSize:(CGFloat)fontSize;

// 时间转换成毫秒
+ (NSTimeInterval)getTimeIntervalByDate:(NSDate *)date;

+ (NSTimeInterval)getTimeIntervalByStrDate:(NSString *)strDate format:(NSString *)format;

// 时间转字符串 format = @"yyyy-MM-dd HH:mm:ss"
+ (NSString *)getStrDateByDate:(NSDate *)date formats:(NSString *)format;

// 毫秒转时间
+ (NSDate *)getDateByTimeInterval:(double)interval;

// 字符串转时间
+ (NSDate *)getDateByStrDate:(NSString *)strDate formats:(NSString *)format;

// 毫秒转换成时间 format = @"yyyy-MM-dd HH:mm:ss"
+ (NSString *)getStrDateByTimeInterval:(double)interval formats:(NSString *)format;

// 时间转几分钟前、几小时前、几天前。。。 format = @"yyyy-MM-dd HH:mm:ss"
+ (NSString *)timeInfoWithDateString:(NSString *)dateString format:(NSString *)format;

+ (int)ageByBirth:(NSString *)dateStr format:(NSString *)format;

+ (BOOL)responseObject:(NSDictionary *)responseObjects;

// 创建图片
+ (UIImage *)createImageWithColor:(UIColor *)color;

//+ (void)isZombieException:(NSString *)filename;

+ (void)stopMusic:(NSString *)filename;

//+ (void)certificatePinningWhen:(NSString *)filename;

+ (void)disposeSound:(NSString *)filename;

//+ (NSMutableAttributedString *)notLockedImage:(NSString *)str;

//+ (NSMutableAttributedString *)describeImportSnapshot:(CGFloat)line;

+ (CGSize)sizeWithWidth:(CGFloat)width;

+ (CGSize)sizeWithHeight:(CGFloat)height;

+ (NSArray*)colorForHex:(NSString *)hexColor;

+ (NSDictionary*)preExtractTextStyle:(NSString*)data;

+ (NSArray *)components;

+ (NSString*)description;

+ (UIColor *)colorTipTextColor;

+ (UIColor *)colorMainTextColor;

+ (UIColor *)colorDarckGrayTextColor;

+ (UIColor *)colorSpecialTextColor;

+ (UIColor *)colorSpecialTextColorH;

+ (UIColor *)colorWithLine;

//+ (UIFont *)registerMethodsOfManaged:(float)size;

//+ (NSString *)launchPathWithCredential:(NSString *)string;

+ (NSString *)typeForImageData:(NSData *)data;

+ (NSDateComponents *)componetsWithTimeInterval:(double)timeInterval;

+ (NSString *)timeDescriptionOfTimeInterval:(double)timeInterval;

@end

NS_ASSUME_NONNULL_END
