#include "../lua/lua_higame_systemutils_auto.hpp"
#include "SystemUtils.h"
#include "scripting/lua-bindings/manual/tolua_fix.h"
#include "scripting/lua-bindings/manual/LuaBasicConversions.h"

int lua_higame_systemutils_SystemUtils_resquestSaveImageToPhotoWithPath(lua_State* tolua_S)
{
    int argc = 0;
    SystemUtils* cobj = nullptr;
    bool ok  = true;

#if COCOS2D_DEBUG >= 1
    tolua_Error tolua_err;
#endif


#if COCOS2D_DEBUG >= 1
    if (!tolua_isusertype(tolua_S,1,"SystemUtils",0,&tolua_err)) goto tolua_lerror;
#endif

    cobj = (SystemUtils*)tolua_tousertype(tolua_S,1,0);

#if COCOS2D_DEBUG >= 1
    if (!cobj) 
    {
        tolua_error(tolua_S,"invalid 'cobj' in function 'lua_higame_systemutils_SystemUtils_resquestSaveImageToPhotoWithPath'", nullptr);
        return 0;
    }
#endif

    argc = lua_gettop(tolua_S)-1;
    if (argc == 1) 
    {
        std::string arg0;

        ok &= luaval_to_std_string(tolua_S, 2,&arg0, "SystemUtils:resquestSaveImageToPhotoWithPath");
        if(!ok)
        {
            tolua_error(tolua_S,"invalid arguments in function 'lua_higame_systemutils_SystemUtils_resquestSaveImageToPhotoWithPath'", nullptr);
            return 0;
        }
        cobj->resquestSaveImageToPhotoWithPath(arg0);
        lua_settop(tolua_S, 1);
        return 1;
    }
    luaL_error(tolua_S, "%s has wrong number of arguments: %d, was expecting %d \n", "SystemUtils:resquestSaveImageToPhotoWithPath",argc, 1);
    return 0;

#if COCOS2D_DEBUG >= 1
    tolua_lerror:
    tolua_error(tolua_S,"#ferror in function 'lua_higame_systemutils_SystemUtils_resquestSaveImageToPhotoWithPath'.",&tolua_err);
#endif

    return 0;
}
int lua_higame_systemutils_SystemUtils_copyToClipboard(lua_State* tolua_S)
{
    int argc = 0;
    SystemUtils* cobj = nullptr;
    bool ok  = true;

#if COCOS2D_DEBUG >= 1
    tolua_Error tolua_err;
#endif


#if COCOS2D_DEBUG >= 1
    if (!tolua_isusertype(tolua_S,1,"SystemUtils",0,&tolua_err)) goto tolua_lerror;
#endif

    cobj = (SystemUtils*)tolua_tousertype(tolua_S,1,0);

#if COCOS2D_DEBUG >= 1
    if (!cobj) 
    {
        tolua_error(tolua_S,"invalid 'cobj' in function 'lua_higame_systemutils_SystemUtils_copyToClipboard'", nullptr);
        return 0;
    }
#endif

    argc = lua_gettop(tolua_S)-1;
    if (argc == 1) 
    {
        std::string arg0;

        ok &= luaval_to_std_string(tolua_S, 2,&arg0, "SystemUtils:copyToClipboard");
        if(!ok)
        {
            tolua_error(tolua_S,"invalid arguments in function 'lua_higame_systemutils_SystemUtils_copyToClipboard'", nullptr);
            return 0;
        }
        cobj->copyToClipboard(arg0);
        lua_settop(tolua_S, 1);
        return 1;
    }
    luaL_error(tolua_S, "%s has wrong number of arguments: %d, was expecting %d \n", "SystemUtils:copyToClipboard",argc, 1);
    return 0;

#if COCOS2D_DEBUG >= 1
    tolua_lerror:
    tolua_error(tolua_S,"#ferror in function 'lua_higame_systemutils_SystemUtils_copyToClipboard'.",&tolua_err);
#endif

    return 0;
}
int lua_higame_systemutils_SystemUtils_getScreenBrightness(lua_State* tolua_S)
{
    int argc = 0;
    SystemUtils* cobj = nullptr;
    bool ok  = true;

#if COCOS2D_DEBUG >= 1
    tolua_Error tolua_err;
#endif


#if COCOS2D_DEBUG >= 1
    if (!tolua_isusertype(tolua_S,1,"SystemUtils",0,&tolua_err)) goto tolua_lerror;
#endif

    cobj = (SystemUtils*)tolua_tousertype(tolua_S,1,0);

#if COCOS2D_DEBUG >= 1
    if (!cobj) 
    {
        tolua_error(tolua_S,"invalid 'cobj' in function 'lua_higame_systemutils_SystemUtils_getScreenBrightness'", nullptr);
        return 0;
    }
#endif

    argc = lua_gettop(tolua_S)-1;
    if (argc == 0) 
    {
        if(!ok)
        {
            tolua_error(tolua_S,"invalid arguments in function 'lua_higame_systemutils_SystemUtils_getScreenBrightness'", nullptr);
            return 0;
        }
        double ret = cobj->getScreenBrightness();
        tolua_pushnumber(tolua_S,(lua_Number)ret);
        return 1;
    }
    luaL_error(tolua_S, "%s has wrong number of arguments: %d, was expecting %d \n", "SystemUtils:getScreenBrightness",argc, 0);
    return 0;

#if COCOS2D_DEBUG >= 1
    tolua_lerror:
    tolua_error(tolua_S,"#ferror in function 'lua_higame_systemutils_SystemUtils_getScreenBrightness'.",&tolua_err);
#endif

    return 0;
}
int lua_higame_systemutils_SystemUtils_getIMEI(lua_State* tolua_S)
{
    int argc = 0;
    SystemUtils* cobj = nullptr;
    bool ok  = true;

#if COCOS2D_DEBUG >= 1
    tolua_Error tolua_err;
#endif


#if COCOS2D_DEBUG >= 1
    if (!tolua_isusertype(tolua_S,1,"SystemUtils",0,&tolua_err)) goto tolua_lerror;
#endif

    cobj = (SystemUtils*)tolua_tousertype(tolua_S,1,0);

#if COCOS2D_DEBUG >= 1
    if (!cobj) 
    {
        tolua_error(tolua_S,"invalid 'cobj' in function 'lua_higame_systemutils_SystemUtils_getIMEI'", nullptr);
        return 0;
    }
#endif

    argc = lua_gettop(tolua_S)-1;
    if (argc == 0) 
    {
        if(!ok)
        {
            tolua_error(tolua_S,"invalid arguments in function 'lua_higame_systemutils_SystemUtils_getIMEI'", nullptr);
            return 0;
        }
        std::string ret = cobj->getIMEI();
        lua_pushlstring(tolua_S,ret.c_str(),ret.length());
        return 1;
    }
    luaL_error(tolua_S, "%s has wrong number of arguments: %d, was expecting %d \n", "SystemUtils:getIMEI",argc, 0);
    return 0;

#if COCOS2D_DEBUG >= 1
    tolua_lerror:
    tolua_error(tolua_S,"#ferror in function 'lua_higame_systemutils_SystemUtils_getIMEI'.",&tolua_err);
#endif

    return 0;
}
int lua_higame_systemutils_SystemUtils_setScreenBrightness(lua_State* tolua_S)
{
    int argc = 0;
    SystemUtils* cobj = nullptr;
    bool ok  = true;

#if COCOS2D_DEBUG >= 1
    tolua_Error tolua_err;
#endif


#if COCOS2D_DEBUG >= 1
    if (!tolua_isusertype(tolua_S,1,"SystemUtils",0,&tolua_err)) goto tolua_lerror;
#endif

    cobj = (SystemUtils*)tolua_tousertype(tolua_S,1,0);

#if COCOS2D_DEBUG >= 1
    if (!cobj) 
    {
        tolua_error(tolua_S,"invalid 'cobj' in function 'lua_higame_systemutils_SystemUtils_setScreenBrightness'", nullptr);
        return 0;
    }
#endif

    argc = lua_gettop(tolua_S)-1;
    if (argc == 1) 
    {
        double arg0;

        ok &= luaval_to_number(tolua_S, 2,&arg0, "SystemUtils:setScreenBrightness");
        if(!ok)
        {
            tolua_error(tolua_S,"invalid arguments in function 'lua_higame_systemutils_SystemUtils_setScreenBrightness'", nullptr);
            return 0;
        }
        cobj->setScreenBrightness(arg0);
        lua_settop(tolua_S, 1);
        return 1;
    }
    luaL_error(tolua_S, "%s has wrong number of arguments: %d, was expecting %d \n", "SystemUtils:setScreenBrightness",argc, 1);
    return 0;

#if COCOS2D_DEBUG >= 1
    tolua_lerror:
    tolua_error(tolua_S,"#ferror in function 'lua_higame_systemutils_SystemUtils_setScreenBrightness'.",&tolua_err);
#endif

    return 0;
}
int lua_higame_systemutils_SystemUtils_getDeviceBrand(lua_State* tolua_S)
{
    int argc = 0;
    SystemUtils* cobj = nullptr;
    bool ok  = true;

#if COCOS2D_DEBUG >= 1
    tolua_Error tolua_err;
#endif


#if COCOS2D_DEBUG >= 1
    if (!tolua_isusertype(tolua_S,1,"SystemUtils",0,&tolua_err)) goto tolua_lerror;
#endif

    cobj = (SystemUtils*)tolua_tousertype(tolua_S,1,0);

#if COCOS2D_DEBUG >= 1
    if (!cobj) 
    {
        tolua_error(tolua_S,"invalid 'cobj' in function 'lua_higame_systemutils_SystemUtils_getDeviceBrand'", nullptr);
        return 0;
    }
#endif

    argc = lua_gettop(tolua_S)-1;
    if (argc == 0) 
    {
        if(!ok)
        {
            tolua_error(tolua_S,"invalid arguments in function 'lua_higame_systemutils_SystemUtils_getDeviceBrand'", nullptr);
            return 0;
        }
        std::string ret = cobj->getDeviceBrand();
        lua_pushlstring(tolua_S,ret.c_str(),ret.length());
        return 1;
    }
    luaL_error(tolua_S, "%s has wrong number of arguments: %d, was expecting %d \n", "SystemUtils:getDeviceBrand",argc, 0);
    return 0;

#if COCOS2D_DEBUG >= 1
    tolua_lerror:
    tolua_error(tolua_S,"#ferror in function 'lua_higame_systemutils_SystemUtils_getDeviceBrand'.",&tolua_err);
#endif

    return 0;
}
int lua_higame_systemutils_SystemUtils_getSystemVersion(lua_State* tolua_S)
{
    int argc = 0;
    SystemUtils* cobj = nullptr;
    bool ok  = true;

#if COCOS2D_DEBUG >= 1
    tolua_Error tolua_err;
#endif


#if COCOS2D_DEBUG >= 1
    if (!tolua_isusertype(tolua_S,1,"SystemUtils",0,&tolua_err)) goto tolua_lerror;
#endif

    cobj = (SystemUtils*)tolua_tousertype(tolua_S,1,0);

#if COCOS2D_DEBUG >= 1
    if (!cobj) 
    {
        tolua_error(tolua_S,"invalid 'cobj' in function 'lua_higame_systemutils_SystemUtils_getSystemVersion'", nullptr);
        return 0;
    }
#endif

    argc = lua_gettop(tolua_S)-1;
    if (argc == 0) 
    {
        if(!ok)
        {
            tolua_error(tolua_S,"invalid arguments in function 'lua_higame_systemutils_SystemUtils_getSystemVersion'", nullptr);
            return 0;
        }
        std::string ret = cobj->getSystemVersion();
        lua_pushlstring(tolua_S,ret.c_str(),ret.length());
        return 1;
    }
    luaL_error(tolua_S, "%s has wrong number of arguments: %d, was expecting %d \n", "SystemUtils:getSystemVersion",argc, 0);
    return 0;

#if COCOS2D_DEBUG >= 1
    tolua_lerror:
    tolua_error(tolua_S,"#ferror in function 'lua_higame_systemutils_SystemUtils_getSystemVersion'.",&tolua_err);
#endif

    return 0;
}
int lua_higame_systemutils_SystemUtils_getSystemModel(lua_State* tolua_S)
{
    int argc = 0;
    SystemUtils* cobj = nullptr;
    bool ok  = true;

#if COCOS2D_DEBUG >= 1
    tolua_Error tolua_err;
#endif


#if COCOS2D_DEBUG >= 1
    if (!tolua_isusertype(tolua_S,1,"SystemUtils",0,&tolua_err)) goto tolua_lerror;
#endif

    cobj = (SystemUtils*)tolua_tousertype(tolua_S,1,0);

#if COCOS2D_DEBUG >= 1
    if (!cobj) 
    {
        tolua_error(tolua_S,"invalid 'cobj' in function 'lua_higame_systemutils_SystemUtils_getSystemModel'", nullptr);
        return 0;
    }
#endif

    argc = lua_gettop(tolua_S)-1;
    if (argc == 0) 
    {
        if(!ok)
        {
            tolua_error(tolua_S,"invalid arguments in function 'lua_higame_systemutils_SystemUtils_getSystemModel'", nullptr);
            return 0;
        }
        std::string ret = cobj->getSystemModel();
        lua_pushlstring(tolua_S,ret.c_str(),ret.length());
        return 1;
    }
    luaL_error(tolua_S, "%s has wrong number of arguments: %d, was expecting %d \n", "SystemUtils:getSystemModel",argc, 0);
    return 0;

#if COCOS2D_DEBUG >= 1
    tolua_lerror:
    tolua_error(tolua_S,"#ferror in function 'lua_higame_systemutils_SystemUtils_getSystemModel'.",&tolua_err);
#endif

    return 0;
}
int lua_higame_systemutils_SystemUtils_getPhoneUDID(lua_State* tolua_S)
{
    int argc = 0;
    SystemUtils* cobj = nullptr;
    bool ok  = true;

#if COCOS2D_DEBUG >= 1
    tolua_Error tolua_err;
#endif


#if COCOS2D_DEBUG >= 1
    if (!tolua_isusertype(tolua_S,1,"SystemUtils",0,&tolua_err)) goto tolua_lerror;
#endif

    cobj = (SystemUtils*)tolua_tousertype(tolua_S,1,0);

#if COCOS2D_DEBUG >= 1
    if (!cobj) 
    {
        tolua_error(tolua_S,"invalid 'cobj' in function 'lua_higame_systemutils_SystemUtils_getPhoneUDID'", nullptr);
        return 0;
    }
#endif

    argc = lua_gettop(tolua_S)-1;
    if (argc == 0) 
    {
        if(!ok)
        {
            tolua_error(tolua_S,"invalid arguments in function 'lua_higame_systemutils_SystemUtils_getPhoneUDID'", nullptr);
            return 0;
        }
        std::string ret = cobj->getPhoneUDID();
        lua_pushlstring(tolua_S,ret.c_str(),ret.length());
        return 1;
    }
    luaL_error(tolua_S, "%s has wrong number of arguments: %d, was expecting %d \n", "SystemUtils:getPhoneUDID",argc, 0);
    return 0;

#if COCOS2D_DEBUG >= 1
    tolua_lerror:
    tolua_error(tolua_S,"#ferror in function 'lua_higame_systemutils_SystemUtils_getPhoneUDID'.",&tolua_err);
#endif

    return 0;
}
int lua_higame_systemutils_SystemUtils_getIDFA(lua_State* tolua_S)
{
    int argc = 0;
    SystemUtils* cobj = nullptr;
    bool ok  = true;

#if COCOS2D_DEBUG >= 1
    tolua_Error tolua_err;
#endif


#if COCOS2D_DEBUG >= 1
    if (!tolua_isusertype(tolua_S,1,"SystemUtils",0,&tolua_err)) goto tolua_lerror;
#endif

    cobj = (SystemUtils*)tolua_tousertype(tolua_S,1,0);

#if COCOS2D_DEBUG >= 1
    if (!cobj) 
    {
        tolua_error(tolua_S,"invalid 'cobj' in function 'lua_higame_systemutils_SystemUtils_getIDFA'", nullptr);
        return 0;
    }
#endif

    argc = lua_gettop(tolua_S)-1;
    if (argc == 0) 
    {
        if(!ok)
        {
            tolua_error(tolua_S,"invalid arguments in function 'lua_higame_systemutils_SystemUtils_getIDFA'", nullptr);
            return 0;
        }
        std::string ret = cobj->getIDFA();
        lua_pushlstring(tolua_S,ret.c_str(),ret.length());
        return 1;
    }
    luaL_error(tolua_S, "%s has wrong number of arguments: %d, was expecting %d \n", "SystemUtils:getIDFA",argc, 0);
    return 0;

#if COCOS2D_DEBUG >= 1
    tolua_lerror:
    tolua_error(tolua_S,"#ferror in function 'lua_higame_systemutils_SystemUtils_getIDFA'.",&tolua_err);
#endif

    return 0;
}
int lua_higame_systemutils_SystemUtils_resquestStartLocation(lua_State* tolua_S)
{
    int argc = 0;
    SystemUtils* cobj = nullptr;
    bool ok  = true;

#if COCOS2D_DEBUG >= 1
    tolua_Error tolua_err;
#endif


#if COCOS2D_DEBUG >= 1
    if (!tolua_isusertype(tolua_S,1,"SystemUtils",0,&tolua_err)) goto tolua_lerror;
#endif

    cobj = (SystemUtils*)tolua_tousertype(tolua_S,1,0);

#if COCOS2D_DEBUG >= 1
    if (!cobj) 
    {
        tolua_error(tolua_S,"invalid 'cobj' in function 'lua_higame_systemutils_SystemUtils_resquestStartLocation'", nullptr);
        return 0;
    }
#endif

    argc = lua_gettop(tolua_S)-1;
    if (argc == 0) 
    {
        if(!ok)
        {
            tolua_error(tolua_S,"invalid arguments in function 'lua_higame_systemutils_SystemUtils_resquestStartLocation'", nullptr);
            return 0;
        }
        cobj->resquestStartLocation();
        lua_settop(tolua_S, 1);
        return 1;
    }
    luaL_error(tolua_S, "%s has wrong number of arguments: %d, was expecting %d \n", "SystemUtils:resquestStartLocation",argc, 0);
    return 0;

#if COCOS2D_DEBUG >= 1
    tolua_lerror:
    tolua_error(tolua_S,"#ferror in function 'lua_higame_systemutils_SystemUtils_resquestStartLocation'.",&tolua_err);
#endif

    return 0;
}
int lua_higame_systemutils_SystemUtils_setOpCallback(lua_State* tolua_S)
{
    int argc = 0;
    SystemUtils* cobj = nullptr;
    bool ok  = true;

#if COCOS2D_DEBUG >= 1
    tolua_Error tolua_err;
#endif


#if COCOS2D_DEBUG >= 1
    if (!tolua_isusertype(tolua_S,1,"SystemUtils",0,&tolua_err)) goto tolua_lerror;
#endif

    cobj = (SystemUtils*)tolua_tousertype(tolua_S,1,0);

#if COCOS2D_DEBUG >= 1
    if (!cobj) 
    {
        tolua_error(tolua_S,"invalid 'cobj' in function 'lua_higame_systemutils_SystemUtils_setOpCallback'", nullptr);
        return 0;
    }
#endif

    argc = lua_gettop(tolua_S)-1;
    if (argc == 1) 
    {
        std::function<void (int, int, std::basic_string<char>)> arg0;

        do {
			// Lambda binding for lua is not supported.
			//assert(false);
		} while(0)
		;
        if(!ok)
        {
            tolua_error(tolua_S,"invalid arguments in function 'lua_higame_systemutils_SystemUtils_setOpCallback'", nullptr);
            return 0;
        }
        cobj->setOpCallback(arg0);
        lua_settop(tolua_S, 1);
        return 1;
    }
    luaL_error(tolua_S, "%s has wrong number of arguments: %d, was expecting %d \n", "SystemUtils:setOpCallback",argc, 1);
    return 0;

#if COCOS2D_DEBUG >= 1
    tolua_lerror:
    tolua_error(tolua_S,"#ferror in function 'lua_higame_systemutils_SystemUtils_setOpCallback'.",&tolua_err);
#endif

    return 0;
}
int lua_higame_systemutils_SystemUtils_getBatteryQuantity(lua_State* tolua_S)
{
    int argc = 0;
    SystemUtils* cobj = nullptr;
    bool ok  = true;

#if COCOS2D_DEBUG >= 1
    tolua_Error tolua_err;
#endif


#if COCOS2D_DEBUG >= 1
    if (!tolua_isusertype(tolua_S,1,"SystemUtils",0,&tolua_err)) goto tolua_lerror;
#endif

    cobj = (SystemUtils*)tolua_tousertype(tolua_S,1,0);

#if COCOS2D_DEBUG >= 1
    if (!cobj) 
    {
        tolua_error(tolua_S,"invalid 'cobj' in function 'lua_higame_systemutils_SystemUtils_getBatteryQuantity'", nullptr);
        return 0;
    }
#endif

    argc = lua_gettop(tolua_S)-1;
    if (argc == 0) 
    {
        if(!ok)
        {
            tolua_error(tolua_S,"invalid arguments in function 'lua_higame_systemutils_SystemUtils_getBatteryQuantity'", nullptr);
            return 0;
        }
        double ret = cobj->getBatteryQuantity();
        tolua_pushnumber(tolua_S,(lua_Number)ret);
        return 1;
    }
    luaL_error(tolua_S, "%s has wrong number of arguments: %d, was expecting %d \n", "SystemUtils:getBatteryQuantity",argc, 0);
    return 0;

#if COCOS2D_DEBUG >= 1
    tolua_lerror:
    tolua_error(tolua_S,"#ferror in function 'lua_higame_systemutils_SystemUtils_getBatteryQuantity'.",&tolua_err);
#endif

    return 0;
}
int lua_higame_systemutils_SystemUtils_getBatteryStauts(lua_State* tolua_S)
{
    int argc = 0;
    SystemUtils* cobj = nullptr;
    bool ok  = true;

#if COCOS2D_DEBUG >= 1
    tolua_Error tolua_err;
#endif


#if COCOS2D_DEBUG >= 1
    if (!tolua_isusertype(tolua_S,1,"SystemUtils",0,&tolua_err)) goto tolua_lerror;
#endif

    cobj = (SystemUtils*)tolua_tousertype(tolua_S,1,0);

#if COCOS2D_DEBUG >= 1
    if (!cobj) 
    {
        tolua_error(tolua_S,"invalid 'cobj' in function 'lua_higame_systemutils_SystemUtils_getBatteryStauts'", nullptr);
        return 0;
    }
#endif

    argc = lua_gettop(tolua_S)-1;
    if (argc == 0) 
    {
        if(!ok)
        {
            tolua_error(tolua_S,"invalid arguments in function 'lua_higame_systemutils_SystemUtils_getBatteryStauts'", nullptr);
            return 0;
        }
        int ret = cobj->getBatteryStauts();
        tolua_pushnumber(tolua_S,(lua_Number)ret);
        return 1;
    }
    luaL_error(tolua_S, "%s has wrong number of arguments: %d, was expecting %d \n", "SystemUtils:getBatteryStauts",argc, 0);
    return 0;

#if COCOS2D_DEBUG >= 1
    tolua_lerror:
    tolua_error(tolua_S,"#ferror in function 'lua_higame_systemutils_SystemUtils_getBatteryStauts'.",&tolua_err);
#endif

    return 0;
}
int lua_higame_systemutils_SystemUtils_getSystemLanguage(lua_State* tolua_S)
{
    int argc = 0;
    SystemUtils* cobj = nullptr;
    bool ok  = true;

#if COCOS2D_DEBUG >= 1
    tolua_Error tolua_err;
#endif


#if COCOS2D_DEBUG >= 1
    if (!tolua_isusertype(tolua_S,1,"SystemUtils",0,&tolua_err)) goto tolua_lerror;
#endif

    cobj = (SystemUtils*)tolua_tousertype(tolua_S,1,0);

#if COCOS2D_DEBUG >= 1
    if (!cobj) 
    {
        tolua_error(tolua_S,"invalid 'cobj' in function 'lua_higame_systemutils_SystemUtils_getSystemLanguage'", nullptr);
        return 0;
    }
#endif

    argc = lua_gettop(tolua_S)-1;
    if (argc == 0) 
    {
        if(!ok)
        {
            tolua_error(tolua_S,"invalid arguments in function 'lua_higame_systemutils_SystemUtils_getSystemLanguage'", nullptr);
            return 0;
        }
        std::string ret = cobj->getSystemLanguage();
        lua_pushlstring(tolua_S,ret.c_str(),ret.length());
        return 1;
    }
    luaL_error(tolua_S, "%s has wrong number of arguments: %d, was expecting %d \n", "SystemUtils:getSystemLanguage",argc, 0);
    return 0;

#if COCOS2D_DEBUG >= 1
    tolua_lerror:
    tolua_error(tolua_S,"#ferror in function 'lua_higame_systemutils_SystemUtils_getSystemLanguage'.",&tolua_err);
#endif

    return 0;
}
int lua_higame_systemutils_SystemUtils_stopVibrate(lua_State* tolua_S)
{
    int argc = 0;
    SystemUtils* cobj = nullptr;
    bool ok  = true;

#if COCOS2D_DEBUG >= 1
    tolua_Error tolua_err;
#endif


#if COCOS2D_DEBUG >= 1
    if (!tolua_isusertype(tolua_S,1,"SystemUtils",0,&tolua_err)) goto tolua_lerror;
#endif

    cobj = (SystemUtils*)tolua_tousertype(tolua_S,1,0);

#if COCOS2D_DEBUG >= 1
    if (!cobj) 
    {
        tolua_error(tolua_S,"invalid 'cobj' in function 'lua_higame_systemutils_SystemUtils_stopVibrate'", nullptr);
        return 0;
    }
#endif

    argc = lua_gettop(tolua_S)-1;
    if (argc == 0) 
    {
        if(!ok)
        {
            tolua_error(tolua_S,"invalid arguments in function 'lua_higame_systemutils_SystemUtils_stopVibrate'", nullptr);
            return 0;
        }
        cobj->stopVibrate();
        lua_settop(tolua_S, 1);
        return 1;
    }
    luaL_error(tolua_S, "%s has wrong number of arguments: %d, was expecting %d \n", "SystemUtils:stopVibrate",argc, 0);
    return 0;

#if COCOS2D_DEBUG >= 1
    tolua_lerror:
    tolua_error(tolua_S,"#ferror in function 'lua_higame_systemutils_SystemUtils_stopVibrate'.",&tolua_err);
#endif

    return 0;
}
int lua_higame_systemutils_SystemUtils_getAPKExpansionZipFilePath(lua_State* tolua_S)
{
    int argc = 0;
    SystemUtils* cobj = nullptr;
    bool ok  = true;

#if COCOS2D_DEBUG >= 1
    tolua_Error tolua_err;
#endif


#if COCOS2D_DEBUG >= 1
    if (!tolua_isusertype(tolua_S,1,"SystemUtils",0,&tolua_err)) goto tolua_lerror;
#endif

    cobj = (SystemUtils*)tolua_tousertype(tolua_S,1,0);

#if COCOS2D_DEBUG >= 1
    if (!cobj) 
    {
        tolua_error(tolua_S,"invalid 'cobj' in function 'lua_higame_systemutils_SystemUtils_getAPKExpansionZipFilePath'", nullptr);
        return 0;
    }
#endif

    argc = lua_gettop(tolua_S)-1;
    if (argc == 0) 
    {
        if(!ok)
        {
            tolua_error(tolua_S,"invalid arguments in function 'lua_higame_systemutils_SystemUtils_getAPKExpansionZipFilePath'", nullptr);
            return 0;
        }
        std::string ret = cobj->getAPKExpansionZipFilePath();
        lua_pushlstring(tolua_S,ret.c_str(),ret.length());
        return 1;
    }
    luaL_error(tolua_S, "%s has wrong number of arguments: %d, was expecting %d \n", "SystemUtils:getAPKExpansionZipFilePath",argc, 0);
    return 0;

#if COCOS2D_DEBUG >= 1
    tolua_lerror:
    tolua_error(tolua_S,"#ferror in function 'lua_higame_systemutils_SystemUtils_getAPKExpansionZipFilePath'.",&tolua_err);
#endif

    return 0;
}
int lua_higame_systemutils_SystemUtils_startVibrate(lua_State* tolua_S)
{
    int argc = 0;
    SystemUtils* cobj = nullptr;
    bool ok  = true;

#if COCOS2D_DEBUG >= 1
    tolua_Error tolua_err;
#endif


#if COCOS2D_DEBUG >= 1
    if (!tolua_isusertype(tolua_S,1,"SystemUtils",0,&tolua_err)) goto tolua_lerror;
#endif

    cobj = (SystemUtils*)tolua_tousertype(tolua_S,1,0);

#if COCOS2D_DEBUG >= 1
    if (!cobj) 
    {
        tolua_error(tolua_S,"invalid 'cobj' in function 'lua_higame_systemutils_SystemUtils_startVibrate'", nullptr);
        return 0;
    }
#endif

    argc = lua_gettop(tolua_S)-1;
    if (argc == 0) 
    {
        if(!ok)
        {
            tolua_error(tolua_S,"invalid arguments in function 'lua_higame_systemutils_SystemUtils_startVibrate'", nullptr);
            return 0;
        }
        cobj->startVibrate();
        lua_settop(tolua_S, 1);
        return 1;
    }
    luaL_error(tolua_S, "%s has wrong number of arguments: %d, was expecting %d \n", "SystemUtils:startVibrate",argc, 0);
    return 0;

#if COCOS2D_DEBUG >= 1
    tolua_lerror:
    tolua_error(tolua_S,"#ferror in function 'lua_higame_systemutils_SystemUtils_startVibrate'.",&tolua_err);
#endif

    return 0;
}
int lua_higame_systemutils_SystemUtils_getInstance(lua_State* tolua_S)
{
    int argc = 0;
    bool ok  = true;

#if COCOS2D_DEBUG >= 1
    tolua_Error tolua_err;
#endif

#if COCOS2D_DEBUG >= 1
    if (!tolua_isusertable(tolua_S,1,"SystemUtils",0,&tolua_err)) goto tolua_lerror;
#endif

    argc = lua_gettop(tolua_S) - 1;

    if (argc == 0)
    {
        if(!ok)
        {
            tolua_error(tolua_S,"invalid arguments in function 'lua_higame_systemutils_SystemUtils_getInstance'", nullptr);
            return 0;
        }
        SystemUtils* ret = SystemUtils::getInstance();
        object_to_luaval<SystemUtils>(tolua_S, "SystemUtils",(SystemUtils*)ret);
        return 1;
    }
    luaL_error(tolua_S, "%s has wrong number of arguments: %d, was expecting %d\n ", "SystemUtils:getInstance",argc, 0);
    return 0;
#if COCOS2D_DEBUG >= 1
    tolua_lerror:
    tolua_error(tolua_S,"#ferror in function 'lua_higame_systemutils_SystemUtils_getInstance'.",&tolua_err);
#endif
    return 0;
}
static int lua_higame_systemutils_SystemUtils_finalize(lua_State* tolua_S)
{
    printf("luabindings: finalizing LUA object (SystemUtils)");
    return 0;
}

int lua_register_higame_systemutils_SystemUtils(lua_State* tolua_S)
{
    tolua_usertype(tolua_S,"SystemUtils");
    tolua_cclass(tolua_S,"SystemUtils","SystemUtils","",nullptr);

    tolua_beginmodule(tolua_S,"SystemUtils");
        tolua_function(tolua_S,"resquestSaveImageToPhotoWithPath",lua_higame_systemutils_SystemUtils_resquestSaveImageToPhotoWithPath);
        tolua_function(tolua_S,"copyToClipboard",lua_higame_systemutils_SystemUtils_copyToClipboard);
        tolua_function(tolua_S,"getScreenBrightness",lua_higame_systemutils_SystemUtils_getScreenBrightness);
        tolua_function(tolua_S,"getIMEI",lua_higame_systemutils_SystemUtils_getIMEI);
        tolua_function(tolua_S,"setScreenBrightness",lua_higame_systemutils_SystemUtils_setScreenBrightness);
        tolua_function(tolua_S,"getDeviceBrand",lua_higame_systemutils_SystemUtils_getDeviceBrand);
        tolua_function(tolua_S,"getSystemVersion",lua_higame_systemutils_SystemUtils_getSystemVersion);
        tolua_function(tolua_S,"getSystemModel",lua_higame_systemutils_SystemUtils_getSystemModel);
        tolua_function(tolua_S,"getPhoneUDID",lua_higame_systemutils_SystemUtils_getPhoneUDID);
        tolua_function(tolua_S,"getIDFA",lua_higame_systemutils_SystemUtils_getIDFA);
        tolua_function(tolua_S,"resquestStartLocation",lua_higame_systemutils_SystemUtils_resquestStartLocation);
        tolua_function(tolua_S,"setOpCallback",lua_higame_systemutils_SystemUtils_setOpCallback);
        tolua_function(tolua_S,"getBatteryQuantity",lua_higame_systemutils_SystemUtils_getBatteryQuantity);
        tolua_function(tolua_S,"getBatteryStauts",lua_higame_systemutils_SystemUtils_getBatteryStauts);
        tolua_function(tolua_S,"getSystemLanguage",lua_higame_systemutils_SystemUtils_getSystemLanguage);
        tolua_function(tolua_S,"stopVibrate",lua_higame_systemutils_SystemUtils_stopVibrate);
        tolua_function(tolua_S,"getAPKExpansionZipFilePath",lua_higame_systemutils_SystemUtils_getAPKExpansionZipFilePath);
        tolua_function(tolua_S,"startVibrate",lua_higame_systemutils_SystemUtils_startVibrate);
        tolua_function(tolua_S,"getInstance", lua_higame_systemutils_SystemUtils_getInstance);
    tolua_endmodule(tolua_S);
    std::string typeName = typeid(SystemUtils).name();
    g_luaType[typeName] = "SystemUtils";
    g_typeCast["SystemUtils"] = "SystemUtils";
    return 1;
}
TOLUA_API int register_all_higame_systemutils(lua_State* tolua_S)
{
	tolua_open(tolua_S);
	
	tolua_module(tolua_S,"higame",0);
	tolua_beginmodule(tolua_S,"higame");

	lua_register_higame_systemutils_SystemUtils(tolua_S);

	tolua_endmodule(tolua_S);
	return 1;
}

