#include "../lua/lua_higame_permission_auto.hpp"
#include "hwCommAStar.h"
#include "hwCommRandom.h"
#include "../andpermission/Permission.h"
#include "scripting/lua-bindings/manual/tolua_fix.h"
#include "scripting/lua-bindings/manual/LuaBasicConversions.h"


int lua_higame_permission_Permission_requestPermission(lua_State* tolua_S)
{
    int argc = 0;
    bool ok  = true;

#if COCOS2D_DEBUG >= 1
    tolua_Error tolua_err;
#endif

#if COCOS2D_DEBUG >= 1
    if (!tolua_isusertable(tolua_S,1,"Permission",0,&tolua_err)) goto tolua_lerror;
#endif

    argc = lua_gettop(tolua_S) - 1;

    if (argc == 1)
    {
        std::string arg0;
        ok &= luaval_to_std_string(tolua_S, 2,&arg0, "Permission:requestPermission");
        if(!ok)
        {
            tolua_error(tolua_S,"invalid arguments in function 'lua_higame_permission_Permission_requestPermission'", nullptr);
            return 0;
        }
        Permission::requestPermission(arg0);
        lua_settop(tolua_S, 1);
        return 1;
    }
    luaL_error(tolua_S, "%s has wrong number of arguments: %d, was expecting %d\n ", "Permission:requestPermission",argc, 1);
    return 0;
#if COCOS2D_DEBUG >= 1
    tolua_lerror:
    tolua_error(tolua_S,"#ferror in function 'lua_higame_permission_Permission_requestPermission'.",&tolua_err);
#endif
    return 0;
}
int lua_higame_permission_Permission_hasPermission(lua_State* tolua_S)
{
    int argc = 0;
    bool ok  = true;

#if COCOS2D_DEBUG >= 1
    tolua_Error tolua_err;
#endif

#if COCOS2D_DEBUG >= 1
    if (!tolua_isusertable(tolua_S,1,"Permission",0,&tolua_err)) goto tolua_lerror;
#endif

    argc = lua_gettop(tolua_S) - 1;

    if (argc == 1)
    {
        std::string arg0;
        ok &= luaval_to_std_string(tolua_S, 2,&arg0, "Permission:hasPermission");
        if(!ok)
        {
            tolua_error(tolua_S,"invalid arguments in function 'lua_higame_permission_Permission_hasPermission'", nullptr);
            return 0;
        }
        bool ret = Permission::hasPermission(arg0);
        tolua_pushboolean(tolua_S,(bool)ret);
        return 1;
    }
    luaL_error(tolua_S, "%s has wrong number of arguments: %d, was expecting %d\n ", "Permission:hasDeniedPermission",argc, 1);
    return 0;
#if COCOS2D_DEBUG >= 1
    tolua_lerror:
    tolua_error(tolua_S,"#ferror in function 'lua_higame_permission_Permission_hasDeniedPermission'.",&tolua_err);
#endif
    return 0;
}
static int lua_higame_permission_Permission_finalize(lua_State* tolua_S)
{
    printf("luabindings: finalizing LUA object (Permission)");
    return 0;
}

int lua_register_higame_permission_Permission(lua_State* tolua_S)
{
    tolua_usertype(tolua_S,"Permission");
    tolua_cclass(tolua_S,"Permission","Permission","",nullptr);

    tolua_beginmodule(tolua_S,"Permission");
        tolua_function(tolua_S,"requestPermission", lua_higame_permission_Permission_requestPermission);
        tolua_function(tolua_S,"hasPermission", lua_higame_permission_Permission_hasPermission);
    tolua_endmodule(tolua_S);
    std::string typeName = typeid(Permission).name();
    g_luaType[typeName] = "Permission";
    g_typeCast["Permission"] = "Permission";
    return 1;
}
TOLUA_API int register_all_higame_permission(lua_State* tolua_S)
{
	tolua_open(tolua_S);
	
	tolua_module(tolua_S,"higame",0);
	tolua_beginmodule(tolua_S,"higame");

	lua_register_higame_permission_Permission(tolua_S);

	tolua_endmodule(tolua_S);
	return 1;
}

