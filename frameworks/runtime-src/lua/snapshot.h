#ifndef __Snapshot_H__
#define __Snapshot_H__

#ifdef __cplusplus
extern "C" {
#endif

#ifndef LUA_JIT
#	include "lua/lua.h"
#	include "lua/lauxlib.h"
#	include "lua/lualib.h"
#else
#	include "luajit/lua.h"
#	include "luajit/lauxlib.h"
#	include "luajit/lualib.h"
#endif

#ifdef __cplusplus
}
#endif

int luaopen_snapshot(lua_State *L);       
extern int snapshot(lua_State *L);
#endif