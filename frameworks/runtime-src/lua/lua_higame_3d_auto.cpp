#include "../lua/lua_higame_3d_auto.hpp"
#include "hw3D_Sprite.h"
#include "hw3D_Camera.h"
#include "hw3D_Mgr.h"
#include "scripting/lua-bindings/manual/tolua_fix.h"
#include "scripting/lua-bindings/manual/LuaBasicConversions.h"

int lua_higame_3d_Chw3D_Camera_Load(lua_State* tolua_S)
{
    int argc = 0;
    Chw3D_Camera* cobj = nullptr;
    bool ok  = true;

#if COCOS2D_DEBUG >= 1
    tolua_Error tolua_err;
#endif


#if COCOS2D_DEBUG >= 1
    if (!tolua_isusertype(tolua_S,1,"Chw3D_Camera",0,&tolua_err)) goto tolua_lerror;
#endif

    cobj = (Chw3D_Camera*)tolua_tousertype(tolua_S,1,0);

#if COCOS2D_DEBUG >= 1
    if (!cobj) 
    {
        tolua_error(tolua_S,"invalid 'cobj' in function 'lua_higame_3d_Chw3D_Camera_Load'", nullptr);
        return 0;
    }
#endif

    argc = lua_gettop(tolua_S)-1;
    if (argc == 1) 
    {
        const char* arg0;

        std::string arg0_tmp; ok &= luaval_to_std_string(tolua_S, 2, &arg0_tmp, "Chw3D_Camera:Load"); arg0 = arg0_tmp.c_str();
        if(!ok)
        {
            tolua_error(tolua_S,"invalid arguments in function 'lua_higame_3d_Chw3D_Camera_Load'", nullptr);
            return 0;
        }
        bool ret = cobj->Load(arg0);
        tolua_pushboolean(tolua_S,(bool)ret);
        return 1;
    }
    luaL_error(tolua_S, "%s has wrong number of arguments: %d, was expecting %d \n", "Chw3D_Camera:Load",argc, 1);
    return 0;

#if COCOS2D_DEBUG >= 1
    tolua_lerror:
    tolua_error(tolua_S,"#ferror in function 'lua_higame_3d_Chw3D_Camera_Load'.",&tolua_err);
#endif

    return 0;
}
int lua_higame_3d_Chw3D_Camera_setDepthRatio(lua_State* tolua_S)
{
    int argc = 0;
    Chw3D_Camera* cobj = nullptr;
    bool ok  = true;

#if COCOS2D_DEBUG >= 1
    tolua_Error tolua_err;
#endif


#if COCOS2D_DEBUG >= 1
    if (!tolua_isusertype(tolua_S,1,"Chw3D_Camera",0,&tolua_err)) goto tolua_lerror;
#endif

    cobj = (Chw3D_Camera*)tolua_tousertype(tolua_S,1,0);

#if COCOS2D_DEBUG >= 1
    if (!cobj) 
    {
        tolua_error(tolua_S,"invalid 'cobj' in function 'lua_higame_3d_Chw3D_Camera_setDepthRatio'", nullptr);
        return 0;
    }
#endif

    argc = lua_gettop(tolua_S)-1;
    if (argc == 1) 
    {
        double arg0;

        ok &= luaval_to_number(tolua_S, 2,&arg0, "Chw3D_Camera:setDepthRatio");
        if(!ok)
        {
            tolua_error(tolua_S,"invalid arguments in function 'lua_higame_3d_Chw3D_Camera_setDepthRatio'", nullptr);
            return 0;
        }
        cobj->setDepthRatio(arg0);
        lua_settop(tolua_S, 1);
        return 1;
    }
    luaL_error(tolua_S, "%s has wrong number of arguments: %d, was expecting %d \n", "Chw3D_Camera:setDepthRatio",argc, 1);
    return 0;

#if COCOS2D_DEBUG >= 1
    tolua_lerror:
    tolua_error(tolua_S,"#ferror in function 'lua_higame_3d_Chw3D_Camera_setDepthRatio'.",&tolua_err);
#endif

    return 0;
}
int lua_higame_3d_Chw3D_Camera_Set(lua_State* tolua_S)
{
    int argc = 0;
    Chw3D_Camera* cobj = nullptr;
    bool ok  = true;

#if COCOS2D_DEBUG >= 1
    tolua_Error tolua_err;
#endif


#if COCOS2D_DEBUG >= 1
    if (!tolua_isusertype(tolua_S,1,"Chw3D_Camera",0,&tolua_err)) goto tolua_lerror;
#endif

    cobj = (Chw3D_Camera*)tolua_tousertype(tolua_S,1,0);

#if COCOS2D_DEBUG >= 1
    if (!cobj) 
    {
        tolua_error(tolua_S,"invalid 'cobj' in function 'lua_higame_3d_Chw3D_Camera_Set'", nullptr);
        return 0;
    }
#endif

    argc = lua_gettop(tolua_S)-1;
    if (argc == 6) 
    {
        double arg0;
        double arg1;
        double arg2;
        double arg3;
        double arg4;
        double arg5;

        ok &= luaval_to_number(tolua_S, 2,&arg0, "Chw3D_Camera:Set");

        ok &= luaval_to_number(tolua_S, 3,&arg1, "Chw3D_Camera:Set");

        ok &= luaval_to_number(tolua_S, 4,&arg2, "Chw3D_Camera:Set");

        ok &= luaval_to_number(tolua_S, 5,&arg3, "Chw3D_Camera:Set");

        ok &= luaval_to_number(tolua_S, 6,&arg4, "Chw3D_Camera:Set");

        ok &= luaval_to_number(tolua_S, 7,&arg5, "Chw3D_Camera:Set");
        if(!ok)
        {
            tolua_error(tolua_S,"invalid arguments in function 'lua_higame_3d_Chw3D_Camera_Set'", nullptr);
            return 0;
        }
        cobj->Set(arg0, arg1, arg2, arg3, arg4, arg5);
        lua_settop(tolua_S, 1);
        return 1;
    }
    luaL_error(tolua_S, "%s has wrong number of arguments: %d, was expecting %d \n", "Chw3D_Camera:Set",argc, 6);
    return 0;

#if COCOS2D_DEBUG >= 1
    tolua_lerror:
    tolua_error(tolua_S,"#ferror in function 'lua_higame_3d_Chw3D_Camera_Set'.",&tolua_err);
#endif

    return 0;
}
int lua_higame_3d_Chw3D_Camera_getViewProjectionMatrix(lua_State* tolua_S)
{
    int argc = 0;
    Chw3D_Camera* cobj = nullptr;
    bool ok  = true;

#if COCOS2D_DEBUG >= 1
    tolua_Error tolua_err;
#endif


#if COCOS2D_DEBUG >= 1
    if (!tolua_isusertype(tolua_S,1,"Chw3D_Camera",0,&tolua_err)) goto tolua_lerror;
#endif

    cobj = (Chw3D_Camera*)tolua_tousertype(tolua_S,1,0);

#if COCOS2D_DEBUG >= 1
    if (!cobj) 
    {
        tolua_error(tolua_S,"invalid 'cobj' in function 'lua_higame_3d_Chw3D_Camera_getViewProjectionMatrix'", nullptr);
        return 0;
    }
#endif

    argc = lua_gettop(tolua_S)-1;
    if (argc == 0) 
    {
        if(!ok)
        {
            tolua_error(tolua_S,"invalid arguments in function 'lua_higame_3d_Chw3D_Camera_getViewProjectionMatrix'", nullptr);
            return 0;
        }
        cocos2d::Mat4& ret = cobj->getViewProjectionMatrix();
        mat4_to_luaval(tolua_S, ret);
        return 1;
    }
    luaL_error(tolua_S, "%s has wrong number of arguments: %d, was expecting %d \n", "Chw3D_Camera:getViewProjectionMatrix",argc, 0);
    return 0;

#if COCOS2D_DEBUG >= 1
    tolua_lerror:
    tolua_error(tolua_S,"#ferror in function 'lua_higame_3d_Chw3D_Camera_getViewProjectionMatrix'.",&tolua_err);
#endif

    return 0;
}
int lua_higame_3d_Chw3D_Camera_isEnabelGuiDepth(lua_State* tolua_S)
{
    int argc = 0;
    Chw3D_Camera* cobj = nullptr;
    bool ok  = true;

#if COCOS2D_DEBUG >= 1
    tolua_Error tolua_err;
#endif


#if COCOS2D_DEBUG >= 1
    if (!tolua_isusertype(tolua_S,1,"Chw3D_Camera",0,&tolua_err)) goto tolua_lerror;
#endif

    cobj = (Chw3D_Camera*)tolua_tousertype(tolua_S,1,0);

#if COCOS2D_DEBUG >= 1
    if (!cobj) 
    {
        tolua_error(tolua_S,"invalid 'cobj' in function 'lua_higame_3d_Chw3D_Camera_isEnabelGuiDepth'", nullptr);
        return 0;
    }
#endif

    argc = lua_gettop(tolua_S)-1;
    if (argc == 0) 
    {
        if(!ok)
        {
            tolua_error(tolua_S,"invalid arguments in function 'lua_higame_3d_Chw3D_Camera_isEnabelGuiDepth'", nullptr);
            return 0;
        }
        bool ret = cobj->isEnabelGuiDepth();
        tolua_pushboolean(tolua_S,(bool)ret);
        return 1;
    }
    luaL_error(tolua_S, "%s has wrong number of arguments: %d, was expecting %d \n", "Chw3D_Camera:isEnabelGuiDepth",argc, 0);
    return 0;

#if COCOS2D_DEBUG >= 1
    tolua_lerror:
    tolua_error(tolua_S,"#ferror in function 'lua_higame_3d_Chw3D_Camera_isEnabelGuiDepth'.",&tolua_err);
#endif

    return 0;
}
int lua_higame_3d_Chw3D_Camera_unproject(lua_State* tolua_S)
{
    int argc = 0;
    Chw3D_Camera* cobj = nullptr;
    bool ok  = true;
#if COCOS2D_DEBUG >= 1
    tolua_Error tolua_err;
#endif

#if COCOS2D_DEBUG >= 1
    if (!tolua_isusertype(tolua_S,1,"Chw3D_Camera",0,&tolua_err)) goto tolua_lerror;
#endif
    cobj = (Chw3D_Camera*)tolua_tousertype(tolua_S,1,0);
#if COCOS2D_DEBUG >= 1
    if (!cobj)
    {
        tolua_error(tolua_S,"invalid 'cobj' in function 'lua_higame_3d_Chw3D_Camera_unproject'", nullptr);
        return 0;
    }
#endif
    argc = lua_gettop(tolua_S)-1;
    do{
        if (argc == 3) {
            cocos2d::Size arg0;
            ok &= luaval_to_size(tolua_S, 2, &arg0, "Chw3D_Camera:unproject");

            if (!ok) { break; }
            const cocos2d::Vec3* arg1;
            ok &= luaval_to_object<const cocos2d::Vec3>(tolua_S, 3, "cc.Vec3",&arg1, "Chw3D_Camera:unproject");

            if (!ok) { break; }
            cocos2d::Vec3* arg2;
            ok &= luaval_to_object<cocos2d::Vec3>(tolua_S, 4, "cc.Vec3",&arg2, "Chw3D_Camera:unproject");

            if (!ok) { break; }
            cobj->unproject(arg0, arg1, arg2);
            lua_settop(tolua_S, 1);
            return 1;
        }
    }while(0);
    ok  = true;
    do{
        if (argc == 1) {
            cocos2d::Vec3 arg0;
            ok &= luaval_to_vec3(tolua_S, 2, &arg0, "Chw3D_Camera:unproject");

            if (!ok) { break; }
            cocos2d::Vec3 ret = cobj->unproject(arg0);
            vec3_to_luaval(tolua_S, ret);
            return 1;
        }
    }while(0);
    ok  = true;
    luaL_error(tolua_S, "%s has wrong number of arguments: %d, was expecting %d \n",  "Chw3D_Camera:unproject",argc, 1);
    return 0;

#if COCOS2D_DEBUG >= 1
    tolua_lerror:
    tolua_error(tolua_S,"#ferror in function 'lua_higame_3d_Chw3D_Camera_unproject'.",&tolua_err);
#endif

    return 0;
}
int lua_higame_3d_Chw3D_Camera_ZDir(lua_State* tolua_S)
{
    int argc = 0;
    Chw3D_Camera* cobj = nullptr;
    bool ok  = true;

#if COCOS2D_DEBUG >= 1
    tolua_Error tolua_err;
#endif


#if COCOS2D_DEBUG >= 1
    if (!tolua_isusertype(tolua_S,1,"Chw3D_Camera",0,&tolua_err)) goto tolua_lerror;
#endif

    cobj = (Chw3D_Camera*)tolua_tousertype(tolua_S,1,0);

#if COCOS2D_DEBUG >= 1
    if (!cobj) 
    {
        tolua_error(tolua_S,"invalid 'cobj' in function 'lua_higame_3d_Chw3D_Camera_ZDir'", nullptr);
        return 0;
    }
#endif

    argc = lua_gettop(tolua_S)-1;
    if (argc == 1) 
    {
        cocos2d::Vec3* arg0;

        ok &= luaval_to_object<cocos2d::Vec3>(tolua_S, 2, "cc.Vec3",&arg0, "Chw3D_Camera:ZDir");
        if(!ok)
        {
            tolua_error(tolua_S,"invalid arguments in function 'lua_higame_3d_Chw3D_Camera_ZDir'", nullptr);
            return 0;
        }
        cobj->ZDir(arg0);
        lua_settop(tolua_S, 1);
        return 1;
    }
    luaL_error(tolua_S, "%s has wrong number of arguments: %d, was expecting %d \n", "Chw3D_Camera:ZDir",argc, 1);
    return 0;

#if COCOS2D_DEBUG >= 1
    tolua_lerror:
    tolua_error(tolua_S,"#ferror in function 'lua_higame_3d_Chw3D_Camera_ZDir'.",&tolua_err);
#endif

    return 0;
}
int lua_higame_3d_Chw3D_Camera_getDepthRatio(lua_State* tolua_S)
{
    int argc = 0;
    Chw3D_Camera* cobj = nullptr;
    bool ok  = true;

#if COCOS2D_DEBUG >= 1
    tolua_Error tolua_err;
#endif


#if COCOS2D_DEBUG >= 1
    if (!tolua_isusertype(tolua_S,1,"Chw3D_Camera",0,&tolua_err)) goto tolua_lerror;
#endif

    cobj = (Chw3D_Camera*)tolua_tousertype(tolua_S,1,0);

#if COCOS2D_DEBUG >= 1
    if (!cobj) 
    {
        tolua_error(tolua_S,"invalid 'cobj' in function 'lua_higame_3d_Chw3D_Camera_getDepthRatio'", nullptr);
        return 0;
    }
#endif

    argc = lua_gettop(tolua_S)-1;
    if (argc == 0) 
    {
        if(!ok)
        {
            tolua_error(tolua_S,"invalid arguments in function 'lua_higame_3d_Chw3D_Camera_getDepthRatio'", nullptr);
            return 0;
        }
        double ret = cobj->getDepthRatio();
        tolua_pushnumber(tolua_S,(lua_Number)ret);
        return 1;
    }
    luaL_error(tolua_S, "%s has wrong number of arguments: %d, was expecting %d \n", "Chw3D_Camera:getDepthRatio",argc, 0);
    return 0;

#if COCOS2D_DEBUG >= 1
    tolua_lerror:
    tolua_error(tolua_S,"#ferror in function 'lua_higame_3d_Chw3D_Camera_getDepthRatio'.",&tolua_err);
#endif

    return 0;
}
int lua_higame_3d_Chw3D_Camera_project(lua_State* tolua_S)
{
    int argc = 0;
    Chw3D_Camera* cobj = nullptr;
    bool ok  = true;

#if COCOS2D_DEBUG >= 1
    tolua_Error tolua_err;
#endif


#if COCOS2D_DEBUG >= 1
    if (!tolua_isusertype(tolua_S,1,"Chw3D_Camera",0,&tolua_err)) goto tolua_lerror;
#endif

    cobj = (Chw3D_Camera*)tolua_tousertype(tolua_S,1,0);

#if COCOS2D_DEBUG >= 1
    if (!cobj) 
    {
        tolua_error(tolua_S,"invalid 'cobj' in function 'lua_higame_3d_Chw3D_Camera_project'", nullptr);
        return 0;
    }
#endif

    argc = lua_gettop(tolua_S)-1;
    if (argc == 1) 
    {
        cocos2d::Vec3 arg0;

        ok &= luaval_to_vec3(tolua_S, 2, &arg0, "Chw3D_Camera:project");
        if(!ok)
        {
            tolua_error(tolua_S,"invalid arguments in function 'lua_higame_3d_Chw3D_Camera_project'", nullptr);
            return 0;
        }
        cocos2d::Vec2 ret = cobj->project(arg0);
        vec2_to_luaval(tolua_S, ret);
        return 1;
    }
    luaL_error(tolua_S, "%s has wrong number of arguments: %d, was expecting %d \n", "Chw3D_Camera:project",argc, 1);
    return 0;

#if COCOS2D_DEBUG >= 1
    tolua_lerror:
    tolua_error(tolua_S,"#ferror in function 'lua_higame_3d_Chw3D_Camera_project'.",&tolua_err);
#endif

    return 0;
}
int lua_higame_3d_Chw3D_Camera_XDir(lua_State* tolua_S)
{
    int argc = 0;
    Chw3D_Camera* cobj = nullptr;
    bool ok  = true;

#if COCOS2D_DEBUG >= 1
    tolua_Error tolua_err;
#endif


#if COCOS2D_DEBUG >= 1
    if (!tolua_isusertype(tolua_S,1,"Chw3D_Camera",0,&tolua_err)) goto tolua_lerror;
#endif

    cobj = (Chw3D_Camera*)tolua_tousertype(tolua_S,1,0);

#if COCOS2D_DEBUG >= 1
    if (!cobj) 
    {
        tolua_error(tolua_S,"invalid 'cobj' in function 'lua_higame_3d_Chw3D_Camera_XDir'", nullptr);
        return 0;
    }
#endif

    argc = lua_gettop(tolua_S)-1;
    if (argc == 1) 
    {
        cocos2d::Vec3* arg0;

        ok &= luaval_to_object<cocos2d::Vec3>(tolua_S, 2, "cc.Vec3",&arg0, "Chw3D_Camera:XDir");
        if(!ok)
        {
            tolua_error(tolua_S,"invalid arguments in function 'lua_higame_3d_Chw3D_Camera_XDir'", nullptr);
            return 0;
        }
        cobj->XDir(arg0);
        lua_settop(tolua_S, 1);
        return 1;
    }
    luaL_error(tolua_S, "%s has wrong number of arguments: %d, was expecting %d \n", "Chw3D_Camera:XDir",argc, 1);
    return 0;

#if COCOS2D_DEBUG >= 1
    tolua_lerror:
    tolua_error(tolua_S,"#ferror in function 'lua_higame_3d_Chw3D_Camera_XDir'.",&tolua_err);
#endif

    return 0;
}
int lua_higame_3d_Chw3D_Camera_unprojectGL(lua_State* tolua_S)
{
    int argc = 0;
    Chw3D_Camera* cobj = nullptr;
    bool ok  = true;
#if COCOS2D_DEBUG >= 1
    tolua_Error tolua_err;
#endif

#if COCOS2D_DEBUG >= 1
    if (!tolua_isusertype(tolua_S,1,"Chw3D_Camera",0,&tolua_err)) goto tolua_lerror;
#endif
    cobj = (Chw3D_Camera*)tolua_tousertype(tolua_S,1,0);
#if COCOS2D_DEBUG >= 1
    if (!cobj)
    {
        tolua_error(tolua_S,"invalid 'cobj' in function 'lua_higame_3d_Chw3D_Camera_unprojectGL'", nullptr);
        return 0;
    }
#endif
    argc = lua_gettop(tolua_S)-1;
    do{
        if (argc == 3) {
            cocos2d::Size arg0;
            ok &= luaval_to_size(tolua_S, 2, &arg0, "Chw3D_Camera:unprojectGL");

            if (!ok) { break; }
            const cocos2d::Vec3* arg1;
            ok &= luaval_to_object<const cocos2d::Vec3>(tolua_S, 3, "cc.Vec3",&arg1, "Chw3D_Camera:unprojectGL");

            if (!ok) { break; }
            cocos2d::Vec3* arg2;
            ok &= luaval_to_object<cocos2d::Vec3>(tolua_S, 4, "cc.Vec3",&arg2, "Chw3D_Camera:unprojectGL");

            if (!ok) { break; }
            cobj->unprojectGL(arg0, arg1, arg2);
            lua_settop(tolua_S, 1);
            return 1;
        }
    }while(0);
    ok  = true;
    do{
        if (argc == 1) {
            cocos2d::Vec3 arg0;
            ok &= luaval_to_vec3(tolua_S, 2, &arg0, "Chw3D_Camera:unprojectGL");

            if (!ok) { break; }
            cocos2d::Vec3 ret = cobj->unprojectGL(arg0);
            vec3_to_luaval(tolua_S, ret);
            return 1;
        }
    }while(0);
    ok  = true;
    luaL_error(tolua_S, "%s has wrong number of arguments: %d, was expecting %d \n",  "Chw3D_Camera:unprojectGL",argc, 1);
    return 0;

#if COCOS2D_DEBUG >= 1
    tolua_lerror:
    tolua_error(tolua_S,"#ferror in function 'lua_higame_3d_Chw3D_Camera_unprojectGL'.",&tolua_err);
#endif

    return 0;
}
int lua_higame_3d_Chw3D_Camera_Ortho(lua_State* tolua_S)
{
    int argc = 0;
    Chw3D_Camera* cobj = nullptr;
    bool ok  = true;

#if COCOS2D_DEBUG >= 1
    tolua_Error tolua_err;
#endif


#if COCOS2D_DEBUG >= 1
    if (!tolua_isusertype(tolua_S,1,"Chw3D_Camera",0,&tolua_err)) goto tolua_lerror;
#endif

    cobj = (Chw3D_Camera*)tolua_tousertype(tolua_S,1,0);

#if COCOS2D_DEBUG >= 1
    if (!cobj) 
    {
        tolua_error(tolua_S,"invalid 'cobj' in function 'lua_higame_3d_Chw3D_Camera_Ortho'", nullptr);
        return 0;
    }
#endif

    argc = lua_gettop(tolua_S)-1;
    if (argc == 4) 
    {
        double arg0;
        double arg1;
        double arg2;
        double arg3;

        ok &= luaval_to_number(tolua_S, 2,&arg0, "Chw3D_Camera:Ortho");

        ok &= luaval_to_number(tolua_S, 3,&arg1, "Chw3D_Camera:Ortho");

        ok &= luaval_to_number(tolua_S, 4,&arg2, "Chw3D_Camera:Ortho");

        ok &= luaval_to_number(tolua_S, 5,&arg3, "Chw3D_Camera:Ortho");
        if(!ok)
        {
            tolua_error(tolua_S,"invalid arguments in function 'lua_higame_3d_Chw3D_Camera_Ortho'", nullptr);
            return 0;
        }
        cobj->Ortho(arg0, arg1, arg2, arg3);
        lua_settop(tolua_S, 1);
        return 1;
    }
    luaL_error(tolua_S, "%s has wrong number of arguments: %d, was expecting %d \n", "Chw3D_Camera:Ortho",argc, 4);
    return 0;

#if COCOS2D_DEBUG >= 1
    tolua_lerror:
    tolua_error(tolua_S,"#ferror in function 'lua_higame_3d_Chw3D_Camera_Ortho'.",&tolua_err);
#endif

    return 0;
}
int lua_higame_3d_Chw3D_Camera_screenPointToWorldPoint(lua_State* tolua_S)
{
    int argc = 0;
    Chw3D_Camera* cobj = nullptr;
    bool ok  = true;

#if COCOS2D_DEBUG >= 1
    tolua_Error tolua_err;
#endif


#if COCOS2D_DEBUG >= 1
    if (!tolua_isusertype(tolua_S,1,"Chw3D_Camera",0,&tolua_err)) goto tolua_lerror;
#endif

    cobj = (Chw3D_Camera*)tolua_tousertype(tolua_S,1,0);

#if COCOS2D_DEBUG >= 1
    if (!cobj) 
    {
        tolua_error(tolua_S,"invalid 'cobj' in function 'lua_higame_3d_Chw3D_Camera_screenPointToWorldPoint'", nullptr);
        return 0;
    }
#endif

    argc = lua_gettop(tolua_S)-1;
    if (argc == 1) 
    {
        cocos2d::Vec2 arg0;

        ok &= luaval_to_vec2(tolua_S, 2, &arg0, "Chw3D_Camera:screenPointToWorldPoint");
        if(!ok)
        {
            tolua_error(tolua_S,"invalid arguments in function 'lua_higame_3d_Chw3D_Camera_screenPointToWorldPoint'", nullptr);
            return 0;
        }
        cocos2d::Vec3 ret = cobj->screenPointToWorldPoint(arg0);
        vec3_to_luaval(tolua_S, ret);
        return 1;
    }
    luaL_error(tolua_S, "%s has wrong number of arguments: %d, was expecting %d \n", "Chw3D_Camera:screenPointToWorldPoint",argc, 1);
    return 0;

#if COCOS2D_DEBUG >= 1
    tolua_lerror:
    tolua_error(tolua_S,"#ferror in function 'lua_higame_3d_Chw3D_Camera_screenPointToWorldPoint'.",&tolua_err);
#endif

    return 0;
}
int lua_higame_3d_Chw3D_Camera_Perspective(lua_State* tolua_S)
{
    int argc = 0;
    Chw3D_Camera* cobj = nullptr;
    bool ok  = true;
#if COCOS2D_DEBUG >= 1
    tolua_Error tolua_err;
#endif

#if COCOS2D_DEBUG >= 1
    if (!tolua_isusertype(tolua_S,1,"Chw3D_Camera",0,&tolua_err)) goto tolua_lerror;
#endif
    cobj = (Chw3D_Camera*)tolua_tousertype(tolua_S,1,0);
#if COCOS2D_DEBUG >= 1
    if (!cobj)
    {
        tolua_error(tolua_S,"invalid 'cobj' in function 'lua_higame_3d_Chw3D_Camera_Perspective'", nullptr);
        return 0;
    }
#endif
    argc = lua_gettop(tolua_S)-1;
    do{
        if (argc == 5) {
            double arg0;
            ok &= luaval_to_number(tolua_S, 2,&arg0, "Chw3D_Camera:Perspective");

            if (!ok) { break; }
            double arg1;
            ok &= luaval_to_number(tolua_S, 3,&arg1, "Chw3D_Camera:Perspective");

            if (!ok) { break; }
            double arg2;
            ok &= luaval_to_number(tolua_S, 4,&arg2, "Chw3D_Camera:Perspective");

            if (!ok) { break; }
            double arg3;
            ok &= luaval_to_number(tolua_S, 5,&arg3, "Chw3D_Camera:Perspective");

            if (!ok) { break; }
            double arg4;
            ok &= luaval_to_number(tolua_S, 6,&arg4, "Chw3D_Camera:Perspective");

            if (!ok) { break; }
            cobj->Perspective(arg0, arg1, arg2, arg3, arg4);
            lua_settop(tolua_S, 1);
            return 1;
        }
    }while(0);
    ok  = true;
    do{
        if (argc == 4) {
            double arg0;
            ok &= luaval_to_number(tolua_S, 2,&arg0, "Chw3D_Camera:Perspective");

            if (!ok) { break; }
            double arg1;
            ok &= luaval_to_number(tolua_S, 3,&arg1, "Chw3D_Camera:Perspective");

            if (!ok) { break; }
            double arg2;
            ok &= luaval_to_number(tolua_S, 4,&arg2, "Chw3D_Camera:Perspective");

            if (!ok) { break; }
            double arg3;
            ok &= luaval_to_number(tolua_S, 5,&arg3, "Chw3D_Camera:Perspective");

            if (!ok) { break; }
            cobj->Perspective(arg0, arg1, arg2, arg3);
            lua_settop(tolua_S, 1);
            return 1;
        }
    }while(0);
    ok  = true;
    luaL_error(tolua_S, "%s has wrong number of arguments: %d, was expecting %d \n",  "Chw3D_Camera:Perspective",argc, 4);
    return 0;

#if COCOS2D_DEBUG >= 1
    tolua_lerror:
    tolua_error(tolua_S,"#ferror in function 'lua_higame_3d_Chw3D_Camera_Perspective'.",&tolua_err);
#endif

    return 0;
}
int lua_higame_3d_Chw3D_Camera_setEnabelGuiDepth(lua_State* tolua_S)
{
    int argc = 0;
    Chw3D_Camera* cobj = nullptr;
    bool ok  = true;

#if COCOS2D_DEBUG >= 1
    tolua_Error tolua_err;
#endif


#if COCOS2D_DEBUG >= 1
    if (!tolua_isusertype(tolua_S,1,"Chw3D_Camera",0,&tolua_err)) goto tolua_lerror;
#endif

    cobj = (Chw3D_Camera*)tolua_tousertype(tolua_S,1,0);

#if COCOS2D_DEBUG >= 1
    if (!cobj) 
    {
        tolua_error(tolua_S,"invalid 'cobj' in function 'lua_higame_3d_Chw3D_Camera_setEnabelGuiDepth'", nullptr);
        return 0;
    }
#endif

    argc = lua_gettop(tolua_S)-1;
    if (argc == 1) 
    {
        bool arg0;

        ok &= luaval_to_boolean(tolua_S, 2,&arg0, "Chw3D_Camera:setEnabelGuiDepth");
        if(!ok)
        {
            tolua_error(tolua_S,"invalid arguments in function 'lua_higame_3d_Chw3D_Camera_setEnabelGuiDepth'", nullptr);
            return 0;
        }
        cobj->setEnabelGuiDepth(arg0);
        lua_settop(tolua_S, 1);
        return 1;
    }
    luaL_error(tolua_S, "%s has wrong number of arguments: %d, was expecting %d \n", "Chw3D_Camera:setEnabelGuiDepth",argc, 1);
    return 0;

#if COCOS2D_DEBUG >= 1
    tolua_lerror:
    tolua_error(tolua_S,"#ferror in function 'lua_higame_3d_Chw3D_Camera_setEnabelGuiDepth'.",&tolua_err);
#endif

    return 0;
}
int lua_higame_3d_Chw3D_Camera_Active(lua_State* tolua_S)
{
    int argc = 0;
    Chw3D_Camera* cobj = nullptr;
    bool ok  = true;

#if COCOS2D_DEBUG >= 1
    tolua_Error tolua_err;
#endif


#if COCOS2D_DEBUG >= 1
    if (!tolua_isusertype(tolua_S,1,"Chw3D_Camera",0,&tolua_err)) goto tolua_lerror;
#endif

    cobj = (Chw3D_Camera*)tolua_tousertype(tolua_S,1,0);

#if COCOS2D_DEBUG >= 1
    if (!cobj) 
    {
        tolua_error(tolua_S,"invalid 'cobj' in function 'lua_higame_3d_Chw3D_Camera_Active'", nullptr);
        return 0;
    }
#endif

    argc = lua_gettop(tolua_S)-1;
    if (argc == 0) 
    {
        if(!ok)
        {
            tolua_error(tolua_S,"invalid arguments in function 'lua_higame_3d_Chw3D_Camera_Active'", nullptr);
            return 0;
        }
        cobj->Active();
        lua_settop(tolua_S, 1);
        return 1;
    }
    luaL_error(tolua_S, "%s has wrong number of arguments: %d, was expecting %d \n", "Chw3D_Camera:Active",argc, 0);
    return 0;

#if COCOS2D_DEBUG >= 1
    tolua_lerror:
    tolua_error(tolua_S,"#ferror in function 'lua_higame_3d_Chw3D_Camera_Active'.",&tolua_err);
#endif

    return 0;
}
int lua_higame_3d_Chw3D_Camera_YDir(lua_State* tolua_S)
{
    int argc = 0;
    Chw3D_Camera* cobj = nullptr;
    bool ok  = true;

#if COCOS2D_DEBUG >= 1
    tolua_Error tolua_err;
#endif


#if COCOS2D_DEBUG >= 1
    if (!tolua_isusertype(tolua_S,1,"Chw3D_Camera",0,&tolua_err)) goto tolua_lerror;
#endif

    cobj = (Chw3D_Camera*)tolua_tousertype(tolua_S,1,0);

#if COCOS2D_DEBUG >= 1
    if (!cobj) 
    {
        tolua_error(tolua_S,"invalid 'cobj' in function 'lua_higame_3d_Chw3D_Camera_YDir'", nullptr);
        return 0;
    }
#endif

    argc = lua_gettop(tolua_S)-1;
    if (argc == 1) 
    {
        cocos2d::Vec3* arg0;

        ok &= luaval_to_object<cocos2d::Vec3>(tolua_S, 2, "cc.Vec3",&arg0, "Chw3D_Camera:YDir");
        if(!ok)
        {
            tolua_error(tolua_S,"invalid arguments in function 'lua_higame_3d_Chw3D_Camera_YDir'", nullptr);
            return 0;
        }
        cobj->YDir(arg0);
        lua_settop(tolua_S, 1);
        return 1;
    }
    luaL_error(tolua_S, "%s has wrong number of arguments: %d, was expecting %d \n", "Chw3D_Camera:YDir",argc, 1);
    return 0;

#if COCOS2D_DEBUG >= 1
    tolua_lerror:
    tolua_error(tolua_S,"#ferror in function 'lua_higame_3d_Chw3D_Camera_YDir'.",&tolua_err);
#endif

    return 0;
}
int lua_higame_3d_Chw3D_Camera_Inactive(lua_State* tolua_S)
{
    int argc = 0;
    Chw3D_Camera* cobj = nullptr;
    bool ok  = true;

#if COCOS2D_DEBUG >= 1
    tolua_Error tolua_err;
#endif


#if COCOS2D_DEBUG >= 1
    if (!tolua_isusertype(tolua_S,1,"Chw3D_Camera",0,&tolua_err)) goto tolua_lerror;
#endif

    cobj = (Chw3D_Camera*)tolua_tousertype(tolua_S,1,0);

#if COCOS2D_DEBUG >= 1
    if (!cobj) 
    {
        tolua_error(tolua_S,"invalid 'cobj' in function 'lua_higame_3d_Chw3D_Camera_Inactive'", nullptr);
        return 0;
    }
#endif

    argc = lua_gettop(tolua_S)-1;
    if (argc == 0) 
    {
        if(!ok)
        {
            tolua_error(tolua_S,"invalid arguments in function 'lua_higame_3d_Chw3D_Camera_Inactive'", nullptr);
            return 0;
        }
        cobj->Inactive();
        lua_settop(tolua_S, 1);
        return 1;
    }
    luaL_error(tolua_S, "%s has wrong number of arguments: %d, was expecting %d \n", "Chw3D_Camera:Inactive",argc, 0);
    return 0;

#if COCOS2D_DEBUG >= 1
    tolua_lerror:
    tolua_error(tolua_S,"#ferror in function 'lua_higame_3d_Chw3D_Camera_Inactive'.",&tolua_err);
#endif

    return 0;
}
int lua_higame_3d_Chw3D_Camera_projectGL(lua_State* tolua_S)
{
    int argc = 0;
    Chw3D_Camera* cobj = nullptr;
    bool ok  = true;

#if COCOS2D_DEBUG >= 1
    tolua_Error tolua_err;
#endif


#if COCOS2D_DEBUG >= 1
    if (!tolua_isusertype(tolua_S,1,"Chw3D_Camera",0,&tolua_err)) goto tolua_lerror;
#endif

    cobj = (Chw3D_Camera*)tolua_tousertype(tolua_S,1,0);

#if COCOS2D_DEBUG >= 1
    if (!cobj) 
    {
        tolua_error(tolua_S,"invalid 'cobj' in function 'lua_higame_3d_Chw3D_Camera_projectGL'", nullptr);
        return 0;
    }
#endif

    argc = lua_gettop(tolua_S)-1;
    if (argc == 1) 
    {
        cocos2d::Vec3 arg0;

        ok &= luaval_to_vec3(tolua_S, 2, &arg0, "Chw3D_Camera:projectGL");
        if(!ok)
        {
            tolua_error(tolua_S,"invalid arguments in function 'lua_higame_3d_Chw3D_Camera_projectGL'", nullptr);
            return 0;
        }
        cocos2d::Vec2 ret = cobj->projectGL(arg0);
        vec2_to_luaval(tolua_S, ret);
        return 1;
    }
    luaL_error(tolua_S, "%s has wrong number of arguments: %d, was expecting %d \n", "Chw3D_Camera:projectGL",argc, 1);
    return 0;

#if COCOS2D_DEBUG >= 1
    tolua_lerror:
    tolua_error(tolua_S,"#ferror in function 'lua_higame_3d_Chw3D_Camera_projectGL'.",&tolua_err);
#endif

    return 0;
}
int lua_higame_3d_Chw3D_Camera_create(lua_State* tolua_S)
{
    int argc = 0;
    bool ok  = true;

#if COCOS2D_DEBUG >= 1
    tolua_Error tolua_err;
#endif

#if COCOS2D_DEBUG >= 1
    if (!tolua_isusertable(tolua_S,1,"Chw3D_Camera",0,&tolua_err)) goto tolua_lerror;
#endif

    argc = lua_gettop(tolua_S) - 1;

    if (argc == 0)
    {
        if(!ok)
        {
            tolua_error(tolua_S,"invalid arguments in function 'lua_higame_3d_Chw3D_Camera_create'", nullptr);
            return 0;
        }
        Chw3D_Camera* ret = Chw3D_Camera::create();
        object_to_luaval<Chw3D_Camera>(tolua_S, "Chw3D_Camera",(Chw3D_Camera*)ret);
        return 1;
    }
    luaL_error(tolua_S, "%s has wrong number of arguments: %d, was expecting %d\n ", "Chw3D_Camera:create",argc, 0);
    return 0;
#if COCOS2D_DEBUG >= 1
    tolua_lerror:
    tolua_error(tolua_S,"#ferror in function 'lua_higame_3d_Chw3D_Camera_create'.",&tolua_err);
#endif
    return 0;
}
int lua_higame_3d_Chw3D_Camera_constructor(lua_State* tolua_S)
{
    int argc = 0;
    Chw3D_Camera* cobj = nullptr;
    bool ok  = true;

#if COCOS2D_DEBUG >= 1
    tolua_Error tolua_err;
#endif



    argc = lua_gettop(tolua_S)-1;
    if (argc == 0) 
    {
        if(!ok)
        {
            tolua_error(tolua_S,"invalid arguments in function 'lua_higame_3d_Chw3D_Camera_constructor'", nullptr);
            return 0;
        }
        cobj = new Chw3D_Camera();
        cobj->autorelease();
        int ID =  (int)cobj->_ID ;
        int* luaID =  &cobj->_luaID ;
        toluafix_pushusertype_ccobject(tolua_S, ID, luaID, (void*)cobj,"Chw3D_Camera");
        return 1;
    }
    luaL_error(tolua_S, "%s has wrong number of arguments: %d, was expecting %d \n", "Chw3D_Camera:Chw3D_Camera",argc, 0);
    return 0;

#if COCOS2D_DEBUG >= 1
    tolua_error(tolua_S,"#ferror in function 'lua_higame_3d_Chw3D_Camera_constructor'.",&tolua_err);
#endif

    return 0;
}

static int lua_higame_3d_Chw3D_Camera_finalize(lua_State* tolua_S)
{
    printf("luabindings: finalizing LUA object (Chw3D_Camera)");
    return 0;
}

int lua_register_higame_3d_Chw3D_Camera(lua_State* tolua_S)
{
    tolua_usertype(tolua_S,"Chw3D_Camera");
    tolua_cclass(tolua_S,"Chw3D_Camera","Chw3D_Camera","cc.Node",nullptr);

    tolua_beginmodule(tolua_S,"Chw3D_Camera");
        tolua_function(tolua_S,"new",lua_higame_3d_Chw3D_Camera_constructor);
        tolua_function(tolua_S,"Load",lua_higame_3d_Chw3D_Camera_Load);
        tolua_function(tolua_S,"setDepthRatio",lua_higame_3d_Chw3D_Camera_setDepthRatio);
        tolua_function(tolua_S,"Set",lua_higame_3d_Chw3D_Camera_Set);
        tolua_function(tolua_S,"getViewProjectionMatrix",lua_higame_3d_Chw3D_Camera_getViewProjectionMatrix);
        tolua_function(tolua_S,"isEnabelGuiDepth",lua_higame_3d_Chw3D_Camera_isEnabelGuiDepth);
        tolua_function(tolua_S,"unproject",lua_higame_3d_Chw3D_Camera_unproject);
        tolua_function(tolua_S,"ZDir",lua_higame_3d_Chw3D_Camera_ZDir);
        tolua_function(tolua_S,"getDepthRatio",lua_higame_3d_Chw3D_Camera_getDepthRatio);
        tolua_function(tolua_S,"project",lua_higame_3d_Chw3D_Camera_project);
        tolua_function(tolua_S,"XDir",lua_higame_3d_Chw3D_Camera_XDir);
        tolua_function(tolua_S,"unprojectGL",lua_higame_3d_Chw3D_Camera_unprojectGL);
        tolua_function(tolua_S,"Ortho",lua_higame_3d_Chw3D_Camera_Ortho);
        tolua_function(tolua_S,"screenPointToWorldPoint",lua_higame_3d_Chw3D_Camera_screenPointToWorldPoint);
        tolua_function(tolua_S,"Perspective",lua_higame_3d_Chw3D_Camera_Perspective);
        tolua_function(tolua_S,"setEnabelGuiDepth",lua_higame_3d_Chw3D_Camera_setEnabelGuiDepth);
        tolua_function(tolua_S,"Active",lua_higame_3d_Chw3D_Camera_Active);
        tolua_function(tolua_S,"YDir",lua_higame_3d_Chw3D_Camera_YDir);
        tolua_function(tolua_S,"Inactive",lua_higame_3d_Chw3D_Camera_Inactive);
        tolua_function(tolua_S,"projectGL",lua_higame_3d_Chw3D_Camera_projectGL);
        tolua_function(tolua_S,"create", lua_higame_3d_Chw3D_Camera_create);
    tolua_endmodule(tolua_S);
    std::string typeName = typeid(Chw3D_Camera).name();
    g_luaType[typeName] = "Chw3D_Camera";
    g_typeCast["Chw3D_Camera"] = "Chw3D_Camera";
    return 1;
}

int lua_higame_3d_Chw3D_Sprite_Scale(lua_State* tolua_S)
{
    int argc = 0;
    Chw3D_Sprite* cobj = nullptr;
    bool ok  = true;

#if COCOS2D_DEBUG >= 1
    tolua_Error tolua_err;
#endif


#if COCOS2D_DEBUG >= 1
    if (!tolua_isusertype(tolua_S,1,"Chw3D_Sprite",0,&tolua_err)) goto tolua_lerror;
#endif

    cobj = (Chw3D_Sprite*)tolua_tousertype(tolua_S,1,0);

#if COCOS2D_DEBUG >= 1
    if (!cobj) 
    {
        tolua_error(tolua_S,"invalid 'cobj' in function 'lua_higame_3d_Chw3D_Sprite_Scale'", nullptr);
        return 0;
    }
#endif

    argc = lua_gettop(tolua_S)-1;
    if (argc == 1) 
    {
        double arg0;

        ok &= luaval_to_number(tolua_S, 2,&arg0, "Chw3D_Sprite:Scale");
        if(!ok)
        {
            tolua_error(tolua_S,"invalid arguments in function 'lua_higame_3d_Chw3D_Sprite_Scale'", nullptr);
            return 0;
        }
        cobj->Scale(arg0);
        lua_settop(tolua_S, 1);
        return 1;
    }
    luaL_error(tolua_S, "%s has wrong number of arguments: %d, was expecting %d \n", "Chw3D_Sprite:Scale",argc, 1);
    return 0;

#if COCOS2D_DEBUG >= 1
    tolua_lerror:
    tolua_error(tolua_S,"#ferror in function 'lua_higame_3d_Chw3D_Sprite_Scale'.",&tolua_err);
#endif

    return 0;
}
int lua_higame_3d_Chw3D_Sprite_Create(lua_State* tolua_S)
{
    int argc = 0;
    Chw3D_Sprite* cobj = nullptr;
    bool ok  = true;

#if COCOS2D_DEBUG >= 1
    tolua_Error tolua_err;
#endif


#if COCOS2D_DEBUG >= 1
    if (!tolua_isusertype(tolua_S,1,"Chw3D_Sprite",0,&tolua_err)) goto tolua_lerror;
#endif

    cobj = (Chw3D_Sprite*)tolua_tousertype(tolua_S,1,0);

#if COCOS2D_DEBUG >= 1
    if (!cobj) 
    {
        tolua_error(tolua_S,"invalid 'cobj' in function 'lua_higame_3d_Chw3D_Sprite_Create'", nullptr);
        return 0;
    }
#endif

    argc = lua_gettop(tolua_S)-1;
    if (argc == 2) 
    {
        const char* arg0;
        const char* arg1;

        std::string arg0_tmp; ok &= luaval_to_std_string(tolua_S, 2, &arg0_tmp, "Chw3D_Sprite:Create"); arg0 = arg0_tmp.c_str();

        std::string arg1_tmp; ok &= luaval_to_std_string(tolua_S, 3, &arg1_tmp, "Chw3D_Sprite:Create"); arg1 = arg1_tmp.c_str();
        if(!ok)
        {
            tolua_error(tolua_S,"invalid arguments in function 'lua_higame_3d_Chw3D_Sprite_Create'", nullptr);
            return 0;
        }
        bool ret = cobj->Create(arg0, arg1);
        tolua_pushboolean(tolua_S,(bool)ret);
        return 1;
    }
    if (argc == 3) 
    {
        const char* arg0;
        const char* arg1;
        bool arg2;

        std::string arg0_tmp; ok &= luaval_to_std_string(tolua_S, 2, &arg0_tmp, "Chw3D_Sprite:Create"); arg0 = arg0_tmp.c_str();

        std::string arg1_tmp; ok &= luaval_to_std_string(tolua_S, 3, &arg1_tmp, "Chw3D_Sprite:Create"); arg1 = arg1_tmp.c_str();

        ok &= luaval_to_boolean(tolua_S, 4,&arg2, "Chw3D_Sprite:Create");
        if(!ok)
        {
            tolua_error(tolua_S,"invalid arguments in function 'lua_higame_3d_Chw3D_Sprite_Create'", nullptr);
            return 0;
        }
        bool ret = cobj->Create(arg0, arg1, arg2);
        tolua_pushboolean(tolua_S,(bool)ret);
        return 1;
    }
    luaL_error(tolua_S, "%s has wrong number of arguments: %d, was expecting %d \n", "Chw3D_Sprite:Create",argc, 2);
    return 0;

#if COCOS2D_DEBUG >= 1
    tolua_lerror:
    tolua_error(tolua_S,"#ferror in function 'lua_higame_3d_Chw3D_Sprite_Create'.",&tolua_err);
#endif

    return 0;
}
int lua_higame_3d_Chw3D_Sprite_loadDefault(lua_State* tolua_S)
{
    int argc = 0;
    Chw3D_Sprite* cobj = nullptr;
    bool ok  = true;

#if COCOS2D_DEBUG >= 1
    tolua_Error tolua_err;
#endif


#if COCOS2D_DEBUG >= 1
    if (!tolua_isusertype(tolua_S,1,"Chw3D_Sprite",0,&tolua_err)) goto tolua_lerror;
#endif

    cobj = (Chw3D_Sprite*)tolua_tousertype(tolua_S,1,0);

#if COCOS2D_DEBUG >= 1
    if (!cobj) 
    {
        tolua_error(tolua_S,"invalid 'cobj' in function 'lua_higame_3d_Chw3D_Sprite_loadDefault'", nullptr);
        return 0;
    }
#endif

    argc = lua_gettop(tolua_S)-1;
    if (argc == 0) 
    {
        if(!ok)
        {
            tolua_error(tolua_S,"invalid arguments in function 'lua_higame_3d_Chw3D_Sprite_loadDefault'", nullptr);
            return 0;
        }
        cobj->loadDefault();
        lua_settop(tolua_S, 1);
        return 1;
    }
    luaL_error(tolua_S, "%s has wrong number of arguments: %d, was expecting %d \n", "Chw3D_Sprite:loadDefault",argc, 0);
    return 0;

#if COCOS2D_DEBUG >= 1
    tolua_lerror:
    tolua_error(tolua_S,"#ferror in function 'lua_higame_3d_Chw3D_Sprite_loadDefault'.",&tolua_err);
#endif

    return 0;
}
int lua_higame_3d_Chw3D_Sprite_Attach(lua_State* tolua_S)
{
    int argc = 0;
    Chw3D_Sprite* cobj = nullptr;
    bool ok  = true;

#if COCOS2D_DEBUG >= 1
    tolua_Error tolua_err;
#endif


#if COCOS2D_DEBUG >= 1
    if (!tolua_isusertype(tolua_S,1,"Chw3D_Sprite",0,&tolua_err)) goto tolua_lerror;
#endif

    cobj = (Chw3D_Sprite*)tolua_tousertype(tolua_S,1,0);

#if COCOS2D_DEBUG >= 1
    if (!cobj) 
    {
        tolua_error(tolua_S,"invalid 'cobj' in function 'lua_higame_3d_Chw3D_Sprite_Attach'", nullptr);
        return 0;
    }
#endif

    argc = lua_gettop(tolua_S)-1;
    if (argc == 0) 
    {
        if(!ok)
        {
            tolua_error(tolua_S,"invalid arguments in function 'lua_higame_3d_Chw3D_Sprite_Attach'", nullptr);
            return 0;
        }
        cobj->Attach();
        lua_settop(tolua_S, 1);
        return 1;
    }
    if (argc == 1) 
    {
        Chw3D_Sprite* arg0;

        ok &= luaval_to_object<Chw3D_Sprite>(tolua_S, 2, "Chw3D_Sprite",&arg0, "Chw3D_Sprite:Attach");
        if(!ok)
        {
            tolua_error(tolua_S,"invalid arguments in function 'lua_higame_3d_Chw3D_Sprite_Attach'", nullptr);
            return 0;
        }
        cobj->Attach(arg0);
        lua_settop(tolua_S, 1);
        return 1;
    }
    if (argc == 2) 
    {
        Chw3D_Sprite* arg0;
        const char* arg1;

        ok &= luaval_to_object<Chw3D_Sprite>(tolua_S, 2, "Chw3D_Sprite",&arg0, "Chw3D_Sprite:Attach");

        std::string arg1_tmp; ok &= luaval_to_std_string(tolua_S, 3, &arg1_tmp, "Chw3D_Sprite:Attach"); arg1 = arg1_tmp.c_str();
        if(!ok)
        {
            tolua_error(tolua_S,"invalid arguments in function 'lua_higame_3d_Chw3D_Sprite_Attach'", nullptr);
            return 0;
        }
        cobj->Attach(arg0, arg1);
        lua_settop(tolua_S, 1);
        return 1;
    }
    luaL_error(tolua_S, "%s has wrong number of arguments: %d, was expecting %d \n", "Chw3D_Sprite:Attach",argc, 0);
    return 0;

#if COCOS2D_DEBUG >= 1
    tolua_lerror:
    tolua_error(tolua_S,"#ferror in function 'lua_higame_3d_Chw3D_Sprite_Attach'.",&tolua_err);
#endif

    return 0;
}
int lua_higame_3d_Chw3D_Sprite_LoadData(lua_State* tolua_S)
{
    int argc = 0;
    Chw3D_Sprite* cobj = nullptr;
    bool ok  = true;

#if COCOS2D_DEBUG >= 1
    tolua_Error tolua_err;
#endif


#if COCOS2D_DEBUG >= 1
    if (!tolua_isusertype(tolua_S,1,"Chw3D_Sprite",0,&tolua_err)) goto tolua_lerror;
#endif

    cobj = (Chw3D_Sprite*)tolua_tousertype(tolua_S,1,0);

#if COCOS2D_DEBUG >= 1
    if (!cobj) 
    {
        tolua_error(tolua_S,"invalid 'cobj' in function 'lua_higame_3d_Chw3D_Sprite_LoadData'", nullptr);
        return 0;
    }
#endif

    argc = lua_gettop(tolua_S)-1;
    if (argc == 2) 
    {
        const char* arg0;
        const char* arg1;

        std::string arg0_tmp; ok &= luaval_to_std_string(tolua_S, 2, &arg0_tmp, "Chw3D_Sprite:LoadData"); arg0 = arg0_tmp.c_str();

        std::string arg1_tmp; ok &= luaval_to_std_string(tolua_S, 3, &arg1_tmp, "Chw3D_Sprite:LoadData"); arg1 = arg1_tmp.c_str();
        if(!ok)
        {
            tolua_error(tolua_S,"invalid arguments in function 'lua_higame_3d_Chw3D_Sprite_LoadData'", nullptr);
            return 0;
        }
        bool ret = cobj->LoadData(arg0, arg1);
        tolua_pushboolean(tolua_S,(bool)ret);
        return 1;
    }
    if (argc == 3) 
    {
        const char* arg0;
        const char* arg1;
        bool arg2;

        std::string arg0_tmp; ok &= luaval_to_std_string(tolua_S, 2, &arg0_tmp, "Chw3D_Sprite:LoadData"); arg0 = arg0_tmp.c_str();

        std::string arg1_tmp; ok &= luaval_to_std_string(tolua_S, 3, &arg1_tmp, "Chw3D_Sprite:LoadData"); arg1 = arg1_tmp.c_str();

        ok &= luaval_to_boolean(tolua_S, 4,&arg2, "Chw3D_Sprite:LoadData");
        if(!ok)
        {
            tolua_error(tolua_S,"invalid arguments in function 'lua_higame_3d_Chw3D_Sprite_LoadData'", nullptr);
            return 0;
        }
        bool ret = cobj->LoadData(arg0, arg1, arg2);
        tolua_pushboolean(tolua_S,(bool)ret);
        return 1;
    }
    luaL_error(tolua_S, "%s has wrong number of arguments: %d, was expecting %d \n", "Chw3D_Sprite:LoadData",argc, 2);
    return 0;

#if COCOS2D_DEBUG >= 1
    tolua_lerror:
    tolua_error(tolua_S,"#ferror in function 'lua_higame_3d_Chw3D_Sprite_LoadData'.",&tolua_err);
#endif

    return 0;
}
int lua_higame_3d_Chw3D_Sprite_Build(lua_State* tolua_S)
{
    int argc = 0;
    Chw3D_Sprite* cobj = nullptr;
    bool ok  = true;

#if COCOS2D_DEBUG >= 1
    tolua_Error tolua_err;
#endif


#if COCOS2D_DEBUG >= 1
    if (!tolua_isusertype(tolua_S,1,"Chw3D_Sprite",0,&tolua_err)) goto tolua_lerror;
#endif

    cobj = (Chw3D_Sprite*)tolua_tousertype(tolua_S,1,0);

#if COCOS2D_DEBUG >= 1
    if (!cobj) 
    {
        tolua_error(tolua_S,"invalid 'cobj' in function 'lua_higame_3d_Chw3D_Sprite_Build'", nullptr);
        return 0;
    }
#endif

    argc = lua_gettop(tolua_S)-1;
    if (argc == 2) 
    {
        double arg0;
        double arg1;

        ok &= luaval_to_number(tolua_S, 2,&arg0, "Chw3D_Sprite:Build");

        ok &= luaval_to_number(tolua_S, 3,&arg1, "Chw3D_Sprite:Build");
        if(!ok)
        {
            tolua_error(tolua_S,"invalid arguments in function 'lua_higame_3d_Chw3D_Sprite_Build'", nullptr);
            return 0;
        }
        bool ret = cobj->Build(arg0, arg1);
        tolua_pushboolean(tolua_S,(bool)ret);
        return 1;
    }
    luaL_error(tolua_S, "%s has wrong number of arguments: %d, was expecting %d \n", "Chw3D_Sprite:Build",argc, 2);
    return 0;

#if COCOS2D_DEBUG >= 1
    tolua_lerror:
    tolua_error(tolua_S,"#ferror in function 'lua_higame_3d_Chw3D_Sprite_Build'.",&tolua_err);
#endif

    return 0;
}
int lua_higame_3d_Chw3D_Sprite_removeAttachChild(lua_State* tolua_S)
{
    int argc = 0;
    Chw3D_Sprite* cobj = nullptr;
    bool ok  = true;

#if COCOS2D_DEBUG >= 1
    tolua_Error tolua_err;
#endif


#if COCOS2D_DEBUG >= 1
    if (!tolua_isusertype(tolua_S,1,"Chw3D_Sprite",0,&tolua_err)) goto tolua_lerror;
#endif

    cobj = (Chw3D_Sprite*)tolua_tousertype(tolua_S,1,0);

#if COCOS2D_DEBUG >= 1
    if (!cobj) 
    {
        tolua_error(tolua_S,"invalid 'cobj' in function 'lua_higame_3d_Chw3D_Sprite_removeAttachChild'", nullptr);
        return 0;
    }
#endif

    argc = lua_gettop(tolua_S)-1;
    if (argc == 1) 
    {
        Chw3D_Sprite* arg0;

        ok &= luaval_to_object<Chw3D_Sprite>(tolua_S, 2, "Chw3D_Sprite",&arg0, "Chw3D_Sprite:removeAttachChild");
        if(!ok)
        {
            tolua_error(tolua_S,"invalid arguments in function 'lua_higame_3d_Chw3D_Sprite_removeAttachChild'", nullptr);
            return 0;
        }
        cobj->removeAttachChild(arg0);
        lua_settop(tolua_S, 1);
        return 1;
    }
    luaL_error(tolua_S, "%s has wrong number of arguments: %d, was expecting %d \n", "Chw3D_Sprite:removeAttachChild",argc, 1);
    return 0;

#if COCOS2D_DEBUG >= 1
    tolua_lerror:
    tolua_error(tolua_S,"#ferror in function 'lua_higame_3d_Chw3D_Sprite_removeAttachChild'.",&tolua_err);
#endif

    return 0;
}
int lua_higame_3d_Chw3D_Sprite_isC3A3FIleExist(lua_State* tolua_S)
{
    int argc = 0;
    Chw3D_Sprite* cobj = nullptr;
    bool ok  = true;

#if COCOS2D_DEBUG >= 1
    tolua_Error tolua_err;
#endif


#if COCOS2D_DEBUG >= 1
    if (!tolua_isusertype(tolua_S,1,"Chw3D_Sprite",0,&tolua_err)) goto tolua_lerror;
#endif

    cobj = (Chw3D_Sprite*)tolua_tousertype(tolua_S,1,0);

#if COCOS2D_DEBUG >= 1
    if (!cobj) 
    {
        tolua_error(tolua_S,"invalid 'cobj' in function 'lua_higame_3d_Chw3D_Sprite_isC3A3FIleExist'", nullptr);
        return 0;
    }
#endif

    argc = lua_gettop(tolua_S)-1;
    if (argc == 0) 
    {
        if(!ok)
        {
            tolua_error(tolua_S,"invalid arguments in function 'lua_higame_3d_Chw3D_Sprite_isC3A3FIleExist'", nullptr);
            return 0;
        }
        bool ret = cobj->isC3A3FIleExist();
        tolua_pushboolean(tolua_S,(bool)ret);
        return 1;
    }
    luaL_error(tolua_S, "%s has wrong number of arguments: %d, was expecting %d \n", "Chw3D_Sprite:isC3A3FIleExist",argc, 0);
    return 0;

#if COCOS2D_DEBUG >= 1
    tolua_lerror:
    tolua_error(tolua_S,"#ferror in function 'lua_higame_3d_Chw3D_Sprite_isC3A3FIleExist'.",&tolua_err);
#endif

    return 0;
}
int lua_higame_3d_Chw3D_Sprite_Translate(lua_State* tolua_S)
{
    int argc = 0;
    Chw3D_Sprite* cobj = nullptr;
    bool ok  = true;

#if COCOS2D_DEBUG >= 1
    tolua_Error tolua_err;
#endif


#if COCOS2D_DEBUG >= 1
    if (!tolua_isusertype(tolua_S,1,"Chw3D_Sprite",0,&tolua_err)) goto tolua_lerror;
#endif

    cobj = (Chw3D_Sprite*)tolua_tousertype(tolua_S,1,0);

#if COCOS2D_DEBUG >= 1
    if (!cobj) 
    {
        tolua_error(tolua_S,"invalid 'cobj' in function 'lua_higame_3d_Chw3D_Sprite_Translate'", nullptr);
        return 0;
    }
#endif

    argc = lua_gettop(tolua_S)-1;
    if (argc == 3) 
    {
        double arg0;
        double arg1;
        double arg2;

        ok &= luaval_to_number(tolua_S, 2,&arg0, "Chw3D_Sprite:Translate");

        ok &= luaval_to_number(tolua_S, 3,&arg1, "Chw3D_Sprite:Translate");

        ok &= luaval_to_number(tolua_S, 4,&arg2, "Chw3D_Sprite:Translate");
        if(!ok)
        {
            tolua_error(tolua_S,"invalid arguments in function 'lua_higame_3d_Chw3D_Sprite_Translate'", nullptr);
            return 0;
        }
        cobj->Translate(arg0, arg1, arg2);
        lua_settop(tolua_S, 1);
        return 1;
    }
    luaL_error(tolua_S, "%s has wrong number of arguments: %d, was expecting %d \n", "Chw3D_Sprite:Translate",argc, 3);
    return 0;

#if COCOS2D_DEBUG >= 1
    tolua_lerror:
    tolua_error(tolua_S,"#ferror in function 'lua_higame_3d_Chw3D_Sprite_Translate'.",&tolua_err);
#endif

    return 0;
}
int lua_higame_3d_Chw3D_Sprite_setSkipDepthTest(lua_State* tolua_S)
{
    int argc = 0;
    Chw3D_Sprite* cobj = nullptr;
    bool ok  = true;

#if COCOS2D_DEBUG >= 1
    tolua_Error tolua_err;
#endif


#if COCOS2D_DEBUG >= 1
    if (!tolua_isusertype(tolua_S,1,"Chw3D_Sprite",0,&tolua_err)) goto tolua_lerror;
#endif

    cobj = (Chw3D_Sprite*)tolua_tousertype(tolua_S,1,0);

#if COCOS2D_DEBUG >= 1
    if (!cobj) 
    {
        tolua_error(tolua_S,"invalid 'cobj' in function 'lua_higame_3d_Chw3D_Sprite_setSkipDepthTest'", nullptr);
        return 0;
    }
#endif

    argc = lua_gettop(tolua_S)-1;
    if (argc == 1) 
    {
        bool arg0;

        ok &= luaval_to_boolean(tolua_S, 2,&arg0, "Chw3D_Sprite:setSkipDepthTest");
        if(!ok)
        {
            tolua_error(tolua_S,"invalid arguments in function 'lua_higame_3d_Chw3D_Sprite_setSkipDepthTest'", nullptr);
            return 0;
        }
        cobj->setSkipDepthTest(arg0);
        lua_settop(tolua_S, 1);
        return 1;
    }
    luaL_error(tolua_S, "%s has wrong number of arguments: %d, was expecting %d \n", "Chw3D_Sprite:setSkipDepthTest",argc, 1);
    return 0;

#if COCOS2D_DEBUG >= 1
    tolua_lerror:
    tolua_error(tolua_S,"#ferror in function 'lua_higame_3d_Chw3D_Sprite_setSkipDepthTest'.",&tolua_err);
#endif

    return 0;
}
int lua_higame_3d_Chw3D_Sprite_AddAction(lua_State* tolua_S)
{
    int argc = 0;
    Chw3D_Sprite* cobj = nullptr;
    bool ok  = true;

#if COCOS2D_DEBUG >= 1
    tolua_Error tolua_err;
#endif


#if COCOS2D_DEBUG >= 1
    if (!tolua_isusertype(tolua_S,1,"Chw3D_Sprite",0,&tolua_err)) goto tolua_lerror;
#endif

    cobj = (Chw3D_Sprite*)tolua_tousertype(tolua_S,1,0);

#if COCOS2D_DEBUG >= 1
    if (!cobj) 
    {
        tolua_error(tolua_S,"invalid 'cobj' in function 'lua_higame_3d_Chw3D_Sprite_AddAction'", nullptr);
        return 0;
    }
#endif

    argc = lua_gettop(tolua_S)-1;
    if (argc == 3) 
    {
        unsigned int arg0;
        unsigned int arg1;
        unsigned int arg2;

        ok &= luaval_to_uint32(tolua_S, 2,&arg0, "Chw3D_Sprite:AddAction");

        ok &= luaval_to_uint32(tolua_S, 3,&arg1, "Chw3D_Sprite:AddAction");

        ok &= luaval_to_uint32(tolua_S, 4,&arg2, "Chw3D_Sprite:AddAction");
        if(!ok)
        {
            tolua_error(tolua_S,"invalid arguments in function 'lua_higame_3d_Chw3D_Sprite_AddAction'", nullptr);
            return 0;
        }
        bool ret = cobj->AddAction(arg0, arg1, arg2);
        tolua_pushboolean(tolua_S,(bool)ret);
        return 1;
    }
    luaL_error(tolua_S, "%s has wrong number of arguments: %d, was expecting %d \n", "Chw3D_Sprite:AddAction",argc, 3);
    return 0;

#if COCOS2D_DEBUG >= 1
    tolua_lerror:
    tolua_error(tolua_S,"#ferror in function 'lua_higame_3d_Chw3D_Sprite_AddAction'.",&tolua_err);
#endif

    return 0;
}
int lua_higame_3d_Chw3D_Sprite_SetRepeatAB(lua_State* tolua_S)
{
    int argc = 0;
    Chw3D_Sprite* cobj = nullptr;
    bool ok  = true;
#if COCOS2D_DEBUG >= 1
    tolua_Error tolua_err;
#endif

#if COCOS2D_DEBUG >= 1
    if (!tolua_isusertype(tolua_S,1,"Chw3D_Sprite",0,&tolua_err)) goto tolua_lerror;
#endif
    cobj = (Chw3D_Sprite*)tolua_tousertype(tolua_S,1,0);
#if COCOS2D_DEBUG >= 1
    if (!cobj)
    {
        tolua_error(tolua_S,"invalid 'cobj' in function 'lua_higame_3d_Chw3D_Sprite_SetRepeatAB'", nullptr);
        return 0;
    }
#endif
    argc = lua_gettop(tolua_S)-1;
    do{
        if (argc == 2) {
            int arg0;
            ok &= luaval_to_int32(tolua_S, 2,(int *)&arg0, "Chw3D_Sprite:SetRepeatAB");

            if (!ok) { break; }
            int arg1;
            ok &= luaval_to_int32(tolua_S, 3,(int *)&arg1, "Chw3D_Sprite:SetRepeatAB");

            if (!ok) { break; }
            cobj->SetRepeatAB(arg0, arg1);
            lua_settop(tolua_S, 1);
            return 1;
        }
    }while(0);
    ok  = true;
    do{
        if (argc == 0) {
            cobj->SetRepeatAB();
            lua_settop(tolua_S, 1);
            return 1;
        }
    }while(0);
    ok  = true;
    do{
        if (argc == 1) {
            bool arg0;
            ok &= luaval_to_boolean(tolua_S, 2,&arg0, "Chw3D_Sprite:SetRepeatAB");

            if (!ok) { break; }
            cobj->SetRepeatAB(arg0);
            lua_settop(tolua_S, 1);
            return 1;
        }
    }while(0);
    ok  = true;
    luaL_error(tolua_S, "%s has wrong number of arguments: %d, was expecting %d \n",  "Chw3D_Sprite:SetRepeatAB",argc, 0);
    return 0;

#if COCOS2D_DEBUG >= 1
    tolua_lerror:
    tolua_error(tolua_S,"#ferror in function 'lua_higame_3d_Chw3D_Sprite_SetRepeatAB'.",&tolua_err);
#endif

    return 0;
}
int lua_higame_3d_Chw3D_Sprite_FinialMatrix(lua_State* tolua_S)
{
    int argc = 0;
    Chw3D_Sprite* cobj = nullptr;
    bool ok  = true;

#if COCOS2D_DEBUG >= 1
    tolua_Error tolua_err;
#endif


#if COCOS2D_DEBUG >= 1
    if (!tolua_isusertype(tolua_S,1,"Chw3D_Sprite",0,&tolua_err)) goto tolua_lerror;
#endif

    cobj = (Chw3D_Sprite*)tolua_tousertype(tolua_S,1,0);

#if COCOS2D_DEBUG >= 1
    if (!cobj) 
    {
        tolua_error(tolua_S,"invalid 'cobj' in function 'lua_higame_3d_Chw3D_Sprite_FinialMatrix'", nullptr);
        return 0;
    }
#endif

    argc = lua_gettop(tolua_S)-1;
    if (argc == 2) 
    {
        unsigned int arg0;
        cocos2d::Mat4* arg1;

        ok &= luaval_to_uint32(tolua_S, 2,&arg0, "Chw3D_Sprite:FinialMatrix");

        ok &= luaval_to_object<cocos2d::Mat4>(tolua_S, 3, "cc.Mat4",&arg1, "Chw3D_Sprite:FinialMatrix");
        if(!ok)
        {
            tolua_error(tolua_S,"invalid arguments in function 'lua_higame_3d_Chw3D_Sprite_FinialMatrix'", nullptr);
            return 0;
        }
        cobj->FinialMatrix(arg0, arg1);
        lua_settop(tolua_S, 1);
        return 1;
    }
    luaL_error(tolua_S, "%s has wrong number of arguments: %d, was expecting %d \n", "Chw3D_Sprite:FinialMatrix",argc, 2);
    return 0;

#if COCOS2D_DEBUG >= 1
    tolua_lerror:
    tolua_error(tolua_S,"#ferror in function 'lua_higame_3d_Chw3D_Sprite_FinialMatrix'.",&tolua_err);
#endif

    return 0;
}
int lua_higame_3d_Chw3D_Sprite_CleanActionGroup(lua_State* tolua_S)
{
    int argc = 0;
    Chw3D_Sprite* cobj = nullptr;
    bool ok  = true;

#if COCOS2D_DEBUG >= 1
    tolua_Error tolua_err;
#endif


#if COCOS2D_DEBUG >= 1
    if (!tolua_isusertype(tolua_S,1,"Chw3D_Sprite",0,&tolua_err)) goto tolua_lerror;
#endif

    cobj = (Chw3D_Sprite*)tolua_tousertype(tolua_S,1,0);

#if COCOS2D_DEBUG >= 1
    if (!cobj) 
    {
        tolua_error(tolua_S,"invalid 'cobj' in function 'lua_higame_3d_Chw3D_Sprite_CleanActionGroup'", nullptr);
        return 0;
    }
#endif

    argc = lua_gettop(tolua_S)-1;
    if (argc == 0) 
    {
        if(!ok)
        {
            tolua_error(tolua_S,"invalid arguments in function 'lua_higame_3d_Chw3D_Sprite_CleanActionGroup'", nullptr);
            return 0;
        }
        bool ret = cobj->CleanActionGroup();
        tolua_pushboolean(tolua_S,(bool)ret);
        return 1;
    }
    luaL_error(tolua_S, "%s has wrong number of arguments: %d, was expecting %d \n", "Chw3D_Sprite:CleanActionGroup",argc, 0);
    return 0;

#if COCOS2D_DEBUG >= 1
    tolua_lerror:
    tolua_error(tolua_S,"#ferror in function 'lua_higame_3d_Chw3D_Sprite_CleanActionGroup'.",&tolua_err);
#endif

    return 0;
}
int lua_higame_3d_Chw3D_Sprite_addAttachChild(lua_State* tolua_S)
{
    int argc = 0;
    Chw3D_Sprite* cobj = nullptr;
    bool ok  = true;

#if COCOS2D_DEBUG >= 1
    tolua_Error tolua_err;
#endif


#if COCOS2D_DEBUG >= 1
    if (!tolua_isusertype(tolua_S,1,"Chw3D_Sprite",0,&tolua_err)) goto tolua_lerror;
#endif

    cobj = (Chw3D_Sprite*)tolua_tousertype(tolua_S,1,0);

#if COCOS2D_DEBUG >= 1
    if (!cobj) 
    {
        tolua_error(tolua_S,"invalid 'cobj' in function 'lua_higame_3d_Chw3D_Sprite_addAttachChild'", nullptr);
        return 0;
    }
#endif

    argc = lua_gettop(tolua_S)-1;
    if (argc == 1) 
    {
        Chw3D_Sprite* arg0;

        ok &= luaval_to_object<Chw3D_Sprite>(tolua_S, 2, "Chw3D_Sprite",&arg0, "Chw3D_Sprite:addAttachChild");
        if(!ok)
        {
            tolua_error(tolua_S,"invalid arguments in function 'lua_higame_3d_Chw3D_Sprite_addAttachChild'", nullptr);
            return 0;
        }
        cobj->addAttachChild(arg0);
        lua_settop(tolua_S, 1);
        return 1;
    }
    luaL_error(tolua_S, "%s has wrong number of arguments: %d, was expecting %d \n", "Chw3D_Sprite:addAttachChild",argc, 1);
    return 0;

#if COCOS2D_DEBUG >= 1
    tolua_lerror:
    tolua_error(tolua_S,"#ferror in function 'lua_higame_3d_Chw3D_Sprite_addAttachChild'.",&tolua_err);
#endif

    return 0;
}
int lua_higame_3d_Chw3D_Sprite_reloadFile(lua_State* tolua_S)
{
    int argc = 0;
    Chw3D_Sprite* cobj = nullptr;
    bool ok  = true;

#if COCOS2D_DEBUG >= 1
    tolua_Error tolua_err;
#endif


#if COCOS2D_DEBUG >= 1
    if (!tolua_isusertype(tolua_S,1,"Chw3D_Sprite",0,&tolua_err)) goto tolua_lerror;
#endif

    cobj = (Chw3D_Sprite*)tolua_tousertype(tolua_S,1,0);

#if COCOS2D_DEBUG >= 1
    if (!cobj) 
    {
        tolua_error(tolua_S,"invalid 'cobj' in function 'lua_higame_3d_Chw3D_Sprite_reloadFile'", nullptr);
        return 0;
    }
#endif

    argc = lua_gettop(tolua_S)-1;
    if (argc == 1) 
    {
        std::string arg0;

        ok &= luaval_to_std_string(tolua_S, 2,&arg0, "Chw3D_Sprite:reloadFile");
        if(!ok)
        {
            tolua_error(tolua_S,"invalid arguments in function 'lua_higame_3d_Chw3D_Sprite_reloadFile'", nullptr);
            return 0;
        }
        bool ret = cobj->reloadFile(arg0);
        tolua_pushboolean(tolua_S,(bool)ret);
        return 1;
    }
    luaL_error(tolua_S, "%s has wrong number of arguments: %d, was expecting %d \n", "Chw3D_Sprite:reloadFile",argc, 1);
    return 0;

#if COCOS2D_DEBUG >= 1
    tolua_lerror:
    tolua_error(tolua_S,"#ferror in function 'lua_higame_3d_Chw3D_Sprite_reloadFile'.",&tolua_err);
#endif

    return 0;
}
int lua_higame_3d_Chw3D_Sprite_GetActualHeight(lua_State* tolua_S)
{
    int argc = 0;
    Chw3D_Sprite* cobj = nullptr;
    bool ok  = true;

#if COCOS2D_DEBUG >= 1
    tolua_Error tolua_err;
#endif


#if COCOS2D_DEBUG >= 1
    if (!tolua_isusertype(tolua_S,1,"Chw3D_Sprite",0,&tolua_err)) goto tolua_lerror;
#endif

    cobj = (Chw3D_Sprite*)tolua_tousertype(tolua_S,1,0);

#if COCOS2D_DEBUG >= 1
    if (!cobj) 
    {
        tolua_error(tolua_S,"invalid 'cobj' in function 'lua_higame_3d_Chw3D_Sprite_GetActualHeight'", nullptr);
        return 0;
    }
#endif

    argc = lua_gettop(tolua_S)-1;
    if (argc == 0) 
    {
        if(!ok)
        {
            tolua_error(tolua_S,"invalid arguments in function 'lua_higame_3d_Chw3D_Sprite_GetActualHeight'", nullptr);
            return 0;
        }
        double ret = cobj->GetActualHeight();
        tolua_pushnumber(tolua_S,(lua_Number)ret);
        return 1;
    }
    luaL_error(tolua_S, "%s has wrong number of arguments: %d, was expecting %d \n", "Chw3D_Sprite:GetActualHeight",argc, 0);
    return 0;

#if COCOS2D_DEBUG >= 1
    tolua_lerror:
    tolua_error(tolua_S,"#ferror in function 'lua_higame_3d_Chw3D_Sprite_GetActualHeight'.",&tolua_err);
#endif

    return 0;
}
int lua_higame_3d_Chw3D_Sprite_GetRepeatABDuration(lua_State* tolua_S)
{
    int argc = 0;
    Chw3D_Sprite* cobj = nullptr;
    bool ok  = true;

#if COCOS2D_DEBUG >= 1
    tolua_Error tolua_err;
#endif


#if COCOS2D_DEBUG >= 1
    if (!tolua_isusertype(tolua_S,1,"Chw3D_Sprite",0,&tolua_err)) goto tolua_lerror;
#endif

    cobj = (Chw3D_Sprite*)tolua_tousertype(tolua_S,1,0);

#if COCOS2D_DEBUG >= 1
    if (!cobj) 
    {
        tolua_error(tolua_S,"invalid 'cobj' in function 'lua_higame_3d_Chw3D_Sprite_GetRepeatABDuration'", nullptr);
        return 0;
    }
#endif

    argc = lua_gettop(tolua_S)-1;
    if (argc == 0) 
    {
        if(!ok)
        {
            tolua_error(tolua_S,"invalid arguments in function 'lua_higame_3d_Chw3D_Sprite_GetRepeatABDuration'", nullptr);
            return 0;
        }
        double ret = cobj->GetRepeatABDuration();
        tolua_pushnumber(tolua_S,(lua_Number)ret);
        return 1;
    }
    luaL_error(tolua_S, "%s has wrong number of arguments: %d, was expecting %d \n", "Chw3D_Sprite:GetRepeatABDuration",argc, 0);
    return 0;

#if COCOS2D_DEBUG >= 1
    tolua_lerror:
    tolua_error(tolua_S,"#ferror in function 'lua_higame_3d_Chw3D_Sprite_GetRepeatABDuration'.",&tolua_err);
#endif

    return 0;
}
int lua_higame_3d_Chw3D_Sprite_onDraw(lua_State* tolua_S)
{
    int argc = 0;
    Chw3D_Sprite* cobj = nullptr;
    bool ok  = true;

#if COCOS2D_DEBUG >= 1
    tolua_Error tolua_err;
#endif


#if COCOS2D_DEBUG >= 1
    if (!tolua_isusertype(tolua_S,1,"Chw3D_Sprite",0,&tolua_err)) goto tolua_lerror;
#endif

    cobj = (Chw3D_Sprite*)tolua_tousertype(tolua_S,1,0);

#if COCOS2D_DEBUG >= 1
    if (!cobj) 
    {
        tolua_error(tolua_S,"invalid 'cobj' in function 'lua_higame_3d_Chw3D_Sprite_onDraw'", nullptr);
        return 0;
    }
#endif

    argc = lua_gettop(tolua_S)-1;
    if (argc == 1) 
    {
        bool arg0;

        ok &= luaval_to_boolean(tolua_S, 2,&arg0, "Chw3D_Sprite:onDraw");
        if(!ok)
        {
            tolua_error(tolua_S,"invalid arguments in function 'lua_higame_3d_Chw3D_Sprite_onDraw'", nullptr);
            return 0;
        }
        cobj->onDraw(arg0);
        lua_settop(tolua_S, 1);
        return 1;
    }
    luaL_error(tolua_S, "%s has wrong number of arguments: %d, was expecting %d \n", "Chw3D_Sprite:onDraw",argc, 1);
    return 0;

#if COCOS2D_DEBUG >= 1
    tolua_lerror:
    tolua_error(tolua_S,"#ferror in function 'lua_higame_3d_Chw3D_Sprite_onDraw'.",&tolua_err);
#endif

    return 0;
}
int lua_higame_3d_Chw3D_Sprite_SetCamera(lua_State* tolua_S)
{
    int argc = 0;
    Chw3D_Sprite* cobj = nullptr;
    bool ok  = true;

#if COCOS2D_DEBUG >= 1
    tolua_Error tolua_err;
#endif


#if COCOS2D_DEBUG >= 1
    if (!tolua_isusertype(tolua_S,1,"Chw3D_Sprite",0,&tolua_err)) goto tolua_lerror;
#endif

    cobj = (Chw3D_Sprite*)tolua_tousertype(tolua_S,1,0);

#if COCOS2D_DEBUG >= 1
    if (!cobj) 
    {
        tolua_error(tolua_S,"invalid 'cobj' in function 'lua_higame_3d_Chw3D_Sprite_SetCamera'", nullptr);
        return 0;
    }
#endif

    argc = lua_gettop(tolua_S)-1;
    if (argc == 1) 
    {
        Chw3D_Camera* arg0;

        ok &= luaval_to_object<Chw3D_Camera>(tolua_S, 2, "Chw3D_Camera",&arg0, "Chw3D_Sprite:SetCamera");
        if(!ok)
        {
            tolua_error(tolua_S,"invalid arguments in function 'lua_higame_3d_Chw3D_Sprite_SetCamera'", nullptr);
            return 0;
        }
        cobj->SetCamera(arg0);
        lua_settop(tolua_S, 1);
        return 1;
    }
    luaL_error(tolua_S, "%s has wrong number of arguments: %d, was expecting %d \n", "Chw3D_Sprite:SetCamera",argc, 1);
    return 0;

#if COCOS2D_DEBUG >= 1
    tolua_lerror:
    tolua_error(tolua_S,"#ferror in function 'lua_higame_3d_Chw3D_Sprite_SetCamera'.",&tolua_err);
#endif

    return 0;
}
int lua_higame_3d_Chw3D_Sprite_draw(lua_State* tolua_S)
{
    int argc = 0;
    Chw3D_Sprite* cobj = nullptr;
    bool ok  = true;

#if COCOS2D_DEBUG >= 1
    tolua_Error tolua_err;
#endif


#if COCOS2D_DEBUG >= 1
    if (!tolua_isusertype(tolua_S,1,"Chw3D_Sprite",0,&tolua_err)) goto tolua_lerror;
#endif

    cobj = (Chw3D_Sprite*)tolua_tousertype(tolua_S,1,0);

#if COCOS2D_DEBUG >= 1
    if (!cobj) 
    {
        tolua_error(tolua_S,"invalid 'cobj' in function 'lua_higame_3d_Chw3D_Sprite_draw'", nullptr);
        return 0;
    }
#endif

    argc = lua_gettop(tolua_S)-1;
    if (argc == 3) 
    {
        cocos2d::Renderer* arg0;
        cocos2d::Mat4 arg1;
        unsigned int arg2;

        ok &= luaval_to_object<cocos2d::Renderer>(tolua_S, 2, "cc.Renderer",&arg0, "Chw3D_Sprite:draw");

        ok &= luaval_to_mat4(tolua_S, 3, &arg1, "Chw3D_Sprite:draw");

        ok &= luaval_to_uint32(tolua_S, 4,&arg2, "Chw3D_Sprite:draw");
        if(!ok)
        {
            tolua_error(tolua_S,"invalid arguments in function 'lua_higame_3d_Chw3D_Sprite_draw'", nullptr);
            return 0;
        }
        cobj->draw(arg0, arg1, arg2);
        lua_settop(tolua_S, 1);
        return 1;
    }
    luaL_error(tolua_S, "%s has wrong number of arguments: %d, was expecting %d \n", "Chw3D_Sprite:draw",argc, 3);
    return 0;

#if COCOS2D_DEBUG >= 1
    tolua_lerror:
    tolua_error(tolua_S,"#ferror in function 'lua_higame_3d_Chw3D_Sprite_draw'.",&tolua_err);
#endif

    return 0;
}
int lua_higame_3d_Chw3D_Sprite_Rotate(lua_State* tolua_S)
{
    int argc = 0;
    Chw3D_Sprite* cobj = nullptr;
    bool ok  = true;

#if COCOS2D_DEBUG >= 1
    tolua_Error tolua_err;
#endif


#if COCOS2D_DEBUG >= 1
    if (!tolua_isusertype(tolua_S,1,"Chw3D_Sprite",0,&tolua_err)) goto tolua_lerror;
#endif

    cobj = (Chw3D_Sprite*)tolua_tousertype(tolua_S,1,0);

#if COCOS2D_DEBUG >= 1
    if (!cobj) 
    {
        tolua_error(tolua_S,"invalid 'cobj' in function 'lua_higame_3d_Chw3D_Sprite_Rotate'", nullptr);
        return 0;
    }
#endif

    argc = lua_gettop(tolua_S)-1;
    if (argc == 4) 
    {
        double arg0;
        double arg1;
        double arg2;
        double arg3;

        ok &= luaval_to_number(tolua_S, 2,&arg0, "Chw3D_Sprite:Rotate");

        ok &= luaval_to_number(tolua_S, 3,&arg1, "Chw3D_Sprite:Rotate");

        ok &= luaval_to_number(tolua_S, 4,&arg2, "Chw3D_Sprite:Rotate");

        ok &= luaval_to_number(tolua_S, 5,&arg3, "Chw3D_Sprite:Rotate");
        if(!ok)
        {
            tolua_error(tolua_S,"invalid arguments in function 'lua_higame_3d_Chw3D_Sprite_Rotate'", nullptr);
            return 0;
        }
        cobj->Rotate(arg0, arg1, arg2, arg3);
        lua_settop(tolua_S, 1);
        return 1;
    }
    luaL_error(tolua_S, "%s has wrong number of arguments: %d, was expecting %d \n", "Chw3D_Sprite:Rotate",argc, 4);
    return 0;

#if COCOS2D_DEBUG >= 1
    tolua_lerror:
    tolua_error(tolua_S,"#ferror in function 'lua_higame_3d_Chw3D_Sprite_Rotate'.",&tolua_err);
#endif

    return 0;
}
int lua_higame_3d_Chw3D_Sprite_isInit(lua_State* tolua_S)
{
    int argc = 0;
    Chw3D_Sprite* cobj = nullptr;
    bool ok  = true;

#if COCOS2D_DEBUG >= 1
    tolua_Error tolua_err;
#endif


#if COCOS2D_DEBUG >= 1
    if (!tolua_isusertype(tolua_S,1,"Chw3D_Sprite",0,&tolua_err)) goto tolua_lerror;
#endif

    cobj = (Chw3D_Sprite*)tolua_tousertype(tolua_S,1,0);

#if COCOS2D_DEBUG >= 1
    if (!cobj) 
    {
        tolua_error(tolua_S,"invalid 'cobj' in function 'lua_higame_3d_Chw3D_Sprite_isInit'", nullptr);
        return 0;
    }
#endif

    argc = lua_gettop(tolua_S)-1;
    if (argc == 0) 
    {
        if(!ok)
        {
            tolua_error(tolua_S,"invalid arguments in function 'lua_higame_3d_Chw3D_Sprite_isInit'", nullptr);
            return 0;
        }
        bool ret = cobj->isInit();
        tolua_pushboolean(tolua_S,(bool)ret);
        return 1;
    }
    luaL_error(tolua_S, "%s has wrong number of arguments: %d, was expecting %d \n", "Chw3D_Sprite:isInit",argc, 0);
    return 0;

#if COCOS2D_DEBUG >= 1
    tolua_lerror:
    tolua_error(tolua_S,"#ferror in function 'lua_higame_3d_Chw3D_Sprite_isInit'.",&tolua_err);
#endif

    return 0;
}
int lua_higame_3d_Chw3D_Sprite_update(lua_State* tolua_S)
{
    int argc = 0;
    Chw3D_Sprite* cobj = nullptr;
    bool ok  = true;

#if COCOS2D_DEBUG >= 1
    tolua_Error tolua_err;
#endif


#if COCOS2D_DEBUG >= 1
    if (!tolua_isusertype(tolua_S,1,"Chw3D_Sprite",0,&tolua_err)) goto tolua_lerror;
#endif

    cobj = (Chw3D_Sprite*)tolua_tousertype(tolua_S,1,0);

#if COCOS2D_DEBUG >= 1
    if (!cobj) 
    {
        tolua_error(tolua_S,"invalid 'cobj' in function 'lua_higame_3d_Chw3D_Sprite_update'", nullptr);
        return 0;
    }
#endif

    argc = lua_gettop(tolua_S)-1;
    if (argc == 1) 
    {
        double arg0;

        ok &= luaval_to_number(tolua_S, 2,&arg0, "Chw3D_Sprite:update");
        if(!ok)
        {
            tolua_error(tolua_S,"invalid arguments in function 'lua_higame_3d_Chw3D_Sprite_update'", nullptr);
            return 0;
        }
        cobj->update(arg0);
        lua_settop(tolua_S, 1);
        return 1;
    }
    luaL_error(tolua_S, "%s has wrong number of arguments: %d, was expecting %d \n", "Chw3D_Sprite:update",argc, 1);
    return 0;

#if COCOS2D_DEBUG >= 1
    tolua_lerror:
    tolua_error(tolua_S,"#ferror in function 'lua_higame_3d_Chw3D_Sprite_update'.",&tolua_err);
#endif

    return 0;
}
int lua_higame_3d_Chw3D_Sprite_CreateAsync(lua_State* tolua_S)
{
    int argc = 0;
    Chw3D_Sprite* cobj = nullptr;
    bool ok  = true;

#if COCOS2D_DEBUG >= 1
    tolua_Error tolua_err;
#endif


#if COCOS2D_DEBUG >= 1
    if (!tolua_isusertype(tolua_S,1,"Chw3D_Sprite",0,&tolua_err)) goto tolua_lerror;
#endif

    cobj = (Chw3D_Sprite*)tolua_tousertype(tolua_S,1,0);

#if COCOS2D_DEBUG >= 1
    if (!cobj) 
    {
        tolua_error(tolua_S,"invalid 'cobj' in function 'lua_higame_3d_Chw3D_Sprite_CreateAsync'", nullptr);
        return 0;
    }
#endif

    argc = lua_gettop(tolua_S)-1;
    if (argc == 2) 
    {
        const char* arg0;
        const char* arg1;

        std::string arg0_tmp; ok &= luaval_to_std_string(tolua_S, 2, &arg0_tmp, "Chw3D_Sprite:CreateAsync"); arg0 = arg0_tmp.c_str();

        std::string arg1_tmp; ok &= luaval_to_std_string(tolua_S, 3, &arg1_tmp, "Chw3D_Sprite:CreateAsync"); arg1 = arg1_tmp.c_str();
        if(!ok)
        {
            tolua_error(tolua_S,"invalid arguments in function 'lua_higame_3d_Chw3D_Sprite_CreateAsync'", nullptr);
            return 0;
        }
        cobj->CreateAsync(arg0, arg1);
        lua_settop(tolua_S, 1);
        return 1;
    }
    if (argc == 3) 
    {
        const char* arg0;
        const char* arg1;
        bool arg2;

        std::string arg0_tmp; ok &= luaval_to_std_string(tolua_S, 2, &arg0_tmp, "Chw3D_Sprite:CreateAsync"); arg0 = arg0_tmp.c_str();

        std::string arg1_tmp; ok &= luaval_to_std_string(tolua_S, 3, &arg1_tmp, "Chw3D_Sprite:CreateAsync"); arg1 = arg1_tmp.c_str();

        ok &= luaval_to_boolean(tolua_S, 4,&arg2, "Chw3D_Sprite:CreateAsync");
        if(!ok)
        {
            tolua_error(tolua_S,"invalid arguments in function 'lua_higame_3d_Chw3D_Sprite_CreateAsync'", nullptr);
            return 0;
        }
        cobj->CreateAsync(arg0, arg1, arg2);
        lua_settop(tolua_S, 1);
        return 1;
    }
    luaL_error(tolua_S, "%s has wrong number of arguments: %d, was expecting %d \n", "Chw3D_Sprite:CreateAsync",argc, 2);
    return 0;

#if COCOS2D_DEBUG >= 1
    tolua_lerror:
    tolua_error(tolua_S,"#ferror in function 'lua_higame_3d_Chw3D_Sprite_CreateAsync'.",&tolua_err);
#endif

    return 0;
}
int lua_higame_3d_Chw3D_Sprite_isSkipDepthTest(lua_State* tolua_S)
{
    int argc = 0;
    Chw3D_Sprite* cobj = nullptr;
    bool ok  = true;

#if COCOS2D_DEBUG >= 1
    tolua_Error tolua_err;
#endif


#if COCOS2D_DEBUG >= 1
    if (!tolua_isusertype(tolua_S,1,"Chw3D_Sprite",0,&tolua_err)) goto tolua_lerror;
#endif

    cobj = (Chw3D_Sprite*)tolua_tousertype(tolua_S,1,0);

#if COCOS2D_DEBUG >= 1
    if (!cobj) 
    {
        tolua_error(tolua_S,"invalid 'cobj' in function 'lua_higame_3d_Chw3D_Sprite_isSkipDepthTest'", nullptr);
        return 0;
    }
#endif

    argc = lua_gettop(tolua_S)-1;
    if (argc == 0) 
    {
        if(!ok)
        {
            tolua_error(tolua_S,"invalid arguments in function 'lua_higame_3d_Chw3D_Sprite_isSkipDepthTest'", nullptr);
            return 0;
        }
        bool ret = cobj->isSkipDepthTest();
        tolua_pushboolean(tolua_S,(bool)ret);
        return 1;
    }
    luaL_error(tolua_S, "%s has wrong number of arguments: %d, was expecting %d \n", "Chw3D_Sprite:isSkipDepthTest",argc, 0);
    return 0;

#if COCOS2D_DEBUG >= 1
    tolua_lerror:
    tolua_error(tolua_S,"#ferror in function 'lua_higame_3d_Chw3D_Sprite_isSkipDepthTest'.",&tolua_err);
#endif

    return 0;
}
int lua_higame_3d_Chw3D_Sprite_LoadC3File(lua_State* tolua_S)
{
    int argc = 0;
    Chw3D_Sprite* cobj = nullptr;
    bool ok  = true;

#if COCOS2D_DEBUG >= 1
    tolua_Error tolua_err;
#endif


#if COCOS2D_DEBUG >= 1
    if (!tolua_isusertype(tolua_S,1,"Chw3D_Sprite",0,&tolua_err)) goto tolua_lerror;
#endif

    cobj = (Chw3D_Sprite*)tolua_tousertype(tolua_S,1,0);

#if COCOS2D_DEBUG >= 1
    if (!cobj) 
    {
        tolua_error(tolua_S,"invalid 'cobj' in function 'lua_higame_3d_Chw3D_Sprite_LoadC3File'", nullptr);
        return 0;
    }
#endif

    argc = lua_gettop(tolua_S)-1;
    if (argc == 2) 
    {
        Chw3D_C* arg0;
        std::string arg1;

        ok &= luaval_to_object<Chw3D_C>(tolua_S, 2, "Chw3D_C",&arg0, "Chw3D_Sprite:LoadC3File");

        ok &= luaval_to_std_string(tolua_S, 3,&arg1, "Chw3D_Sprite:LoadC3File");
        if(!ok)
        {
            tolua_error(tolua_S,"invalid arguments in function 'lua_higame_3d_Chw3D_Sprite_LoadC3File'", nullptr);
            return 0;
        }
        bool ret = cobj->LoadC3File(arg0, arg1);
        tolua_pushboolean(tolua_S,(bool)ret);
        return 1;
    }
    luaL_error(tolua_S, "%s has wrong number of arguments: %d, was expecting %d \n", "Chw3D_Sprite:LoadC3File",argc, 2);
    return 0;

#if COCOS2D_DEBUG >= 1
    tolua_lerror:
    tolua_error(tolua_S,"#ferror in function 'lua_higame_3d_Chw3D_Sprite_LoadC3File'.",&tolua_err);
#endif

    return 0;
}
int lua_higame_3d_Chw3D_Sprite_isHardwareSkinMesh(lua_State* tolua_S)
{
    int argc = 0;
    Chw3D_Sprite* cobj = nullptr;
    bool ok  = true;

#if COCOS2D_DEBUG >= 1
    tolua_Error tolua_err;
#endif


#if COCOS2D_DEBUG >= 1
    if (!tolua_isusertype(tolua_S,1,"Chw3D_Sprite",0,&tolua_err)) goto tolua_lerror;
#endif

    cobj = (Chw3D_Sprite*)tolua_tousertype(tolua_S,1,0);

#if COCOS2D_DEBUG >= 1
    if (!cobj) 
    {
        tolua_error(tolua_S,"invalid 'cobj' in function 'lua_higame_3d_Chw3D_Sprite_isHardwareSkinMesh'", nullptr);
        return 0;
    }
#endif

    argc = lua_gettop(tolua_S)-1;
    if (argc == 0) 
    {
        if(!ok)
        {
            tolua_error(tolua_S,"invalid arguments in function 'lua_higame_3d_Chw3D_Sprite_isHardwareSkinMesh'", nullptr);
            return 0;
        }
        bool ret = cobj->isHardwareSkinMesh();
        tolua_pushboolean(tolua_S,(bool)ret);
        return 1;
    }
    luaL_error(tolua_S, "%s has wrong number of arguments: %d, was expecting %d \n", "Chw3D_Sprite:isHardwareSkinMesh",argc, 0);
    return 0;

#if COCOS2D_DEBUG >= 1
    tolua_lerror:
    tolua_error(tolua_S,"#ferror in function 'lua_higame_3d_Chw3D_Sprite_isHardwareSkinMesh'.",&tolua_err);
#endif

    return 0;
}
int lua_higame_3d_Chw3D_Sprite_LoadA3File(lua_State* tolua_S)
{
    int argc = 0;
    Chw3D_Sprite* cobj = nullptr;
    bool ok  = true;

#if COCOS2D_DEBUG >= 1
    tolua_Error tolua_err;
#endif


#if COCOS2D_DEBUG >= 1
    if (!tolua_isusertype(tolua_S,1,"Chw3D_Sprite",0,&tolua_err)) goto tolua_lerror;
#endif

    cobj = (Chw3D_Sprite*)tolua_tousertype(tolua_S,1,0);

#if COCOS2D_DEBUG >= 1
    if (!cobj) 
    {
        tolua_error(tolua_S,"invalid 'cobj' in function 'lua_higame_3d_Chw3D_Sprite_LoadA3File'", nullptr);
        return 0;
    }
#endif

    argc = lua_gettop(tolua_S)-1;
    if (argc == 2) 
    {
        Chw3D_A* arg0;
        std::string arg1;

        ok &= luaval_to_object<Chw3D_A>(tolua_S, 2, "Chw3D_A",&arg0, "Chw3D_Sprite:LoadA3File");

        ok &= luaval_to_std_string(tolua_S, 3,&arg1, "Chw3D_Sprite:LoadA3File");
        if(!ok)
        {
            tolua_error(tolua_S,"invalid arguments in function 'lua_higame_3d_Chw3D_Sprite_LoadA3File'", nullptr);
            return 0;
        }
        bool ret = cobj->LoadA3File(arg0, arg1);
        tolua_pushboolean(tolua_S,(bool)ret);
        return 1;
    }
    luaL_error(tolua_S, "%s has wrong number of arguments: %d, was expecting %d \n", "Chw3D_Sprite:LoadA3File",argc, 2);
    return 0;

#if COCOS2D_DEBUG >= 1
    tolua_lerror:
    tolua_error(tolua_S,"#ferror in function 'lua_higame_3d_Chw3D_Sprite_LoadA3File'.",&tolua_err);
#endif

    return 0;
}
int lua_higame_3d_Chw3D_Sprite_SetA(lua_State* tolua_S)
{
    int argc = 0;
    Chw3D_Sprite* cobj = nullptr;
    bool ok  = true;

#if COCOS2D_DEBUG >= 1
    tolua_Error tolua_err;
#endif


#if COCOS2D_DEBUG >= 1
    if (!tolua_isusertype(tolua_S,1,"Chw3D_Sprite",0,&tolua_err)) goto tolua_lerror;
#endif

    cobj = (Chw3D_Sprite*)tolua_tousertype(tolua_S,1,0);

#if COCOS2D_DEBUG >= 1
    if (!cobj) 
    {
        tolua_error(tolua_S,"invalid 'cobj' in function 'lua_higame_3d_Chw3D_Sprite_SetA'", nullptr);
        return 0;
    }
#endif

    argc = lua_gettop(tolua_S)-1;
    if (argc == 1) 
    {
        const char* arg0;

        std::string arg0_tmp; ok &= luaval_to_std_string(tolua_S, 2, &arg0_tmp, "Chw3D_Sprite:SetA"); arg0 = arg0_tmp.c_str();
        if(!ok)
        {
            tolua_error(tolua_S,"invalid arguments in function 'lua_higame_3d_Chw3D_Sprite_SetA'", nullptr);
            return 0;
        }
        bool ret = cobj->SetA(arg0);
        tolua_pushboolean(tolua_S,(bool)ret);
        return 1;
    }
    if (argc == 2) 
    {
        const char* arg0;
        bool arg1;

        std::string arg0_tmp; ok &= luaval_to_std_string(tolua_S, 2, &arg0_tmp, "Chw3D_Sprite:SetA"); arg0 = arg0_tmp.c_str();

        ok &= luaval_to_boolean(tolua_S, 3,&arg1, "Chw3D_Sprite:SetA");
        if(!ok)
        {
            tolua_error(tolua_S,"invalid arguments in function 'lua_higame_3d_Chw3D_Sprite_SetA'", nullptr);
            return 0;
        }
        bool ret = cobj->SetA(arg0, arg1);
        tolua_pushboolean(tolua_S,(bool)ret);
        return 1;
    }
    luaL_error(tolua_S, "%s has wrong number of arguments: %d, was expecting %d \n", "Chw3D_Sprite:SetA",argc, 1);
    return 0;

#if COCOS2D_DEBUG >= 1
    tolua_lerror:
    tolua_error(tolua_S,"#ferror in function 'lua_higame_3d_Chw3D_Sprite_SetA'.",&tolua_err);
#endif

    return 0;
}
int lua_higame_3d_Chw3D_Sprite_Color(lua_State* tolua_S)
{
    int argc = 0;
    Chw3D_Sprite* cobj = nullptr;
    bool ok  = true;

#if COCOS2D_DEBUG >= 1
    tolua_Error tolua_err;
#endif


#if COCOS2D_DEBUG >= 1
    if (!tolua_isusertype(tolua_S,1,"Chw3D_Sprite",0,&tolua_err)) goto tolua_lerror;
#endif

    cobj = (Chw3D_Sprite*)tolua_tousertype(tolua_S,1,0);

#if COCOS2D_DEBUG >= 1
    if (!cobj) 
    {
        tolua_error(tolua_S,"invalid 'cobj' in function 'lua_higame_3d_Chw3D_Sprite_Color'", nullptr);
        return 0;
    }
#endif

    argc = lua_gettop(tolua_S)-1;
    if (argc == 4) 
    {
        double arg0;
        double arg1;
        double arg2;
        double arg3;

        ok &= luaval_to_number(tolua_S, 2,&arg0, "Chw3D_Sprite:Color");

        ok &= luaval_to_number(tolua_S, 3,&arg1, "Chw3D_Sprite:Color");

        ok &= luaval_to_number(tolua_S, 4,&arg2, "Chw3D_Sprite:Color");

        ok &= luaval_to_number(tolua_S, 5,&arg3, "Chw3D_Sprite:Color");
        if(!ok)
        {
            tolua_error(tolua_S,"invalid arguments in function 'lua_higame_3d_Chw3D_Sprite_Color'", nullptr);
            return 0;
        }
        cobj->Color(arg0, arg1, arg2, arg3);
        lua_settop(tolua_S, 1);
        return 1;
    }
    luaL_error(tolua_S, "%s has wrong number of arguments: %d, was expecting %d \n", "Chw3D_Sprite:Color",argc, 4);
    return 0;

#if COCOS2D_DEBUG >= 1
    tolua_lerror:
    tolua_error(tolua_S,"#ferror in function 'lua_higame_3d_Chw3D_Sprite_Color'.",&tolua_err);
#endif

    return 0;
}
int lua_higame_3d_Chw3D_Sprite_cleanData(lua_State* tolua_S)
{
    int argc = 0;
    Chw3D_Sprite* cobj = nullptr;
    bool ok  = true;

#if COCOS2D_DEBUG >= 1
    tolua_Error tolua_err;
#endif


#if COCOS2D_DEBUG >= 1
    if (!tolua_isusertype(tolua_S,1,"Chw3D_Sprite",0,&tolua_err)) goto tolua_lerror;
#endif

    cobj = (Chw3D_Sprite*)tolua_tousertype(tolua_S,1,0);

#if COCOS2D_DEBUG >= 1
    if (!cobj) 
    {
        tolua_error(tolua_S,"invalid 'cobj' in function 'lua_higame_3d_Chw3D_Sprite_cleanData'", nullptr);
        return 0;
    }
#endif

    argc = lua_gettop(tolua_S)-1;
    if (argc == 0) 
    {
        if(!ok)
        {
            tolua_error(tolua_S,"invalid arguments in function 'lua_higame_3d_Chw3D_Sprite_cleanData'", nullptr);
            return 0;
        }
        bool ret = cobj->cleanData();
        tolua_pushboolean(tolua_S,(bool)ret);
        return 1;
    }
    luaL_error(tolua_S, "%s has wrong number of arguments: %d, was expecting %d \n", "Chw3D_Sprite:cleanData",argc, 0);
    return 0;

#if COCOS2D_DEBUG >= 1
    tolua_lerror:
    tolua_error(tolua_S,"#ferror in function 'lua_higame_3d_Chw3D_Sprite_cleanData'.",&tolua_err);
#endif

    return 0;
}
int lua_higame_3d_Chw3D_Sprite_set3dType(lua_State* tolua_S)
{
    int argc = 0;
    Chw3D_Sprite* cobj = nullptr;
    bool ok  = true;

#if COCOS2D_DEBUG >= 1
    tolua_Error tolua_err;
#endif


#if COCOS2D_DEBUG >= 1
    if (!tolua_isusertype(tolua_S,1,"Chw3D_Sprite",0,&tolua_err)) goto tolua_lerror;
#endif

    cobj = (Chw3D_Sprite*)tolua_tousertype(tolua_S,1,0);

#if COCOS2D_DEBUG >= 1
    if (!cobj) 
    {
        tolua_error(tolua_S,"invalid 'cobj' in function 'lua_higame_3d_Chw3D_Sprite_set3dType'", nullptr);
        return 0;
    }
#endif

    argc = lua_gettop(tolua_S)-1;
    if (argc == 1) 
    {
        unsigned int arg0;

        ok &= luaval_to_uint32(tolua_S, 2,&arg0, "Chw3D_Sprite:set3dType");
        if(!ok)
        {
            tolua_error(tolua_S,"invalid arguments in function 'lua_higame_3d_Chw3D_Sprite_set3dType'", nullptr);
            return 0;
        }
        cobj->set3dType(arg0);
        lua_settop(tolua_S, 1);
        return 1;
    }
    luaL_error(tolua_S, "%s has wrong number of arguments: %d, was expecting %d \n", "Chw3D_Sprite:set3dType",argc, 1);
    return 0;

#if COCOS2D_DEBUG >= 1
    tolua_lerror:
    tolua_error(tolua_S,"#ferror in function 'lua_higame_3d_Chw3D_Sprite_set3dType'.",&tolua_err);
#endif

    return 0;
}
int lua_higame_3d_Chw3D_Sprite_setHardwareSkinMesh(lua_State* tolua_S)
{
    int argc = 0;
    Chw3D_Sprite* cobj = nullptr;
    bool ok  = true;

#if COCOS2D_DEBUG >= 1
    tolua_Error tolua_err;
#endif


#if COCOS2D_DEBUG >= 1
    if (!tolua_isusertype(tolua_S,1,"Chw3D_Sprite",0,&tolua_err)) goto tolua_lerror;
#endif

    cobj = (Chw3D_Sprite*)tolua_tousertype(tolua_S,1,0);

#if COCOS2D_DEBUG >= 1
    if (!cobj) 
    {
        tolua_error(tolua_S,"invalid 'cobj' in function 'lua_higame_3d_Chw3D_Sprite_setHardwareSkinMesh'", nullptr);
        return 0;
    }
#endif

    argc = lua_gettop(tolua_S)-1;
    if (argc == 1) 
    {
        bool arg0;

        ok &= luaval_to_boolean(tolua_S, 2,&arg0, "Chw3D_Sprite:setHardwareSkinMesh");
        if(!ok)
        {
            tolua_error(tolua_S,"invalid arguments in function 'lua_higame_3d_Chw3D_Sprite_setHardwareSkinMesh'", nullptr);
            return 0;
        }
        cobj->setHardwareSkinMesh(arg0);
        lua_settop(tolua_S, 1);
        return 1;
    }
    luaL_error(tolua_S, "%s has wrong number of arguments: %d, was expecting %d \n", "Chw3D_Sprite:setHardwareSkinMesh",argc, 1);
    return 0;

#if COCOS2D_DEBUG >= 1
    tolua_lerror:
    tolua_error(tolua_S,"#ferror in function 'lua_higame_3d_Chw3D_Sprite_setHardwareSkinMesh'.",&tolua_err);
#endif

    return 0;
}
int lua_higame_3d_Chw3D_Sprite_Shake(lua_State* tolua_S)
{
    int argc = 0;
    Chw3D_Sprite* cobj = nullptr;
    bool ok  = true;

#if COCOS2D_DEBUG >= 1
    tolua_Error tolua_err;
#endif


#if COCOS2D_DEBUG >= 1
    if (!tolua_isusertype(tolua_S,1,"Chw3D_Sprite",0,&tolua_err)) goto tolua_lerror;
#endif

    cobj = (Chw3D_Sprite*)tolua_tousertype(tolua_S,1,0);

#if COCOS2D_DEBUG >= 1
    if (!cobj) 
    {
        tolua_error(tolua_S,"invalid 'cobj' in function 'lua_higame_3d_Chw3D_Sprite_Shake'", nullptr);
        return 0;
    }
#endif

    argc = lua_gettop(tolua_S)-1;
    if (argc == 3) 
    {
        double arg0;
        double arg1;
        unsigned int arg2;

        ok &= luaval_to_number(tolua_S, 2,&arg0, "Chw3D_Sprite:Shake");

        ok &= luaval_to_number(tolua_S, 3,&arg1, "Chw3D_Sprite:Shake");

        ok &= luaval_to_uint32(tolua_S, 4,&arg2, "Chw3D_Sprite:Shake");
        if(!ok)
        {
            tolua_error(tolua_S,"invalid arguments in function 'lua_higame_3d_Chw3D_Sprite_Shake'", nullptr);
            return 0;
        }
        cobj->Shake(arg0, arg1, arg2);
        lua_settop(tolua_S, 1);
        return 1;
    }
    luaL_error(tolua_S, "%s has wrong number of arguments: %d, was expecting %d \n", "Chw3D_Sprite:Shake",argc, 3);
    return 0;

#if COCOS2D_DEBUG >= 1
    tolua_lerror:
    tolua_error(tolua_S,"#ferror in function 'lua_higame_3d_Chw3D_Sprite_Shake'.",&tolua_err);
#endif

    return 0;
}
int lua_higame_3d_Chw3D_Sprite_get3dType(lua_State* tolua_S)
{
    int argc = 0;
    Chw3D_Sprite* cobj = nullptr;
    bool ok  = true;

#if COCOS2D_DEBUG >= 1
    tolua_Error tolua_err;
#endif


#if COCOS2D_DEBUG >= 1
    if (!tolua_isusertype(tolua_S,1,"Chw3D_Sprite",0,&tolua_err)) goto tolua_lerror;
#endif

    cobj = (Chw3D_Sprite*)tolua_tousertype(tolua_S,1,0);

#if COCOS2D_DEBUG >= 1
    if (!cobj) 
    {
        tolua_error(tolua_S,"invalid 'cobj' in function 'lua_higame_3d_Chw3D_Sprite_get3dType'", nullptr);
        return 0;
    }
#endif

    argc = lua_gettop(tolua_S)-1;
    if (argc == 0) 
    {
        if(!ok)
        {
            tolua_error(tolua_S,"invalid arguments in function 'lua_higame_3d_Chw3D_Sprite_get3dType'", nullptr);
            return 0;
        }
        unsigned int ret = cobj->get3dType();
        tolua_pushnumber(tolua_S,(lua_Number)ret);
        return 1;
    }
    luaL_error(tolua_S, "%s has wrong number of arguments: %d, was expecting %d \n", "Chw3D_Sprite:get3dType",argc, 0);
    return 0;

#if COCOS2D_DEBUG >= 1
    tolua_lerror:
    tolua_error(tolua_S,"#ferror in function 'lua_higame_3d_Chw3D_Sprite_get3dType'.",&tolua_err);
#endif

    return 0;
}
int lua_higame_3d_Chw3D_Sprite_SetApplyCameraName(lua_State* tolua_S)
{
    int argc = 0;
    Chw3D_Sprite* cobj = nullptr;
    bool ok  = true;

#if COCOS2D_DEBUG >= 1
    tolua_Error tolua_err;
#endif


#if COCOS2D_DEBUG >= 1
    if (!tolua_isusertype(tolua_S,1,"Chw3D_Sprite",0,&tolua_err)) goto tolua_lerror;
#endif

    cobj = (Chw3D_Sprite*)tolua_tousertype(tolua_S,1,0);

#if COCOS2D_DEBUG >= 1
    if (!cobj) 
    {
        tolua_error(tolua_S,"invalid 'cobj' in function 'lua_higame_3d_Chw3D_Sprite_SetApplyCameraName'", nullptr);
        return 0;
    }
#endif

    argc = lua_gettop(tolua_S)-1;
    if (argc == 1) 
    {
        std::string arg0;

        ok &= luaval_to_std_string(tolua_S, 2,&arg0, "Chw3D_Sprite:SetApplyCameraName");
        if(!ok)
        {
            tolua_error(tolua_S,"invalid arguments in function 'lua_higame_3d_Chw3D_Sprite_SetApplyCameraName'", nullptr);
            return 0;
        }
        cobj->SetApplyCameraName(arg0);
        lua_settop(tolua_S, 1);
        return 1;
    }
    luaL_error(tolua_S, "%s has wrong number of arguments: %d, was expecting %d \n", "Chw3D_Sprite:SetApplyCameraName",argc, 1);
    return 0;

#if COCOS2D_DEBUG >= 1
    tolua_lerror:
    tolua_error(tolua_S,"#ferror in function 'lua_higame_3d_Chw3D_Sprite_SetApplyCameraName'.",&tolua_err);
#endif

    return 0;
}
int lua_higame_3d_Chw3D_Sprite_create(lua_State* tolua_S)
{
    int argc = 0;
    bool ok  = true;

#if COCOS2D_DEBUG >= 1
    tolua_Error tolua_err;
#endif

#if COCOS2D_DEBUG >= 1
    if (!tolua_isusertable(tolua_S,1,"Chw3D_Sprite",0,&tolua_err)) goto tolua_lerror;
#endif

    argc = lua_gettop(tolua_S) - 1;

    if (argc == 2)
    {
        const char* arg0;
        const char* arg1;
        std::string arg0_tmp; ok &= luaval_to_std_string(tolua_S, 2, &arg0_tmp, "Chw3D_Sprite:create"); arg0 = arg0_tmp.c_str();
        std::string arg1_tmp; ok &= luaval_to_std_string(tolua_S, 3, &arg1_tmp, "Chw3D_Sprite:create"); arg1 = arg1_tmp.c_str();
        if(!ok)
        {
            tolua_error(tolua_S,"invalid arguments in function 'lua_higame_3d_Chw3D_Sprite_create'", nullptr);
            return 0;
        }
        Chw3D_Sprite* ret = Chw3D_Sprite::create(arg0, arg1);
        object_to_luaval<Chw3D_Sprite>(tolua_S, "Chw3D_Sprite",(Chw3D_Sprite*)ret);
        return 1;
    }
    if (argc == 3)
    {
        const char* arg0;
        const char* arg1;
        bool arg2;
        std::string arg0_tmp; ok &= luaval_to_std_string(tolua_S, 2, &arg0_tmp, "Chw3D_Sprite:create"); arg0 = arg0_tmp.c_str();
        std::string arg1_tmp; ok &= luaval_to_std_string(tolua_S, 3, &arg1_tmp, "Chw3D_Sprite:create"); arg1 = arg1_tmp.c_str();
        ok &= luaval_to_boolean(tolua_S, 4,&arg2, "Chw3D_Sprite:create");
        if(!ok)
        {
            tolua_error(tolua_S,"invalid arguments in function 'lua_higame_3d_Chw3D_Sprite_create'", nullptr);
            return 0;
        }
        Chw3D_Sprite* ret = Chw3D_Sprite::create(arg0, arg1, arg2);
        object_to_luaval<Chw3D_Sprite>(tolua_S, "Chw3D_Sprite",(Chw3D_Sprite*)ret);
        return 1;
    }
    if (argc == 4)
    {
        const char* arg0;
        const char* arg1;
        bool arg2;
        unsigned int arg3;
        std::string arg0_tmp; ok &= luaval_to_std_string(tolua_S, 2, &arg0_tmp, "Chw3D_Sprite:create"); arg0 = arg0_tmp.c_str();
        std::string arg1_tmp; ok &= luaval_to_std_string(tolua_S, 3, &arg1_tmp, "Chw3D_Sprite:create"); arg1 = arg1_tmp.c_str();
        ok &= luaval_to_boolean(tolua_S, 4,&arg2, "Chw3D_Sprite:create");
        ok &= luaval_to_uint32(tolua_S, 5,&arg3, "Chw3D_Sprite:create");
        if(!ok)
        {
            tolua_error(tolua_S,"invalid arguments in function 'lua_higame_3d_Chw3D_Sprite_create'", nullptr);
            return 0;
        }
        Chw3D_Sprite* ret = Chw3D_Sprite::create(arg0, arg1, arg2, arg3);
        object_to_luaval<Chw3D_Sprite>(tolua_S, "Chw3D_Sprite",(Chw3D_Sprite*)ret);
        return 1;
    }
    luaL_error(tolua_S, "%s has wrong number of arguments: %d, was expecting %d\n ", "Chw3D_Sprite:create",argc, 2);
    return 0;
#if COCOS2D_DEBUG >= 1
    tolua_lerror:
    tolua_error(tolua_S,"#ferror in function 'lua_higame_3d_Chw3D_Sprite_create'.",&tolua_err);
#endif
    return 0;
}
int lua_higame_3d_Chw3D_Sprite_createAsync(lua_State* tolua_S)
{
    int argc = 0;
    bool ok  = true;

#if COCOS2D_DEBUG >= 1
    tolua_Error tolua_err;
#endif

#if COCOS2D_DEBUG >= 1
    if (!tolua_isusertable(tolua_S,1,"Chw3D_Sprite",0,&tolua_err)) goto tolua_lerror;
#endif

    argc = lua_gettop(tolua_S) - 1;

    if (argc == 2)
    {
        const char* arg0;
        const char* arg1;
        std::string arg0_tmp; ok &= luaval_to_std_string(tolua_S, 2, &arg0_tmp, "Chw3D_Sprite:createAsync"); arg0 = arg0_tmp.c_str();
        std::string arg1_tmp; ok &= luaval_to_std_string(tolua_S, 3, &arg1_tmp, "Chw3D_Sprite:createAsync"); arg1 = arg1_tmp.c_str();
        if(!ok)
        {
            tolua_error(tolua_S,"invalid arguments in function 'lua_higame_3d_Chw3D_Sprite_createAsync'", nullptr);
            return 0;
        }
        Chw3D_Sprite* ret = Chw3D_Sprite::createAsync(arg0, arg1);
        object_to_luaval<Chw3D_Sprite>(tolua_S, "Chw3D_Sprite",(Chw3D_Sprite*)ret);
        return 1;
    }
    if (argc == 3)
    {
        const char* arg0;
        const char* arg1;
        bool arg2;
        std::string arg0_tmp; ok &= luaval_to_std_string(tolua_S, 2, &arg0_tmp, "Chw3D_Sprite:createAsync"); arg0 = arg0_tmp.c_str();
        std::string arg1_tmp; ok &= luaval_to_std_string(tolua_S, 3, &arg1_tmp, "Chw3D_Sprite:createAsync"); arg1 = arg1_tmp.c_str();
        ok &= luaval_to_boolean(tolua_S, 4,&arg2, "Chw3D_Sprite:createAsync");
        if(!ok)
        {
            tolua_error(tolua_S,"invalid arguments in function 'lua_higame_3d_Chw3D_Sprite_createAsync'", nullptr);
            return 0;
        }
        Chw3D_Sprite* ret = Chw3D_Sprite::createAsync(arg0, arg1, arg2);
        object_to_luaval<Chw3D_Sprite>(tolua_S, "Chw3D_Sprite",(Chw3D_Sprite*)ret);
        return 1;
    }
    if (argc == 4)
    {
        const char* arg0;
        const char* arg1;
        bool arg2;
        unsigned int arg3;
        std::string arg0_tmp; ok &= luaval_to_std_string(tolua_S, 2, &arg0_tmp, "Chw3D_Sprite:createAsync"); arg0 = arg0_tmp.c_str();
        std::string arg1_tmp; ok &= luaval_to_std_string(tolua_S, 3, &arg1_tmp, "Chw3D_Sprite:createAsync"); arg1 = arg1_tmp.c_str();
        ok &= luaval_to_boolean(tolua_S, 4,&arg2, "Chw3D_Sprite:createAsync");
        ok &= luaval_to_uint32(tolua_S, 5,&arg3, "Chw3D_Sprite:createAsync");
        if(!ok)
        {
            tolua_error(tolua_S,"invalid arguments in function 'lua_higame_3d_Chw3D_Sprite_createAsync'", nullptr);
            return 0;
        }
        Chw3D_Sprite* ret = Chw3D_Sprite::createAsync(arg0, arg1, arg2, arg3);
        object_to_luaval<Chw3D_Sprite>(tolua_S, "Chw3D_Sprite",(Chw3D_Sprite*)ret);
        return 1;
    }
    luaL_error(tolua_S, "%s has wrong number of arguments: %d, was expecting %d\n ", "Chw3D_Sprite:createAsync",argc, 2);
    return 0;
#if COCOS2D_DEBUG >= 1
    tolua_lerror:
    tolua_error(tolua_S,"#ferror in function 'lua_higame_3d_Chw3D_Sprite_createAsync'.",&tolua_err);
#endif
    return 0;
}
int lua_higame_3d_Chw3D_Sprite_constructor(lua_State* tolua_S)
{
    int argc = 0;
    Chw3D_Sprite* cobj = nullptr;
    bool ok  = true;

#if COCOS2D_DEBUG >= 1
    tolua_Error tolua_err;
#endif



    argc = lua_gettop(tolua_S)-1;
    if (argc == 0) 
    {
        if(!ok)
        {
            tolua_error(tolua_S,"invalid arguments in function 'lua_higame_3d_Chw3D_Sprite_constructor'", nullptr);
            return 0;
        }
        cobj = new Chw3D_Sprite();
        cobj->autorelease();
        int ID =  (int)cobj->_ID ;
        int* luaID =  &cobj->_luaID ;
        toluafix_pushusertype_ccobject(tolua_S, ID, luaID, (void*)cobj,"Chw3D_Sprite");
        return 1;
    }
    luaL_error(tolua_S, "%s has wrong number of arguments: %d, was expecting %d \n", "Chw3D_Sprite:Chw3D_Sprite",argc, 0);
    return 0;

#if COCOS2D_DEBUG >= 1
    tolua_error(tolua_S,"#ferror in function 'lua_higame_3d_Chw3D_Sprite_constructor'.",&tolua_err);
#endif

    return 0;
}

static int lua_higame_3d_Chw3D_Sprite_finalize(lua_State* tolua_S)
{
    printf("luabindings: finalizing LUA object (Chw3D_Sprite)");
    return 0;
}

int lua_register_higame_3d_Chw3D_Sprite(lua_State* tolua_S)
{
    tolua_usertype(tolua_S,"Chw3D_Sprite");
    tolua_cclass(tolua_S,"Chw3D_Sprite","Chw3D_Sprite","cc.Node",nullptr);

    tolua_beginmodule(tolua_S,"Chw3D_Sprite");
        tolua_function(tolua_S,"new",lua_higame_3d_Chw3D_Sprite_constructor);
        tolua_function(tolua_S,"Scale",lua_higame_3d_Chw3D_Sprite_Scale);
        tolua_function(tolua_S,"Create",lua_higame_3d_Chw3D_Sprite_Create);
        tolua_function(tolua_S,"loadDefault",lua_higame_3d_Chw3D_Sprite_loadDefault);
        tolua_function(tolua_S,"Attach",lua_higame_3d_Chw3D_Sprite_Attach);
        tolua_function(tolua_S,"LoadData",lua_higame_3d_Chw3D_Sprite_LoadData);
        tolua_function(tolua_S,"Build",lua_higame_3d_Chw3D_Sprite_Build);
        tolua_function(tolua_S,"removeAttachChild",lua_higame_3d_Chw3D_Sprite_removeAttachChild);
        tolua_function(tolua_S,"isC3A3FIleExist",lua_higame_3d_Chw3D_Sprite_isC3A3FIleExist);
        tolua_function(tolua_S,"Translate",lua_higame_3d_Chw3D_Sprite_Translate);
        tolua_function(tolua_S,"setSkipDepthTest",lua_higame_3d_Chw3D_Sprite_setSkipDepthTest);
        tolua_function(tolua_S,"AddAction",lua_higame_3d_Chw3D_Sprite_AddAction);
        tolua_function(tolua_S,"SetRepeatAB",lua_higame_3d_Chw3D_Sprite_SetRepeatAB);
        tolua_function(tolua_S,"FinialMatrix",lua_higame_3d_Chw3D_Sprite_FinialMatrix);
        tolua_function(tolua_S,"CleanActionGroup",lua_higame_3d_Chw3D_Sprite_CleanActionGroup);
        tolua_function(tolua_S,"addAttachChild",lua_higame_3d_Chw3D_Sprite_addAttachChild);
        tolua_function(tolua_S,"reloadFile",lua_higame_3d_Chw3D_Sprite_reloadFile);
        tolua_function(tolua_S,"GetActualHeight",lua_higame_3d_Chw3D_Sprite_GetActualHeight);
        tolua_function(tolua_S,"GetRepeatABDuration",lua_higame_3d_Chw3D_Sprite_GetRepeatABDuration);
        tolua_function(tolua_S,"onDraw",lua_higame_3d_Chw3D_Sprite_onDraw);
        tolua_function(tolua_S,"SetCamera",lua_higame_3d_Chw3D_Sprite_SetCamera);
        tolua_function(tolua_S,"draw",lua_higame_3d_Chw3D_Sprite_draw);
        tolua_function(tolua_S,"Rotate",lua_higame_3d_Chw3D_Sprite_Rotate);
        tolua_function(tolua_S,"isInit",lua_higame_3d_Chw3D_Sprite_isInit);
        tolua_function(tolua_S,"update",lua_higame_3d_Chw3D_Sprite_update);
        tolua_function(tolua_S,"CreateAsync",lua_higame_3d_Chw3D_Sprite_CreateAsync);
        tolua_function(tolua_S,"isSkipDepthTest",lua_higame_3d_Chw3D_Sprite_isSkipDepthTest);
        tolua_function(tolua_S,"LoadC3File",lua_higame_3d_Chw3D_Sprite_LoadC3File);
        tolua_function(tolua_S,"isHardwareSkinMesh",lua_higame_3d_Chw3D_Sprite_isHardwareSkinMesh);
        tolua_function(tolua_S,"LoadA3File",lua_higame_3d_Chw3D_Sprite_LoadA3File);
        tolua_function(tolua_S,"SetA",lua_higame_3d_Chw3D_Sprite_SetA);
        tolua_function(tolua_S,"Color",lua_higame_3d_Chw3D_Sprite_Color);
        tolua_function(tolua_S,"cleanData",lua_higame_3d_Chw3D_Sprite_cleanData);
        tolua_function(tolua_S,"set3dType",lua_higame_3d_Chw3D_Sprite_set3dType);
        tolua_function(tolua_S,"setHardwareSkinMesh",lua_higame_3d_Chw3D_Sprite_setHardwareSkinMesh);
        tolua_function(tolua_S,"Shake",lua_higame_3d_Chw3D_Sprite_Shake);
        tolua_function(tolua_S,"get3dType",lua_higame_3d_Chw3D_Sprite_get3dType);
        tolua_function(tolua_S,"SetApplyCameraName",lua_higame_3d_Chw3D_Sprite_SetApplyCameraName);
        tolua_function(tolua_S,"create", lua_higame_3d_Chw3D_Sprite_create);
        tolua_function(tolua_S,"createAsync", lua_higame_3d_Chw3D_Sprite_createAsync);
    tolua_endmodule(tolua_S);
    std::string typeName = typeid(Chw3D_Sprite).name();
    g_luaType[typeName] = "Chw3D_Sprite";
    g_typeCast["Chw3D_Sprite"] = "Chw3D_Sprite";
    return 1;
}

int lua_higame_3d_Chw3D_Mgr_queryA3FileAsync(lua_State* tolua_S)
{
    int argc = 0;
    Chw3D_Mgr* cobj = nullptr;
    bool ok  = true;

#if COCOS2D_DEBUG >= 1
    tolua_Error tolua_err;
#endif


#if COCOS2D_DEBUG >= 1
    if (!tolua_isusertype(tolua_S,1,"Chw3D_Mgr",0,&tolua_err)) goto tolua_lerror;
#endif

    cobj = (Chw3D_Mgr*)tolua_tousertype(tolua_S,1,0);

#if COCOS2D_DEBUG >= 1
    if (!cobj) 
    {
        tolua_error(tolua_S,"invalid 'cobj' in function 'lua_higame_3d_Chw3D_Mgr_queryA3FileAsync'", nullptr);
        return 0;
    }
#endif

    argc = lua_gettop(tolua_S)-1;
    if (argc == 2) 
    {
        const char* arg0;
        std::function<void (Chw3D_A *)> arg1;

        std::string arg0_tmp; ok &= luaval_to_std_string(tolua_S, 2, &arg0_tmp, "Chw3D_Mgr:queryA3FileAsync"); arg0 = arg0_tmp.c_str();

        do {
			// Lambda binding for lua is not supported.
//            assert(false);
		} while(0)
		;
        if(!ok)
        {
            tolua_error(tolua_S,"invalid arguments in function 'lua_higame_3d_Chw3D_Mgr_queryA3FileAsync'", nullptr);
            return 0;
        }
        cobj->queryA3FileAsync(arg0, arg1);
        lua_settop(tolua_S, 1);
        return 1;
    }
    luaL_error(tolua_S, "%s has wrong number of arguments: %d, was expecting %d \n", "Chw3D_Mgr:queryA3FileAsync",argc, 2);
    return 0;

#if COCOS2D_DEBUG >= 1
    tolua_lerror:
    tolua_error(tolua_S,"#ferror in function 'lua_higame_3d_Chw3D_Mgr_queryA3FileAsync'.",&tolua_err);
#endif

    return 0;
}
int lua_higame_3d_Chw3D_Mgr_queryC3File(lua_State* tolua_S)
{
    int argc = 0;
    Chw3D_Mgr* cobj = nullptr;
    bool ok  = true;

#if COCOS2D_DEBUG >= 1
    tolua_Error tolua_err;
#endif


#if COCOS2D_DEBUG >= 1
    if (!tolua_isusertype(tolua_S,1,"Chw3D_Mgr",0,&tolua_err)) goto tolua_lerror;
#endif

    cobj = (Chw3D_Mgr*)tolua_tousertype(tolua_S,1,0);

#if COCOS2D_DEBUG >= 1
    if (!cobj) 
    {
        tolua_error(tolua_S,"invalid 'cobj' in function 'lua_higame_3d_Chw3D_Mgr_queryC3File'", nullptr);
        return 0;
    }
#endif

    argc = lua_gettop(tolua_S)-1;
    if (argc == 1) 
    {
        const char* arg0;

        std::string arg0_tmp; ok &= luaval_to_std_string(tolua_S, 2, &arg0_tmp, "Chw3D_Mgr:queryC3File"); arg0 = arg0_tmp.c_str();
        if(!ok)
        {
            tolua_error(tolua_S,"invalid arguments in function 'lua_higame_3d_Chw3D_Mgr_queryC3File'", nullptr);
            return 0;
        }
        Chw3D_C* ret = cobj->queryC3File(arg0);
        object_to_luaval<Chw3D_C>(tolua_S, "Chw3D_C",(Chw3D_C*)ret);
        return 1;
    }
    luaL_error(tolua_S, "%s has wrong number of arguments: %d, was expecting %d \n", "Chw3D_Mgr:queryC3File",argc, 1);
    return 0;

#if COCOS2D_DEBUG >= 1
    tolua_lerror:
    tolua_error(tolua_S,"#ferror in function 'lua_higame_3d_Chw3D_Mgr_queryC3File'.",&tolua_err);
#endif

    return 0;
}
int lua_higame_3d_Chw3D_Mgr_NewA(lua_State* tolua_S)
{
    int argc = 0;
    Chw3D_Mgr* cobj = nullptr;
    bool ok  = true;

#if COCOS2D_DEBUG >= 1
    tolua_Error tolua_err;
#endif


#if COCOS2D_DEBUG >= 1
    if (!tolua_isusertype(tolua_S,1,"Chw3D_Mgr",0,&tolua_err)) goto tolua_lerror;
#endif

    cobj = (Chw3D_Mgr*)tolua_tousertype(tolua_S,1,0);

#if COCOS2D_DEBUG >= 1
    if (!cobj) 
    {
        tolua_error(tolua_S,"invalid 'cobj' in function 'lua_higame_3d_Chw3D_Mgr_NewA'", nullptr);
        return 0;
    }
#endif

    argc = lua_gettop(tolua_S)-1;
    if (argc == 1) 
    {
        const char* arg0;

        std::string arg0_tmp; ok &= luaval_to_std_string(tolua_S, 2, &arg0_tmp, "Chw3D_Mgr:NewA"); arg0 = arg0_tmp.c_str();
        if(!ok)
        {
            tolua_error(tolua_S,"invalid arguments in function 'lua_higame_3d_Chw3D_Mgr_NewA'", nullptr);
            return 0;
        }
        Chw3D_A* ret = cobj->NewA(arg0);
        object_to_luaval<Chw3D_A>(tolua_S, "Chw3D_A",(Chw3D_A*)ret);
        return 1;
    }
    luaL_error(tolua_S, "%s has wrong number of arguments: %d, was expecting %d \n", "Chw3D_Mgr:NewA",argc, 1);
    return 0;

#if COCOS2D_DEBUG >= 1
    tolua_lerror:
    tolua_error(tolua_S,"#ferror in function 'lua_higame_3d_Chw3D_Mgr_NewA'.",&tolua_err);
#endif

    return 0;
}
int lua_higame_3d_Chw3D_Mgr_NewCEx(lua_State* tolua_S)
{
    int argc = 0;
    Chw3D_Mgr* cobj = nullptr;
    bool ok  = true;

#if COCOS2D_DEBUG >= 1
    tolua_Error tolua_err;
#endif


#if COCOS2D_DEBUG >= 1
    if (!tolua_isusertype(tolua_S,1,"Chw3D_Mgr",0,&tolua_err)) goto tolua_lerror;
#endif

    cobj = (Chw3D_Mgr*)tolua_tousertype(tolua_S,1,0);

#if COCOS2D_DEBUG >= 1
    if (!cobj) 
    {
        tolua_error(tolua_S,"invalid 'cobj' in function 'lua_higame_3d_Chw3D_Mgr_NewCEx'", nullptr);
        return 0;
    }
#endif

    argc = lua_gettop(tolua_S)-1;
    if (argc == 1) 
    {
        const char* arg0;

        std::string arg0_tmp; ok &= luaval_to_std_string(tolua_S, 2, &arg0_tmp, "Chw3D_Mgr:NewCEx"); arg0 = arg0_tmp.c_str();
        if(!ok)
        {
            tolua_error(tolua_S,"invalid arguments in function 'lua_higame_3d_Chw3D_Mgr_NewCEx'", nullptr);
            return 0;
        }
        Chw3D_C* ret = cobj->NewCEx(arg0);
        object_to_luaval<Chw3D_C>(tolua_S, "Chw3D_C",(Chw3D_C*)ret);
        return 1;
    }
    luaL_error(tolua_S, "%s has wrong number of arguments: %d, was expecting %d \n", "Chw3D_Mgr:NewCEx",argc, 1);
    return 0;

#if COCOS2D_DEBUG >= 1
    tolua_lerror:
    tolua_error(tolua_S,"#ferror in function 'lua_higame_3d_Chw3D_Mgr_NewCEx'.",&tolua_err);
#endif

    return 0;
}
int lua_higame_3d_Chw3D_Mgr_setHardwareSkinMesh(lua_State* tolua_S)
{
    int argc = 0;
    Chw3D_Mgr* cobj = nullptr;
    bool ok  = true;

#if COCOS2D_DEBUG >= 1
    tolua_Error tolua_err;
#endif


#if COCOS2D_DEBUG >= 1
    if (!tolua_isusertype(tolua_S,1,"Chw3D_Mgr",0,&tolua_err)) goto tolua_lerror;
#endif

    cobj = (Chw3D_Mgr*)tolua_tousertype(tolua_S,1,0);

#if COCOS2D_DEBUG >= 1
    if (!cobj) 
    {
        tolua_error(tolua_S,"invalid 'cobj' in function 'lua_higame_3d_Chw3D_Mgr_setHardwareSkinMesh'", nullptr);
        return 0;
    }
#endif

    argc = lua_gettop(tolua_S)-1;
    if (argc == 1) 
    {
        bool arg0;

        ok &= luaval_to_boolean(tolua_S, 2,&arg0, "Chw3D_Mgr:setHardwareSkinMesh");
        if(!ok)
        {
            tolua_error(tolua_S,"invalid arguments in function 'lua_higame_3d_Chw3D_Mgr_setHardwareSkinMesh'", nullptr);
            return 0;
        }
        cobj->setHardwareSkinMesh(arg0);
        lua_settop(tolua_S, 1);
        return 1;
    }
    luaL_error(tolua_S, "%s has wrong number of arguments: %d, was expecting %d \n", "Chw3D_Mgr:setHardwareSkinMesh",argc, 1);
    return 0;

#if COCOS2D_DEBUG >= 1
    tolua_lerror:
    tolua_error(tolua_S,"#ferror in function 'lua_higame_3d_Chw3D_Mgr_setHardwareSkinMesh'.",&tolua_err);
#endif

    return 0;
}
int lua_higame_3d_Chw3D_Mgr_getDefaultCamera(lua_State* tolua_S)
{
    int argc = 0;
    Chw3D_Mgr* cobj = nullptr;
    bool ok  = true;

#if COCOS2D_DEBUG >= 1
    tolua_Error tolua_err;
#endif


#if COCOS2D_DEBUG >= 1
    if (!tolua_isusertype(tolua_S,1,"Chw3D_Mgr",0,&tolua_err)) goto tolua_lerror;
#endif

    cobj = (Chw3D_Mgr*)tolua_tousertype(tolua_S,1,0);

#if COCOS2D_DEBUG >= 1
    if (!cobj) 
    {
        tolua_error(tolua_S,"invalid 'cobj' in function 'lua_higame_3d_Chw3D_Mgr_getDefaultCamera'", nullptr);
        return 0;
    }
#endif

    argc = lua_gettop(tolua_S)-1;
    if (argc == 0) 
    {
        if(!ok)
        {
            tolua_error(tolua_S,"invalid arguments in function 'lua_higame_3d_Chw3D_Mgr_getDefaultCamera'", nullptr);
            return 0;
        }
        Chw3D_Camera* ret = cobj->getDefaultCamera();
        object_to_luaval<Chw3D_Camera>(tolua_S, "Chw3D_Camera",(Chw3D_Camera*)ret);
        return 1;
    }
    luaL_error(tolua_S, "%s has wrong number of arguments: %d, was expecting %d \n", "Chw3D_Mgr:getDefaultCamera",argc, 0);
    return 0;

#if COCOS2D_DEBUG >= 1
    tolua_lerror:
    tolua_error(tolua_S,"#ferror in function 'lua_higame_3d_Chw3D_Mgr_getDefaultCamera'.",&tolua_err);
#endif

    return 0;
}
int lua_higame_3d_Chw3D_Mgr_queryA3File(lua_State* tolua_S)
{
    int argc = 0;
    Chw3D_Mgr* cobj = nullptr;
    bool ok  = true;

#if COCOS2D_DEBUG >= 1
    tolua_Error tolua_err;
#endif


#if COCOS2D_DEBUG >= 1
    if (!tolua_isusertype(tolua_S,1,"Chw3D_Mgr",0,&tolua_err)) goto tolua_lerror;
#endif

    cobj = (Chw3D_Mgr*)tolua_tousertype(tolua_S,1,0);

#if COCOS2D_DEBUG >= 1
    if (!cobj) 
    {
        tolua_error(tolua_S,"invalid 'cobj' in function 'lua_higame_3d_Chw3D_Mgr_queryA3File'", nullptr);
        return 0;
    }
#endif

    argc = lua_gettop(tolua_S)-1;
    if (argc == 1) 
    {
        const char* arg0;

        std::string arg0_tmp; ok &= luaval_to_std_string(tolua_S, 2, &arg0_tmp, "Chw3D_Mgr:queryA3File"); arg0 = arg0_tmp.c_str();
        if(!ok)
        {
            tolua_error(tolua_S,"invalid arguments in function 'lua_higame_3d_Chw3D_Mgr_queryA3File'", nullptr);
            return 0;
        }
        Chw3D_A* ret = cobj->queryA3File(arg0);
        object_to_luaval<Chw3D_A>(tolua_S, "Chw3D_A",(Chw3D_A*)ret);
        return 1;
    }
    luaL_error(tolua_S, "%s has wrong number of arguments: %d, was expecting %d \n", "Chw3D_Mgr:queryA3File",argc, 1);
    return 0;

#if COCOS2D_DEBUG >= 1
    tolua_lerror:
    tolua_error(tolua_S,"#ferror in function 'lua_higame_3d_Chw3D_Mgr_queryA3File'.",&tolua_err);
#endif

    return 0;
}
int lua_higame_3d_Chw3D_Mgr_init(lua_State* tolua_S)
{
    int argc = 0;
    Chw3D_Mgr* cobj = nullptr;
    bool ok  = true;

#if COCOS2D_DEBUG >= 1
    tolua_Error tolua_err;
#endif


#if COCOS2D_DEBUG >= 1
    if (!tolua_isusertype(tolua_S,1,"Chw3D_Mgr",0,&tolua_err)) goto tolua_lerror;
#endif

    cobj = (Chw3D_Mgr*)tolua_tousertype(tolua_S,1,0);

#if COCOS2D_DEBUG >= 1
    if (!cobj) 
    {
        tolua_error(tolua_S,"invalid 'cobj' in function 'lua_higame_3d_Chw3D_Mgr_init'", nullptr);
        return 0;
    }
#endif

    argc = lua_gettop(tolua_S)-1;
    if (argc == 0) 
    {
        if(!ok)
        {
            tolua_error(tolua_S,"invalid arguments in function 'lua_higame_3d_Chw3D_Mgr_init'", nullptr);
            return 0;
        }
        bool ret = cobj->init();
        tolua_pushboolean(tolua_S,(bool)ret);
        return 1;
    }
    luaL_error(tolua_S, "%s has wrong number of arguments: %d, was expecting %d \n", "Chw3D_Mgr:init",argc, 0);
    return 0;

#if COCOS2D_DEBUG >= 1
    tolua_lerror:
    tolua_error(tolua_S,"#ferror in function 'lua_higame_3d_Chw3D_Mgr_init'.",&tolua_err);
#endif

    return 0;
}
int lua_higame_3d_Chw3D_Mgr_setDefaultTypeFile(lua_State* tolua_S)
{
    int argc = 0;
    Chw3D_Mgr* cobj = nullptr;
    bool ok  = true;

#if COCOS2D_DEBUG >= 1
    tolua_Error tolua_err;
#endif


#if COCOS2D_DEBUG >= 1
    if (!tolua_isusertype(tolua_S,1,"Chw3D_Mgr",0,&tolua_err)) goto tolua_lerror;
#endif

    cobj = (Chw3D_Mgr*)tolua_tousertype(tolua_S,1,0);

#if COCOS2D_DEBUG >= 1
    if (!cobj) 
    {
        tolua_error(tolua_S,"invalid 'cobj' in function 'lua_higame_3d_Chw3D_Mgr_setDefaultTypeFile'", nullptr);
        return 0;
    }
#endif

    argc = lua_gettop(tolua_S)-1;
    if (argc == 3) 
    {
        unsigned int arg0;
        std::string arg1;
        std::string arg2;

        ok &= luaval_to_uint32(tolua_S, 2,&arg0, "Chw3D_Mgr:setDefaultTypeFile");

        ok &= luaval_to_std_string(tolua_S, 3,&arg1, "Chw3D_Mgr:setDefaultTypeFile");

        ok &= luaval_to_std_string(tolua_S, 4,&arg2, "Chw3D_Mgr:setDefaultTypeFile");
        if(!ok)
        {
            tolua_error(tolua_S,"invalid arguments in function 'lua_higame_3d_Chw3D_Mgr_setDefaultTypeFile'", nullptr);
            return 0;
        }
        cobj->setDefaultTypeFile(arg0, arg1, arg2);
        lua_settop(tolua_S, 1);
        return 1;
    }
    luaL_error(tolua_S, "%s has wrong number of arguments: %d, was expecting %d \n", "Chw3D_Mgr:setDefaultTypeFile",argc, 3);
    return 0;

#if COCOS2D_DEBUG >= 1
    tolua_lerror:
    tolua_error(tolua_S,"#ferror in function 'lua_higame_3d_Chw3D_Mgr_setDefaultTypeFile'.",&tolua_err);
#endif

    return 0;
}
int lua_higame_3d_Chw3D_Mgr_processCacheFile(lua_State* tolua_S)
{
    int argc = 0;
    Chw3D_Mgr* cobj = nullptr;
    bool ok  = true;

#if COCOS2D_DEBUG >= 1
    tolua_Error tolua_err;
#endif


#if COCOS2D_DEBUG >= 1
    if (!tolua_isusertype(tolua_S,1,"Chw3D_Mgr",0,&tolua_err)) goto tolua_lerror;
#endif

    cobj = (Chw3D_Mgr*)tolua_tousertype(tolua_S,1,0);

#if COCOS2D_DEBUG >= 1
    if (!cobj) 
    {
        tolua_error(tolua_S,"invalid 'cobj' in function 'lua_higame_3d_Chw3D_Mgr_processCacheFile'", nullptr);
        return 0;
    }
#endif

    argc = lua_gettop(tolua_S)-1;
    if (argc == 1) 
    {
        double arg0;

        ok &= luaval_to_number(tolua_S, 2,&arg0, "Chw3D_Mgr:processCacheFile");
        if(!ok)
        {
            tolua_error(tolua_S,"invalid arguments in function 'lua_higame_3d_Chw3D_Mgr_processCacheFile'", nullptr);
            return 0;
        }
        cobj->processCacheFile(arg0);
        lua_settop(tolua_S, 1);
        return 1;
    }
    luaL_error(tolua_S, "%s has wrong number of arguments: %d, was expecting %d \n", "Chw3D_Mgr:processCacheFile",argc, 1);
    return 0;

#if COCOS2D_DEBUG >= 1
    tolua_lerror:
    tolua_error(tolua_S,"#ferror in function 'lua_higame_3d_Chw3D_Mgr_processCacheFile'.",&tolua_err);
#endif

    return 0;
}
int lua_higame_3d_Chw3D_Mgr_reloadMaterial(lua_State* tolua_S)
{
    int argc = 0;
    Chw3D_Mgr* cobj = nullptr;
    bool ok  = true;

#if COCOS2D_DEBUG >= 1
    tolua_Error tolua_err;
#endif


#if COCOS2D_DEBUG >= 1
    if (!tolua_isusertype(tolua_S,1,"Chw3D_Mgr",0,&tolua_err)) goto tolua_lerror;
#endif

    cobj = (Chw3D_Mgr*)tolua_tousertype(tolua_S,1,0);

#if COCOS2D_DEBUG >= 1
    if (!cobj) 
    {
        tolua_error(tolua_S,"invalid 'cobj' in function 'lua_higame_3d_Chw3D_Mgr_reloadMaterial'", nullptr);
        return 0;
    }
#endif

    argc = lua_gettop(tolua_S)-1;
    if (argc == 1) 
    {
        std::string arg0;

        ok &= luaval_to_std_string(tolua_S, 2,&arg0, "Chw3D_Mgr:reloadMaterial");
        if(!ok)
        {
            tolua_error(tolua_S,"invalid arguments in function 'lua_higame_3d_Chw3D_Mgr_reloadMaterial'", nullptr);
            return 0;
        }
        cobj->reloadMaterial(arg0);
        lua_settop(tolua_S, 1);
        return 1;
    }
    luaL_error(tolua_S, "%s has wrong number of arguments: %d, was expecting %d \n", "Chw3D_Mgr:reloadMaterial",argc, 1);
    return 0;

#if COCOS2D_DEBUG >= 1
    tolua_lerror:
    tolua_error(tolua_S,"#ferror in function 'lua_higame_3d_Chw3D_Mgr_reloadMaterial'.",&tolua_err);
#endif

    return 0;
}
int lua_higame_3d_Chw3D_Mgr_getDefaultMaterialPath(lua_State* tolua_S)
{
    int argc = 0;
    Chw3D_Mgr* cobj = nullptr;
    bool ok  = true;

#if COCOS2D_DEBUG >= 1
    tolua_Error tolua_err;
#endif


#if COCOS2D_DEBUG >= 1
    if (!tolua_isusertype(tolua_S,1,"Chw3D_Mgr",0,&tolua_err)) goto tolua_lerror;
#endif

    cobj = (Chw3D_Mgr*)tolua_tousertype(tolua_S,1,0);

#if COCOS2D_DEBUG >= 1
    if (!cobj) 
    {
        tolua_error(tolua_S,"invalid 'cobj' in function 'lua_higame_3d_Chw3D_Mgr_getDefaultMaterialPath'", nullptr);
        return 0;
    }
#endif

    argc = lua_gettop(tolua_S)-1;
    if (argc == 0) 
    {
        if(!ok)
        {
            tolua_error(tolua_S,"invalid arguments in function 'lua_higame_3d_Chw3D_Mgr_getDefaultMaterialPath'", nullptr);
            return 0;
        }
        std::string ret = cobj->getDefaultMaterialPath();
        lua_pushlstring(tolua_S,ret.c_str(),ret.length());
        return 1;
    }
    luaL_error(tolua_S, "%s has wrong number of arguments: %d, was expecting %d \n", "Chw3D_Mgr:getDefaultMaterialPath",argc, 0);
    return 0;

#if COCOS2D_DEBUG >= 1
    tolua_lerror:
    tolua_error(tolua_S,"#ferror in function 'lua_higame_3d_Chw3D_Mgr_getDefaultMaterialPath'.",&tolua_err);
#endif

    return 0;
}
int lua_higame_3d_Chw3D_Mgr_queryC3FileAsync(lua_State* tolua_S)
{
    int argc = 0;
    Chw3D_Mgr* cobj = nullptr;
    bool ok  = true;

#if COCOS2D_DEBUG >= 1
    tolua_Error tolua_err;
#endif


#if COCOS2D_DEBUG >= 1
    if (!tolua_isusertype(tolua_S,1,"Chw3D_Mgr",0,&tolua_err)) goto tolua_lerror;
#endif

    cobj = (Chw3D_Mgr*)tolua_tousertype(tolua_S,1,0);

#if COCOS2D_DEBUG >= 1
    if (!cobj) 
    {
        tolua_error(tolua_S,"invalid 'cobj' in function 'lua_higame_3d_Chw3D_Mgr_queryC3FileAsync'", nullptr);
        return 0;
    }
#endif

    argc = lua_gettop(tolua_S)-1;
    if (argc == 2) 
    {
        const char* arg0;
        std::function<void (Chw3D_C *)> arg1;

        std::string arg0_tmp; ok &= luaval_to_std_string(tolua_S, 2, &arg0_tmp, "Chw3D_Mgr:queryC3FileAsync"); arg0 = arg0_tmp.c_str();

        do {
			// Lambda binding for lua is not supported.
			assert(false);
		} while(0)
		;
        if(!ok)
        {
            tolua_error(tolua_S,"invalid arguments in function 'lua_higame_3d_Chw3D_Mgr_queryC3FileAsync'", nullptr);
            return 0;
        }
        cobj->queryC3FileAsync(arg0, arg1);
        lua_settop(tolua_S, 1);
        return 1;
    }
    luaL_error(tolua_S, "%s has wrong number of arguments: %d, was expecting %d \n", "Chw3D_Mgr:queryC3FileAsync",argc, 2);
    return 0;

#if COCOS2D_DEBUG >= 1
    tolua_lerror:
    tolua_error(tolua_S,"#ferror in function 'lua_higame_3d_Chw3D_Mgr_queryC3FileAsync'.",&tolua_err);
#endif

    return 0;
}
int lua_higame_3d_Chw3D_Mgr_getDefaultTypeA3File(lua_State* tolua_S)
{
    int argc = 0;
    Chw3D_Mgr* cobj = nullptr;
    bool ok  = true;

#if COCOS2D_DEBUG >= 1
    tolua_Error tolua_err;
#endif


#if COCOS2D_DEBUG >= 1
    if (!tolua_isusertype(tolua_S,1,"Chw3D_Mgr",0,&tolua_err)) goto tolua_lerror;
#endif

    cobj = (Chw3D_Mgr*)tolua_tousertype(tolua_S,1,0);

#if COCOS2D_DEBUG >= 1
    if (!cobj) 
    {
        tolua_error(tolua_S,"invalid 'cobj' in function 'lua_higame_3d_Chw3D_Mgr_getDefaultTypeA3File'", nullptr);
        return 0;
    }
#endif

    argc = lua_gettop(tolua_S)-1;
    if (argc == 1) 
    {
        unsigned int arg0;

        ok &= luaval_to_uint32(tolua_S, 2,&arg0, "Chw3D_Mgr:getDefaultTypeA3File");
        if(!ok)
        {
            tolua_error(tolua_S,"invalid arguments in function 'lua_higame_3d_Chw3D_Mgr_getDefaultTypeA3File'", nullptr);
            return 0;
        }
        std::string& ret = cobj->getDefaultTypeA3File(arg0);
        lua_pushlstring(tolua_S,ret.c_str(),ret.length());
        return 1;
    }
    luaL_error(tolua_S, "%s has wrong number of arguments: %d, was expecting %d \n", "Chw3D_Mgr:getDefaultTypeA3File",argc, 1);
    return 0;

#if COCOS2D_DEBUG >= 1
    tolua_lerror:
    tolua_error(tolua_S,"#ferror in function 'lua_higame_3d_Chw3D_Mgr_getDefaultTypeA3File'.",&tolua_err);
#endif

    return 0;
}
int lua_higame_3d_Chw3D_Mgr_isDebug(lua_State* tolua_S)
{
    int argc = 0;
    Chw3D_Mgr* cobj = nullptr;
    bool ok  = true;

#if COCOS2D_DEBUG >= 1
    tolua_Error tolua_err;
#endif


#if COCOS2D_DEBUG >= 1
    if (!tolua_isusertype(tolua_S,1,"Chw3D_Mgr",0,&tolua_err)) goto tolua_lerror;
#endif

    cobj = (Chw3D_Mgr*)tolua_tousertype(tolua_S,1,0);

#if COCOS2D_DEBUG >= 1
    if (!cobj) 
    {
        tolua_error(tolua_S,"invalid 'cobj' in function 'lua_higame_3d_Chw3D_Mgr_isDebug'", nullptr);
        return 0;
    }
#endif

    argc = lua_gettop(tolua_S)-1;
    if (argc == 0) 
    {
        if(!ok)
        {
            tolua_error(tolua_S,"invalid arguments in function 'lua_higame_3d_Chw3D_Mgr_isDebug'", nullptr);
            return 0;
        }
        bool ret = cobj->isDebug();
        tolua_pushboolean(tolua_S,(bool)ret);
        return 1;
    }
    luaL_error(tolua_S, "%s has wrong number of arguments: %d, was expecting %d \n", "Chw3D_Mgr:isDebug",argc, 0);
    return 0;

#if COCOS2D_DEBUG >= 1
    tolua_lerror:
    tolua_error(tolua_S,"#ferror in function 'lua_higame_3d_Chw3D_Mgr_isDebug'.",&tolua_err);
#endif

    return 0;
}
int lua_higame_3d_Chw3D_Mgr_setDebug(lua_State* tolua_S)
{
    int argc = 0;
    Chw3D_Mgr* cobj = nullptr;
    bool ok  = true;

#if COCOS2D_DEBUG >= 1
    tolua_Error tolua_err;
#endif


#if COCOS2D_DEBUG >= 1
    if (!tolua_isusertype(tolua_S,1,"Chw3D_Mgr",0,&tolua_err)) goto tolua_lerror;
#endif

    cobj = (Chw3D_Mgr*)tolua_tousertype(tolua_S,1,0);

#if COCOS2D_DEBUG >= 1
    if (!cobj) 
    {
        tolua_error(tolua_S,"invalid 'cobj' in function 'lua_higame_3d_Chw3D_Mgr_setDebug'", nullptr);
        return 0;
    }
#endif

    argc = lua_gettop(tolua_S)-1;
    if (argc == 1) 
    {
        bool arg0;

        ok &= luaval_to_boolean(tolua_S, 2,&arg0, "Chw3D_Mgr:setDebug");
        if(!ok)
        {
            tolua_error(tolua_S,"invalid arguments in function 'lua_higame_3d_Chw3D_Mgr_setDebug'", nullptr);
            return 0;
        }
        cobj->setDebug(arg0);
        lua_settop(tolua_S, 1);
        return 1;
    }
    luaL_error(tolua_S, "%s has wrong number of arguments: %d, was expecting %d \n", "Chw3D_Mgr:setDebug",argc, 1);
    return 0;

#if COCOS2D_DEBUG >= 1
    tolua_lerror:
    tolua_error(tolua_S,"#ferror in function 'lua_higame_3d_Chw3D_Mgr_setDebug'.",&tolua_err);
#endif

    return 0;
}
int lua_higame_3d_Chw3D_Mgr_getDefaultTypeC3File(lua_State* tolua_S)
{
    int argc = 0;
    Chw3D_Mgr* cobj = nullptr;
    bool ok  = true;

#if COCOS2D_DEBUG >= 1
    tolua_Error tolua_err;
#endif


#if COCOS2D_DEBUG >= 1
    if (!tolua_isusertype(tolua_S,1,"Chw3D_Mgr",0,&tolua_err)) goto tolua_lerror;
#endif

    cobj = (Chw3D_Mgr*)tolua_tousertype(tolua_S,1,0);

#if COCOS2D_DEBUG >= 1
    if (!cobj) 
    {
        tolua_error(tolua_S,"invalid 'cobj' in function 'lua_higame_3d_Chw3D_Mgr_getDefaultTypeC3File'", nullptr);
        return 0;
    }
#endif

    argc = lua_gettop(tolua_S)-1;
    if (argc == 1) 
    {
        unsigned int arg0;

        ok &= luaval_to_uint32(tolua_S, 2,&arg0, "Chw3D_Mgr:getDefaultTypeC3File");
        if(!ok)
        {
            tolua_error(tolua_S,"invalid arguments in function 'lua_higame_3d_Chw3D_Mgr_getDefaultTypeC3File'", nullptr);
            return 0;
        }
        std::string& ret = cobj->getDefaultTypeC3File(arg0);
        lua_pushlstring(tolua_S,ret.c_str(),ret.length());
        return 1;
    }
    luaL_error(tolua_S, "%s has wrong number of arguments: %d, was expecting %d \n", "Chw3D_Mgr:getDefaultTypeC3File",argc, 1);
    return 0;

#if COCOS2D_DEBUG >= 1
    tolua_lerror:
    tolua_error(tolua_S,"#ferror in function 'lua_higame_3d_Chw3D_Mgr_getDefaultTypeC3File'.",&tolua_err);
#endif

    return 0;
}
int lua_higame_3d_Chw3D_Mgr_setCacheFileExpiryTime(lua_State* tolua_S)
{
    int argc = 0;
    Chw3D_Mgr* cobj = nullptr;
    bool ok  = true;

#if COCOS2D_DEBUG >= 1
    tolua_Error tolua_err;
#endif


#if COCOS2D_DEBUG >= 1
    if (!tolua_isusertype(tolua_S,1,"Chw3D_Mgr",0,&tolua_err)) goto tolua_lerror;
#endif

    cobj = (Chw3D_Mgr*)tolua_tousertype(tolua_S,1,0);

#if COCOS2D_DEBUG >= 1
    if (!cobj) 
    {
        tolua_error(tolua_S,"invalid 'cobj' in function 'lua_higame_3d_Chw3D_Mgr_setCacheFileExpiryTime'", nullptr);
        return 0;
    }
#endif

    argc = lua_gettop(tolua_S)-1;
    if (argc == 1) 
    {
        double arg0;

        ok &= luaval_to_number(tolua_S, 2,&arg0, "Chw3D_Mgr:setCacheFileExpiryTime");
        if(!ok)
        {
            tolua_error(tolua_S,"invalid arguments in function 'lua_higame_3d_Chw3D_Mgr_setCacheFileExpiryTime'", nullptr);
            return 0;
        }
        cobj->setCacheFileExpiryTime(arg0);
        lua_settop(tolua_S, 1);
        return 1;
    }
    luaL_error(tolua_S, "%s has wrong number of arguments: %d, was expecting %d \n", "Chw3D_Mgr:setCacheFileExpiryTime",argc, 1);
    return 0;

#if COCOS2D_DEBUG >= 1
    tolua_lerror:
    tolua_error(tolua_S,"#ferror in function 'lua_higame_3d_Chw3D_Mgr_setCacheFileExpiryTime'.",&tolua_err);
#endif

    return 0;
}
int lua_higame_3d_Chw3D_Mgr_isHardwareSkinMesh(lua_State* tolua_S)
{
    int argc = 0;
    Chw3D_Mgr* cobj = nullptr;
    bool ok  = true;

#if COCOS2D_DEBUG >= 1
    tolua_Error tolua_err;
#endif


#if COCOS2D_DEBUG >= 1
    if (!tolua_isusertype(tolua_S,1,"Chw3D_Mgr",0,&tolua_err)) goto tolua_lerror;
#endif

    cobj = (Chw3D_Mgr*)tolua_tousertype(tolua_S,1,0);

#if COCOS2D_DEBUG >= 1
    if (!cobj) 
    {
        tolua_error(tolua_S,"invalid 'cobj' in function 'lua_higame_3d_Chw3D_Mgr_isHardwareSkinMesh'", nullptr);
        return 0;
    }
#endif

    argc = lua_gettop(tolua_S)-1;
    if (argc == 0) 
    {
        if(!ok)
        {
            tolua_error(tolua_S,"invalid arguments in function 'lua_higame_3d_Chw3D_Mgr_isHardwareSkinMesh'", nullptr);
            return 0;
        }
        bool ret = cobj->isHardwareSkinMesh();
        tolua_pushboolean(tolua_S,(bool)ret);
        return 1;
    }
    luaL_error(tolua_S, "%s has wrong number of arguments: %d, was expecting %d \n", "Chw3D_Mgr:isHardwareSkinMesh",argc, 0);
    return 0;

#if COCOS2D_DEBUG >= 1
    tolua_lerror:
    tolua_error(tolua_S,"#ferror in function 'lua_higame_3d_Chw3D_Mgr_isHardwareSkinMesh'.",&tolua_err);
#endif

    return 0;
}
int lua_higame_3d_Chw3D_Mgr_setCacheFileProcessTime(lua_State* tolua_S)
{
    int argc = 0;
    Chw3D_Mgr* cobj = nullptr;
    bool ok  = true;

#if COCOS2D_DEBUG >= 1
    tolua_Error tolua_err;
#endif


#if COCOS2D_DEBUG >= 1
    if (!tolua_isusertype(tolua_S,1,"Chw3D_Mgr",0,&tolua_err)) goto tolua_lerror;
#endif

    cobj = (Chw3D_Mgr*)tolua_tousertype(tolua_S,1,0);

#if COCOS2D_DEBUG >= 1
    if (!cobj) 
    {
        tolua_error(tolua_S,"invalid 'cobj' in function 'lua_higame_3d_Chw3D_Mgr_setCacheFileProcessTime'", nullptr);
        return 0;
    }
#endif

    argc = lua_gettop(tolua_S)-1;
    if (argc == 1) 
    {
        double arg0;

        ok &= luaval_to_number(tolua_S, 2,&arg0, "Chw3D_Mgr:setCacheFileProcessTime");
        if(!ok)
        {
            tolua_error(tolua_S,"invalid arguments in function 'lua_higame_3d_Chw3D_Mgr_setCacheFileProcessTime'", nullptr);
            return 0;
        }
        cobj->setCacheFileProcessTime(arg0);
        lua_settop(tolua_S, 1);
        return 1;
    }
    luaL_error(tolua_S, "%s has wrong number of arguments: %d, was expecting %d \n", "Chw3D_Mgr:setCacheFileProcessTime",argc, 1);
    return 0;

#if COCOS2D_DEBUG >= 1
    tolua_lerror:
    tolua_error(tolua_S,"#ferror in function 'lua_higame_3d_Chw3D_Mgr_setCacheFileProcessTime'.",&tolua_err);
#endif

    return 0;
}
int lua_higame_3d_Chw3D_Mgr_removeAllCacheFiles(lua_State* tolua_S)
{
    int argc = 0;
    Chw3D_Mgr* cobj = nullptr;
    bool ok  = true;

#if COCOS2D_DEBUG >= 1
    tolua_Error tolua_err;
#endif


#if COCOS2D_DEBUG >= 1
    if (!tolua_isusertype(tolua_S,1,"Chw3D_Mgr",0,&tolua_err)) goto tolua_lerror;
#endif

    cobj = (Chw3D_Mgr*)tolua_tousertype(tolua_S,1,0);

#if COCOS2D_DEBUG >= 1
    if (!cobj) 
    {
        tolua_error(tolua_S,"invalid 'cobj' in function 'lua_higame_3d_Chw3D_Mgr_removeAllCacheFiles'", nullptr);
        return 0;
    }
#endif

    argc = lua_gettop(tolua_S)-1;
    if (argc == 0) 
    {
        if(!ok)
        {
            tolua_error(tolua_S,"invalid arguments in function 'lua_higame_3d_Chw3D_Mgr_removeAllCacheFiles'", nullptr);
            return 0;
        }
        cobj->removeAllCacheFiles();
        lua_settop(tolua_S, 1);
        return 1;
    }
    luaL_error(tolua_S, "%s has wrong number of arguments: %d, was expecting %d \n", "Chw3D_Mgr:removeAllCacheFiles",argc, 0);
    return 0;

#if COCOS2D_DEBUG >= 1
    tolua_lerror:
    tolua_error(tolua_S,"#ferror in function 'lua_higame_3d_Chw3D_Mgr_removeAllCacheFiles'.",&tolua_err);
#endif

    return 0;
}
int lua_higame_3d_Chw3D_Mgr_setDefaultMaterialPath(lua_State* tolua_S)
{
    int argc = 0;
    Chw3D_Mgr* cobj = nullptr;
    bool ok  = true;

#if COCOS2D_DEBUG >= 1
    tolua_Error tolua_err;
#endif


#if COCOS2D_DEBUG >= 1
    if (!tolua_isusertype(tolua_S,1,"Chw3D_Mgr",0,&tolua_err)) goto tolua_lerror;
#endif

    cobj = (Chw3D_Mgr*)tolua_tousertype(tolua_S,1,0);

#if COCOS2D_DEBUG >= 1
    if (!cobj) 
    {
        tolua_error(tolua_S,"invalid 'cobj' in function 'lua_higame_3d_Chw3D_Mgr_setDefaultMaterialPath'", nullptr);
        return 0;
    }
#endif

    argc = lua_gettop(tolua_S)-1;
    if (argc == 1) 
    {
        std::string arg0;

        ok &= luaval_to_std_string(tolua_S, 2,&arg0, "Chw3D_Mgr:setDefaultMaterialPath");
        if(!ok)
        {
            tolua_error(tolua_S,"invalid arguments in function 'lua_higame_3d_Chw3D_Mgr_setDefaultMaterialPath'", nullptr);
            return 0;
        }
        cobj->setDefaultMaterialPath(arg0);
        lua_settop(tolua_S, 1);
        return 1;
    }
    luaL_error(tolua_S, "%s has wrong number of arguments: %d, was expecting %d \n", "Chw3D_Mgr:setDefaultMaterialPath",argc, 1);
    return 0;

#if COCOS2D_DEBUG >= 1
    tolua_lerror:
    tolua_error(tolua_S,"#ferror in function 'lua_higame_3d_Chw3D_Mgr_setDefaultMaterialPath'.",&tolua_err);
#endif

    return 0;
}
int lua_higame_3d_Chw3D_Mgr_handleMissMaterial(lua_State* tolua_S)
{
    int argc = 0;
    Chw3D_Mgr* cobj = nullptr;
    bool ok  = true;

#if COCOS2D_DEBUG >= 1
    tolua_Error tolua_err;
#endif


#if COCOS2D_DEBUG >= 1
    if (!tolua_isusertype(tolua_S,1,"Chw3D_Mgr",0,&tolua_err)) goto tolua_lerror;
#endif

    cobj = (Chw3D_Mgr*)tolua_tousertype(tolua_S,1,0);

#if COCOS2D_DEBUG >= 1
    if (!cobj) 
    {
        tolua_error(tolua_S,"invalid 'cobj' in function 'lua_higame_3d_Chw3D_Mgr_handleMissMaterial'", nullptr);
        return 0;
    }
#endif

    argc = lua_gettop(tolua_S)-1;
    if (argc == 1) 
    {
        std::string arg0;

        ok &= luaval_to_std_string(tolua_S, 2,&arg0, "Chw3D_Mgr:handleMissMaterial");
        if(!ok)
        {
            tolua_error(tolua_S,"invalid arguments in function 'lua_higame_3d_Chw3D_Mgr_handleMissMaterial'", nullptr);
            return 0;
        }
        cobj->handleMissMaterial(arg0);
        lua_settop(tolua_S, 1);
        return 1;
    }
    luaL_error(tolua_S, "%s has wrong number of arguments: %d, was expecting %d \n", "Chw3D_Mgr:handleMissMaterial",argc, 1);
    return 0;

#if COCOS2D_DEBUG >= 1
    tolua_lerror:
    tolua_error(tolua_S,"#ferror in function 'lua_higame_3d_Chw3D_Mgr_handleMissMaterial'.",&tolua_err);
#endif

    return 0;
}
int lua_higame_3d_Chw3D_Mgr_destroyInstance(lua_State* tolua_S)
{
    int argc = 0;
    bool ok  = true;

#if COCOS2D_DEBUG >= 1
    tolua_Error tolua_err;
#endif

#if COCOS2D_DEBUG >= 1
    if (!tolua_isusertable(tolua_S,1,"Chw3D_Mgr",0,&tolua_err)) goto tolua_lerror;
#endif

    argc = lua_gettop(tolua_S) - 1;

    if (argc == 0)
    {
        if(!ok)
        {
            tolua_error(tolua_S,"invalid arguments in function 'lua_higame_3d_Chw3D_Mgr_destroyInstance'", nullptr);
            return 0;
        }
        Chw3D_Mgr::destroyInstance();
        lua_settop(tolua_S, 1);
        return 1;
    }
    luaL_error(tolua_S, "%s has wrong number of arguments: %d, was expecting %d\n ", "Chw3D_Mgr:destroyInstance",argc, 0);
    return 0;
#if COCOS2D_DEBUG >= 1
    tolua_lerror:
    tolua_error(tolua_S,"#ferror in function 'lua_higame_3d_Chw3D_Mgr_destroyInstance'.",&tolua_err);
#endif
    return 0;
}
int lua_higame_3d_Chw3D_Mgr_getInstance(lua_State* tolua_S)
{
    int argc = 0;
    bool ok  = true;

#if COCOS2D_DEBUG >= 1
    tolua_Error tolua_err;
#endif

#if COCOS2D_DEBUG >= 1
    if (!tolua_isusertable(tolua_S,1,"Chw3D_Mgr",0,&tolua_err)) goto tolua_lerror;
#endif

    argc = lua_gettop(tolua_S) - 1;

    if (argc == 0)
    {
        if(!ok)
        {
            tolua_error(tolua_S,"invalid arguments in function 'lua_higame_3d_Chw3D_Mgr_getInstance'", nullptr);
            return 0;
        }
        Chw3D_Mgr* ret = Chw3D_Mgr::getInstance();
        object_to_luaval<Chw3D_Mgr>(tolua_S, "Chw3D_Mgr",(Chw3D_Mgr*)ret);
        return 1;
    }
    luaL_error(tolua_S, "%s has wrong number of arguments: %d, was expecting %d\n ", "Chw3D_Mgr:getInstance",argc, 0);
    return 0;
#if COCOS2D_DEBUG >= 1
    tolua_lerror:
    tolua_error(tolua_S,"#ferror in function 'lua_higame_3d_Chw3D_Mgr_getInstance'.",&tolua_err);
#endif
    return 0;
}
static int lua_higame_3d_Chw3D_Mgr_finalize(lua_State* tolua_S)
{
    printf("luabindings: finalizing LUA object (Chw3D_Mgr)");
    return 0;
}

int lua_register_higame_3d_Chw3D_Mgr(lua_State* tolua_S)
{
    tolua_usertype(tolua_S,"Chw3D_Mgr");
    tolua_cclass(tolua_S,"Chw3D_Mgr","Chw3D_Mgr","",nullptr);

    tolua_beginmodule(tolua_S,"Chw3D_Mgr");
        tolua_function(tolua_S,"queryA3FileAsync",lua_higame_3d_Chw3D_Mgr_queryA3FileAsync);
        tolua_function(tolua_S,"queryC3File",lua_higame_3d_Chw3D_Mgr_queryC3File);
        tolua_function(tolua_S,"NewA",lua_higame_3d_Chw3D_Mgr_NewA);
        tolua_function(tolua_S,"NewCEx",lua_higame_3d_Chw3D_Mgr_NewCEx);
        tolua_function(tolua_S,"setHardwareSkinMesh",lua_higame_3d_Chw3D_Mgr_setHardwareSkinMesh);
        tolua_function(tolua_S,"getDefaultCamera",lua_higame_3d_Chw3D_Mgr_getDefaultCamera);
        tolua_function(tolua_S,"queryA3File",lua_higame_3d_Chw3D_Mgr_queryA3File);
        tolua_function(tolua_S,"init",lua_higame_3d_Chw3D_Mgr_init);
        tolua_function(tolua_S,"setDefaultTypeFile",lua_higame_3d_Chw3D_Mgr_setDefaultTypeFile);
        tolua_function(tolua_S,"processCacheFile",lua_higame_3d_Chw3D_Mgr_processCacheFile);
        tolua_function(tolua_S,"reloadMaterial",lua_higame_3d_Chw3D_Mgr_reloadMaterial);
        tolua_function(tolua_S,"getDefaultMaterialPath",lua_higame_3d_Chw3D_Mgr_getDefaultMaterialPath);
        tolua_function(tolua_S,"queryC3FileAsync",lua_higame_3d_Chw3D_Mgr_queryC3FileAsync);
        tolua_function(tolua_S,"getDefaultTypeA3File",lua_higame_3d_Chw3D_Mgr_getDefaultTypeA3File);
        tolua_function(tolua_S,"isDebug",lua_higame_3d_Chw3D_Mgr_isDebug);
        tolua_function(tolua_S,"setDebug",lua_higame_3d_Chw3D_Mgr_setDebug);
        tolua_function(tolua_S,"getDefaultTypeC3File",lua_higame_3d_Chw3D_Mgr_getDefaultTypeC3File);
        tolua_function(tolua_S,"setCacheFileExpiryTime",lua_higame_3d_Chw3D_Mgr_setCacheFileExpiryTime);
        tolua_function(tolua_S,"isHardwareSkinMesh",lua_higame_3d_Chw3D_Mgr_isHardwareSkinMesh);
        tolua_function(tolua_S,"setCacheFileProcessTime",lua_higame_3d_Chw3D_Mgr_setCacheFileProcessTime);
        tolua_function(tolua_S,"removeAllCacheFiles",lua_higame_3d_Chw3D_Mgr_removeAllCacheFiles);
        tolua_function(tolua_S,"setDefaultMaterialPath",lua_higame_3d_Chw3D_Mgr_setDefaultMaterialPath);
        tolua_function(tolua_S,"handleMissMaterial",lua_higame_3d_Chw3D_Mgr_handleMissMaterial);
        tolua_function(tolua_S,"destroyInstance", lua_higame_3d_Chw3D_Mgr_destroyInstance);
        tolua_function(tolua_S,"getInstance", lua_higame_3d_Chw3D_Mgr_getInstance);
    tolua_endmodule(tolua_S);
    std::string typeName = typeid(Chw3D_Mgr).name();
    g_luaType[typeName] = "Chw3D_Mgr";
    g_typeCast["Chw3D_Mgr"] = "Chw3D_Mgr";
    return 1;
}
TOLUA_API int register_all_higame_3d(lua_State* tolua_S)
{
	tolua_open(tolua_S);
	
	tolua_module(tolua_S,"higame",0);
	tolua_beginmodule(tolua_S,"higame");

	lua_register_higame_3d_Chw3D_Sprite(tolua_S);
	lua_register_higame_3d_Chw3D_Camera(tolua_S);
	lua_register_higame_3d_Chw3D_Mgr(tolua_S);

	tolua_endmodule(tolua_S);
	return 1;
}

