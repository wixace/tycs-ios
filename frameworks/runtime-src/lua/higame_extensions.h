
#ifndef __LUA_EXTRA_H_
#define __LUA_EXTRA_H_

#if defined(_USRDLL)
    #define HIGAME_EXTENSIONS_DLL     __declspec(dllexport)
#else         /* use a DLL library */
    #define HIGAME_EXTENSIONS_DLL
#endif

#if __cplusplus
extern "C" {
#endif

#include "lauxlib.h"

void HIGAME_EXTENSIONS_DLL luaopen_higame_extensions(lua_State *L);
    
#if __cplusplus
}
#endif

#endif /* __LUA_EXTRA_H_ */
