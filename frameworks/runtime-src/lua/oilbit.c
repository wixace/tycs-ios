/*
* Copyright (c) 2006 Tecgraf, PUC-Rio.
* All rights reserved.
*
* Module that exports support for conversion of numbers between different
* binary formats and also support for bit manipulation.
*/

#include "oilbit.h"

#include <string.h>
#include "lauxlib.h"
/*
    为了保证mysql和sqlite保存二进制数据时的一致性，这里提供两个函数
  供转换二进制数据为处理过的字符串，保证这个处理后的字符串在mysql和
  sqlite里保存后再读取不会变样（mysql内部会对\做一些转义处理）。

  bits.encode把以下字符替换成后面的字符串（decode则做相反处理）
    '='  : "=="
    '\0' : "=0"
    '\n' : "=n"
    '\r' : "=r"
    '26' : "=Z"
    '\"' : "=Q"
    '\'' : "=q"
    '\\' : "=S"

    bits.encode -- 把二进制串编码成处理后的字符串
    bits.decode -- 把编码后的字符串还原成二进制数据
    
  local enc_string = bits.encode(stream)
  local dec_string = bits.decode(enc_string)
*/


int _encode(lpack_Buffer* b, const char* s, size_t len)
{
    const char *tail = s + len;
    lpack_buffinit(b);
    while(s != tail)
    {
		int l_ret = 0;
        char c = *s++;
        switch(c)
        {
        case '=':   l_ret = lpack_addstring(b, "=="); break;
        case '\0':  l_ret = lpack_addstring(b, "=0"); break;
        case '\n':  l_ret = lpack_addstring(b, "=n"); break;
        case '\r':  l_ret = lpack_addstring(b, "=r"); break;
        case 26:    l_ret = lpack_addstring(b, "=Z"); break;
        case '\"':  l_ret = lpack_addstring(b, "=Q"); break;
        case '\'':  l_ret = lpack_addstring(b, "=q"); break;
        case '\\':  l_ret = lpack_addstring(b, "=S"); break;
        default:    l_ret = lpack_addchar(b, c); break;
        }

		if(l_ret == 0)
		{
			return 0;
		}
    }
	return 1;
}

static int l_encode(lua_State *L) 		/** s = encode(s) */
{
    size_t len;

    lpack_Buffer b;

    const char *s = luaL_checklstring(L, 1, &len);
    
    if(_encode(&b, s, len) == 0)
	{
		return 0;
	}
    lpack_pushresult(L, &b);

    return 1;
}

static int l_decode(lua_State *L) 		/** s = decode(s) */
{
    size_t len;
	lpack_Buffer b;
    const char *s = luaL_checklstring(L, 1, &len);
    const char *tail = s + len;

	lpack_buffinit(&b);
    while(s != tail)
    {
		int l_ret = 0;
		char c = *s++;
		if(c != '=')
		{
			if(lpack_addchar(&b, c) == 0)
			{
				return 0;
			}
			continue;
		}

        c = *s++;		
        switch(c)
        {
        case '=':   l_ret = lpack_addchar(&b, '='); break;
        case '0':   l_ret = lpack_addchar(&b, '\0'); break;
        case 'n':   l_ret = lpack_addchar(&b, '\n'); break;
        case 'r':   l_ret = lpack_addchar(&b, '\r'); break;
        case 'Z':   l_ret = lpack_addchar(&b, 26); break;
        case 'Q':   l_ret = lpack_addchar(&b, '\"'); break;
        case 'q':   l_ret = lpack_addchar(&b, '\''); break;
        case 'S':   l_ret = lpack_addchar(&b, '\\'); break;
        default:    luaL_error(L, "bits.decode - unknown code '=%c' ", c); break;
        }

		if(l_ret == 0)
		{
			return 0;
		}
    }
    lpack_pushresult(L, &b);

    return 1;
}

static const luaL_reg sfuncs[] =
{
    //{"pack",	l_pack},
    //{"unpack",	l_unpack},
    {"encode",  l_encode},
    {"decode",  l_decode},
    {NULL,	NULL}
};

extern int luaopen_package_core(lua_State *L); /* lpackage.c */

int luaopen_oilbit_core(lua_State *L) {
    /* define library functions */
    luaL_openlib(L, "bits", sfuncs, 0);
    luaopen_package_core(L);
    return 0;
}
