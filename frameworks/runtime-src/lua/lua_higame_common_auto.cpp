#include "../lua/lua_higame_common_auto.hpp"
#include "hwCommAStar.h"
#include "hwCommRandom.h"
#include "scripting/lua-bindings/manual/tolua_fix.h"
#include "scripting/lua-bindings/manual/LuaBasicConversions.h"

int lua_higame_common_ChwCommAStar_clearObstacle(lua_State* tolua_S)
{
    int argc = 0;
    ChwCommAStar* cobj = nullptr;
    bool ok  = true;

#if COCOS2D_DEBUG >= 1
    tolua_Error tolua_err;
#endif


#if COCOS2D_DEBUG >= 1
    if (!tolua_isusertype(tolua_S,1,"ChwCommAStar",0,&tolua_err)) goto tolua_lerror;
#endif

    cobj = (ChwCommAStar*)tolua_tousertype(tolua_S,1,0);

#if COCOS2D_DEBUG >= 1
    if (!cobj) 
    {
        tolua_error(tolua_S,"invalid 'cobj' in function 'lua_higame_common_ChwCommAStar_clearObstacle'", nullptr);
        return 0;
    }
#endif

    argc = lua_gettop(tolua_S)-1;
    if (argc == 0) 
    {
        if(!ok)
        {
            tolua_error(tolua_S,"invalid arguments in function 'lua_higame_common_ChwCommAStar_clearObstacle'", nullptr);
            return 0;
        }
        cobj->clearObstacle();
        lua_settop(tolua_S, 1);
        return 1;
    }
    luaL_error(tolua_S, "%s has wrong number of arguments: %d, was expecting %d \n", "ChwCommAStar:clearObstacle",argc, 0);
    return 0;

#if COCOS2D_DEBUG >= 1
    tolua_lerror:
    tolua_error(tolua_S,"#ferror in function 'lua_higame_common_ChwCommAStar_clearObstacle'.",&tolua_err);
#endif

    return 0;
}
int lua_higame_common_ChwCommAStar_getElementType(lua_State* tolua_S)
{
    int argc = 0;
    ChwCommAStar* cobj = nullptr;
    bool ok  = true;

#if COCOS2D_DEBUG >= 1
    tolua_Error tolua_err;
#endif


#if COCOS2D_DEBUG >= 1
    if (!tolua_isusertype(tolua_S,1,"ChwCommAStar",0,&tolua_err)) goto tolua_lerror;
#endif

    cobj = (ChwCommAStar*)tolua_tousertype(tolua_S,1,0);

#if COCOS2D_DEBUG >= 1
    if (!cobj) 
    {
        tolua_error(tolua_S,"invalid 'cobj' in function 'lua_higame_common_ChwCommAStar_getElementType'", nullptr);
        return 0;
    }
#endif

    argc = lua_gettop(tolua_S)-1;
    if (argc == 2) 
    {
        int arg0;
        int arg1;

        ok &= luaval_to_int32(tolua_S, 2,(int *)&arg0, "ChwCommAStar:getElementType");

        ok &= luaval_to_int32(tolua_S, 3,(int *)&arg1, "ChwCommAStar:getElementType");
        if(!ok)
        {
            tolua_error(tolua_S,"invalid arguments in function 'lua_higame_common_ChwCommAStar_getElementType'", nullptr);
            return 0;
        }
        int32_t ret = cobj->getElementType(arg0, arg1);
        tolua_pushnumber(tolua_S,(lua_Number)ret);
        return 1;
    }
    luaL_error(tolua_S, "%s has wrong number of arguments: %d, was expecting %d \n", "ChwCommAStar:getElementType",argc, 2);
    return 0;

#if COCOS2D_DEBUG >= 1
    tolua_lerror:
    tolua_error(tolua_S,"#ferror in function 'lua_higame_common_ChwCommAStar_getElementType'.",&tolua_err);
#endif

    return 0;
}
int lua_higame_common_ChwCommAStar_findPath(lua_State* tolua_S)
{
    int argc = 0;
    ChwCommAStar* cobj = nullptr;
    bool ok  = true;

#if COCOS2D_DEBUG >= 1
    tolua_Error tolua_err;
#endif


#if COCOS2D_DEBUG >= 1
    if (!tolua_isusertype(tolua_S,1,"ChwCommAStar",0,&tolua_err)) goto tolua_lerror;
#endif

    cobj = (ChwCommAStar*)tolua_tousertype(tolua_S,1,0);

#if COCOS2D_DEBUG >= 1
    if (!cobj) 
    {
        tolua_error(tolua_S,"invalid 'cobj' in function 'lua_higame_common_ChwCommAStar_findPath'", nullptr);
        return 0;
    }
#endif

    argc = lua_gettop(tolua_S)-1;
    if (argc == 5) 
    {
        int arg0;
        int arg1;
        int arg2;
        int arg3;
        int arg4;

        ok &= luaval_to_int32(tolua_S, 2,(int *)&arg0, "ChwCommAStar:findPath");

        ok &= luaval_to_int32(tolua_S, 3,(int *)&arg1, "ChwCommAStar:findPath");

        ok &= luaval_to_int32(tolua_S, 4,(int *)&arg2, "ChwCommAStar:findPath");

        ok &= luaval_to_int32(tolua_S, 5,(int *)&arg3, "ChwCommAStar:findPath");

        ok &= luaval_to_int32(tolua_S, 6,(int *)&arg4, "ChwCommAStar:findPath");
        if(!ok)
        {
            tolua_error(tolua_S,"invalid arguments in function 'lua_higame_common_ChwCommAStar_findPath'", nullptr);
            return 0;
        }
        cobj->findPath(arg0, arg1, arg2, arg3, arg4);
        lua_settop(tolua_S, 1);
        return 1;
    }
    luaL_error(tolua_S, "%s has wrong number of arguments: %d, was expecting %d \n", "ChwCommAStar:findPath",argc, 5);
    return 0;

#if COCOS2D_DEBUG >= 1
    tolua_lerror:
    tolua_error(tolua_S,"#ferror in function 'lua_higame_common_ChwCommAStar_findPath'.",&tolua_err);
#endif

    return 0;
}
int lua_higame_common_ChwCommAStar_loadMapData(lua_State* tolua_S)
{
    int argc = 0;
    ChwCommAStar* cobj = nullptr;
    bool ok  = true;

#if COCOS2D_DEBUG >= 1
    tolua_Error tolua_err;
#endif


#if COCOS2D_DEBUG >= 1
    if (!tolua_isusertype(tolua_S,1,"ChwCommAStar",0,&tolua_err)) goto tolua_lerror;
#endif

    cobj = (ChwCommAStar*)tolua_tousertype(tolua_S,1,0);

#if COCOS2D_DEBUG >= 1
    if (!cobj) 
    {
        tolua_error(tolua_S,"invalid 'cobj' in function 'lua_higame_common_ChwCommAStar_loadMapData'", nullptr);
        return 0;
    }
#endif

    argc = lua_gettop(tolua_S)-1;
    if (argc == 3) 
    {
        int arg0;
        int arg1;
        std::string arg2;

        ok &= luaval_to_int32(tolua_S, 2,(int *)&arg0, "ChwCommAStar:loadMapData");

        ok &= luaval_to_int32(tolua_S, 3,(int *)&arg1, "ChwCommAStar:loadMapData");

        ok &= luaval_to_std_string(tolua_S, 4,&arg2, "ChwCommAStar:loadMapData");
        if(!ok)
        {
            tolua_error(tolua_S,"invalid arguments in function 'lua_higame_common_ChwCommAStar_loadMapData'", nullptr);
            return 0;
        }
        bool ret = cobj->loadMapData(arg0, arg1, arg2);
        tolua_pushboolean(tolua_S,(bool)ret);
        return 1;
    }
    luaL_error(tolua_S, "%s has wrong number of arguments: %d, was expecting %d \n", "ChwCommAStar:loadMapData",argc, 3);
    return 0;

#if COCOS2D_DEBUG >= 1
    tolua_lerror:
    tolua_error(tolua_S,"#ferror in function 'lua_higame_common_ChwCommAStar_loadMapData'.",&tolua_err);
#endif

    return 0;
}
int lua_higame_common_ChwCommAStar_getElementID(lua_State* tolua_S)
{
    int argc = 0;
    ChwCommAStar* cobj = nullptr;
    bool ok  = true;

#if COCOS2D_DEBUG >= 1
    tolua_Error tolua_err;
#endif


#if COCOS2D_DEBUG >= 1
    if (!tolua_isusertype(tolua_S,1,"ChwCommAStar",0,&tolua_err)) goto tolua_lerror;
#endif

    cobj = (ChwCommAStar*)tolua_tousertype(tolua_S,1,0);

#if COCOS2D_DEBUG >= 1
    if (!cobj) 
    {
        tolua_error(tolua_S,"invalid 'cobj' in function 'lua_higame_common_ChwCommAStar_getElementID'", nullptr);
        return 0;
    }
#endif

    argc = lua_gettop(tolua_S)-1;
    if (argc == 2) 
    {
        int arg0;
        int arg1;

        ok &= luaval_to_int32(tolua_S, 2,(int *)&arg0, "ChwCommAStar:getElementID");

        ok &= luaval_to_int32(tolua_S, 3,(int *)&arg1, "ChwCommAStar:getElementID");
        if(!ok)
        {
            tolua_error(tolua_S,"invalid arguments in function 'lua_higame_common_ChwCommAStar_getElementID'", nullptr);
            return 0;
        }
        int32_t ret = cobj->getElementID(arg0, arg1);
        tolua_pushnumber(tolua_S,(lua_Number)ret);
        return 1;
    }
    luaL_error(tolua_S, "%s has wrong number of arguments: %d, was expecting %d \n", "ChwCommAStar:getElementID",argc, 2);
    return 0;

#if COCOS2D_DEBUG >= 1
    tolua_lerror:
    tolua_error(tolua_S,"#ferror in function 'lua_higame_common_ChwCommAStar_getElementID'.",&tolua_err);
#endif

    return 0;
}
int lua_higame_common_ChwCommAStar_setObstacle(lua_State* tolua_S)
{
    int argc = 0;
    ChwCommAStar* cobj = nullptr;
    bool ok  = true;

#if COCOS2D_DEBUG >= 1
    tolua_Error tolua_err;
#endif


#if COCOS2D_DEBUG >= 1
    if (!tolua_isusertype(tolua_S,1,"ChwCommAStar",0,&tolua_err)) goto tolua_lerror;
#endif

    cobj = (ChwCommAStar*)tolua_tousertype(tolua_S,1,0);

#if COCOS2D_DEBUG >= 1
    if (!cobj) 
    {
        tolua_error(tolua_S,"invalid 'cobj' in function 'lua_higame_common_ChwCommAStar_setObstacle'", nullptr);
        return 0;
    }
#endif

    argc = lua_gettop(tolua_S)-1;
    if (argc == 2) 
    {
        int arg0;
        int arg1;

        ok &= luaval_to_int32(tolua_S, 2,(int *)&arg0, "ChwCommAStar:setObstacle");

        ok &= luaval_to_int32(tolua_S, 3,(int *)&arg1, "ChwCommAStar:setObstacle");
        if(!ok)
        {
            tolua_error(tolua_S,"invalid arguments in function 'lua_higame_common_ChwCommAStar_setObstacle'", nullptr);
            return 0;
        }
        cobj->setObstacle(arg0, arg1);
        lua_settop(tolua_S, 1);
        return 1;
    }
    luaL_error(tolua_S, "%s has wrong number of arguments: %d, was expecting %d \n", "ChwCommAStar:setObstacle",argc, 2);
    return 0;

#if COCOS2D_DEBUG >= 1
    tolua_lerror:
    tolua_error(tolua_S,"#ferror in function 'lua_higame_common_ChwCommAStar_setObstacle'.",&tolua_err);
#endif

    return 0;
}
int lua_higame_common_ChwCommAStar_initMap(lua_State* tolua_S)
{
    int argc = 0;
    ChwCommAStar* cobj = nullptr;
    bool ok  = true;

#if COCOS2D_DEBUG >= 1
    tolua_Error tolua_err;
#endif


#if COCOS2D_DEBUG >= 1
    if (!tolua_isusertype(tolua_S,1,"ChwCommAStar",0,&tolua_err)) goto tolua_lerror;
#endif

    cobj = (ChwCommAStar*)tolua_tousertype(tolua_S,1,0);

#if COCOS2D_DEBUG >= 1
    if (!cobj) 
    {
        tolua_error(tolua_S,"invalid 'cobj' in function 'lua_higame_common_ChwCommAStar_initMap'", nullptr);
        return 0;
    }
#endif

    argc = lua_gettop(tolua_S)-1;
    if (argc == 0) 
    {
        if(!ok)
        {
            tolua_error(tolua_S,"invalid arguments in function 'lua_higame_common_ChwCommAStar_initMap'", nullptr);
            return 0;
        }
        cobj->initMap();
        lua_settop(tolua_S, 1);
        return 1;
    }
    luaL_error(tolua_S, "%s has wrong number of arguments: %d, was expecting %d \n", "ChwCommAStar:initMap",argc, 0);
    return 0;

#if COCOS2D_DEBUG >= 1
    tolua_lerror:
    tolua_error(tolua_S,"#ferror in function 'lua_higame_common_ChwCommAStar_initMap'.",&tolua_err);
#endif

    return 0;
}
int lua_higame_common_ChwCommAStar_setElementType(lua_State* tolua_S)
{
    int argc = 0;
    ChwCommAStar* cobj = nullptr;
    bool ok  = true;

#if COCOS2D_DEBUG >= 1
    tolua_Error tolua_err;
#endif


#if COCOS2D_DEBUG >= 1
    if (!tolua_isusertype(tolua_S,1,"ChwCommAStar",0,&tolua_err)) goto tolua_lerror;
#endif

    cobj = (ChwCommAStar*)tolua_tousertype(tolua_S,1,0);

#if COCOS2D_DEBUG >= 1
    if (!cobj) 
    {
        tolua_error(tolua_S,"invalid 'cobj' in function 'lua_higame_common_ChwCommAStar_setElementType'", nullptr);
        return 0;
    }
#endif

    argc = lua_gettop(tolua_S)-1;
    if (argc == 3) 
    {
        int arg0;
        int arg1;
        int arg2;

        ok &= luaval_to_int32(tolua_S, 2,(int *)&arg0, "ChwCommAStar:setElementType");

        ok &= luaval_to_int32(tolua_S, 3,(int *)&arg1, "ChwCommAStar:setElementType");

        ok &= luaval_to_int32(tolua_S, 4,(int *)&arg2, "ChwCommAStar:setElementType");
        if(!ok)
        {
            tolua_error(tolua_S,"invalid arguments in function 'lua_higame_common_ChwCommAStar_setElementType'", nullptr);
            return 0;
        }
        cobj->setElementType(arg0, arg1, arg2);
        lua_settop(tolua_S, 1);
        return 1;
    }
    luaL_error(tolua_S, "%s has wrong number of arguments: %d, was expecting %d \n", "ChwCommAStar:setElementType",argc, 3);
    return 0;

#if COCOS2D_DEBUG >= 1
    tolua_lerror:
    tolua_error(tolua_S,"#ferror in function 'lua_higame_common_ChwCommAStar_setElementType'.",&tolua_err);
#endif

    return 0;
}
int lua_higame_common_ChwCommAStar_initMapBound(lua_State* tolua_S)
{
    int argc = 0;
    ChwCommAStar* cobj = nullptr;
    bool ok  = true;

#if COCOS2D_DEBUG >= 1
    tolua_Error tolua_err;
#endif


#if COCOS2D_DEBUG >= 1
    if (!tolua_isusertype(tolua_S,1,"ChwCommAStar",0,&tolua_err)) goto tolua_lerror;
#endif

    cobj = (ChwCommAStar*)tolua_tousertype(tolua_S,1,0);

#if COCOS2D_DEBUG >= 1
    if (!cobj) 
    {
        tolua_error(tolua_S,"invalid 'cobj' in function 'lua_higame_common_ChwCommAStar_initMapBound'", nullptr);
        return 0;
    }
#endif

    argc = lua_gettop(tolua_S)-1;
    if (argc == 2) 
    {
        int arg0;
        int arg1;

        ok &= luaval_to_int32(tolua_S, 2,(int *)&arg0, "ChwCommAStar:initMapBound");

        ok &= luaval_to_int32(tolua_S, 3,(int *)&arg1, "ChwCommAStar:initMapBound");
        if(!ok)
        {
            tolua_error(tolua_S,"invalid arguments in function 'lua_higame_common_ChwCommAStar_initMapBound'", nullptr);
            return 0;
        }
        cobj->initMapBound(arg0, arg1);
        lua_settop(tolua_S, 1);
        return 1;
    }
    luaL_error(tolua_S, "%s has wrong number of arguments: %d, was expecting %d \n", "ChwCommAStar:initMapBound",argc, 2);
    return 0;

#if COCOS2D_DEBUG >= 1
    tolua_lerror:
    tolua_error(tolua_S,"#ferror in function 'lua_higame_common_ChwCommAStar_initMapBound'.",&tolua_err);
#endif

    return 0;
}
int lua_higame_common_ChwCommAStar_setElementID(lua_State* tolua_S)
{
    int argc = 0;
    ChwCommAStar* cobj = nullptr;
    bool ok  = true;

#if COCOS2D_DEBUG >= 1
    tolua_Error tolua_err;
#endif


#if COCOS2D_DEBUG >= 1
    if (!tolua_isusertype(tolua_S,1,"ChwCommAStar",0,&tolua_err)) goto tolua_lerror;
#endif

    cobj = (ChwCommAStar*)tolua_tousertype(tolua_S,1,0);

#if COCOS2D_DEBUG >= 1
    if (!cobj) 
    {
        tolua_error(tolua_S,"invalid 'cobj' in function 'lua_higame_common_ChwCommAStar_setElementID'", nullptr);
        return 0;
    }
#endif

    argc = lua_gettop(tolua_S)-1;
    if (argc == 3) 
    {
        int arg0;
        int arg1;
        int arg2;

        ok &= luaval_to_int32(tolua_S, 2,(int *)&arg0, "ChwCommAStar:setElementID");

        ok &= luaval_to_int32(tolua_S, 3,(int *)&arg1, "ChwCommAStar:setElementID");

        ok &= luaval_to_int32(tolua_S, 4,(int *)&arg2, "ChwCommAStar:setElementID");
        if(!ok)
        {
            tolua_error(tolua_S,"invalid arguments in function 'lua_higame_common_ChwCommAStar_setElementID'", nullptr);
            return 0;
        }
        cobj->setElementID(arg0, arg1, arg2);
        lua_settop(tolua_S, 1);
        return 1;
    }
    luaL_error(tolua_S, "%s has wrong number of arguments: %d, was expecting %d \n", "ChwCommAStar:setElementID",argc, 3);
    return 0;

#if COCOS2D_DEBUG >= 1
    tolua_lerror:
    tolua_error(tolua_S,"#ferror in function 'lua_higame_common_ChwCommAStar_setElementID'.",&tolua_err);
#endif

    return 0;
}
int lua_higame_common_ChwCommAStar_initSTL(lua_State* tolua_S)
{
    int argc = 0;
    ChwCommAStar* cobj = nullptr;
    bool ok  = true;

#if COCOS2D_DEBUG >= 1
    tolua_Error tolua_err;
#endif


#if COCOS2D_DEBUG >= 1
    if (!tolua_isusertype(tolua_S,1,"ChwCommAStar",0,&tolua_err)) goto tolua_lerror;
#endif

    cobj = (ChwCommAStar*)tolua_tousertype(tolua_S,1,0);

#if COCOS2D_DEBUG >= 1
    if (!cobj) 
    {
        tolua_error(tolua_S,"invalid 'cobj' in function 'lua_higame_common_ChwCommAStar_initSTL'", nullptr);
        return 0;
    }
#endif

    argc = lua_gettop(tolua_S)-1;
    if (argc == 0) 
    {
        if(!ok)
        {
            tolua_error(tolua_S,"invalid arguments in function 'lua_higame_common_ChwCommAStar_initSTL'", nullptr);
            return 0;
        }
        cobj->initSTL();
        lua_settop(tolua_S, 1);
        return 1;
    }
    luaL_error(tolua_S, "%s has wrong number of arguments: %d, was expecting %d \n", "ChwCommAStar:initSTL",argc, 0);
    return 0;

#if COCOS2D_DEBUG >= 1
    tolua_lerror:
    tolua_error(tolua_S,"#ferror in function 'lua_higame_common_ChwCommAStar_initSTL'.",&tolua_err);
#endif

    return 0;
}
int lua_higame_common_ChwCommAStar_constructor(lua_State* tolua_S)
{
    int argc = 0;
    ChwCommAStar* cobj = nullptr;
    bool ok  = true;

#if COCOS2D_DEBUG >= 1
    tolua_Error tolua_err;
#endif



    argc = lua_gettop(tolua_S)-1;
    if (argc == 0) 
    {
        if(!ok)
        {
            tolua_error(tolua_S,"invalid arguments in function 'lua_higame_common_ChwCommAStar_constructor'", nullptr);
            return 0;
        }
        cobj = new ChwCommAStar();
        cobj->autorelease();
        int ID =  (int)cobj->_ID ;
        int* luaID =  &cobj->_luaID ;
        toluafix_pushusertype_ccobject(tolua_S, ID, luaID, (void*)cobj,"ChwCommAStar");
        return 1;
    }
    luaL_error(tolua_S, "%s has wrong number of arguments: %d, was expecting %d \n", "ChwCommAStar:ChwCommAStar",argc, 0);
    return 0;

#if COCOS2D_DEBUG >= 1
    tolua_error(tolua_S,"#ferror in function 'lua_higame_common_ChwCommAStar_constructor'.",&tolua_err);
#endif

    return 0;
}

static int lua_higame_common_ChwCommAStar_finalize(lua_State* tolua_S)
{
    printf("luabindings: finalizing LUA object (ChwCommAStar)");
    return 0;
}

int lua_register_higame_common_ChwCommAStar(lua_State* tolua_S)
{
    tolua_usertype(tolua_S,"ChwCommAStar");
    tolua_cclass(tolua_S,"ChwCommAStar","ChwCommAStar","cc.Ref",nullptr);

    tolua_beginmodule(tolua_S,"ChwCommAStar");
        tolua_function(tolua_S,"new",lua_higame_common_ChwCommAStar_constructor);
        tolua_function(tolua_S,"clearObstacle",lua_higame_common_ChwCommAStar_clearObstacle);
        tolua_function(tolua_S,"getElementType",lua_higame_common_ChwCommAStar_getElementType);
        tolua_function(tolua_S,"findPath",lua_higame_common_ChwCommAStar_findPath);
        tolua_function(tolua_S,"loadMapData",lua_higame_common_ChwCommAStar_loadMapData);
        tolua_function(tolua_S,"getElementID",lua_higame_common_ChwCommAStar_getElementID);
        tolua_function(tolua_S,"setObstacle",lua_higame_common_ChwCommAStar_setObstacle);
        tolua_function(tolua_S,"initMap",lua_higame_common_ChwCommAStar_initMap);
        tolua_function(tolua_S,"setElementType",lua_higame_common_ChwCommAStar_setElementType);
        tolua_function(tolua_S,"initMapBound",lua_higame_common_ChwCommAStar_initMapBound);
        tolua_function(tolua_S,"setElementID",lua_higame_common_ChwCommAStar_setElementID);
        tolua_function(tolua_S,"initSTL",lua_higame_common_ChwCommAStar_initSTL);
    tolua_endmodule(tolua_S);
    std::string typeName = typeid(ChwCommAStar).name();
    g_luaType[typeName] = "ChwCommAStar";
    g_typeCast["ChwCommAStar"] = "ChwCommAStar";
    return 1;
}

int lua_higame_common_ChwCommRandom_Int(lua_State* tolua_S)
{
    int argc = 0;
    ChwCommRandom* cobj = nullptr;
    bool ok  = true;

#if COCOS2D_DEBUG >= 1
    tolua_Error tolua_err;
#endif


#if COCOS2D_DEBUG >= 1
    if (!tolua_isusertype(tolua_S,1,"ChwCommRandom",0,&tolua_err)) goto tolua_lerror;
#endif

    cobj = (ChwCommRandom*)tolua_tousertype(tolua_S,1,0);

#if COCOS2D_DEBUG >= 1
    if (!cobj) 
    {
        tolua_error(tolua_S,"invalid 'cobj' in function 'lua_higame_common_ChwCommRandom_Int'", nullptr);
        return 0;
    }
#endif

    argc = lua_gettop(tolua_S)-1;
    if (argc == 2) 
    {
        int arg0;
        int arg1;

        ok &= luaval_to_int32(tolua_S, 2,(int *)&arg0, "ChwCommRandom:Int");

        ok &= luaval_to_int32(tolua_S, 3,(int *)&arg1, "ChwCommRandom:Int");
        if(!ok)
        {
            tolua_error(tolua_S,"invalid arguments in function 'lua_higame_common_ChwCommRandom_Int'", nullptr);
            return 0;
        }
        int ret = cobj->Int(arg0, arg1);
        tolua_pushnumber(tolua_S,(lua_Number)ret);
        return 1;
    }
    luaL_error(tolua_S, "%s has wrong number of arguments: %d, was expecting %d \n", "ChwCommRandom:Int",argc, 2);
    return 0;

#if COCOS2D_DEBUG >= 1
    tolua_lerror:
    tolua_error(tolua_S,"#ferror in function 'lua_higame_common_ChwCommRandom_Int'.",&tolua_err);
#endif

    return 0;
}
int lua_higame_common_ChwCommRandom_Double(lua_State* tolua_S)
{
    int argc = 0;
    ChwCommRandom* cobj = nullptr;
    bool ok  = true;

#if COCOS2D_DEBUG >= 1
    tolua_Error tolua_err;
#endif


#if COCOS2D_DEBUG >= 1
    if (!tolua_isusertype(tolua_S,1,"ChwCommRandom",0,&tolua_err)) goto tolua_lerror;
#endif

    cobj = (ChwCommRandom*)tolua_tousertype(tolua_S,1,0);

#if COCOS2D_DEBUG >= 1
    if (!cobj) 
    {
        tolua_error(tolua_S,"invalid 'cobj' in function 'lua_higame_common_ChwCommRandom_Double'", nullptr);
        return 0;
    }
#endif

    argc = lua_gettop(tolua_S)-1;
    if (argc == 2) 
    {
        double arg0;
        double arg1;

        ok &= luaval_to_number(tolua_S, 2,&arg0, "ChwCommRandom:Double");

        ok &= luaval_to_number(tolua_S, 3,&arg1, "ChwCommRandom:Double");
        if(!ok)
        {
            tolua_error(tolua_S,"invalid arguments in function 'lua_higame_common_ChwCommRandom_Double'", nullptr);
            return 0;
        }
        double ret = cobj->Double(arg0, arg1);
        tolua_pushnumber(tolua_S,(lua_Number)ret);
        return 1;
    }
    luaL_error(tolua_S, "%s has wrong number of arguments: %d, was expecting %d \n", "ChwCommRandom:Double",argc, 2);
    return 0;

#if COCOS2D_DEBUG >= 1
    tolua_lerror:
    tolua_error(tolua_S,"#ferror in function 'lua_higame_common_ChwCommRandom_Double'.",&tolua_err);
#endif

    return 0;
}
int lua_higame_common_ChwCommRandom_Seed(lua_State* tolua_S)
{
    int argc = 0;
    ChwCommRandom* cobj = nullptr;
    bool ok  = true;

#if COCOS2D_DEBUG >= 1
    tolua_Error tolua_err;
#endif


#if COCOS2D_DEBUG >= 1
    if (!tolua_isusertype(tolua_S,1,"ChwCommRandom",0,&tolua_err)) goto tolua_lerror;
#endif

    cobj = (ChwCommRandom*)tolua_tousertype(tolua_S,1,0);

#if COCOS2D_DEBUG >= 1
    if (!cobj) 
    {
        tolua_error(tolua_S,"invalid 'cobj' in function 'lua_higame_common_ChwCommRandom_Seed'", nullptr);
        return 0;
    }
#endif

    argc = lua_gettop(tolua_S)-1;
    if (argc == 1) 
    {
        unsigned int arg0;

        ok &= luaval_to_uint32(tolua_S, 2,&arg0, "ChwCommRandom:Seed");
        if(!ok)
        {
            tolua_error(tolua_S,"invalid arguments in function 'lua_higame_common_ChwCommRandom_Seed'", nullptr);
            return 0;
        }
        cobj->Seed(arg0);
        lua_settop(tolua_S, 1);
        return 1;
    }
    luaL_error(tolua_S, "%s has wrong number of arguments: %d, was expecting %d \n", "ChwCommRandom:Seed",argc, 1);
    return 0;

#if COCOS2D_DEBUG >= 1
    tolua_lerror:
    tolua_error(tolua_S,"#ferror in function 'lua_higame_common_ChwCommRandom_Seed'.",&tolua_err);
#endif

    return 0;
}
int lua_higame_common_ChwCommRandom_GenUUID(lua_State* tolua_S)
{
    int argc = 0;
    ChwCommRandom* cobj = nullptr;
    bool ok  = true;

#if COCOS2D_DEBUG >= 1
    tolua_Error tolua_err;
#endif


#if COCOS2D_DEBUG >= 1
    if (!tolua_isusertype(tolua_S,1,"ChwCommRandom",0,&tolua_err)) goto tolua_lerror;
#endif

    cobj = (ChwCommRandom*)tolua_tousertype(tolua_S,1,0);

#if COCOS2D_DEBUG >= 1
    if (!cobj) 
    {
        tolua_error(tolua_S,"invalid 'cobj' in function 'lua_higame_common_ChwCommRandom_GenUUID'", nullptr);
        return 0;
    }
#endif

    argc = lua_gettop(tolua_S)-1;
    if (argc == 0) 
    {
        if(!ok)
        {
            tolua_error(tolua_S,"invalid arguments in function 'lua_higame_common_ChwCommRandom_GenUUID'", nullptr);
            return 0;
        }
        std::string ret = cobj->GenUUID();
        lua_pushlstring(tolua_S,ret.c_str(),ret.length());
        return 1;
    }
    luaL_error(tolua_S, "%s has wrong number of arguments: %d, was expecting %d \n", "ChwCommRandom:GenUUID",argc, 0);
    return 0;

#if COCOS2D_DEBUG >= 1
    tolua_lerror:
    tolua_error(tolua_S,"#ferror in function 'lua_higame_common_ChwCommRandom_GenUUID'.",&tolua_err);
#endif

    return 0;
}
int lua_higame_common_ChwCommRandom_constructor(lua_State* tolua_S)
{
    int argc = 0;
    ChwCommRandom* cobj = nullptr;
    bool ok  = true;

#if COCOS2D_DEBUG >= 1
    tolua_Error tolua_err;
#endif



    argc = lua_gettop(tolua_S)-1;
    if (argc == 0) 
    {
        if(!ok)
        {
            tolua_error(tolua_S,"invalid arguments in function 'lua_higame_common_ChwCommRandom_constructor'", nullptr);
            return 0;
        }
        cobj = new ChwCommRandom();
        cobj->autorelease();
        int ID =  (int)cobj->_ID ;
        int* luaID =  &cobj->_luaID ;
        toluafix_pushusertype_ccobject(tolua_S, ID, luaID, (void*)cobj,"ChwCommRandom");
        return 1;
    }
    luaL_error(tolua_S, "%s has wrong number of arguments: %d, was expecting %d \n", "ChwCommRandom:ChwCommRandom",argc, 0);
    return 0;

#if COCOS2D_DEBUG >= 1
    tolua_error(tolua_S,"#ferror in function 'lua_higame_common_ChwCommRandom_constructor'.",&tolua_err);
#endif

    return 0;
}

static int lua_higame_common_ChwCommRandom_finalize(lua_State* tolua_S)
{
    printf("luabindings: finalizing LUA object (ChwCommRandom)");
    return 0;
}

int lua_register_higame_common_ChwCommRandom(lua_State* tolua_S)
{
    tolua_usertype(tolua_S,"ChwCommRandom");
    tolua_cclass(tolua_S,"ChwCommRandom","ChwCommRandom","cc.Ref",nullptr);

    tolua_beginmodule(tolua_S,"ChwCommRandom");
        tolua_function(tolua_S,"new",lua_higame_common_ChwCommRandom_constructor);
        tolua_function(tolua_S,"Int",lua_higame_common_ChwCommRandom_Int);
        tolua_function(tolua_S,"Double",lua_higame_common_ChwCommRandom_Double);
        tolua_function(tolua_S,"Seed",lua_higame_common_ChwCommRandom_Seed);
        tolua_function(tolua_S,"GenUUID",lua_higame_common_ChwCommRandom_GenUUID);
    tolua_endmodule(tolua_S);
    std::string typeName = typeid(ChwCommRandom).name();
    g_luaType[typeName] = "ChwCommRandom";
    g_typeCast["ChwCommRandom"] = "ChwCommRandom";
    return 1;
}
TOLUA_API int register_all_higame_common(lua_State* tolua_S)
{
	tolua_open(tolua_S);
	
	tolua_module(tolua_S,"higame",0);
	tolua_beginmodule(tolua_S,"higame");

	lua_register_higame_common_ChwCommRandom(tolua_S);
	lua_register_higame_common_ChwCommAStar(tolua_S);

	tolua_endmodule(tolua_S);
	return 1;
}

