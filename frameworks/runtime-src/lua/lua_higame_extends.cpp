#include "lua_higame_extends.hpp"


#include <time.h>
#include "tolua_fix.h"
#include "hwNetworkClient.h"
#include "hwCommAStar.h"
#include "LuaBasicConversions.h"
#include "CCLuaEngine.h"
#include "base/ccUtils.h"
#include "DownloadWorker.h"

#include "cocos2dx_extra_luabinding.h"
#include "higame_extensions.h"
#include "lua_higame_common_auto.hpp"
#include "lua_higame_network_auto.hpp"
#include "lua_higame_3d_auto.hpp"
#include "lua_higame_ui_auto.hpp"
#include "lua_higame_shader_auto.hpp"
#include "lua_tthw_extends_voice.hpp"
#include "lua_tthw_voice_auto.hpp"
#include "lua_higame_systemutils_auto.hpp"
#include "lua_higame_qiniu_auto.hpp"

#if (CC_TARGET_PLATFORM == CC_PLATFORM_ANDROID)
    #include "lua_higame_permission_auto.hpp"
#endif

#include "pbc-lua.h"
#include "lua-plus.h"
#include "lua-plus.h"
#include "oilbit.h"
#include "hw3D_Sprite.h"
#include "hw3D_Camera.h"
#include "hw3D_Mgr.h"
#include "UIDyListView.h"
#include "SystemUtils.h"

#include "HiGameSDK.h"
#include "WaveShader.h"
#include "lua_higame_sdk_auto.hpp"
#include "HW_QiNiuSDK.h"

#if (CC_TARGET_PLATFORM == CC_PLATFORM_WIN32)
#include "lua/snapshot.h"
#include <sys/timeb.h>
#endif

//
//int tolua_higame_ChwNetwokClient_RegisterHandler(lua_State* tolua_S)
//{
//    if (NULL == tolua_S)
//        return 0;
//    int argc = 0;
//       
//	ChwNetworkClient* cobj = nullptr;
//	cobj = (ChwNetworkClient*)tolua_tousertype(tolua_S,1,0);
// 
//	argc = lua_gettop(tolua_S)-1;  
//    if (2 == argc)
//    {
//        //第3个参数，就是Lua里的function 这里要通过toluafix_ref_function这个函数映射成一个Int值
//        std::string type = (std::string)tolua_tocppstring(tolua_S,2,0);
//        int handler = (toluafix_ref_function(tolua_S,3,0)); 
//        cobj->RegisterHandler(type, handler);
//         
//        return 0;
//    }
//    return 0;
//}
//

//static void  extends_ChwNetwokClient(lua_State* tolua_S)
//{
//	lua_pushstring(tolua_S, "ChwNetworkClient");
//	lua_rawget(tolua_S, LUA_REGISTRYINDEX);
//	if (lua_istable(tolua_S,-1))
//	{
//		lua_pushstring(tolua_S, "RegisterHandler");
//		lua_pushcfunction(tolua_S, tolua_higame_ChwNetwokClient_RegisterHandler);
//		lua_rawset(tolua_S,-3);
//	}
//
//	lua_pop(tolua_S, 1);
//}

int tolua_higame_ChwCommAstar_GetPath(lua_State* tolua_S)
{
	if (NULL == tolua_S)
		return 0;
	int argc = 0;

	ChwCommAStar* cobj = nullptr;
	cobj = (ChwCommAStar*)tolua_tousertype(tolua_S,1,0);
	cobj->getPath(tolua_S);
	return 1;
}

static void  extends_ChwCommAStar(lua_State* tolua_S)
{
	tolua_usertype(tolua_S,"ChwCommAStar");
	tolua_cclass(tolua_S,"ChwCommAStar","ChwCommAStar","cc.Ref",nullptr);

	tolua_module(tolua_S, "ChwCommAStar", 0);
	tolua_beginmodule(tolua_S,"ChwCommAStar");
		tolua_function(tolua_S, "getPath", tolua_higame_ChwCommAstar_GetPath);
	tolua_endmodule(tolua_S);
}

int tolua_GetSysTime(lua_State* tolua_S)
{
	long long nowTime = 0;
#if (CC_TARGET_PLATFORM == CC_PLATFORM_WIN32)

	timeb t;
	ftime(&t);
	nowTime = t.time * 1000 + t.millitm;
	//nowTime = cocos2d::utils::getTimeInMilliseconds();
#else
	nowTime = cocos2d::utils::getTimeInMilliseconds();
#endif		
	//CCLOG("tolua_GetSysTime %ld", nowTime);
	tolua_pushnumber(tolua_S, nowTime);
	return 1;
}

int tolua_higame_ChwNetwokClient_RegisterHandler(lua_State* tolua_S)
{
	if (NULL == tolua_S)
		return 0;
	int argc = 0;

	ChwNetworkClient* cobj = nullptr;
	cobj = (ChwNetworkClient*)tolua_tousertype(tolua_S,1,0);

	argc = lua_gettop(tolua_S)-1;  
	if (2 == argc)
	{
		//第3个参数，就是Lua里的function 这里要通过toluafix_ref_function这个函数映射成一个Int值
		std::string type = (std::string)tolua_tocppstring(tolua_S,2,0);
		int handler = (toluafix_ref_function(tolua_S,3,0)); 
		cobj->RegisterHandler(type, handler);

		return 0;
	}
	return 0;
}

static void  extends_ChwChwNetwokClient(lua_State* tolua_S)
{
	tolua_usertype(tolua_S,"ChwNetworkClient");
	tolua_cclass(tolua_S,"ChwNetworkClient","ChwNetworkClient","cc.Ref",nullptr);

	tolua_module(tolua_S, "ChwNetworkClient", 0);
	tolua_beginmodule(tolua_S,"ChwNetworkClient");
	tolua_function(tolua_S, "RegisterHandler", tolua_higame_ChwNetwokClient_RegisterHandler);
	tolua_endmodule(tolua_S);
}


static int lua_cocos2dx_SystemUtils_setOpCallback(lua_State* L)
{
	if (nullptr == L)
		return 0;

	int argc = 0;
	SystemUtils* self = nullptr;

#if COCOS2D_DEBUG >= 1
	tolua_Error tolua_err;
	if (!tolua_isusertype(L, 1, "higame.SystemUtils", 0, &tolua_err)) goto tolua_lerror;
#endif

	self = static_cast<SystemUtils*>(tolua_tousertype(L, 1, 0));

#if COCOS2D_DEBUG >= 1
	if (nullptr == self) {
		tolua_error(L, "invalid 'self' in function 'lua_cocos2dx_SystemUtils_setOpCallback'\n", NULL);
		return 0;
	}
#endif
	argc = lua_gettop(L) - 1;
	if (1 == argc)
	{
#if COCOS2D_DEBUG >= 1
		if (!toluafix_isfunction(L, 2, "LUA_FUNCTION", 0, &tolua_err))
		{
			goto tolua_lerror;
		}
#endif
		LUA_FUNCTION handler = (toluafix_ref_function(L, 2, 0));
		auto callback = [=](int ft, int code, std::string data) {

			LuaStack* stack = LuaEngine::getInstance()->getLuaStack();
			stack->pushInt(ft);
            stack->pushInt(code);
			stack->pushString(data.c_str(), data.length());
			stack->executeFunctionByHandler(handler, 3);
			stack->clean();
		};
		self->setOpCallback(callback);
		return 0;
	}

	luaL_error(L, "'setOpCallback' function of SystemUtils has wrong number of arguments: %d, was expecting %d\n", argc, 1);

	return 0;

#if COCOS2D_DEBUG >= 1
	tolua_lerror:
				tolua_error(L, "#ferror in function 'lua_cocos2dx_SystemUtils_setOpCallback'.", &tolua_err);
				return 0;
#endif
}

static void  extends_SystemUtils(lua_State* tolua_S)
{
	tolua_module(tolua_S, "SystemUtils", 0);
	tolua_beginmodule(tolua_S, "SystemUtils");
	tolua_function(tolua_S, "setOpCallback", lua_cocos2dx_SystemUtils_setOpCallback);
	tolua_endmodule(tolua_S);
}

static int lua_cocos2dx_DyListView_setViewDelegate(lua_State* L)
{
	if (nullptr == L)
		return 0;

	int argc = 0;
	ui::DyListView* self = nullptr;

#if COCOS2D_DEBUG >= 1
	tolua_Error tolua_err;
	if (!tolua_isusertype(L, 1, "ccui.DyListView", 0, &tolua_err)) goto tolua_lerror;
#endif

	self = static_cast<ui::DyListView*>(tolua_tousertype(L, 1, 0));

#if COCOS2D_DEBUG >= 1
	if (nullptr == self) {
		tolua_error(L, "invalid 'self' in function 'lua_cocos2dx_DyListView_setViewDelegate'\n", NULL);
		return 0;
	}
#endif
	argc = lua_gettop(L) - 1;
	if (1 == argc)
	{
#if COCOS2D_DEBUG >= 1
		if (!toluafix_isfunction(L, 2, "LUA_FUNCTION", 0, &tolua_err))
		{
			goto tolua_lerror;
		}
#endif
		LUA_FUNCTION handler = (toluafix_ref_function(L, 2, 0));
		auto listViewDelegate = [=](Ref* ref, ui::DyListView::DelegateTag tag, unsigned int index, ui::Widget* widget) {
			
			LuaStack* stack = LuaEngine::getInstance()->getLuaStack();
			stack->pushObject(ref, "cc.Ref");
			stack->pushInt((int)tag);
			stack->pushInt(index);
			if (widget)
			{
				stack->pushObject((ui::DyListViewItem*)widget, "ccui.DyListViewItem");
			}
			else
			{
				stack->pushNil();
			}
			

			stack->executeFunctionByHandler(handler, 4);
			stack->clean();
		};
		self->setViewDelegate(listViewDelegate);

		ScriptHandlerMgr::getInstance()->addCustomHandler((void*)self, handler);
		return 0;
	}

	luaL_error(L, "'addEventListener' function of ListView has wrong number of arguments: %d, was expecting %d\n", argc, 1);

	return 0;

#if COCOS2D_DEBUG >= 1
	tolua_lerror:
				tolua_error(L, "#ferror in function 'addEventListener'.", &tolua_err);
				return 0;
#endif
}

static void  extends_DyListView(lua_State* tolua_S)
{
	
	tolua_module(tolua_S, "ccui", 0);
	tolua_beginmodule(tolua_S, "ccui");

	tolua_module(tolua_S, "DyListView", 0);
	tolua_beginmodule(tolua_S, "DyListView");
	tolua_function(tolua_S, "setViewDelegate", lua_cocos2dx_DyListView_setViewDelegate);
	tolua_endmodule(tolua_S);
	tolua_endmodule(tolua_S);
}


int extends_lua_cocos2dx_ZipUtils_uncompressDir_thread(std::string& filename, std::string& destPath, int callFunc, bool isCallbckCount=false)
{
	if(isCallbckCount)
	{
		bool isSuccess = cocos2d::ZipUtils::uncompressDir(filename.c_str(), destPath.c_str(), [=](unsigned int count, unsigned int total)
			{
				Scheduler *sched = Director::getInstance()->getScheduler();
				sched->performFunctionInCocosThread([=]() {

					LuaStack *stack = LuaEngine::getInstance()->getLuaStack();
					stack->pushInt(2);
					stack->pushInt(count);
					stack->pushInt(total);
					stack->executeFunctionByHandler(callFunc, 3);
				});

			});

		Scheduler *sched = Director::getInstance()->getScheduler();
		sched->performFunctionInCocosThread([=]() {

			LuaStack *stack = LuaEngine::getInstance()->getLuaStack();
			stack->pushInt(isSuccess);
			stack->pushInt(0);
			stack->pushInt(0);
			stack->executeFunctionByHandler(callFunc, 3);
		});

		return 1;
	}
	else
	{
		auto isSuccess = cocos2d::ZipUtils::uncompressDir(filename.c_str(), destPath.c_str());
		if(callFunc != 0)
		{
			Scheduler *sched = Director::getInstance()->getScheduler();
			sched->performFunctionInCocosThread( [=](){

				LuaStack *stack = LuaEngine::getInstance()->getLuaStack();
				stack->pushBoolean(isSuccess);
				stack->executeFunctionByHandler(callFunc, 1);
			});
		}
		return 1;
	}
}


int extends_lua_cocos2dx_ZipUtils_uncompressDir(lua_State* tolua_S)
{
    int argc = 0;
    bool ok  = true;

#if COCOS2D_DEBUG >= 1
    tolua_Error tolua_err;
#endif

#if COCOS2D_DEBUG >= 1
    if (!tolua_isusertable(tolua_S,1,"cc.ZipUtils",0,&tolua_err)) goto tolua_lerror;
#endif

    argc = lua_gettop(tolua_S) - 1;

    if (argc == 2)
    {
        const char* arg0;
        const char* arg1;
        std::string arg0_tmp; ok &= luaval_to_std_string(tolua_S, 2, &arg0_tmp, "cc.ZipUtils:uncompressDir"); arg0 = arg0_tmp.c_str();
        std::string arg1_tmp; ok &= luaval_to_std_string(tolua_S, 3, &arg1_tmp, "cc.ZipUtils:uncompressDir"); arg1 = arg1_tmp.c_str();
        if(!ok)
        {
            tolua_error(tolua_S,"invalid arguments in function 'lua_cocos2dx_ZipUtils_uncompressDir'", nullptr);
            return 0;
        }
        bool ret = cocos2d::ZipUtils::uncompressDir(arg0, arg1);
        tolua_pushboolean(tolua_S,(bool)ret);
        return 1;
    }
	else if (argc == 3)
	{
		
        const char* arg0;
        const char* arg1;
        std::string arg0_tmp; ok &= luaval_to_std_string(tolua_S, 2, &arg0_tmp, "cc.ZipUtils:uncompressDir"); arg0 = arg0_tmp.c_str();
        std::string arg1_tmp; ok &= luaval_to_std_string(tolua_S, 3, &arg1_tmp, "cc.ZipUtils:uncompressDir"); arg1 = arg1_tmp.c_str();
        int handler = (toluafix_ref_function(tolua_S,4,0));
		if(!ok)
        {
            tolua_error(tolua_S,"invalid arguments in function 'lua_cocos2dx_ZipUtils_uncompressDir'", nullptr);
            return 0;
        }

		auto uncompressThread = std::thread(std::bind(extends_lua_cocos2dx_ZipUtils_uncompressDir_thread, arg0_tmp, arg1_tmp, handler, false));
		uncompressThread.detach();
		tolua_pushboolean(tolua_S,(bool)true);
        return 1;	
	}
	else if (argc == 4)
	{
		
        const char* arg0;
        const char* arg1;
        std::string arg0_tmp; ok &= luaval_to_std_string(tolua_S, 2, &arg0_tmp, "cc.ZipUtils:uncompressDir"); arg0 = arg0_tmp.c_str();
        std::string arg1_tmp; ok &= luaval_to_std_string(tolua_S, 3, &arg1_tmp, "cc.ZipUtils:uncompressDir"); arg1 = arg1_tmp.c_str();
        int handler = (toluafix_ref_function(tolua_S,4,0));
		bool arg3;
		ok &= luaval_to_boolean(tolua_S, 5, &arg3, "cc.ZipUtils:uncompressDir");
		if(!ok)
        {
            tolua_error(tolua_S,"invalid arguments in function 'lua_cocos2dx_ZipUtils_uncompressDir'", nullptr);
            return 0;
        }

		auto uncompressThread = std::thread(std::bind(extends_lua_cocos2dx_ZipUtils_uncompressDir_thread, arg0_tmp, arg1_tmp, handler, arg3));
		uncompressThread.detach();
		tolua_pushboolean(tolua_S,(bool)true);
        return 1;	
	}
	luaL_error(tolua_S, "%s has wrong number of arguments: %d, was expecting %d\n ", "cc.ZipUtils:uncompressDirAsync",argc, 2);
    return 0;
#if COCOS2D_DEBUG >= 1
    tolua_lerror:
    tolua_error(tolua_S,"#ferror in function 'lua_cocos2dx_ZipUtils_uncompressDir'.",&tolua_err);
#endif
    return 0;
}


int extends_lua_cocos2dx_ZipUtils_unzipXorFile_thread(std::string& filename, std::string& destPath, std::string& key, int callFunc)
{

	bool isSuccess = cocos2d::ZipUtils::unzipXorFile(filename.c_str(), destPath.c_str(), key.c_str(), key.length(), [=](unsigned int count, unsigned int total)
		{
			Scheduler *sched = Director::getInstance()->getScheduler();
			sched->performFunctionInCocosThread([=]() {

				LuaStack *stack = LuaEngine::getInstance()->getLuaStack();
				stack->pushInt(2);
				stack->pushInt(count);
				stack->pushInt(total);
				stack->executeFunctionByHandler(callFunc, 3);
			});

		});

	Scheduler *sched = Director::getInstance()->getScheduler();
	sched->performFunctionInCocosThread([=]() {

		LuaStack *stack = LuaEngine::getInstance()->getLuaStack();
		stack->pushInt(isSuccess);
		stack->pushInt(0);
		stack->pushInt(0);
		stack->executeFunctionByHandler(callFunc, 3);
	});

	return 1;
}


int extends_lua_cocos2dx_ZipUtils_unzipXorFile(lua_State* tolua_S)
{
	int argc = 0;
	bool ok = true;

#if COCOS2D_DEBUG >= 1
	tolua_Error tolua_err;
#endif

#if COCOS2D_DEBUG >= 1
	if (!tolua_isusertable(tolua_S, 1, "cc.ZipUtils", 0, &tolua_err)) goto tolua_lerror;
#endif

	argc = lua_gettop(tolua_S) - 1;

	if (argc == 4)
	{

		std::string arg0_tmp; ok &= luaval_to_std_string(tolua_S, 2, &arg0_tmp, "cc.ZipUtils:unzipEncryptFileToDir");
		std::string arg1_tmp; ok &= luaval_to_std_string(tolua_S, 3, &arg1_tmp, "cc.ZipUtils:unzipEncryptFileToDir");
		std::string arg2_tmp; ok &= luaval_to_std_string(tolua_S, 4, &arg2_tmp, "cc.ZipUtils:unzipEncryptFileToDir");
		int handler = (toluafix_ref_function(tolua_S, 5, 0));
		if (!ok)
		{
			tolua_error(tolua_S, "invalid arguments in function 'lua_cocos2dx_ZipUtils_uncompressDir'", nullptr);
			return 0;
		}

		auto uncompressThread = std::thread(std::bind(extends_lua_cocos2dx_ZipUtils_unzipXorFile_thread, arg0_tmp, arg1_tmp, arg2_tmp, handler));
		uncompressThread.detach();
		tolua_pushboolean(tolua_S, (bool)true);
		return 1;
	}
	luaL_error(tolua_S, "%s has wrong number of arguments: %d, was expecting %d\n ", "cc.ZipUtils:uncompressDirAsync", argc, 2);
	return 0;
#if COCOS2D_DEBUG >= 1
	tolua_lerror:
				tolua_error(tolua_S, "#ferror in function 'lua_cocos2dx_ZipUtils_uncompressDir'.", &tolua_err);
#endif
	return 0;
}



int extends_lua_cocos2dx_ZipUtils_createUncompressTask(lua_State* tolua_S)
{
	int argc = 0;
	bool ok = true;

#if COCOS2D_DEBUG >= 1
	tolua_Error tolua_err;
#endif

#if COCOS2D_DEBUG >= 1
	if (!tolua_isusertable(tolua_S, 1, "cc.ZipUtils", 0, &tolua_err)) goto tolua_lerror;
#endif

	argc = lua_gettop(tolua_S) - 1;
	if (argc == 4)
	{

		std::string arg0_tmp; ok &= luaval_to_std_string(tolua_S, 2, &arg0_tmp, "createUncompressTask");
		std::string arg1_tmp; ok &= luaval_to_std_string(tolua_S, 3, &arg1_tmp, "createUncompressTask");
		std::string arg2_tmp; ok &= luaval_to_std_string(tolua_S, 4, &arg2_tmp, "createUncompressTask");
		int handler = (toluafix_ref_function(tolua_S, 5, 0));
		if (!ok)
		{
			tolua_error(tolua_S, "invalid arguments in function 'lua_cocos2dx_ZipUtils_uncompressDir'", nullptr);
			return 0;
		}



		struct AsyncData
		{
			std::string customId;
			std::string zipFile;
			std::string desPath;
			int handler;
			bool succeed;
		};

		AsyncData* asyncData = new AsyncData;
		asyncData->customId = arg2_tmp;
		asyncData->zipFile = arg0_tmp;
		asyncData->desPath = arg1_tmp;
		asyncData->succeed = false;
		asyncData->handler = handler;

		std::function<void(void*)> decompressFinished = [](void* param) {
			auto dataInner = reinterpret_cast<AsyncData*>(param);
			
			Scheduler *sched = Director::getInstance()->getScheduler();
			sched->performFunctionInCocosThread([=]() {

				LuaStack *stack = LuaEngine::getInstance()->getLuaStack();
				stack->pushBoolean(dataInner->succeed);
				stack->pushString(dataInner->zipFile.c_str(), dataInner->zipFile.length());
				stack->pushString(dataInner->desPath.c_str(), dataInner->desPath.length());
				stack->pushString(dataInner->customId.c_str(), dataInner->customId.length());
				stack->executeFunctionByHandler(dataInner->handler, 4);
				delete dataInner;
			});


		};
		AsyncTaskPool::getInstance()->enqueue(AsyncTaskPool::TaskType::TASK_OTHER, std::move(decompressFinished), (void*)asyncData, [asyncData]() {
			// Decompress all compressed files
			asyncData->succeed = cocos2d::ZipUtils::uncompressDir(asyncData->zipFile.c_str(), asyncData->desPath.c_str(), [=](unsigned int count, unsigned int total)
			{

			});
		});

		tolua_pushboolean(tolua_S, (bool)true);
		return 1;
	}

	luaL_error(tolua_S, "%s has wrong number of arguments: %d, was expecting %d\n ", "cc.ZipUtils:uncompressDirAsync", argc, 2);
	return 0;
#if COCOS2D_DEBUG >= 1
	tolua_lerror:
				tolua_error(tolua_S, "#ferror in function 'lua_cocos2dx_ZipUtils_uncompressDir'.", &tolua_err);
#endif
				return 0;
}


static void  extends_ZipUtils_uncompressDir(lua_State* tolua_S)
{
	tolua_beginmodule(tolua_S, "cc");
	tolua_usertype(tolua_S,"cc.ZipUtils");
    tolua_cclass(tolua_S,"ZipUtils","cc.ZipUtils","",nullptr);

    tolua_beginmodule(tolua_S,"ZipUtils");
		tolua_function(tolua_S, "uncompressDir", extends_lua_cocos2dx_ZipUtils_uncompressDir);
		tolua_function(tolua_S, "createUncompressTask", extends_lua_cocos2dx_ZipUtils_createUncompressTask);
		tolua_function(tolua_S, "unzipXorFile", extends_lua_cocos2dx_ZipUtils_unzipXorFile);

	tolua_endmodule(tolua_S);
	std::string typeName = typeid(cocos2d::ZipUtils).name();
	g_luaType[typeName] = "cc.ZipUtils";
	g_typeCast["ZipUtils"] = "cc.ZipUtils";
	tolua_endmodule(tolua_S);
}


int lua_higame_3d_Chw3D_Sprite_SetState(lua_State* tolua_S)
{
	int argc = 0;
	Chw3D_Sprite* cobj = nullptr;
	bool ok = true;

#if COCOS2D_DEBUG >= 1
	tolua_Error tolua_err;
#endif


#if COCOS2D_DEBUG >= 1
	if (!tolua_isusertype(tolua_S, 1, "Chw3D_Sprite", 0, &tolua_err)) goto tolua_lerror;
#endif

	cobj = (Chw3D_Sprite*)tolua_tousertype(tolua_S, 1, 0);

#if COCOS2D_DEBUG >= 1
	if (!cobj)
	{
		tolua_error(tolua_S, "invalid 'cobj' in function 'lua_higame_3d_Chw3D_Sprite_SetState'", nullptr);
		return 0;
	}
#endif

	argc = lua_gettop(tolua_S) - 1;
	if (argc == 1)
	{
		STATE arg0;

		ok &= luaval_to_int32(tolua_S, 2, (int *)&arg0, "Chw3D_Sprite:SetState");
		if (!ok)
		{
			tolua_error(tolua_S, "invalid arguments in function 'lua_higame_3d_Chw3D_Sprite_SetState'", nullptr);
			return 0;
		}
		cobj->SetState(arg0);
		lua_settop(tolua_S, 1);
		return 1;
	}
	if (argc == 2)
	{
		STATE arg0;
		double arg1;

		ok &= luaval_to_int32(tolua_S, 2, (int *)&arg0, "Chw3D_Sprite:SetState");

		ok &= luaval_to_number(tolua_S, 3, &arg1, "Chw3D_Sprite:SetState");
		if (!ok)
		{
			tolua_error(tolua_S, "invalid arguments in function 'lua_higame_3d_Chw3D_Sprite_SetState'", nullptr);
			return 0;
		}
		cobj->SetState(arg0, arg1);
		lua_settop(tolua_S, 1);
		return 1;
	}
	if (argc == 3)
	{
		STATE arg0;
		double arg1;
		double arg2;

		ok &= luaval_to_int32(tolua_S, 2, (int *)&arg0, "Chw3D_Sprite:SetState");

		ok &= luaval_to_number(tolua_S, 3, &arg1, "Chw3D_Sprite:SetState");

		ok &= luaval_to_number(tolua_S, 4, &arg2, "Chw3D_Sprite:SetState");
		if (!ok)
		{
			tolua_error(tolua_S, "invalid arguments in function 'lua_higame_3d_Chw3D_Sprite_SetState'", nullptr);
			return 0;
		}
		cobj->SetState(arg0, arg1, arg2);
		lua_settop(tolua_S, 1);
		return 1;
	}
	if (argc == 4)
	{
		STATE arg0;
		double arg1;
		double arg2;

		ok &= luaval_to_int32(tolua_S, 2, (int *)&arg0, "Chw3D_Sprite:SetState");

		ok &= luaval_to_number(tolua_S, 3, &arg1, "Chw3D_Sprite:SetState");

		ok &= luaval_to_number(tolua_S, 4, &arg2, "Chw3D_Sprite:SetState");

#if COCOS2D_DEBUG >= 1
		if (!toluafix_isfunction(tolua_S, 5, "LUA_FUNCTION", 0, &tolua_err))
		{
			goto tolua_lerror;
		}
#endif
		LUA_FUNCTION handler = 0;
		if (ok) {
			handler = (toluafix_ref_function(tolua_S, 5, 0));
			cobj->SetState(arg0, arg1, arg2, [=]()
			{
				LuaEngine::getInstance()->getLuaStack()->executeFunctionByHandler(handler, 0);
			});
			//ScriptHandlerMgr::getInstance()->addCustomHandler((void*)cobj, handler);
		}
		lua_settop(tolua_S, 1);
		return 1;
	}
	luaL_error(tolua_S, "%s has wrong number of arguments: %d, was expecting %d \n", "Chw3D_Sprite:SetState", argc, 1);
	return 0;

#if COCOS2D_DEBUG >= 1
	tolua_lerror:
				tolua_error(tolua_S, "#ferror in function 'lua_higame_3d_Chw3D_Sprite_SetState'.", &tolua_err);
#endif

	return 0;
}

int tolua_hw_HiGameSDK_SetCallback(lua_State* tolua_S)
{
	int argc = 0;
	HiGameSDK* cobj = nullptr;
	bool ok = true;

#if COCOS2D_DEBUG >= 1
	tolua_Error tolua_err;
#endif


#if COCOS2D_DEBUG >= 1
	if (!tolua_isusertype(tolua_S, 1, "HiGameSDK", 0, &tolua_err)) goto tolua_lerror;
#endif

	cobj = (HiGameSDK*)tolua_tousertype(tolua_S, 1, 0);

#if COCOS2D_DEBUG >= 1
	if (!cobj)
	{
		tolua_error(tolua_S, "invalid 'cobj' in function 'lua_higame_3d_Chw3D_Sprite_SetState'", nullptr);
		return 0;
	}
#endif

	argc = lua_gettop(tolua_S) - 1;
	if (argc == 1)
	{

#if COCOS2D_DEBUG >= 1
		if (!toluafix_isfunction(tolua_S, 2, "LUA_FUNCTION", 0, &tolua_err))
		{
			goto tolua_lerror;
		}
#endif

		if (ok) {
			LUA_FUNCTION handler = (toluafix_ref_function(tolua_S, 2, 0));
			auto callback = [=](int callbackType, int resultCode, std::string& data){

				LuaStack* stack = LuaEngine::getInstance()->getLuaStack();
				stack->pushInt((int)callbackType);
				stack->pushInt(resultCode);
				stack->pushString(data.c_str(), data.size());
				stack->executeFunctionByHandler(handler, 3);
				stack->clean();
			};

			cobj->setCallback(callback);
		}
		lua_settop(tolua_S, 1);
		return 1;
	}
	luaL_error(tolua_S, "%s has wrong number of arguments: %d, was expecting %d \n", "Chw3D_Sprite:SetState", argc, 1);
	return 0;

#if COCOS2D_DEBUG >= 1
	tolua_lerror:
				tolua_error(tolua_S, "#ferror in function 'lua_higame_3d_Chw3D_Sprite_SetState'.", &tolua_err);
#endif

				return 0;
}

static void  extends_HiGameSDK(lua_State* tolua_S)
{
	tolua_usertype(tolua_S, "HiGameSDK");
	tolua_cclass(tolua_S, "HiGameSDK", "HiGameSDK", "cc.Ref", nullptr);

	tolua_module(tolua_S, "HiGameSDK", 0);
	tolua_beginmodule(tolua_S, "HiGameSDK");
	tolua_function(tolua_S, "setCallback", tolua_hw_HiGameSDK_SetCallback);
	tolua_endmodule(tolua_S);
}


int lua_tthw_graphics_Chw3D_Mgr_registerMissMaterialHandler(lua_State* tolua_S)
{
	if (nullptr == tolua_S)
		return 0;

	int argc = 0;
	Chw3D_Mgr* self = nullptr;

#if COCOS2D_DEBUG >= 1
	tolua_Error tolua_err;
	if (!tolua_isusertype(tolua_S, 1, "Chw3D_Mgr", 0, &tolua_err)) goto tolua_lerror;
#endif

	self = (Chw3D_Mgr*)tolua_tousertype(tolua_S, 1, 0);

#if COCOS2D_DEBUG >= 1
	if (nullptr == self) {
		tolua_error(tolua_S, "invalid 'self' in function 'lua_tthw_graphics_Chw3D_Mgr_registerMissMaterialHandler'\n", NULL);
		return 0;
	}
#endif
	argc = lua_gettop(tolua_S) - 1;
	if (1 == argc)
	{
#if COCOS2D_DEBUG >= 1
		if (!toluafix_isfunction(tolua_S, 2, "LUA_FUNCTION", 0, &tolua_err))
		{
			goto tolua_lerror;
		}
#endif
		LUA_FUNCTION handler = (toluafix_ref_function(tolua_S, 2, 0));
		auto missMaterialHandler = [=](std::string materialPath) {

			LuaStack* stack = LuaEngine::getInstance()->getLuaStack();
			stack->pushString(materialPath.c_str(), materialPath.length());
			stack->executeFunctionByHandler(handler, 1);
			stack->clean();
		};
		self->registerMissMaterialHandler(missMaterialHandler);
		return 0;
	}

	luaL_error(tolua_S, "'addEventListener' function of ListView has wrong number of arguments: %d, was expecting %d\n", argc, 1);

	return 0;

#if COCOS2D_DEBUG >= 1
	tolua_lerror:
				tolua_error(tolua_S, "#ferror in function 'lua_tthw_graphics_Chw3D_Mgr_registerMissMaterialHandler'.", &tolua_err);
				return 0;
#endif
}


int lua_higame_network_DownloadWorker_initWorkerLua(lua_State* tolua_S)
{
	if (nullptr == tolua_S)
		return 0;

	bool ok = true;
	int argc = 0;
	DownloadWorker* self = nullptr;

#if COCOS2D_DEBUG >= 1
	tolua_Error tolua_err;
	if (!tolua_isusertype(tolua_S, 1, "DownloadWorker", 0, &tolua_err)) goto tolua_lerror;
#endif

	self = (DownloadWorker*)tolua_tousertype(tolua_S, 1, 0);

#if COCOS2D_DEBUG >= 1
	if (nullptr == self) {
		tolua_error(tolua_S, "invalid 'self' in function 'lua_tthw_graphics_Chw3D_Mgr_registerMissMaterialHandler'\n", NULL);
		return 0;
	}
#endif
	argc = lua_gettop(tolua_S) - 1;
	if (1 == argc)
	{
#if COCOS2D_DEBUG >= 1
		if (!toluafix_isfunction(tolua_S, 2, "LUA_FUNCTION", 0, &tolua_err))
		{
			goto tolua_lerror;
		}
#endif

		LUA_FUNCTION handler = (toluafix_ref_function(tolua_S, 2, 0));
		if (ok)
		{
			self->initWorkerLua(handler);
		}
	}
	else if (3 == argc)
	{

#if COCOS2D_DEBUG >= 1
		if (!toluafix_isfunction(tolua_S, 2, "LUA_FUNCTION", 0, &tolua_err))
		{
			goto tolua_lerror;
		}
#endif

		LUA_FUNCTION handler = (toluafix_ref_function(tolua_S, 2, 0));
		unsigned int arg1;
		ok &= luaval_to_int32(tolua_S, 3, (int *)&arg1, "DownloadWorker:initWorker");
		unsigned int arg2;
		ok &= luaval_to_int32(tolua_S, 4, (int *)&arg2, "DownloadWorker:initWorker");
		if (ok) {
			self->initWorkerLua(handler, arg1, arg2);
		}

		return 0;
	}

	luaL_error(tolua_S, "'initWorker' function of ListView has wrong number of arguments: %d, was expecting %d\n", argc, 1);

	return 0;

#if COCOS2D_DEBUG >= 1
	tolua_lerror:
				tolua_error(tolua_S, "#ferror in function 'lua_higame_network_DownloadWorker_initWorkerLua'.", &tolua_err);
				return 0;
#endif
}


int lua_higame_qiniu_QiNiuSDK_uploadImage(lua_State* tolua_S)
{
	int argc = 0;
	QiNiuSDK* cobj = nullptr;
	bool ok = true;

#if COCOS2D_DEBUG >= 1
	tolua_Error tolua_err;
#endif


#if COCOS2D_DEBUG >= 1
	if (!tolua_isusertype(tolua_S, 1, "QiNiuSDK", 0, &tolua_err)) goto tolua_lerror;
#endif

	cobj = (QiNiuSDK*)tolua_tousertype(tolua_S, 1, 0);

#if COCOS2D_DEBUG >= 1
	if (!cobj)
	{
		tolua_error(tolua_S, "invalid 'cobj' in function 'lua_higame_qiniu_QiNiuSDK_uploadImage'", nullptr);
		return 0;
	}
#endif

	argc = lua_gettop(tolua_S) - 1;
	if (argc == 4)
	{
		
#if COCOS2D_DEBUG >= 1
		if (!toluafix_isfunction(tolua_S, 5, "LUA_FUNCTION", 0, &tolua_err))
		{
			goto tolua_lerror;
		}
#endif

		std::string token; ok &= luaval_to_std_string(tolua_S, 2, &token, "lua_higame_qiniu_QiNiuSDK_uploadImage");
		int type; ok &= luaval_to_int32(tolua_S, 3, (int *)&type, "lua_higame_qiniu_QiNiuSDK_uploadImage");
		std::string filePath; ok &= luaval_to_std_string(tolua_S, 4, &filePath, "lua_higame_qiniu_QiNiuSDK_uploadImage");
		LUA_FUNCTION handler = (toluafix_ref_function(tolua_S, 5, 0));

		if (ok) {

			auto callback = [=](int code, std::string key) {

				LuaStack* stack = LuaEngine::getInstance()->getLuaStack();
				stack->pushInt(code);
				stack->pushString(key.c_str(), key.length());
				stack->executeFunctionByHandler(handler, 2);
				stack->clean();
			};

			cobj->uploadImage(token, type, filePath, callback);
		}
		lua_settop(tolua_S, 1);
		return 1;
	}
	luaL_error(tolua_S, "%s has wrong number of arguments: %d, was expecting %d \n", "WaveShader:play", argc, 1);
	return 0;

#if COCOS2D_DEBUG >= 1
	tolua_lerror:
				tolua_error(tolua_S, "#ferror in function 'lua_higame_3d_Chw3D_Sprite_SetState'.", &tolua_err);
#endif

				return 0;
}


int lua_higame_shader_WaveShader_play(lua_State* tolua_S)
{
	int argc = 0;
	WaveShader* cobj = nullptr;
	bool ok = true;

#if COCOS2D_DEBUG >= 1
	tolua_Error tolua_err;
#endif


#if COCOS2D_DEBUG >= 1
	if (!tolua_isusertype(tolua_S, 1, "WaveShader", 0, &tolua_err)) goto tolua_lerror;
#endif

	cobj = (WaveShader*)tolua_tousertype(tolua_S, 1, 0);

#if COCOS2D_DEBUG >= 1
	if (!cobj)
	{
		tolua_error(tolua_S, "invalid 'cobj' in function 'lua_higame_shader_WaveShader_play'", nullptr);
		return 0;
	}
#endif

	argc = lua_gettop(tolua_S) - 1;
	if (argc == 2)
	{

#if COCOS2D_DEBUG >= 1
		if (!toluafix_isfunction(tolua_S, 3, "LUA_FUNCTION", 0, &tolua_err))
		{
			goto tolua_lerror;
		}
#endif
		double endtime;
		ok &= luaval_to_number(tolua_S, 2, (double *)&endtime, "lua_higame_shader_WaveShader_play");

		if (ok) {
			LUA_FUNCTION handler = (toluafix_ref_function(tolua_S, 3, 0));
			auto callback = [=](void) {

				LuaStack* stack = LuaEngine::getInstance()->getLuaStack();
				stack->executeFunctionByHandler(handler, 0);
				stack->clean();
			};

			cobj->play(endtime, callback);
		}
		lua_settop(tolua_S, 1);
		return 1;
	}
	luaL_error(tolua_S, "%s has wrong number of arguments: %d, was expecting %d \n", "WaveShader:play", argc, 1);
	return 0;

#if COCOS2D_DEBUG >= 1
	tolua_lerror:
				tolua_error(tolua_S, "#ferror in function 'lua_higame_3d_Chw3D_Sprite_SetState'.", &tolua_err);
#endif

				return 0;
}


TOLUA_API int register_higame_module(lua_State* tolua_S)
{
	
	luaopen_cocos2dx_extra_luabinding(tolua_S);
	luaopen_higame_extensions(tolua_S);
	register_all_higame_common(tolua_S);
    register_all_higame_network(tolua_S);
	register_all_higame_3d(tolua_S);
	register_all_higame_ui(tolua_S);
	register_all_higame_shader(tolua_S);
	register_all_tthw_voice(tolua_S);
	register_all_higame_systemutils(tolua_S);
	register_all_higame_qiniu(tolua_S);
#if (CC_TARGET_PLATFORM == CC_PLATFORM_ANDROID)
	register_all_higame_permission(tolua_S);
#endif
	register_all_higamesdk(tolua_S);
	extends_HiGameSDK(tolua_S);
	register_tthw_all_extends_voice(tolua_S);

    luaopen_protobuf_c(tolua_S);
	register_lua_plus(tolua_S);
	

	tolua_open(tolua_S);

	extends_ZipUtils_uncompressDir(tolua_S);

	tolua_function(tolua_S, "SysTime", tolua_GetSysTime);

	extends_DyListView(tolua_S);


	tolua_module(tolua_S,"higame",0);
	tolua_beginmodule(tolua_S,"higame");
		extends_ChwChwNetwokClient(tolua_S);
		extends_ChwCommAStar(tolua_S);
		extends_SystemUtils(tolua_S);

		tolua_beginmodule(tolua_S, "Chw3D_Sprite");
			tolua_function(tolua_S, "SetState", lua_higame_3d_Chw3D_Sprite_SetState);
		tolua_endmodule(tolua_S);

		tolua_beginmodule(tolua_S, "DownloadWorker");
			tolua_function(tolua_S, "initWorkers", lua_higame_network_DownloadWorker_initWorkerLua);
		tolua_endmodule(tolua_S);

		tolua_beginmodule(tolua_S, "Chw3D_Mgr");
			tolua_function(tolua_S, "registerMissMaterialHandler", lua_tthw_graphics_Chw3D_Mgr_registerMissMaterialHandler);
		tolua_endmodule(tolua_S);

		tolua_beginmodule(tolua_S, "WaveShader");
			tolua_function(tolua_S, "play", lua_higame_shader_WaveShader_play);
		tolua_endmodule(tolua_S);

		tolua_beginmodule(tolua_S, "QiNiuSDK");
		tolua_function(tolua_S, "uploadImage", lua_higame_qiniu_QiNiuSDK_uploadImage);
		tolua_endmodule(tolua_S);

	tolua_endmodule(tolua_S);

	luaopen_oilbit_core(tolua_S);
	//luaopen_package_core(tolua_S);
#if (CC_TARGET_PLATFORM == CC_PLATFORM_WIN32)
	luaopen_snapshot(tolua_S);
#endif
	return 1;
}


