/*
* Copyright (c) 2006 Tecgraf, PUC-Rio.
* All rights reserved.
*
* Module that exports support for conversion of numbers between different
* binary formats and also support for bit manipulation.
*
* $Id$
*/

#ifndef OILBIT_H
#define OILBIT_H

#ifndef OIL_API
#define OIL_API
#endif

#ifdef __cplusplus
extern "C" {
#endif

#include "lua.h"
#include "lualib.h"
#include "lauxlib.h"
#include "tolua++.h"

int luaopen_oilbit_core(lua_State*);

typedef char * RESULT_COL;
typedef RESULT_COL* RESULT_ROWS;

typedef struct t_packetresult_
{
    char *buffer;
    RESULT_ROWS *rows;
    short closed;
    int numcols, numrows, currow;
    int tagref;
} t_packetresult, *p_packetresult;

typedef struct _lpack_Buffer
{
    int offset;
    char data[128 * 1024]; /* MAX_DB_REQUSET_LEN */
} lpack_Buffer;   

int _encode(lpack_Buffer* b, const char* s, size_t len);
void lpack_buffinit(lpack_Buffer *B);
int lpack_addchar(lpack_Buffer *B, char c);
int lpack_addlstring(lpack_Buffer *B, const char *s, size_t l);
int lpack_addstring(lpack_Buffer *B, const char *s);
void lpack_pushresult(lua_State *L, lpack_Buffer *B);

#ifdef __cplusplus
}
#endif

#endif
