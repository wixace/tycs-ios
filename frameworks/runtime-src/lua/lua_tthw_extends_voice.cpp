#include "lua_tthw_extends_voice.hpp"

#include "tolua_fix.h"
#include "VoiceMgr.hpp"
#include "LuaBasicConversions.h"
#include "CCLuaEngine.h"

int tolua_tthw_voice_VoiceMgr_RegisterMessageNotify(lua_State* tolua_S)
{
	if (NULL == tolua_S)
		return 0;
	int argc = 0;

	VoiceMgr* cobj = nullptr;
	cobj = (VoiceMgr*)tolua_tousertype(tolua_S, 1, 0);

	argc = lua_gettop(tolua_S) - 1;
	if (2 == argc)
	{
		//第3个参数，就是Lua里的function 这里要通过toluafix_ref_function这个函数映射成一个Int值
		std::string type = (std::string)tolua_tocppstring(tolua_S, 2, 0);
		int handler = (toluafix_ref_function(tolua_S, 3, 0));
		cobj->RegisterMessageNotify(type, handler);

		return 0;
	}
	return 0;
}

static void  extends_VoiceMgr(lua_State* tolua_S)
{
	tolua_usertype(tolua_S, "VoiceMgr");
	tolua_cclass(tolua_S, "VoiceMgr", "VoiceMgr", "cc.Ref", nullptr);

	tolua_module(tolua_S, "VoiceMgr", 0);
	tolua_beginmodule(tolua_S, "VoiceMgr");
	tolua_function(tolua_S, "RegisterMessageNotify", tolua_tthw_voice_VoiceMgr_RegisterMessageNotify);
	tolua_endmodule(tolua_S);
}

TOLUA_API int register_tthw_all_extends_voice(lua_State* tolua_S)
{
	tolua_open(tolua_S);
	tolua_module(tolua_S,"higame",0);
	tolua_beginmodule(tolua_S,"higame");

	extends_VoiceMgr(tolua_S);
	tolua_endmodule(tolua_S);

	return 1;
}


