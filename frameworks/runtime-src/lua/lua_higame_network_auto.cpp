#include "../lua/lua_higame_network_auto.hpp"
#include "ODSocket.h"
#include "hwNetworkClient.h"
#include "ODSocket.h"
#include "DownloadWorker.h"
#include "scripting/lua-bindings/manual/tolua_fix.h"
#include "scripting/lua-bindings/manual/LuaBasicConversions.h"

int lua_higame_network_ChwNetworkClient_GetState(lua_State* tolua_S)
{
    int argc = 0;
    ChwNetworkClient* cobj = nullptr;
    bool ok  = true;

#if COCOS2D_DEBUG >= 1
    tolua_Error tolua_err;
#endif


#if COCOS2D_DEBUG >= 1
    if (!tolua_isusertype(tolua_S,1,"ChwNetworkClient",0,&tolua_err)) goto tolua_lerror;
#endif

    cobj = (ChwNetworkClient*)tolua_tousertype(tolua_S,1,0);

#if COCOS2D_DEBUG >= 1
    if (!cobj) 
    {
        tolua_error(tolua_S,"invalid 'cobj' in function 'lua_higame_network_ChwNetworkClient_GetState'", nullptr);
        return 0;
    }
#endif

    argc = lua_gettop(tolua_S)-1;
    if (argc == 0) 
    {
        if(!ok)
        {
            tolua_error(tolua_S,"invalid arguments in function 'lua_higame_network_ChwNetworkClient_GetState'", nullptr);
            return 0;
        }
        int ret = cobj->GetState();
        tolua_pushnumber(tolua_S,(lua_Number)ret);
        return 1;
    }
    luaL_error(tolua_S, "%s has wrong number of arguments: %d, was expecting %d \n", "ChwNetworkClient:GetState",argc, 0);
    return 0;

#if COCOS2D_DEBUG >= 1
    tolua_lerror:
    tolua_error(tolua_S,"#ferror in function 'lua_higame_network_ChwNetworkClient_GetState'.",&tolua_err);
#endif

    return 0;
}
int lua_higame_network_ChwNetworkClient_update(lua_State* tolua_S)
{
    int argc = 0;
    ChwNetworkClient* cobj = nullptr;
    bool ok  = true;

#if COCOS2D_DEBUG >= 1
    tolua_Error tolua_err;
#endif


#if COCOS2D_DEBUG >= 1
    if (!tolua_isusertype(tolua_S,1,"ChwNetworkClient",0,&tolua_err)) goto tolua_lerror;
#endif

    cobj = (ChwNetworkClient*)tolua_tousertype(tolua_S,1,0);

#if COCOS2D_DEBUG >= 1
    if (!cobj) 
    {
        tolua_error(tolua_S,"invalid 'cobj' in function 'lua_higame_network_ChwNetworkClient_update'", nullptr);
        return 0;
    }
#endif

    argc = lua_gettop(tolua_S)-1;
    if (argc == 1) 
    {
        double arg0;

        ok &= luaval_to_number(tolua_S, 2,&arg0, "ChwNetworkClient:update");
        if(!ok)
        {
            tolua_error(tolua_S,"invalid arguments in function 'lua_higame_network_ChwNetworkClient_update'", nullptr);
            return 0;
        }
        cobj->update(arg0);
        lua_settop(tolua_S, 1);
        return 1;
    }
    luaL_error(tolua_S, "%s has wrong number of arguments: %d, was expecting %d \n", "ChwNetworkClient:update",argc, 1);
    return 0;

#if COCOS2D_DEBUG >= 1
    tolua_lerror:
    tolua_error(tolua_S,"#ferror in function 'lua_higame_network_ChwNetworkClient_update'.",&tolua_err);
#endif

    return 0;
}
int lua_higame_network_ChwNetworkClient_Send(lua_State* tolua_S)
{
    int argc = 0;
    ChwNetworkClient* cobj = nullptr;
    bool ok  = true;

#if COCOS2D_DEBUG >= 1
    tolua_Error tolua_err;
#endif


#if COCOS2D_DEBUG >= 1
    if (!tolua_isusertype(tolua_S,1,"ChwNetworkClient",0,&tolua_err)) goto tolua_lerror;
#endif

    cobj = (ChwNetworkClient*)tolua_tousertype(tolua_S,1,0);

#if COCOS2D_DEBUG >= 1
    if (!cobj) 
    {
        tolua_error(tolua_S,"invalid 'cobj' in function 'lua_higame_network_ChwNetworkClient_Send'", nullptr);
        return 0;
    }
#endif

    argc = lua_gettop(tolua_S)-1;
    if (argc == 2) 
    {
        unsigned short arg0;
        std::string arg1;

        ok &= luaval_to_ushort(tolua_S, 2, &arg0, "ChwNetworkClient:Send");

        ok &= luaval_to_std_string(tolua_S, 3,&arg1, "ChwNetworkClient:Send");
        if(!ok)
        {
            tolua_error(tolua_S,"invalid arguments in function 'lua_higame_network_ChwNetworkClient_Send'", nullptr);
            return 0;
        }
        bool ret = cobj->Send(arg0, arg1);
        tolua_pushboolean(tolua_S,(bool)ret);
        return 1;
    }
    luaL_error(tolua_S, "%s has wrong number of arguments: %d, was expecting %d \n", "ChwNetworkClient:Send",argc, 2);
    return 0;

#if COCOS2D_DEBUG >= 1
    tolua_lerror:
    tolua_error(tolua_S,"#ferror in function 'lua_higame_network_ChwNetworkClient_Send'.",&tolua_err);
#endif

    return 0;
}
int lua_higame_network_ChwNetworkClient_Connect(lua_State* tolua_S)
{
    int argc = 0;
    ChwNetworkClient* cobj = nullptr;
    bool ok  = true;

#if COCOS2D_DEBUG >= 1
    tolua_Error tolua_err;
#endif


#if COCOS2D_DEBUG >= 1
    if (!tolua_isusertype(tolua_S,1,"ChwNetworkClient",0,&tolua_err)) goto tolua_lerror;
#endif

    cobj = (ChwNetworkClient*)tolua_tousertype(tolua_S,1,0);

#if COCOS2D_DEBUG >= 1
    if (!cobj) 
    {
        tolua_error(tolua_S,"invalid 'cobj' in function 'lua_higame_network_ChwNetworkClient_Connect'", nullptr);
        return 0;
    }
#endif

    argc = lua_gettop(tolua_S)-1;
    if (argc == 2) 
    {
        std::string arg0;
        unsigned short arg1;

        ok &= luaval_to_std_string(tolua_S, 2,&arg0, "ChwNetworkClient:Connect");

        ok &= luaval_to_ushort(tolua_S, 3, &arg1, "ChwNetworkClient:Connect");
        if(!ok)
        {
            tolua_error(tolua_S,"invalid arguments in function 'lua_higame_network_ChwNetworkClient_Connect'", nullptr);
            return 0;
        }
        bool ret = cobj->Connect(arg0, arg1);
        tolua_pushboolean(tolua_S,(bool)ret);
        return 1;
    }
    luaL_error(tolua_S, "%s has wrong number of arguments: %d, was expecting %d \n", "ChwNetworkClient:Connect",argc, 2);
    return 0;

#if COCOS2D_DEBUG >= 1
    tolua_lerror:
    tolua_error(tolua_S,"#ferror in function 'lua_higame_network_ChwNetworkClient_Connect'.",&tolua_err);
#endif

    return 0;
}
int lua_higame_network_ChwNetworkClient_Close(lua_State* tolua_S)
{
    int argc = 0;
    ChwNetworkClient* cobj = nullptr;
    bool ok  = true;

#if COCOS2D_DEBUG >= 1
    tolua_Error tolua_err;
#endif


#if COCOS2D_DEBUG >= 1
    if (!tolua_isusertype(tolua_S,1,"ChwNetworkClient",0,&tolua_err)) goto tolua_lerror;
#endif

    cobj = (ChwNetworkClient*)tolua_tousertype(tolua_S,1,0);

#if COCOS2D_DEBUG >= 1
    if (!cobj) 
    {
        tolua_error(tolua_S,"invalid 'cobj' in function 'lua_higame_network_ChwNetworkClient_Close'", nullptr);
        return 0;
    }
#endif

    argc = lua_gettop(tolua_S)-1;
    if (argc == 0) 
    {
        if(!ok)
        {
            tolua_error(tolua_S,"invalid arguments in function 'lua_higame_network_ChwNetworkClient_Close'", nullptr);
            return 0;
        }
        cobj->Close();
        lua_settop(tolua_S, 1);
        return 1;
    }
    if (argc == 1) 
    {
        unsigned short arg0;

        ok &= luaval_to_ushort(tolua_S, 2, &arg0, "ChwNetworkClient:Close");
        if(!ok)
        {
            tolua_error(tolua_S,"invalid arguments in function 'lua_higame_network_ChwNetworkClient_Close'", nullptr);
            return 0;
        }
        cobj->Close(arg0);
        lua_settop(tolua_S, 1);
        return 1;
    }
    luaL_error(tolua_S, "%s has wrong number of arguments: %d, was expecting %d \n", "ChwNetworkClient:Close",argc, 0);
    return 0;

#if COCOS2D_DEBUG >= 1
    tolua_lerror:
    tolua_error(tolua_S,"#ferror in function 'lua_higame_network_ChwNetworkClient_Close'.",&tolua_err);
#endif

    return 0;
}
int lua_higame_network_ChwNetworkClient_SetTimeout(lua_State* tolua_S)
{
    int argc = 0;
    ChwNetworkClient* cobj = nullptr;
    bool ok  = true;

#if COCOS2D_DEBUG >= 1
    tolua_Error tolua_err;
#endif


#if COCOS2D_DEBUG >= 1
    if (!tolua_isusertype(tolua_S,1,"ChwNetworkClient",0,&tolua_err)) goto tolua_lerror;
#endif

    cobj = (ChwNetworkClient*)tolua_tousertype(tolua_S,1,0);

#if COCOS2D_DEBUG >= 1
    if (!cobj) 
    {
        tolua_error(tolua_S,"invalid 'cobj' in function 'lua_higame_network_ChwNetworkClient_SetTimeout'", nullptr);
        return 0;
    }
#endif

    argc = lua_gettop(tolua_S)-1;
    if (argc == 1) 
    {
        double arg0;

        ok &= luaval_to_number(tolua_S, 2,&arg0, "ChwNetworkClient:SetTimeout");
        if(!ok)
        {
            tolua_error(tolua_S,"invalid arguments in function 'lua_higame_network_ChwNetworkClient_SetTimeout'", nullptr);
            return 0;
        }
        cobj->SetTimeout(arg0);
        lua_settop(tolua_S, 1);
        return 1;
    }
    luaL_error(tolua_S, "%s has wrong number of arguments: %d, was expecting %d \n", "ChwNetworkClient:SetTimeout",argc, 1);
    return 0;

#if COCOS2D_DEBUG >= 1
    tolua_lerror:
    tolua_error(tolua_S,"#ferror in function 'lua_higame_network_ChwNetworkClient_SetTimeout'.",&tolua_err);
#endif

    return 0;
}
int lua_higame_network_ChwNetworkClient_constructor(lua_State* tolua_S)
{
    int argc = 0;
    ChwNetworkClient* cobj = nullptr;
    bool ok  = true;

#if COCOS2D_DEBUG >= 1
    tolua_Error tolua_err;
#endif



    argc = lua_gettop(tolua_S)-1;
    if (argc == 0) 
    {
        if(!ok)
        {
            tolua_error(tolua_S,"invalid arguments in function 'lua_higame_network_ChwNetworkClient_constructor'", nullptr);
            return 0;
        }
        cobj = new ChwNetworkClient();
        cobj->autorelease();
        int ID =  (int)cobj->_ID ;
        int* luaID =  &cobj->_luaID ;
        toluafix_pushusertype_ccobject(tolua_S, ID, luaID, (void*)cobj,"ChwNetworkClient");
        return 1;
    }
    luaL_error(tolua_S, "%s has wrong number of arguments: %d, was expecting %d \n", "ChwNetworkClient:ChwNetworkClient",argc, 0);
    return 0;

#if COCOS2D_DEBUG >= 1
    tolua_error(tolua_S,"#ferror in function 'lua_higame_network_ChwNetworkClient_constructor'.",&tolua_err);
#endif

    return 0;
}

static int lua_higame_network_ChwNetworkClient_finalize(lua_State* tolua_S)
{
    printf("luabindings: finalizing LUA object (ChwNetworkClient)");
    return 0;
}

int lua_register_higame_network_ChwNetworkClient(lua_State* tolua_S)
{
    tolua_usertype(tolua_S,"ChwNetworkClient");
    tolua_cclass(tolua_S,"ChwNetworkClient","ChwNetworkClient","cc.Node",nullptr);

    tolua_beginmodule(tolua_S,"ChwNetworkClient");
        tolua_function(tolua_S,"new",lua_higame_network_ChwNetworkClient_constructor);
        tolua_function(tolua_S,"GetState",lua_higame_network_ChwNetworkClient_GetState);
        tolua_function(tolua_S,"update",lua_higame_network_ChwNetworkClient_update);
        tolua_function(tolua_S,"Send",lua_higame_network_ChwNetworkClient_Send);
        tolua_function(tolua_S,"Connect",lua_higame_network_ChwNetworkClient_Connect);
        tolua_function(tolua_S,"Close",lua_higame_network_ChwNetworkClient_Close);
        tolua_function(tolua_S,"SetTimeout",lua_higame_network_ChwNetworkClient_SetTimeout);
    tolua_endmodule(tolua_S);
    std::string typeName = typeid(ChwNetworkClient).name();
    g_luaType[typeName] = "ChwNetworkClient";
    g_typeCast["ChwNetworkClient"] = "ChwNetworkClient";
    return 1;
}

int lua_higame_network_DownloadWorker_createDownloadFileTask(lua_State* tolua_S)
{
    int argc = 0;
    DownloadWorker* cobj = nullptr;
    bool ok  = true;

#if COCOS2D_DEBUG >= 1
    tolua_Error tolua_err;
#endif


#if COCOS2D_DEBUG >= 1
    if (!tolua_isusertype(tolua_S,1,"DownloadWorker",0,&tolua_err)) goto tolua_lerror;
#endif

    cobj = (DownloadWorker*)tolua_tousertype(tolua_S,1,0);

#if COCOS2D_DEBUG >= 1
    if (!cobj) 
    {
        tolua_error(tolua_S,"invalid 'cobj' in function 'lua_higame_network_DownloadWorker_createDownloadFileTask'", nullptr);
        return 0;
    }
#endif

    argc = lua_gettop(tolua_S)-1;
    if (argc == 2) 
    {
        std::string arg0;
        std::string arg1;

        ok &= luaval_to_std_string(tolua_S, 2,&arg0, "DownloadWorker:createDownloadFileTask");

        ok &= luaval_to_std_string(tolua_S, 3,&arg1, "DownloadWorker:createDownloadFileTask");
        if(!ok)
        {
            tolua_error(tolua_S,"invalid arguments in function 'lua_higame_network_DownloadWorker_createDownloadFileTask'", nullptr);
            return 0;
        }
        bool ret = cobj->createDownloadFileTask(arg0, arg1);
        tolua_pushboolean(tolua_S,(bool)ret);
        return 1;
    }
    if (argc == 3) 
    {
        std::string arg0;
        std::string arg1;
        std::string arg2;

        ok &= luaval_to_std_string(tolua_S, 2,&arg0, "DownloadWorker:createDownloadFileTask");

        ok &= luaval_to_std_string(tolua_S, 3,&arg1, "DownloadWorker:createDownloadFileTask");

        ok &= luaval_to_std_string(tolua_S, 4,&arg2, "DownloadWorker:createDownloadFileTask");
        if(!ok)
        {
            tolua_error(tolua_S,"invalid arguments in function 'lua_higame_network_DownloadWorker_createDownloadFileTask'", nullptr);
            return 0;
        }
        bool ret = cobj->createDownloadFileTask(arg0, arg1, arg2);
        tolua_pushboolean(tolua_S,(bool)ret);
        return 1;
    }
    luaL_error(tolua_S, "%s has wrong number of arguments: %d, was expecting %d \n", "DownloadWorker:createDownloadFileTask",argc, 2);
    return 0;

#if COCOS2D_DEBUG >= 1
    tolua_lerror:
    tolua_error(tolua_S,"#ferror in function 'lua_higame_network_DownloadWorker_createDownloadFileTask'.",&tolua_err);
#endif

    return 0;
}
int lua_higame_network_DownloadWorker_createDownloadDataTask(lua_State* tolua_S)
{
    int argc = 0;
    DownloadWorker* cobj = nullptr;
    bool ok  = true;

#if COCOS2D_DEBUG >= 1
    tolua_Error tolua_err;
#endif


#if COCOS2D_DEBUG >= 1
    if (!tolua_isusertype(tolua_S,1,"DownloadWorker",0,&tolua_err)) goto tolua_lerror;
#endif

    cobj = (DownloadWorker*)tolua_tousertype(tolua_S,1,0);

#if COCOS2D_DEBUG >= 1
    if (!cobj) 
    {
        tolua_error(tolua_S,"invalid 'cobj' in function 'lua_higame_network_DownloadWorker_createDownloadDataTask'", nullptr);
        return 0;
    }
#endif

    argc = lua_gettop(tolua_S)-1;
    if (argc == 1) 
    {
        std::string arg0;

        ok &= luaval_to_std_string(tolua_S, 2,&arg0, "DownloadWorker:createDownloadDataTask");
        if(!ok)
        {
            tolua_error(tolua_S,"invalid arguments in function 'lua_higame_network_DownloadWorker_createDownloadDataTask'", nullptr);
            return 0;
        }
        bool ret = cobj->createDownloadDataTask(arg0);
        tolua_pushboolean(tolua_S,(bool)ret);
        return 1;
    }
    if (argc == 2) 
    {
        std::string arg0;
        std::string arg1;

        ok &= luaval_to_std_string(tolua_S, 2,&arg0, "DownloadWorker:createDownloadDataTask");

        ok &= luaval_to_std_string(tolua_S, 3,&arg1, "DownloadWorker:createDownloadDataTask");
        if(!ok)
        {
            tolua_error(tolua_S,"invalid arguments in function 'lua_higame_network_DownloadWorker_createDownloadDataTask'", nullptr);
            return 0;
        }
        bool ret = cobj->createDownloadDataTask(arg0, arg1);
        tolua_pushboolean(tolua_S,(bool)ret);
        return 1;
    }
    luaL_error(tolua_S, "%s has wrong number of arguments: %d, was expecting %d \n", "DownloadWorker:createDownloadDataTask",argc, 1);
    return 0;

#if COCOS2D_DEBUG >= 1
    tolua_lerror:
    tolua_error(tolua_S,"#ferror in function 'lua_higame_network_DownloadWorker_createDownloadDataTask'.",&tolua_err);
#endif

    return 0;
}
int lua_higame_network_DownloadWorker_initWorker(lua_State* tolua_S)
{
    int argc = 0;
    DownloadWorker* cobj = nullptr;
    bool ok  = true;

#if COCOS2D_DEBUG >= 1
    tolua_Error tolua_err;
#endif


#if COCOS2D_DEBUG >= 1
    if (!tolua_isusertype(tolua_S,1,"DownloadWorker",0,&tolua_err)) goto tolua_lerror;
#endif

    cobj = (DownloadWorker*)tolua_tousertype(tolua_S,1,0);

#if COCOS2D_DEBUG >= 1
    if (!cobj) 
    {
        tolua_error(tolua_S,"invalid 'cobj' in function 'lua_higame_network_DownloadWorker_initWorker'", nullptr);
        return 0;
    }
#endif

    argc = lua_gettop(tolua_S)-1;
    if (argc == 2) 
    {
        unsigned int arg0;
        unsigned int arg1;

        ok &= luaval_to_uint32(tolua_S, 2,&arg0, "DownloadWorker:initWorker");

        ok &= luaval_to_uint32(tolua_S, 3,&arg1, "DownloadWorker:initWorker");
        if(!ok)
        {
            tolua_error(tolua_S,"invalid arguments in function 'lua_higame_network_DownloadWorker_initWorker'", nullptr);
            return 0;
        }
        bool ret = cobj->initWorker(arg0, arg1);
        tolua_pushboolean(tolua_S,(bool)ret);
        return 1;
    }
    luaL_error(tolua_S, "%s has wrong number of arguments: %d, was expecting %d \n", "DownloadWorker:initWorker",argc, 2);
    return 0;

#if COCOS2D_DEBUG >= 1
    tolua_lerror:
    tolua_error(tolua_S,"#ferror in function 'lua_higame_network_DownloadWorker_initWorker'.",&tolua_err);
#endif

    return 0;
}
int lua_higame_network_DownloadWorker_stopWork(lua_State* tolua_S)
{
    int argc = 0;
    DownloadWorker* cobj = nullptr;
    bool ok  = true;

#if COCOS2D_DEBUG >= 1
    tolua_Error tolua_err;
#endif


#if COCOS2D_DEBUG >= 1
    if (!tolua_isusertype(tolua_S,1,"DownloadWorker",0,&tolua_err)) goto tolua_lerror;
#endif

    cobj = (DownloadWorker*)tolua_tousertype(tolua_S,1,0);

#if COCOS2D_DEBUG >= 1
    if (!cobj) 
    {
        tolua_error(tolua_S,"invalid 'cobj' in function 'lua_higame_network_DownloadWorker_stopWork'", nullptr);
        return 0;
    }
#endif

    argc = lua_gettop(tolua_S)-1;
    if (argc == 0) 
    {
        if(!ok)
        {
            tolua_error(tolua_S,"invalid arguments in function 'lua_higame_network_DownloadWorker_stopWork'", nullptr);
            return 0;
        }
        cobj->stopWork();
        lua_settop(tolua_S, 1);
        return 1;
    }
    luaL_error(tolua_S, "%s has wrong number of arguments: %d, was expecting %d \n", "DownloadWorker:stopWork",argc, 0);
    return 0;

#if COCOS2D_DEBUG >= 1
    tolua_lerror:
    tolua_error(tolua_S,"#ferror in function 'lua_higame_network_DownloadWorker_stopWork'.",&tolua_err);
#endif

    return 0;
}
int lua_higame_network_DownloadWorker_destroyInstance(lua_State* tolua_S)
{
    int argc = 0;
    bool ok  = true;

#if COCOS2D_DEBUG >= 1
    tolua_Error tolua_err;
#endif

#if COCOS2D_DEBUG >= 1
    if (!tolua_isusertable(tolua_S,1,"DownloadWorker",0,&tolua_err)) goto tolua_lerror;
#endif

    argc = lua_gettop(tolua_S) - 1;

    if (argc == 0)
    {
        if(!ok)
        {
            tolua_error(tolua_S,"invalid arguments in function 'lua_higame_network_DownloadWorker_destroyInstance'", nullptr);
            return 0;
        }
        DownloadWorker::destroyInstance();
        lua_settop(tolua_S, 1);
        return 1;
    }
    luaL_error(tolua_S, "%s has wrong number of arguments: %d, was expecting %d\n ", "DownloadWorker:destroyInstance",argc, 0);
    return 0;
#if COCOS2D_DEBUG >= 1
    tolua_lerror:
    tolua_error(tolua_S,"#ferror in function 'lua_higame_network_DownloadWorker_destroyInstance'.",&tolua_err);
#endif
    return 0;
}
int lua_higame_network_DownloadWorker_getInstance(lua_State* tolua_S)
{
    int argc = 0;
    bool ok  = true;

#if COCOS2D_DEBUG >= 1
    tolua_Error tolua_err;
#endif

#if COCOS2D_DEBUG >= 1
    if (!tolua_isusertable(tolua_S,1,"DownloadWorker",0,&tolua_err)) goto tolua_lerror;
#endif

    argc = lua_gettop(tolua_S) - 1;

    if (argc == 0)
    {
        if(!ok)
        {
            tolua_error(tolua_S,"invalid arguments in function 'lua_higame_network_DownloadWorker_getInstance'", nullptr);
            return 0;
        }
        DownloadWorker* ret = DownloadWorker::getInstance();
        object_to_luaval<DownloadWorker>(tolua_S, "DownloadWorker",(DownloadWorker*)ret);
        return 1;
    }
    luaL_error(tolua_S, "%s has wrong number of arguments: %d, was expecting %d\n ", "DownloadWorker:getInstance",argc, 0);
    return 0;
#if COCOS2D_DEBUG >= 1
    tolua_lerror:
    tolua_error(tolua_S,"#ferror in function 'lua_higame_network_DownloadWorker_getInstance'.",&tolua_err);
#endif
    return 0;
}
int lua_higame_network_DownloadWorker_constructor(lua_State* tolua_S)
{
    int argc = 0;
    DownloadWorker* cobj = nullptr;
    bool ok  = true;

#if COCOS2D_DEBUG >= 1
    tolua_Error tolua_err;
#endif



    argc = lua_gettop(tolua_S)-1;
    if (argc == 0) 
    {
        if(!ok)
        {
            tolua_error(tolua_S,"invalid arguments in function 'lua_higame_network_DownloadWorker_constructor'", nullptr);
            return 0;
        }
        cobj = new DownloadWorker();
        cobj->autorelease();
        int ID =  (int)cobj->_ID ;
        int* luaID =  &cobj->_luaID ;
        toluafix_pushusertype_ccobject(tolua_S, ID, luaID, (void*)cobj,"DownloadWorker");
        return 1;
    }
    luaL_error(tolua_S, "%s has wrong number of arguments: %d, was expecting %d \n", "DownloadWorker:DownloadWorker",argc, 0);
    return 0;

#if COCOS2D_DEBUG >= 1
    tolua_error(tolua_S,"#ferror in function 'lua_higame_network_DownloadWorker_constructor'.",&tolua_err);
#endif

    return 0;
}

static int lua_higame_network_DownloadWorker_finalize(lua_State* tolua_S)
{
    printf("luabindings: finalizing LUA object (DownloadWorker)");
    return 0;
}

int lua_register_higame_network_DownloadWorker(lua_State* tolua_S)
{
    tolua_usertype(tolua_S,"DownloadWorker");
    tolua_cclass(tolua_S,"DownloadWorker","DownloadWorker","cc.Ref",nullptr);

    tolua_beginmodule(tolua_S,"DownloadWorker");
        tolua_function(tolua_S,"new",lua_higame_network_DownloadWorker_constructor);
        tolua_function(tolua_S,"createDownloadFileTask",lua_higame_network_DownloadWorker_createDownloadFileTask);
        tolua_function(tolua_S,"createDownloadDataTask",lua_higame_network_DownloadWorker_createDownloadDataTask);
        tolua_function(tolua_S,"initWorker",lua_higame_network_DownloadWorker_initWorker);
        tolua_function(tolua_S,"stopWork",lua_higame_network_DownloadWorker_stopWork);
        tolua_function(tolua_S,"destroyInstance", lua_higame_network_DownloadWorker_destroyInstance);
        tolua_function(tolua_S,"getInstance", lua_higame_network_DownloadWorker_getInstance);
    tolua_endmodule(tolua_S);
    std::string typeName = typeid(DownloadWorker).name();
    g_luaType[typeName] = "DownloadWorker";
    g_typeCast["DownloadWorker"] = "DownloadWorker";
    return 1;
}
TOLUA_API int register_all_higame_network(lua_State* tolua_S)
{
	tolua_open(tolua_S);
	
	tolua_module(tolua_S,"higame",0);
	tolua_beginmodule(tolua_S,"higame");

	lua_register_higame_network_DownloadWorker(tolua_S);
	lua_register_higame_network_ChwNetworkClient(tolua_S);

	tolua_endmodule(tolua_S);
	return 1;
}

