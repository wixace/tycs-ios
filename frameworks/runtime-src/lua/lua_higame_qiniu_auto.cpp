#include "../lua/lua_higame_qiniu_auto.hpp"
#include "hwCommAStar.h"
#include "hwCommRandom.h"
#include "HW_QiNiuSDK.h"
#include "scripting/lua-bindings/manual/tolua_fix.h"
#include "scripting/lua-bindings/manual/LuaBasicConversions.h"

int lua_higame_qiniu_QiNiuSDK_getCompressFormat(lua_State* tolua_S)
{
    int argc = 0;
    QiNiuSDK* cobj = nullptr;
    bool ok  = true;

#if COCOS2D_DEBUG >= 1
    tolua_Error tolua_err;
#endif


#if COCOS2D_DEBUG >= 1
    if (!tolua_isusertype(tolua_S,1,"QiNiuSDK",0,&tolua_err)) goto tolua_lerror;
#endif

    cobj = (QiNiuSDK*)tolua_tousertype(tolua_S,1,0);

#if COCOS2D_DEBUG >= 1
    if (!cobj) 
    {
        tolua_error(tolua_S,"invalid 'cobj' in function 'lua_higame_qiniu_QiNiuSDK_getCompressFormat'", nullptr);
        return 0;
    }
#endif

    argc = lua_gettop(tolua_S)-1;
    if (argc == 0) 
    {
        if(!ok)
        {
            tolua_error(tolua_S,"invalid arguments in function 'lua_higame_qiniu_QiNiuSDK_getCompressFormat'", nullptr);
            return 0;
        }
        int ret = cobj->getCompressFormat();
        tolua_pushnumber(tolua_S,(lua_Number)ret);
        return 1;
    }
    luaL_error(tolua_S, "%s has wrong number of arguments: %d, was expecting %d \n", "QiNiuSDK:getCompressFormat",argc, 0);
    return 0;

#if COCOS2D_DEBUG >= 1
    tolua_lerror:
    tolua_error(tolua_S,"#ferror in function 'lua_higame_qiniu_QiNiuSDK_getCompressFormat'.",&tolua_err);
#endif

    return 0;
}
int lua_higame_qiniu_QiNiuSDK_setCompressFormat(lua_State* tolua_S)
{
    int argc = 0;
    QiNiuSDK* cobj = nullptr;
    bool ok  = true;

#if COCOS2D_DEBUG >= 1
    tolua_Error tolua_err;
#endif


#if COCOS2D_DEBUG >= 1
    if (!tolua_isusertype(tolua_S,1,"QiNiuSDK",0,&tolua_err)) goto tolua_lerror;
#endif

    cobj = (QiNiuSDK*)tolua_tousertype(tolua_S,1,0);

#if COCOS2D_DEBUG >= 1
    if (!cobj) 
    {
        tolua_error(tolua_S,"invalid 'cobj' in function 'lua_higame_qiniu_QiNiuSDK_setCompressFormat'", nullptr);
        return 0;
    }
#endif

    argc = lua_gettop(tolua_S)-1;
    if (argc == 1) 
    {
        int arg0;

        ok &= luaval_to_int32(tolua_S, 2,(int *)&arg0, "QiNiuSDK:setCompressFormat");
        if(!ok)
        {
            tolua_error(tolua_S,"invalid arguments in function 'lua_higame_qiniu_QiNiuSDK_setCompressFormat'", nullptr);
            return 0;
        }
        cobj->setCompressFormat(arg0);
        lua_settop(tolua_S, 1);
        return 1;
    }
    luaL_error(tolua_S, "%s has wrong number of arguments: %d, was expecting %d \n", "QiNiuSDK:setCompressFormat",argc, 1);
    return 0;

#if COCOS2D_DEBUG >= 1
    tolua_lerror:
    tolua_error(tolua_S,"#ferror in function 'lua_higame_qiniu_QiNiuSDK_setCompressFormat'.",&tolua_err);
#endif

    return 0;
}
int lua_higame_qiniu_QiNiuSDK_setMaxWidth(lua_State* tolua_S)
{
    int argc = 0;
    QiNiuSDK* cobj = nullptr;
    bool ok  = true;

#if COCOS2D_DEBUG >= 1
    tolua_Error tolua_err;
#endif


#if COCOS2D_DEBUG >= 1
    if (!tolua_isusertype(tolua_S,1,"QiNiuSDK",0,&tolua_err)) goto tolua_lerror;
#endif

    cobj = (QiNiuSDK*)tolua_tousertype(tolua_S,1,0);

#if COCOS2D_DEBUG >= 1
    if (!cobj) 
    {
        tolua_error(tolua_S,"invalid 'cobj' in function 'lua_higame_qiniu_QiNiuSDK_setMaxWidth'", nullptr);
        return 0;
    }
#endif

    argc = lua_gettop(tolua_S)-1;
    if (argc == 1) 
    {
        int arg0;

        ok &= luaval_to_int32(tolua_S, 2,(int *)&arg0, "QiNiuSDK:setMaxWidth");
        if(!ok)
        {
            tolua_error(tolua_S,"invalid arguments in function 'lua_higame_qiniu_QiNiuSDK_setMaxWidth'", nullptr);
            return 0;
        }
        cobj->setMaxWidth(arg0);
        lua_settop(tolua_S, 1);
        return 1;
    }
    luaL_error(tolua_S, "%s has wrong number of arguments: %d, was expecting %d \n", "QiNiuSDK:setMaxWidth",argc, 1);
    return 0;

#if COCOS2D_DEBUG >= 1
    tolua_lerror:
    tolua_error(tolua_S,"#ferror in function 'lua_higame_qiniu_QiNiuSDK_setMaxWidth'.",&tolua_err);
#endif

    return 0;
}
int lua_higame_qiniu_QiNiuSDK_getMaxFileSize(lua_State* tolua_S)
{
    int argc = 0;
    QiNiuSDK* cobj = nullptr;
    bool ok  = true;

#if COCOS2D_DEBUG >= 1
    tolua_Error tolua_err;
#endif


#if COCOS2D_DEBUG >= 1
    if (!tolua_isusertype(tolua_S,1,"QiNiuSDK",0,&tolua_err)) goto tolua_lerror;
#endif

    cobj = (QiNiuSDK*)tolua_tousertype(tolua_S,1,0);

#if COCOS2D_DEBUG >= 1
    if (!cobj) 
    {
        tolua_error(tolua_S,"invalid 'cobj' in function 'lua_higame_qiniu_QiNiuSDK_getMaxFileSize'", nullptr);
        return 0;
    }
#endif

    argc = lua_gettop(tolua_S)-1;
    if (argc == 0) 
    {
        if(!ok)
        {
            tolua_error(tolua_S,"invalid arguments in function 'lua_higame_qiniu_QiNiuSDK_getMaxFileSize'", nullptr);
            return 0;
        }
        int ret = cobj->getMaxFileSize();
        tolua_pushnumber(tolua_S,(lua_Number)ret);
        return 1;
    }
    luaL_error(tolua_S, "%s has wrong number of arguments: %d, was expecting %d \n", "QiNiuSDK:getMaxFileSize",argc, 0);
    return 0;

#if COCOS2D_DEBUG >= 1
    tolua_lerror:
    tolua_error(tolua_S,"#ferror in function 'lua_higame_qiniu_QiNiuSDK_getMaxFileSize'.",&tolua_err);
#endif

    return 0;
}
int lua_higame_qiniu_QiNiuSDK_setMaxHeight(lua_State* tolua_S)
{
    int argc = 0;
    QiNiuSDK* cobj = nullptr;
    bool ok  = true;

#if COCOS2D_DEBUG >= 1
    tolua_Error tolua_err;
#endif


#if COCOS2D_DEBUG >= 1
    if (!tolua_isusertype(tolua_S,1,"QiNiuSDK",0,&tolua_err)) goto tolua_lerror;
#endif

    cobj = (QiNiuSDK*)tolua_tousertype(tolua_S,1,0);

#if COCOS2D_DEBUG >= 1
    if (!cobj) 
    {
        tolua_error(tolua_S,"invalid 'cobj' in function 'lua_higame_qiniu_QiNiuSDK_setMaxHeight'", nullptr);
        return 0;
    }
#endif

    argc = lua_gettop(tolua_S)-1;
    if (argc == 1) 
    {
        int arg0;

        ok &= luaval_to_int32(tolua_S, 2,(int *)&arg0, "QiNiuSDK:setMaxHeight");
        if(!ok)
        {
            tolua_error(tolua_S,"invalid arguments in function 'lua_higame_qiniu_QiNiuSDK_setMaxHeight'", nullptr);
            return 0;
        }
        cobj->setMaxHeight(arg0);
        lua_settop(tolua_S, 1);
        return 1;
    }
    luaL_error(tolua_S, "%s has wrong number of arguments: %d, was expecting %d \n", "QiNiuSDK:setMaxHeight",argc, 1);
    return 0;

#if COCOS2D_DEBUG >= 1
    tolua_lerror:
    tolua_error(tolua_S,"#ferror in function 'lua_higame_qiniu_QiNiuSDK_setMaxHeight'.",&tolua_err);
#endif

    return 0;
}
int lua_higame_qiniu_QiNiuSDK_setMaxFileSize(lua_State* tolua_S)
{
    int argc = 0;
    QiNiuSDK* cobj = nullptr;
    bool ok  = true;

#if COCOS2D_DEBUG >= 1
    tolua_Error tolua_err;
#endif


#if COCOS2D_DEBUG >= 1
    if (!tolua_isusertype(tolua_S,1,"QiNiuSDK",0,&tolua_err)) goto tolua_lerror;
#endif

    cobj = (QiNiuSDK*)tolua_tousertype(tolua_S,1,0);

#if COCOS2D_DEBUG >= 1
    if (!cobj) 
    {
        tolua_error(tolua_S,"invalid 'cobj' in function 'lua_higame_qiniu_QiNiuSDK_setMaxFileSize'", nullptr);
        return 0;
    }
#endif

    argc = lua_gettop(tolua_S)-1;
    if (argc == 1) 
    {
        int arg0;

        ok &= luaval_to_int32(tolua_S, 2,(int *)&arg0, "QiNiuSDK:setMaxFileSize");
        if(!ok)
        {
            tolua_error(tolua_S,"invalid arguments in function 'lua_higame_qiniu_QiNiuSDK_setMaxFileSize'", nullptr);
            return 0;
        }
        cobj->setMaxFileSize(arg0);
        lua_settop(tolua_S, 1);
        return 1;
    }
    luaL_error(tolua_S, "%s has wrong number of arguments: %d, was expecting %d \n", "QiNiuSDK:setMaxFileSize",argc, 1);
    return 0;

#if COCOS2D_DEBUG >= 1
    tolua_lerror:
    tolua_error(tolua_S,"#ferror in function 'lua_higame_qiniu_QiNiuSDK_setMaxFileSize'.",&tolua_err);
#endif

    return 0;
}
int lua_higame_qiniu_QiNiuSDK_getMaxWidth(lua_State* tolua_S)
{
    int argc = 0;
    QiNiuSDK* cobj = nullptr;
    bool ok  = true;

#if COCOS2D_DEBUG >= 1
    tolua_Error tolua_err;
#endif


#if COCOS2D_DEBUG >= 1
    if (!tolua_isusertype(tolua_S,1,"QiNiuSDK",0,&tolua_err)) goto tolua_lerror;
#endif

    cobj = (QiNiuSDK*)tolua_tousertype(tolua_S,1,0);

#if COCOS2D_DEBUG >= 1
    if (!cobj) 
    {
        tolua_error(tolua_S,"invalid 'cobj' in function 'lua_higame_qiniu_QiNiuSDK_getMaxWidth'", nullptr);
        return 0;
    }
#endif

    argc = lua_gettop(tolua_S)-1;
    if (argc == 0) 
    {
        if(!ok)
        {
            tolua_error(tolua_S,"invalid arguments in function 'lua_higame_qiniu_QiNiuSDK_getMaxWidth'", nullptr);
            return 0;
        }
        int ret = cobj->getMaxWidth();
        tolua_pushnumber(tolua_S,(lua_Number)ret);
        return 1;
    }
    luaL_error(tolua_S, "%s has wrong number of arguments: %d, was expecting %d \n", "QiNiuSDK:getMaxWidth",argc, 0);
    return 0;

#if COCOS2D_DEBUG >= 1
    tolua_lerror:
    tolua_error(tolua_S,"#ferror in function 'lua_higame_qiniu_QiNiuSDK_getMaxWidth'.",&tolua_err);
#endif

    return 0;
}
int lua_higame_qiniu_QiNiuSDK_getMinCompress(lua_State* tolua_S)
{
    int argc = 0;
    QiNiuSDK* cobj = nullptr;
    bool ok  = true;

#if COCOS2D_DEBUG >= 1
    tolua_Error tolua_err;
#endif


#if COCOS2D_DEBUG >= 1
    if (!tolua_isusertype(tolua_S,1,"QiNiuSDK",0,&tolua_err)) goto tolua_lerror;
#endif

    cobj = (QiNiuSDK*)tolua_tousertype(tolua_S,1,0);

#if COCOS2D_DEBUG >= 1
    if (!cobj) 
    {
        tolua_error(tolua_S,"invalid 'cobj' in function 'lua_higame_qiniu_QiNiuSDK_getMinCompress'", nullptr);
        return 0;
    }
#endif

    argc = lua_gettop(tolua_S)-1;
    if (argc == 0) 
    {
        if(!ok)
        {
            tolua_error(tolua_S,"invalid arguments in function 'lua_higame_qiniu_QiNiuSDK_getMinCompress'", nullptr);
            return 0;
        }
        int ret = cobj->getMinCompress();
        tolua_pushnumber(tolua_S,(lua_Number)ret);
        return 1;
    }
    luaL_error(tolua_S, "%s has wrong number of arguments: %d, was expecting %d \n", "QiNiuSDK:getMinCompress",argc, 0);
    return 0;

#if COCOS2D_DEBUG >= 1
    tolua_lerror:
    tolua_error(tolua_S,"#ferror in function 'lua_higame_qiniu_QiNiuSDK_getMinCompress'.",&tolua_err);
#endif

    return 0;
}
int lua_higame_qiniu_QiNiuSDK_onCallback(lua_State* tolua_S)
{
    int argc = 0;
    QiNiuSDK* cobj = nullptr;
    bool ok  = true;

#if COCOS2D_DEBUG >= 1
    tolua_Error tolua_err;
#endif


#if COCOS2D_DEBUG >= 1
    if (!tolua_isusertype(tolua_S,1,"QiNiuSDK",0,&tolua_err)) goto tolua_lerror;
#endif

    cobj = (QiNiuSDK*)tolua_tousertype(tolua_S,1,0);

#if COCOS2D_DEBUG >= 1
    if (!cobj) 
    {
        tolua_error(tolua_S,"invalid 'cobj' in function 'lua_higame_qiniu_QiNiuSDK_onCallback'", nullptr);
        return 0;
    }
#endif

    argc = lua_gettop(tolua_S)-1;
    if (argc == 3) 
    {
        unsigned int arg0;
        int arg1;
        std::string arg2;

        ok &= luaval_to_uint32(tolua_S, 2,&arg0, "QiNiuSDK:onCallback");

        ok &= luaval_to_int32(tolua_S, 3,(int *)&arg1, "QiNiuSDK:onCallback");

        ok &= luaval_to_std_string(tolua_S, 4,&arg2, "QiNiuSDK:onCallback");
        if(!ok)
        {
            tolua_error(tolua_S,"invalid arguments in function 'lua_higame_qiniu_QiNiuSDK_onCallback'", nullptr);
            return 0;
        }
        cobj->onCallback(arg0, arg1, arg2);
        lua_settop(tolua_S, 1);
        return 1;
    }
    luaL_error(tolua_S, "%s has wrong number of arguments: %d, was expecting %d \n", "QiNiuSDK:onCallback",argc, 3);
    return 0;

#if COCOS2D_DEBUG >= 1
    tolua_lerror:
    tolua_error(tolua_S,"#ferror in function 'lua_higame_qiniu_QiNiuSDK_onCallback'.",&tolua_err);
#endif

    return 0;
}
int lua_higame_qiniu_QiNiuSDK_setMinCompress(lua_State* tolua_S)
{
    int argc = 0;
    QiNiuSDK* cobj = nullptr;
    bool ok  = true;

#if COCOS2D_DEBUG >= 1
    tolua_Error tolua_err;
#endif


#if COCOS2D_DEBUG >= 1
    if (!tolua_isusertype(tolua_S,1,"QiNiuSDK",0,&tolua_err)) goto tolua_lerror;
#endif

    cobj = (QiNiuSDK*)tolua_tousertype(tolua_S,1,0);

#if COCOS2D_DEBUG >= 1
    if (!cobj) 
    {
        tolua_error(tolua_S,"invalid 'cobj' in function 'lua_higame_qiniu_QiNiuSDK_setMinCompress'", nullptr);
        return 0;
    }
#endif

    argc = lua_gettop(tolua_S)-1;
    if (argc == 1) 
    {
        int arg0;

        ok &= luaval_to_int32(tolua_S, 2,(int *)&arg0, "QiNiuSDK:setMinCompress");
        if(!ok)
        {
            tolua_error(tolua_S,"invalid arguments in function 'lua_higame_qiniu_QiNiuSDK_setMinCompress'", nullptr);
            return 0;
        }
        cobj->setMinCompress(arg0);
        lua_settop(tolua_S, 1);
        return 1;
    }
    luaL_error(tolua_S, "%s has wrong number of arguments: %d, was expecting %d \n", "QiNiuSDK:setMinCompress",argc, 1);
    return 0;

#if COCOS2D_DEBUG >= 1
    tolua_lerror:
    tolua_error(tolua_S,"#ferror in function 'lua_higame_qiniu_QiNiuSDK_setMinCompress'.",&tolua_err);
#endif

    return 0;
}
int lua_higame_qiniu_QiNiuSDK_getMaxHeight(lua_State* tolua_S)
{
    int argc = 0;
    QiNiuSDK* cobj = nullptr;
    bool ok  = true;

#if COCOS2D_DEBUG >= 1
    tolua_Error tolua_err;
#endif


#if COCOS2D_DEBUG >= 1
    if (!tolua_isusertype(tolua_S,1,"QiNiuSDK",0,&tolua_err)) goto tolua_lerror;
#endif

    cobj = (QiNiuSDK*)tolua_tousertype(tolua_S,1,0);

#if COCOS2D_DEBUG >= 1
    if (!cobj) 
    {
        tolua_error(tolua_S,"invalid 'cobj' in function 'lua_higame_qiniu_QiNiuSDK_getMaxHeight'", nullptr);
        return 0;
    }
#endif

    argc = lua_gettop(tolua_S)-1;
    if (argc == 0) 
    {
        if(!ok)
        {
            tolua_error(tolua_S,"invalid arguments in function 'lua_higame_qiniu_QiNiuSDK_getMaxHeight'", nullptr);
            return 0;
        }
        int ret = cobj->getMaxHeight();
        tolua_pushnumber(tolua_S,(lua_Number)ret);
        return 1;
    }
    luaL_error(tolua_S, "%s has wrong number of arguments: %d, was expecting %d \n", "QiNiuSDK:getMaxHeight",argc, 0);
    return 0;

#if COCOS2D_DEBUG >= 1
    tolua_lerror:
    tolua_error(tolua_S,"#ferror in function 'lua_higame_qiniu_QiNiuSDK_getMaxHeight'.",&tolua_err);
#endif

    return 0;
}
int lua_higame_qiniu_QiNiuSDK_destroyInstance(lua_State* tolua_S)
{
    int argc = 0;
    bool ok  = true;

#if COCOS2D_DEBUG >= 1
    tolua_Error tolua_err;
#endif

#if COCOS2D_DEBUG >= 1
    if (!tolua_isusertable(tolua_S,1,"QiNiuSDK",0,&tolua_err)) goto tolua_lerror;
#endif

    argc = lua_gettop(tolua_S) - 1;

    if (argc == 0)
    {
        if(!ok)
        {
            tolua_error(tolua_S,"invalid arguments in function 'lua_higame_qiniu_QiNiuSDK_destroyInstance'", nullptr);
            return 0;
        }
        QiNiuSDK::destroyInstance();
        lua_settop(tolua_S, 1);
        return 1;
    }
    luaL_error(tolua_S, "%s has wrong number of arguments: %d, was expecting %d\n ", "QiNiuSDK:destroyInstance",argc, 0);
    return 0;
#if COCOS2D_DEBUG >= 1
    tolua_lerror:
    tolua_error(tolua_S,"#ferror in function 'lua_higame_qiniu_QiNiuSDK_destroyInstance'.",&tolua_err);
#endif
    return 0;
}
int lua_higame_qiniu_QiNiuSDK_getInstance(lua_State* tolua_S)
{
    int argc = 0;
    bool ok  = true;

#if COCOS2D_DEBUG >= 1
    tolua_Error tolua_err;
#endif

#if COCOS2D_DEBUG >= 1
    if (!tolua_isusertable(tolua_S,1,"QiNiuSDK",0,&tolua_err)) goto tolua_lerror;
#endif

    argc = lua_gettop(tolua_S) - 1;

    if (argc == 0)
    {
        if(!ok)
        {
            tolua_error(tolua_S,"invalid arguments in function 'lua_higame_qiniu_QiNiuSDK_getInstance'", nullptr);
            return 0;
        }
        QiNiuSDK* ret = QiNiuSDK::getInstance();
        object_to_luaval<QiNiuSDK>(tolua_S, "QiNiuSDK",(QiNiuSDK*)ret);
        return 1;
    }
    luaL_error(tolua_S, "%s has wrong number of arguments: %d, was expecting %d\n ", "QiNiuSDK:getInstance",argc, 0);
    return 0;
#if COCOS2D_DEBUG >= 1
    tolua_lerror:
    tolua_error(tolua_S,"#ferror in function 'lua_higame_qiniu_QiNiuSDK_getInstance'.",&tolua_err);
#endif
    return 0;
}
int lua_higame_qiniu_QiNiuSDK_constructor(lua_State* tolua_S)
{
    int argc = 0;
    QiNiuSDK* cobj = nullptr;
    bool ok  = true;

#if COCOS2D_DEBUG >= 1
    tolua_Error tolua_err;
#endif



    argc = lua_gettop(tolua_S)-1;
    if (argc == 0) 
    {
        if(!ok)
        {
            tolua_error(tolua_S,"invalid arguments in function 'lua_higame_qiniu_QiNiuSDK_constructor'", nullptr);
            return 0;
        }
        cobj = new QiNiuSDK();
        tolua_pushusertype(tolua_S,(void*)cobj,"QiNiuSDK");
        tolua_register_gc(tolua_S,lua_gettop(tolua_S));
        return 1;
    }
    luaL_error(tolua_S, "%s has wrong number of arguments: %d, was expecting %d \n", "QiNiuSDK:QiNiuSDK",argc, 0);
    return 0;

#if COCOS2D_DEBUG >= 1
    tolua_error(tolua_S,"#ferror in function 'lua_higame_qiniu_QiNiuSDK_constructor'.",&tolua_err);
#endif

    return 0;
}

static int lua_higame_qiniu_QiNiuSDK_finalize(lua_State* tolua_S)
{
    printf("luabindings: finalizing LUA object (QiNiuSDK)");
    return 0;
}

int lua_register_higame_qiniu_QiNiuSDK(lua_State* tolua_S)
{
    tolua_usertype(tolua_S,"QiNiuSDK");
    tolua_cclass(tolua_S,"QiNiuSDK","QiNiuSDK","",nullptr);

    tolua_beginmodule(tolua_S,"QiNiuSDK");
        tolua_function(tolua_S,"new",lua_higame_qiniu_QiNiuSDK_constructor);
        tolua_function(tolua_S,"getCompressFormat",lua_higame_qiniu_QiNiuSDK_getCompressFormat);
        tolua_function(tolua_S,"setCompressFormat",lua_higame_qiniu_QiNiuSDK_setCompressFormat);
        tolua_function(tolua_S,"setMaxWidth",lua_higame_qiniu_QiNiuSDK_setMaxWidth);
        tolua_function(tolua_S,"getMaxFileSize",lua_higame_qiniu_QiNiuSDK_getMaxFileSize);
        tolua_function(tolua_S,"setMaxHeight",lua_higame_qiniu_QiNiuSDK_setMaxHeight);
        tolua_function(tolua_S,"setMaxFileSize",lua_higame_qiniu_QiNiuSDK_setMaxFileSize);
        tolua_function(tolua_S,"getMaxWidth",lua_higame_qiniu_QiNiuSDK_getMaxWidth);
        tolua_function(tolua_S,"getMinCompress",lua_higame_qiniu_QiNiuSDK_getMinCompress);
        tolua_function(tolua_S,"onCallback",lua_higame_qiniu_QiNiuSDK_onCallback);
        tolua_function(tolua_S,"setMinCompress",lua_higame_qiniu_QiNiuSDK_setMinCompress);
        tolua_function(tolua_S,"getMaxHeight",lua_higame_qiniu_QiNiuSDK_getMaxHeight);
        tolua_function(tolua_S,"destroyInstance", lua_higame_qiniu_QiNiuSDK_destroyInstance);
        tolua_function(tolua_S,"getInstance", lua_higame_qiniu_QiNiuSDK_getInstance);
    tolua_endmodule(tolua_S);
    std::string typeName = typeid(QiNiuSDK).name();
    g_luaType[typeName] = "QiNiuSDK";
    g_typeCast["QiNiuSDK"] = "QiNiuSDK";
    return 1;
}
TOLUA_API int register_all_higame_qiniu(lua_State* tolua_S)
{
	tolua_open(tolua_S);
	
	tolua_module(tolua_S,"higame",0);
	tolua_beginmodule(tolua_S,"higame");

	lua_register_higame_qiniu_QiNiuSDK(tolua_S);

	tolua_endmodule(tolua_S);
	return 1;
}

