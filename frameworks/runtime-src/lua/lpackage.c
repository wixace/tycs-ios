/*
// File:    lpackage.c
// Author:  Saniko
// Desc:    table打包解包函数（用于数值系统储存）
// Date:    2009.4.23
// Last:
// Copyright (c) 2008 shangyoo Entertainment All right reserved.
*/

#include <assert.h>
#include "oilbit.h"

#include <string.h>
#include "lauxlib.h"

#ifdef WIN32
#define l_assert(_Expression) if(!(_Expression)) __asm int 3;
#else
#define l_assert(_Expression) assert(_Expression)
#endif

const int PackVersion = 0x101;  /* 当前版本号 */
const unsigned int MAX_LUA_PACK_STRING_LEN = 128 * 1024;

/******************************************************************************/
enum DATATYPE
{
    TypeBoolean,
    TypeUint8,
    TypeUint16,
    TypeUint32,
    TypeDouble,
    TypeString,
    TypeTable,
    TypeTableTail,
    TypeEnd,
    TypeString8,
    TypeString16,
};

/******************************************************************************/
void lpack_buffinit(lpack_Buffer *B)
{
    B->offset = 0;
}

int lpack_addchar(lpack_Buffer *B, char c)
{
    if(B->offset + sizeof(c) >= sizeof(B->data)) return 0;
    B->data[B->offset++] = c;
	return 1;
}

int lpack_addlstring(lpack_Buffer *B, const char *s, size_t l)
{
    if (B->offset + l >= sizeof(B->data)) return 0;
    memcpy(B->data + B->offset, s, l);
    B->offset += l;
	return 1;
}

int lpack_addstring(lpack_Buffer *B, const char *s)
{
    int l;
    if(s == NULL) return 0;
    l = strlen(s);
    if(B->offset + l >= (int)sizeof(B->data)) return 0;
    return lpack_addlstring(B, s, l);	
}

void lpack_pushresult(lua_State *L, lpack_Buffer *B)
{
    // l_assert(B->offset != 0);
    lua_pushlstring(L, B->data, B->offset);
}

/******************************************************************************/
static int packvalue(lua_State *L, int idx, lpack_Buffer *b)
{
    switch(lua_type(L, idx))
    {
    case LUA_TNIL:  /* 是空值，忽略 */
        break;

    case LUA_TBOOLEAN:
        {
            int value = lua_toboolean(L, idx);
            if(lpack_addchar(b, TypeBoolean) == 0)
			{
				return 0;
			}
            if(lpack_addchar(b, value ? 1 : 0) == 0)
			{
				return 0;
			}
        }
        break;

    case LUA_TNUMBER:
        {
            double value = lua_tonumber(L, idx);
            unsigned long long uvalue = (unsigned long long) value;
            if(value == uvalue)
            {
                if(uvalue <= 0xFF)
                {
                    if (lpack_addchar(b, TypeUint8) == 0)
                    {
						return 0;
                    }
                    
                    if(lpack_addlstring(b, (const char *) &uvalue, sizeof(unsigned char)) == 0)
					{
						return 0;
					}
                }
                else if(uvalue <= 0xFFFF)
                {
                    if(lpack_addchar(b, TypeUint16) == 0)
					{
						return 0;
					}
                    if(lpack_addlstring(b, (const char *) &uvalue, sizeof(unsigned short)) == 0 )
					{
						return 0;
					}
                }
                else if(uvalue <= 0xFFFFFFFF)
                {
                    if(lpack_addchar(b, TypeUint32) == 0)
					{
						return 0;
					}
                    if(lpack_addlstring(b, (const char *) &uvalue, sizeof(unsigned int)) == 0)
					{
						return 0;
					}
                }
                else
                {
                    if(lpack_addchar(b, TypeDouble) == 0)
					{
						return 0;
					}
                    if(lpack_addlstring(b, (const char *) &value, sizeof(value)) == 0)
					{
						return 0;
					}
                }
            }
            else
            {
                if (lpack_addchar(b, TypeDouble) == 0)
                {
					return 0;
                }                
                if(lpack_addlstring(b, (const char *) &value, sizeof(value)) == 0)
				{
					return 0;
				}
            }
        }
        break;

    case LUA_TSTRING:
        {
            unsigned int len;
            const char *value = lua_tolstring(L, idx, &len);
			l_assert(strlen(value) == len);
            if(len <= 0xFF)
            {
                if (lpack_addchar(b, TypeString8) == 0)
                {
					return 0;
                }                
                if(lpack_addchar(b, (char) len) == 0)
				{
					return 0;
				}
            }
            else if(len <= 0xFFFF)
            {
                if(lpack_addchar(b, TypeString16) == 0)
				{
					return 0;
				}
                if(lpack_addlstring(b, (const char *) &len, sizeof(short)) == 0)
				{
					return 0;
				}
            }
            else
            {
                if(lpack_addchar(b, TypeString) == 0) 
				{
					return 0;
				}
                if(lpack_addlstring(b, (const char *) &len, sizeof(len)) == 0)
				{
					return 0;
				}
            }
            if(lpack_addlstring(b, value, len) == 0)
			{
				return 0;
			}
        }
        break;

    default: /* 类型不支持，返回0出错 */
        return 0;
    }
    return 1;
}

static int packtable(lua_State *L, lpack_Buffer *b)
{
    lua_pushnil(L);  /* first key */
    while(lua_next(L, -2))
    {
        /* 首先把key压入缓冲区，如果key的类型不被支持，则返回0出错 */
        if(!packvalue(L, -2, b))
        {
            lua_getglobal(L, "print");
            lua_pushstring(L, "packtable - 不支持的key值 - ");
            lua_pushvalue(L, -4);
            lua_call(L, 2, 0);
            return 0;
        }

        /* 如果value是table，进入table再次遍历 */
        if(lua_istable(L, -1))
        {
            /* 添加table开始标志 */
            if(lpack_addchar(b, TypeTable) == 0)
			{
				return 0;
			}
            if(!packtable(L, b))
                return 0;
            /* 添加table结束标志 */
            if(lpack_addchar(b, TypeTableTail) == 0)
			{
				return 0;
			}
        }
        /* 把value压入缓冲区，如果key的类型不被支持，则返回0出错 */
        else if(!packvalue(L, -1, b))
        {
            lua_getglobal(L, "print");
            lua_pushstring(L, "packtable - 不支持的value值 - ");
            lua_pushvalue(L, -3);
            lua_call(L, 2, 0);
            return 0;
        }
        lua_pop(L, 1); // pop value
    }
    return 1;
}

static int l_packtable(lua_State *L)
{
    luaL_checktype(L, 1, LUA_TTABLE);
    {
        lpack_Buffer b;
        lpack_buffinit(&b);
        /* 版本号 */
        if(lpack_addlstring(&b, (const char *) &PackVersion, sizeof(PackVersion)) == 0)
		{
			return 0;
		}

        if(!packtable(L, &b))
        {
            /* 打包出错，返回nil */
            lua_pushnil(L);
            return 1;
        }
        if (lpack_addchar(&b, TypeEnd) == 0)
        {
			return 0;
        }        
        /* 返回打包后的结果 */
        lpack_pushresult(L, &b);
        return 1;
    }
}

static int unpackvalue(lua_State *L, int type, const char *buffer)
{
    unsigned int len;

    switch(type)
    {
    case TypeBoolean:
		{
			char value;
			memcpy((void*)&value, buffer, 1);
			lua_pushboolean(L, value);

			return 1;
		}

    case TypeUint8:
		{
			unsigned char value;
			memcpy((void*)&value, buffer, sizeof(unsigned char));
			lua_pushnumber(L, value);
			return sizeof(unsigned char);
		}

    case TypeUint16:
		{
			unsigned short value;
			memcpy((void*)&value, buffer, sizeof(unsigned short));
			lua_pushnumber(L, value);
			return sizeof(unsigned short);
		}

    case TypeUint32:
		{
			unsigned int value;
			memcpy((void*)&value, buffer, sizeof(unsigned int));
			lua_pushnumber(L, value);
			return sizeof(unsigned int);
		}

    case TypeDouble:
		{
			double value;
			memcpy((void*)&value, buffer, sizeof(double));
			lua_pushnumber(L, value);
			return sizeof(double);
		}

    case TypeString8:
		{
			len = *(unsigned char *) buffer; buffer += sizeof(unsigned char);
			if (len >= MAX_LUA_PACK_STRING_LEN)
			{
				printf("unpackvalue1 len = %d\n", len);
				fflush(stdout);
				return 0;
			}

			lua_pushlstring(L, buffer, len);
			return len + sizeof(unsigned char);
		}

    case TypeString16:
		{
			len = *(unsigned short *) buffer; buffer += sizeof(unsigned short);
			if (len >= MAX_LUA_PACK_STRING_LEN)
			{
				printf("unpackvalue2 len = %d\n", len);
				fflush(stdout);
				return 0;
			}

			lua_pushlstring(L, buffer, len);
			return len + sizeof(unsigned short);
		}

    case TypeString:
		{
			len = *(unsigned int *) buffer; buffer += sizeof(unsigned int);
			if (len >= MAX_LUA_PACK_STRING_LEN)
			{
				printf("unpackvalue3 len = %d\n", len);
				fflush(stdout);
				return 0;
			}

			lua_pushlstring(L, buffer, len);
			return len + sizeof(unsigned int);
		}
    default:
        return 0;
    }
}

static int unpacktable(lua_State *L, const char *buffer, size_t len)
{
    /* 记录解包开始时的buffer位置 */
    const char *oldbuf = buffer;
    const char *tail = buffer + len;
    char type;
    int nsize;

    while(buffer < tail)
    {
        /* 取得key的类型 */
        type = *buffer++;
        /* 如果是table结束标志，返回解包table读取了的缓冲大小 */
        if(type == TypeTableTail)
            return buffer - oldbuf;
        else if(type == TypeEnd)
            break;

        /* 解包key */
        nsize = unpackvalue(L, type, buffer);
        if(nsize == 0)
            return 0;
        buffer += nsize;

        /* 解包value */
        type = *buffer++;
        /* value的类型是table */
        if(type == TypeTable)
        {
            lua_pushvalue(L, -1);   /* 上次unpackvalue压入的子表的key */
            lua_rawget(L, -3);  /* 取得当前表的子表 */

            /* 如果当前表的子表存在，则不newtable（节省效率，同时为了防止丢失原有子表里的数据） */
            if(!lua_istable(L, -1))
            {
                lua_pop(L, 1); /* 弹出nil（取子表时获取的值） */
                lua_newtable(L);
            }
            /* 递归调用函数解包子表 */
            if((nsize = unpacktable(L, buffer, tail - buffer)) == 0)
                return 0;
        }
        else if((nsize = unpackvalue(L, type, buffer)) == 0)
        {
            return 0;
        }
        buffer += nsize;
        lua_rawset(L, -3);
    }

    if(buffer != tail)
        return 0;

    return 1;
}

static int l_unpacktable(lua_State *L)
{
    int version;
    size_t len;
    const char *buffer = luaL_checklstring(L, 1, &len);

    if(len == 0)
    {
        lua_pushnil(L);
        return 1;
    }

    if(!lua_istable(L, -1))
        lua_newtable(L);

    version = *(const int *) buffer; buffer += sizeof(int); len -= sizeof(int);
    l_assert(version == PackVersion);
    if(!unpacktable(L, buffer, len))
    {
        lua_pop(L, 1);
        lua_pushnil(L);
        return 1;
    }
    return 1;
}

/******************************************************************************/
static const luaL_reg sfuncs[] =
{
    {"packtable",	l_packtable},
    {"unpacktable",	l_unpacktable},
    {NULL,	NULL}
};

OIL_API int luaopen_package_core(lua_State *L) {
    /* define library functions */
    luaL_openlib(L, "bits", sfuncs, 0);

    return 0;
}
