#include "hwNetworkClient.h"
#include "CCLuaEngine.h"


ChwNetworkClient::ChwNetworkClient():
	m_unreadLength(0),
	m_readOffset(0),
	m_state(S_INIT),
	m_isRecvThreadClose(true),
	m_isConnected(false),
	m_closeReason(CR_CLOSE_SELF),
	m_timeout(5)
{
	m_luaHandles["CONNECT"] = 0;
	m_luaHandles["DATA"] = 0;
	m_luaHandles["CLOSE"] = 0;
	m_isDebug = false;
	m_socket = ODSocket();
}

ChwNetworkClient::~ChwNetworkClient()
{
	m_state = S_CLOSE;
	Closeed();
}

bool ChwNetworkClient::Connect(std::string ip, unsigned short wPort)
{
#if COCOS2D_DEBUG > 0

	CCLOG("======>Connect m_state:%d ip:%s port:%d", m_state, ip.c_str(), wPort);

#endif

	if(m_state == S_INIT)
	{
		m_closeReason  = CR_CLOSE_SELF;
		m_socket.Init();
		m_socket.Create(ip.c_str(), SOCK_STREAM);

		m_unreadLength = 0;
		m_readOffset = 0;
		m_Buffer.clear();

		m_ip = ip;
		m_wPort = wPort;

		m_isConnected = false;
		m_state = S_CONNECTING;

		m_rcveThread = std::thread(&ChwNetworkClient::recvThread, this);
		m_rcveThread.detach();

		return true;
	}
	else
	{
		Close(CR_CONNECT_FAILED_ERR);
		return false;
	}
}

void ChwNetworkClient::SetTimeout(float t)
{
	m_timeout = t;
}

void ChwNetworkClient::recvThread()
{	
	m_isConnected = m_socket.ConnectBoth46(m_ip.c_str(), m_wPort, m_timeout);

	if(m_isConnected)
	{
		std::lock_guard<std::mutex> lock(m_changeStateMutex);
		m_state = S_ESTABLISHED;
	}
	else
	{
		std::lock_guard<std::mutex> lock(m_changeStateMutex);
		m_state = S_INIT;
	}

	Scheduler *sched = Director::getInstance()->getScheduler();
	sched->performFunctionInCocosThread( [=](){

		LuaStack *stack = LuaEngine::getInstance()->getLuaStack();
		stack->pushBoolean(m_isConnected);
		stack->executeFunctionByHandler(m_luaHandles["CONNECT"], 1);

		if(m_isConnected)
		{
			scheduleUpdate();
		}

	});

	if(m_isConnected)
	{
		char Buffer[MAX_BUFSIZE];
		while(m_state == S_ESTABLISHED)
		{
			int nLen = m_socket.Recv(Buffer, MAX_BUFSIZE, 0);
			if(nLen == SOCKET_ERROR)
			{
				Close(CR_NET_CLOSE);
				break;
			}
			if (nLen == 0)
			{
				Close(CR_SERVER_CLOSE);
				break;
			}

			std::lock_guard<std::mutex> lock(m_readMutex);
			unsigned int dwEnd = m_readOffset + m_unreadLength;
			m_Buffer.resize(dwEnd + nLen);
			memcpy(&m_Buffer[dwEnd], Buffer, nLen);
			m_unreadLength += nLen;
		}
	}
}

void ChwNetworkClient::Closeed()
{
	unscheduleUpdate();
	m_socket.ShutDown(2);
	auto ret = m_socket.Close();
	if(m_rcveThread.joinable())
	{
		m_rcveThread.join();
	}
	
	m_unreadLength = 0;
	m_readOffset = 0;
	m_Buffer.clear();

	m_isConnected = false;

}

void ChwNetworkClient::Close(unsigned short reason)
{
#if COCOS2D_DEBUG > 0
	CCLOG("======>Close reason:%d state:%d", reason, m_state);
#endif

	if(m_state != S_CLOSE && m_state != S_INIT)
	{
		//m_changeStateMutex.lock();
		m_state = S_CLOSE;
		//m_changeStateMutex.unlock();		
		
        
		Scheduler *sched = Director::getInstance()->getScheduler();
		sched->performFunctionInCocosThread( [=](){
		
			Closeed();

            m_state = S_INIT;
			// 回调LUA Close 通知
			LuaStack *stack = LuaEngine::getInstance()->getLuaStack();
			stack->pushInt(reason);
			stack->executeFunctionByHandler(m_luaHandles["CLOSE"], 1);
		});
	}
}

void ChwNetworkClient::RegisterHandler(std::string type, int luaHandle)
{
	m_luaHandles[type] = luaHandle;
}

bool ChwNetworkClient::Send(unsigned short msgType, std::string msg)
{

#if COCOS2D_DEBUG > 0

	CCLOG("======>Send state:%d isConnected:%d msgType:%d msgLen:%d", m_state, m_isConnected, msgType, msg.length());

#endif

	if(m_isConnected == false || m_state != S_ESTABLISHED)
	{
		CCLOG("Error: Send CurState %d , %d", msgType, m_state);
		return false;
	}

	unsigned int msgLength = msg.length();
	if (msgLength > (MAX_BUFSIZE - HEADER_LENGTH))
	{
		CCLOG("Error: Send Max Msg Length, MsgType: %d, msgLength:%d", msgType, msgLength);
		msgLength = MAX_BUFSIZE - HEADER_LENGTH;
		return false;
	}

	msgLength += HEADER_LENGTH;

	const char* pMsg = new char[HEADER_LENGTH + msg.length()];
	memcpy((void*)pMsg, &msgType, sizeof(unsigned short));

	memcpy((void*)(pMsg + sizeof(unsigned short)), &msgLength, sizeof(unsigned int));

	memcpy((void*)(pMsg + HEADER_LENGTH), msg.c_str(), msg.length());

	if(m_socket.Send(pMsg, msgLength) == SOCKET_ERROR)
	{
		Close(CR_SEND_FAILED_ERR);
		CCLOG("Send Error Socket Close!");
		delete[] pMsg;
		return false;
	}
	delete[] pMsg;

	return true;
}

void ChwNetworkClient::update(float delta)
{

	if(m_state == S_ESTABLISHED)
	{
		std::lock_guard<std::mutex> lock(m_readMutex);

		// 将处理缓冲区中无法识别的数据复制到开始处
		if(m_readOffset > 0)
		{
			memcpy(&m_Buffer[0], &m_Buffer[0] + m_readOffset, m_unreadLength);
			m_readOffset = 0;
		}

		while(m_unreadLength >= HEADER_LENGTH)
		{
			unsigned short wType = 0;

			memcpy(&wType, &m_Buffer[0] + m_readOffset, sizeof(unsigned short));

			unsigned int dwLength = 0;
			memcpy(&dwLength, &m_Buffer[0] + m_readOffset + sizeof(unsigned short), sizeof(unsigned int));
			//CCLOG("%lld Socket recv: msgType: %d, msgLength %d", GetCurTime(), wType, dwLength);
			// 看看是否包含整条消息
			if(m_unreadLength >= dwLength)
			{
				std::string msg(&m_Buffer[0] + m_readOffset + HEADER_LENGTH, dwLength - HEADER_LENGTH);

				// 分析出一条消息了, 回调lua
				if (m_luaHandles["DATA"] != 0)
				{
#if COCOS2D_DEBUG > 0
					CCLOG("======>Recv msgType:%d msgLen:%d", wType, msg.length());
#endif
					LuaStack *stack = LuaEngine::getInstance()->getLuaStack();
					stack->pushInt(wType);
					stack->pushString(msg.c_str(), msg.length());
					stack->executeFunctionByHandler(m_luaHandles["DATA"], 2);
				}
				//
				m_unreadLength -= dwLength;
				m_readOffset += dwLength;	

			}
			else
			{
				break;
			}

		}
	}
}

int ChwNetworkClient::GetState()
{
	return m_state;
}
