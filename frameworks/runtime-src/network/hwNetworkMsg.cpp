#include "hwNetworkMsg.h"

ChwNetworkMsg::ChwNetworkMsg()
{

}

ChwNetworkMsg::ChwNetworkMsg(char* pBuf, unsigned short wLength)
	:ChwCommByteBuff(pBuf, wLength)
{
	m_wType = ReadWORD();
	m_wLength = ReadWORD();
}

ChwNetworkMsg::ChwNetworkMsg(unsigned short wType)
{
	m_wType = wType;
	WriteWORD(wType);
	WriteWORD(0);
}

void ChwNetworkMsg::Finish(void)
{
	m_wLength = GetSize();
	unsigned int dwWriteIndex = m_dwWriteIndex;
	WriteIndex(sizeof(unsigned short));
	WriteWORD(m_wLength);
	WriteIndex(dwWriteIndex);
}

unsigned short ChwNetworkMsg::Type()
{
	return m_wType;
}

unsigned short ChwNetworkMsg::Length()
{
	return m_wLength;
}
