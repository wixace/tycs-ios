#pragma once


#include "cocos2d.h"
#include "network/CCDownloader.h"


USING_NS_CC;


class DownloadWorker :public cocos2d::Ref
{
public:

	typedef std::function<void(const std::string& eventName, const std::string& identifier, const std::string& srcUrl)> EventDelegate;

	DownloadWorker();
	virtual ~DownloadWorker();

	static DownloadWorker* getInstance();
	static void destroyInstance();

	bool initWorker(unsigned int countOfMaxProcessingTasks, unsigned int timeout);

	bool initWorkerLua(int listener, unsigned int countOfMaxProcessingTasks = 6, unsigned int timeout = 20);

	void stopWork();

	bool createDownloadDataTask(const std::string& srcUrl, const std::string& identifier = "");

	bool createDownloadFileTask(const std::string& srcUrl, const std::string& storagePath, const std::string& identifier = "");

private:

	std::unique_ptr<network::Downloader> m_downloader;

	int m_listener;
};
