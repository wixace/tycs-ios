/*
 * define file about portable socket class. 
 * description:this sock is suit both windows and linux
 * design:odison
 * e-mail:odison@126.com>
 * 
 */

#ifndef _ODSOCKET_H_
#define _ODSOCKET_H_

#define WIN32_LEAN_AND_MEAN

#ifdef WIN32
	//#include <Windows.h>
	//#include<iostream>
	//#include<vector>
	//#include<string>
	//#include <winsock2.h>

	#include <windows.h>
	#include <winsock2.h>
	#include <ws2tcpip.h>
	#include <mstcpip.h>
	#include <stdlib.h>
	#include <stdio.h>
	#include <vector>
	#include <string>

	typedef int				socklen_t;
#else
    #include "cocos2d.h"
	#include <sys/socket.h>
	#include <netinet/in.h>
	#include <netdb.h>
	#include <fcntl.h>
	#include <unistd.h>
	#include <sys/stat.h>
	#include <sys/types.h>
	#include <arpa/inet.h>
	typedef int				SOCKET;

	//#pragma region define win32 const variable in linux
	#define INVALID_SOCKET	-1
	#define SOCKET_ERROR	-1
	//#pragma endregion
#endif


class ODSocket {

public:

	ODSocket(SOCKET sock = INVALID_SOCKET);
	~ODSocket();

	// Create socket object for snd/recv data
	bool Create(const char* ip, int type, int protocol = 0);

	// Connect socket
    bool ConnectWithSockaddrIn(sockaddr* svraddr, long svraddrlen, long ftimeout=0);
    bool ConnectBoth46(const char* ip, unsigned short port, unsigned long ftimeout=0);

    
	bool Connect(const char* ip, unsigned short port, unsigned long ftimeout=0);
	

	//#region server
	// Bind socket
	bool Bind(unsigned short port);

	// Listen socket
	bool Listen(int backlog = 5); 

	// Accept socket
	bool Accept(ODSocket& s, char* fromip = NULL);
	//#endregion
	
	// Send socket
	int Send(const char* buf, int len, int flags = 0);

	// Recv socket
	int Recv(char* buf, int len, int flags = 0);
	
	// Close socket
	int Close();

	// Get errno
	int GetError();

	int ShutDown(int how);

	//#pragma region just for win32
	// Init winsock DLL 
	static int Init();	
	// Clean winsock DLL
	static int Clean();
	//#pragma endregion

	// Domain parse
	static bool DnsParse(const char* domain, char* ip);

	ODSocket& operator = (SOCKET s);

	operator SOCKET ();

protected:
	SOCKET m_sock;
	float m_connectTimeout;
};

#endif
