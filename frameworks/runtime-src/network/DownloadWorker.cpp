#include "DownloadWorker.h"
extern "C" {
#include "lua.h"
}
#include "CCLuaEngine.h"

static DownloadWorker* sharedDownloadWorker = nullptr;

DownloadWorker* DownloadWorker::getInstance()
{
	if (!sharedDownloadWorker) {
		sharedDownloadWorker = new (std::nothrow) DownloadWorker();
	}
	return sharedDownloadWorker;
}

void DownloadWorker::destroyInstance()
{
	if (sharedDownloadWorker != nullptr)
	{
		CC_SAFE_DELETE(sharedDownloadWorker);
	}

}

DownloadWorker::DownloadWorker():
	m_listener(0)
{
}

DownloadWorker::~DownloadWorker()
{
	stopWork();
}

bool DownloadWorker::initWorkerLua(int listener, unsigned int countOfMaxProcessingTasks, unsigned int timeout)
{
	m_listener = listener;
	initWorker(countOfMaxProcessingTasks, timeout);

	return true;
}

bool DownloadWorker::initWorker(unsigned int countOfMaxProcessingTasks, unsigned int timeout)
{

	network::DownloaderHints hints =
	{
		countOfMaxProcessingTasks,
		timeout,
		".tmp"
	};

	m_downloader.reset(new network::Downloader(hints));
	// define progress callback
	m_downloader->onTaskProgress = [this](const network::DownloadTask& task,
		int64_t bytesReceived,
		int64_t totalBytesReceived,
		int64_t totalBytesExpected)
	{
		if (m_listener)
		{
			LuaValueDict dict;

			dict["name"] = LuaValue::stringValue("onProgress");
			dict["bytesReceived"] = LuaValue::intValue((int)bytesReceived);
			dict["totalBytesReceived"] = LuaValue::intValue((int)totalBytesReceived);
			dict["totalBytesExpected"] = LuaValue::intValue((int)totalBytesExpected);
			dict["identifier"] = LuaValue::stringValue(task.identifier);
			dict["requestURL"] = LuaValue::stringValue(task.requestURL);

			LuaStack *stack = LuaEngine::getInstance()->getLuaStack();
			stack->clean();
			stack->pushLuaValueDict(dict);
			stack->executeFunctionByHandler(m_listener, 1);
		}
	};

	// define success callback
	m_downloader->onDataTaskSuccess = [this](const cocos2d::network::DownloadTask& task,
		std::vector<unsigned char>& data)
	{
		if (m_listener)
		{
			LuaValueDict dict;

			std::string dataString;
			dataString.insert(dataString.begin(), data.begin(), data.end());

			dict["name"] = LuaValue::stringValue("onDataSuccess");
			dict["data"] = LuaValue::stringValue(dataString);
			dict["identifier"] = LuaValue::stringValue(task.identifier);
			dict["requestURL"] = LuaValue::stringValue(task.requestURL);

			LuaStack *stack = LuaEngine::getInstance()->getLuaStack();
			stack->clean();
			stack->pushLuaValueDict(dict);
			stack->executeFunctionByHandler(m_listener, 1);
		}
	};

	m_downloader->onFileTaskSuccess = [this](const cocos2d::network::DownloadTask& task)
	{
		if (m_listener)
		{
			LuaValueDict dict;

			dict["name"] = LuaValue::stringValue("onFileSuccess");
			dict["storagePath"] = LuaValue::stringValue(task.storagePath);
			dict["identifier"] = LuaValue::stringValue(task.identifier);
			dict["requestURL"] = LuaValue::stringValue(task.requestURL);

			LuaStack *stack = LuaEngine::getInstance()->getLuaStack();
			stack->clean();
			stack->pushLuaValueDict(dict);
			stack->executeFunctionByHandler(m_listener, 1);
		}
	};

	// define failed callback
	m_downloader->onTaskError = [this](const cocos2d::network::DownloadTask& task,
		int errorCode,
		int errorCodeInternal,
		const std::string& errorStr)
	{
		if (m_listener)
		{
			LuaValueDict dict;

			dict["name"] = LuaValue::stringValue("onError");
			dict["errorCode"] = LuaValue::intValue((int)errorCode);
			dict["errorCodeInternal"] = LuaValue::intValue((int)errorCodeInternal);
			dict["errorStr"] = LuaValue::stringValue(errorStr);
			dict["identifier"] = LuaValue::stringValue(task.identifier);
			dict["requestURL"] = LuaValue::stringValue(task.requestURL);

			LuaStack *stack = LuaEngine::getInstance()->getLuaStack();
			stack->clean();
			stack->pushLuaValueDict(dict);
			stack->executeFunctionByHandler(m_listener, 1);
		}
	};

	return true;
}

bool DownloadWorker::createDownloadDataTask(const std::string& srcUrl, const std::string& identifier)
{

	m_downloader->createDownloadDataTask(srcUrl, identifier);
	return true;
}

bool DownloadWorker::createDownloadFileTask(const std::string& srcUrl, const std::string& storagePath, const std::string& identifier)
{
	m_downloader->createDownloadFileTask(srcUrl, storagePath, identifier);
	return true;
}

void DownloadWorker::stopWork()
{
	m_downloader.reset(nullptr);
}