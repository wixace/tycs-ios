#pragma once

#include "../common/hwCommByteBuff.h"

class ChwNetworkMsg: public ChwCommByteBuff
{

public:
	unsigned short	m_wType;
	unsigned short	m_wLength;

	unsigned int	m_dwSID;
public:
	ChwNetworkMsg();
	ChwNetworkMsg(char* pBuf, unsigned short wLength);
	ChwNetworkMsg(unsigned short wType);

public:
	void			Finish();
	unsigned short  Type();
	unsigned short  Length();
};
