/****************************************************************************
Copyright (c) 2013-2017 Chukong Technologies Inc.

http://www.cocos2d-x.org

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
****************************************************************************/


#ifndef __UIDYLISTVIEW_H__
#define __UIDYLISTVIEW_H__

#include "ui/UIListView.h"
#include "ui/GUIExport.h"

/**
 * @addtogroup ui
 * @{
 */
NS_CC_BEGIN

namespace ui {

class DyListView;
class DyListViewItem : public Layout
{
public:

	DyListViewItem();
	virtual ~DyListViewItem();

	static DyListViewItem* create(DyListView* _listView);

	virtual bool init() override;

	bool isLoaded();
	void setLoaded(bool loaded);
	virtual  void setContentSize(const Size& contentSize);

protected:
	bool _isLoaded;
	bool _isSizeDirty;
	DyListView* _listView;

};


class DyListView : public ListView
{

public:

	enum class DelegateTag
	{
		ItemsCount,
		ItemSize,
		LoadItem,
		UnloadItem,
	};

	enum class ReloadDirection
	{
		FRONT,
		BACK,
	};


	typedef std::function<void(Ref*, DelegateTag, unsigned int index, Widget*)> DyListViewDelegate;

	DyListView();

	virtual ~DyListView();

	static DyListView* create();

	virtual bool init() override;

	virtual void jumpToBottom() override;
	virtual void jumpToTop() override;
	virtual void jumpToLeft() override;
	virtual void jumpToRight() override;

	virtual void jumpToItem(ssize_t itemIndex, const Vec2& positionRatioInView, const Vec2& itemAnchorPoint);
	
	void setItemsCount(unsigned int count);
	void setDefaultItemSize(const Size& size);
	Vec2 getItemPositionRatioInView(Widget* item);
	void setViewDelegate(const DyListViewDelegate& delegate);
	void setCacheItemsEnable(bool enable);
	bool isCacheItemsEnable();
	bool isItemInView(Widget* item);
	bool isItemInView(unsigned int itemIndex);
	void setCacheViewSize(float size);
	float getInnerContainerPos();
	void setItemSizeDirty(bool dirty);
	float getItemDistanceInCurrentView(Widget* item);
	void reload(bool isUseOldPos=false);
	void reloadToItem(ssize_t itemIndex);
	Vector<Widget*>& getShowedItems();
	Widget* getItemLua(ssize_t index)const;
	ssize_t getIndexLua(Widget* item) const;

	Widget* getDatumItem();
	const Vec2 getDatumPoint();

	virtual void update(float dt) override;

protected:

	void updateDirtyItemSize(bool isAdjustDistance);


protected:

	std::function<void(void)> _loadingComplete;
	unsigned int _itemsCount;
	DyListViewDelegate _delegate;
	float _lastContainerPos;
	float _cacheViewSize;
	bool _isCacheItems;
	bool _isItemSizeDirty;
	Size _defaultItemSize;
	bool _isInitAllItems;
	ReloadDirection _reloadDirection;
	Vector<Widget*> _showedItems;
};
}
NS_CC_END
// end of ui group
/// @}

#endif /* defined(__ListView__) */
