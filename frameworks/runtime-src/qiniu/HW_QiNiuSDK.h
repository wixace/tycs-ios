#ifndef _QiNiuML_H_
#define _QiNiuML_H_

#include <map>
#include <functional>

class UploadCallback {
    
public:
    
    typedef std::function<void(int, int, std::string&)> onUploadCallback;
    
    UploadCallback(const onUploadCallback& callback);
    void onCallback(int callbackCode, int resultCode, const char *data);
    
private:
    onUploadCallback m_callback;
};


class QiNiuSDK
{

	typedef std::function<void(int code, std::string key)> uploadCallback;
public:
	QiNiuSDK();
	~QiNiuSDK();

	static QiNiuSDK* getInstance();
	static void destroyInstance();


public:

	void setMaxFileSize(int maxFileSize);
	int getMaxFileSize();
	void setMinCompress(int minCompress);
	int getMinCompress();
	void setMaxWidth(int maxWidth);
	int getMaxWidth();
	void setMaxHeight(int maxHeight);
	int getMaxHeight();
	void setCompressFormat(int compressFormat);
	int getCompressFormat();

	void uploadImage(std::string& token, int type, std::string& filePath, uploadCallback callback);
	void onCallback(unsigned int updloadid, int retCode, std::string& msg);

private:

	void updateOptions();

	std::map<unsigned int,uploadCallback> _callbacks;
	int _uploadid;

	int _maxFileSize;
	int _minCompress;
	int _maxWidth;
	int _maxHeight;
	int _compressFormat;
    UploadCallback* _UploadCallback;
};

#endif
