//
//  UpImageSDKiOS.m
//  libhigame
//
//  Created by higame on 2018/11/2.
//  Copyright © 2018年 higame. All rights reserved.
//

#include "QiNiuSDK_Interface.h"
#include <string>

#import "UpImageSDKiOS.h"

void QiNiuSDK_Interface::setUploadCallback(UploadCallback* callback)
{
    [[UpImageSDKiOS shareInstance] setUploadCallback:callback];
}


void QiNiuSDK_Interface::uploadImage(int uploadid, std::string token, int optype, std::string filePath)
{
    [[UpImageSDKiOS shareInstance] uploadImage:uploadid token:[NSString stringWithCString:token.c_str() encoding:NSUTF8StringEncoding] opType:optype filePath:[NSString stringWithCString:filePath.c_str() encoding:NSUTF8StringEncoding]];
}

void QiNiuSDK_Interface::setUploadOptions(int maxFileSize, int minCompress, int maxWidth, int maxHeight, int compressFormat)
{
    
}
