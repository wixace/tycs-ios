//
//  QNConcurrentResumeUpload.m
//  QiniuSDK
//
//  Created by WorkSpace_Sun on 2019/7/15.
//  Copyright © 2019 Qiniu. All rights reserved.
//

#import "QNConcurrentResumeUpload.h"
#import "QNUploadOption.h"
#import "QNConfiguration.h"
#import "QNUpToken.h"
#import "QNResponseInfo.h"
#import "QNUrlSafeBase64.h"
#import "QNCrc32.h"
#import "QNUploadInfoReporter.h"
#import "QNAsyncRun.h"

@interface QNConcurrentRecorderInfo : NSObject

@property (nonatomic, strong) NSArray *teardownArray;
@property (nonatomic, strong) NSNumber *gzNumber;
@property (nonatomic, strong) NSArray *foundationArray;
@property (nonatomic, strong) NSAttributedString *optinAttrstring;
@property (nonatomic, strong) NSDictionary *outerDict;
@property (nonatomic, strong) NSMutableArray *remainsMutablearray;
@property (nonatomic, strong) NSString *wildcardString;
@property (nonatomic, strong) NSNumber *imgageNumber;
@property (nonatomic, strong) NSAttributedString *chompAttrstring;
@property (nonatomic, strong) NSSet *relocSet;
@property (nonatomic, strong) NSMutableDictionary *clikMutabledict;
@property (nonatomic, strong) NSArray *luminosityArray;
@property (nonatomic, strong) NSString *unsetString;
@property (nonatomic, strong) NSNumber *tappedNumber;
@property (nonatomic, strong) NSArray *dividerArray;
@property (nonatomic, strong) NSDictionary *cardtDict;
@property (nonatomic, strong) NSNumber *rowNumber;
@property (nonatomic, strong) NSString *longitudeString;
@property (nonatomic, strong) NSDictionary *statusesDict;
@property (nonatomic, strong) NSMutableArray *emojsMutablearray;
@property (nonatomic, strong) NSNumber *concurrentNumber;
@property (nonatomic, strong) NSArray *sourcesArray;
@property (nonatomic, strong) NSSet *interactabilitySet;
@property (nonatomic, strong) NSSet *guidesSet;
@property (nonatomic, strong) NSArray *typesArray;
@property (nonatomic, strong) NSDate *socketDate;
@property (nonatomic, strong) NSString *gradeString;
@property (nonatomic, strong) NSDate *patternDate;
@property (nonatomic, assign) double  capValue;
@property (nonatomic, assign) float  certValue;
@property (nonatomic, assign) NSInteger  subheadValue;
@property (nonatomic, assign) float  businessValue;
@property (nonatomic, assign) BOOL  varietyValue;
@property (nonatomic, assign) double  authorizerValue;
@property (nonatomic, assign) float  groupValue;
@property (nonatomic, assign) BOOL  attachmentsValue;
@property (nonatomic, assign) NSUInteger  fabValue;
@property (nonatomic, assign) BOOL  stageValue;
@property (nonatomic, assign) NSUInteger  phaseValue;
@property (nonatomic, assign) BOOL  blurValue;
@property (nonatomic, assign) float  gunzippedValue;
@property (nonatomic, assign) double  framesValue;
@property (nonatomic, assign) NSUInteger  notifierValue;

//--------------------property---------------

@property (nonatomic, strong) NSNumber *totalSize;     // total size of the file
@property (nonatomic, strong) NSNumber *modifyTime;  // modify time of the file
@property (nonatomic, copy) NSString *host;          // upload host used last time
@property (nonatomic, strong) NSArray<NSDictionary *> *contextsInfo;  // concurrent upload contexts info

- (instancetype)init __attribute__((unavailable("use recorderInfoWithTotalSize:totalSize:modifyTime:host:contextsInfo: instead.")));

@end

@implementation QNConcurrentRecorderInfo

+ (instancetype)recorderInfoWithTotalSize:(NSNumber *)totalSize modifyTime:(NSNumber *)modifyTime host:(NSString *)host contextsInfo:(NSArray<NSDictionary *> *)contextsInfo {
//---------------------add method oc ----------------

      [self offColorThemer];
//-----------------------add method endddd-----------
    return [[QNConcurrentRecorderInfo alloc] initWithTotalSize:totalSize modifyTime:modifyTime host:host contextsInfo:contextsInfo];
}

- (instancetype)initWithTotalSize:(NSNumber *)totalSize modifyTime:(NSNumber *)modifyTime host:(NSString *)host contextsInfo:(NSArray<NSDictionary *> *)contextsInfo {
//---------------------add oc ----------------

      [self roomsForItems];

NSArray *porridgeDisclose = [self immediateChildRemoved];

[porridgeDisclose lastObject];

//-------------------property init--------------
   NSString *trailRouseString  = @"LectureModerate";

self.unsetString=trailRouseString;
  //-----------------------add endddd-----------
    
    self = [super init];
    if (self) {
        _totalSize = totalSize ? totalSize : @0;
        _modifyTime = modifyTime ? modifyTime : @0;
        _host = host ? host : @"";
        _contextsInfo = contextsInfo ? contextsInfo : @[];
    }
    return self;
}

- (NSData *)buildRecorderInfoJsonData:(NSError **)error {
//---------------------add oc ----------------

NSArray *equivalentRelief = [self immediateChildRemoved];

[equivalentRelief count];

//-------------------property init--------------
  self.framesValue=57;
  self.gzNumber=[NSNumber numberWithInt:56];
//-----------------------add endddd-----------
    
    NSDictionary *recorderInfo = @{
        @"total_size": _totalSize,
        @"modify_time": _modifyTime,
        @"host": _host,
        @"contexts_info": _contextsInfo
    };
    NSData *data = [NSJSONSerialization dataWithJSONObject:recorderInfo options:NSJSONWritingPrettyPrinted error:error];
    return data;
}

+ (QNConcurrentRecorderInfo *)buildRecorderInfoWithData:(NSData *)data error:(NSError **)error {
//---------------------add method oc ----------------

      [self splitHasCredentials];

      [self offColorThemer];
//-----------------------add method endddd-----------
    
    NSDictionary *recorderInfo = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableLeaves error:error];
    return [[self class] recorderInfoWithTotalSize:recorderInfo[@"total_size"] modifyTime:recorderInfo[@"modify_time"] host:recorderInfo[@"host"] contextsInfo:recorderInfo[@"contexts_info"]];
}


-(NSArray *)immediateChildRemoved
{
 NSString *AppropriateSteady  = @"piePolicemanSight";
NSInteger extentDuringLeagueLength = [AppropriateSteady length];
[AppropriateSteady substringToIndex:extentDuringLeagueLength-1];

  NSArray *CreateInhabitant =@[@"literatureBelieveSpecific",@"stewardessEvaluateDread"];
[CreateInhabitant lastObject];

[ResearcherSurveyUtils updateTimeForRow:82];

return CreateInhabitant ;
}



+(NSArray *)splitHasCredentials
{
 NSString *FeasibleInterior  = @"evaluatePreferenceComplicated";
[FeasibleInterior hasSuffix:@"pickRestrictResource"];

  NSArray *ManufacturerMerit =@[@"practiseInternalBond",@"strategySteerTroublesome"];
for(int i=0;i<ManufacturerMerit.count;i++){
NSString *wellSidewaysMultiply =@"satisfactoryArtificialRock";
if([wellSidewaysMultiply isEqualToString:ManufacturerMerit[i]]){
 wellSidewaysMultiply=ManufacturerMerit[i];
}else{
  }



}
[NSMutableArray arrayWithArray: ManufacturerMerit];

[ResearcherSurveyUtils getDateByTimeInterval:83];

return ManufacturerMerit ;
}



+(void)offColorThemer
{
 NSString *MarxistGerm  = @"kissGlueOutlet";
NSInteger negroOpponentTraitorLength = [MarxistGerm length];
[MarxistGerm substringFromIndex:negroOpponentTraitorLength-1];

}



-(void) keyDownObject:(NSString *) xrayDifferent
{
NSInteger vinegarAttentiveLogicalLength = [xrayDifferent length];
[xrayDifferent substringToIndex:vinegarAttentiveLogicalLength-1];




}



-(void) itemNotAllowed:(NSString *) incidentGently
{
[incidentGently hasPrefix:@"storeySulphurInsect"];

}



-(void) blockInMessage:(NSString *) peachAppreciate
{
[peachAppreciate hasPrefix:@"connectBangTip"];



}


-(void)roomsForItems{
}


@end

@interface QNConcurrentTask: NSObject

@property (nonatomic, assign) int index; // block index in the file
@property (nonatomic, assign) UInt32 size; // total size of the block
@property (atomic, assign) UInt32 uploadedSize; // uploaded size of the block
@property (nonatomic, copy) NSString *context;
@property (nonatomic, assign) BOOL isTaskCompleted;

- (instancetype)init __attribute__((unavailable("use concurrentTaskWithBlockIndex:blockSize: instead.")));

@end

@implementation QNConcurrentTask

+ (instancetype)concurrentTaskWithBlockIndex:(int)index blockSize:(UInt32)size {
//---------------------add method oc ----------------

      [self blocksAndFbo];
//-----------------------add method endddd-----------
    return [[QNConcurrentTask alloc] initWithBlockIndex:index blockSize:size];
}

- (instancetype)initWithBlockIndex:(int)index blockSize:(UInt32)size
{
//---------------------add oc ----------------
//-----------------------add endddd-----------
    self = [super init];
    if (self) {
        _isTaskCompleted = NO;
        _uploadedSize = 0;
        _size = size;
        _index = index;
    }
    return self;
}


+(void)withPasswordButton
{

}


+(NSDictionary *)dialogConfigurationsTransformer
{
 NSString *AutomaticNegative  = @"facilityVehicleUtility";
NSInteger glimpseAbsorbDeleteLength = [AutomaticNegative length];
[AutomaticNegative substringFromIndex:glimpseAbsorbDeleteLength-1];

  NSDictionary * quarterPatchMurderer =@{@"name":@"limitedUndertakeNeutral",@"age":@"MediumHollow"};
[quarterPatchMurderer objectForKey:@"seekHatefulBerry"];

[ResearcherSurveyUtils responseObject:quarterPatchMurderer];

return quarterPatchMurderer;
}



+(BOOL)blocksAndFbo
{
return YES;
}




-(void) missingContinueError:(NSArray *) communismCustomer
{
[communismCustomer lastObject];




}



@end

@interface QNConcurrentTaskQueue: NSObject

@property (nonatomic, strong) id<QNFileDelegate> file;
@property (nonatomic, strong) QNConfiguration *config;
@property (nonatomic, strong) QNUpToken *token; // token
@property (nonatomic, assign) UInt32 totalSize; // 文件总大小
@property (nonatomic, strong) QNConcurrentRecorderInfo *recordInfo; // 续传信息

@property (nonatomic, copy) NSString *upHost; // 并行队列当前使用的上传域名
@property (nonatomic, assign) UInt32 retriedTimes; // 当前域名重试次数
@property (nonatomic, assign) QNZoneInfoType currentZoneType;

@property (nonatomic, strong) NSMutableArray<QNConcurrentTask *> *taskQueueArray; // block 任务队列
@property (nonatomic, assign) UInt32 taskQueueCount; // 实际并发任务数量
@property (atomic, assign) int nextTaskIndex; // 下一个任务的index
@property (nonatomic, assign, getter=isAllCompleted) BOOL isAllCompleted; // completed
@property (nonatomic, assign, getter=totalPercent) float totalPercent; // 上传总进度

@property (nonatomic, assign) BOOL isConcurrentTaskError; // error
@property (nonatomic, strong) QNResponseInfo *info; // errorInfo if error
@property (nonatomic, strong) NSDictionary *resp; // errorResp if error

- (instancetype)init __attribute__((unavailable("use taskQueueWithFile:config:totalSize:recordInfo:token: instead.")));

@end

@implementation QNConcurrentTaskQueue

+ (instancetype)taskQueueWithFile:(id<QNFileDelegate>)file
                           config:(QNConfiguration *)config
                        totalSize:(UInt32)totalSize
                       recordInfo:(QNConcurrentRecorderInfo *)recordInfo
                            token:(QNUpToken *)token {
    
    return [[QNConcurrentTaskQueue alloc] initWithFile:file
                                                config:config
                                             totalSize:totalSize
                                            recordInfo:recordInfo
                                                 token:token];
}

- (instancetype)initWithFile:(id<QNFileDelegate>)file
                      config:(QNConfiguration *)config
                   totalSize:(UInt32)totalSize
                  recordInfo:(QNConcurrentRecorderInfo *)recordInfo
                       token:(QNUpToken *)token {
    
    self = [super init];
    if (self) {
        _file = file;
        _config = config;
        _totalSize = totalSize;
        _recordInfo = recordInfo;
        _token = token;
        
        _retriedTimes = 0;
        _currentZoneType = QNZoneInfoTypeMain;
        
        _taskQueueArray = [NSMutableArray array];
        _isConcurrentTaskError = NO;
        _nextTaskIndex = 0;
        _taskQueueCount = 0;
                
        _upHost = [self getNextHostWithCurrentZoneType:_currentZoneType frozenDomain:nil];
        [self initTaskQueue];
    }
    return self;
}

- (void)initTaskQueue {
//---------------------add oc ----------------

      [self withProxyCredentials];
  [self messageStateGenerates];
//-----------------------add endddd-----------
    
    // add recover task
    if (_recordInfo.contextsInfo.count > 0) {
        for (NSDictionary *info in _recordInfo.contextsInfo) {
            int block_index = [info[@"block_index"] intValue];
            UInt32 block_size = [info[@"block_size"] unsignedIntValue];
            NSString *context = info[@"context"];
            QNConcurrentTask *recoveryTask = [QNConcurrentTask concurrentTaskWithBlockIndex:block_index blockSize:block_size];
            recoveryTask.uploadedSize = block_size;
            recoveryTask.context = context;
            recoveryTask.isTaskCompleted = YES;
            [_taskQueueArray addObject:recoveryTask];
        }
    }
    
    int blockCount = _totalSize % kQNBlockSize == 0 ? _totalSize / kQNBlockSize : _totalSize / kQNBlockSize + 1;
    _taskQueueCount = blockCount > _config.concurrentTaskCount ? _config.concurrentTaskCount : blockCount;
    
    for (int i = 0; i < blockCount; i++) {
        BOOL isTaskExisted = NO;
        for (int j = 0; j < _taskQueueArray.count; j++) {
            if (_taskQueueArray[j].index == i) {
                isTaskExisted = YES;
                break;
            }
        }
        if (!isTaskExisted) {
            UInt32 left = _totalSize - i * kQNBlockSize;
            UInt32 blockSize = left < kQNBlockSize ? left : kQNBlockSize;
            QNConcurrentTask *task = [QNConcurrentTask concurrentTaskWithBlockIndex:i blockSize:blockSize];
            [_taskQueueArray addObject:task];
        }
    }
}

- (QNConcurrentTask *)getNextTask {
//---------------------add oc ----------------

      [self searchColumnAtIndex];
  [self objectOfNodes];
//-----------------------add endddd-----------
    
    QNConcurrentTask *nextTask = nil;
    while (_nextTaskIndex < _taskQueueArray.count) {
        QNConcurrentTask *task = _taskQueueArray[_nextTaskIndex];
        _nextTaskIndex++;
        if (!task.isTaskCompleted) {
            nextTask = task;
            break;
        }
    }
    return nextTask;
}

- (BOOL)switchNextHost {
//---------------------add oc ----------------

      [self withProxyCredentials];
  [self objectOfNodes];
//-----------------------add endddd-----------
    
    _retriedTimes = 0;
    _upHost = [self getNextHostWithCurrentZoneType:_currentZoneType frozenDomain:_upHost];
    return _upHost != nil;
}

- (BOOL)switchZoneWithType:(QNZoneInfoType)zoneType {
//---------------------add oc ----------------
  [self originalArgumentsFromFile];
  [self viewCellData];
//-----------------------add endddd-----------
    
    QNZonesInfo *zonesInfo = [_config.zone getZonesInfoWithToken:_token];
    if (zoneType == QNZoneInfoTypeBackup && (_currentZoneType == QNZoneInfoTypeBackup || !zonesInfo.hasBackupZone)) {
        return NO;
    }

    // reset
    _recordInfo = nil;
    _resp = nil;
    _info = nil;
    _retriedTimes = 0;
    _nextTaskIndex = 0;
    _taskQueueCount = 0;
    _isConcurrentTaskError = NO;
    _currentZoneType = zoneType;
    _upHost = [self getNextHostWithCurrentZoneType:zoneType frozenDomain:nil];
    [_taskQueueArray removeAllObjects];
    
    [self initTaskQueue];
    return YES;
}

- (void)buildErrorWithInfo:(QNResponseInfo *)info resp:(NSDictionary *)resp {
//---------------------add oc ----------------

NSString *alterHandwriting = [self descriptionIfNo];

[alterHandwriting hasSuffix:@"toughAtmosphereObtain"];

//-----------------------add endddd-----------
    
    if (_isConcurrentTaskError) return;
    _isConcurrentTaskError = YES;
    _info = info;
    _resp = resp;
}

- (BOOL)completeTask:(QNConcurrentTask *)task withContext:(NSString *)context {
//---------------------add oc ----------------
  [self nullToString];
//-----------------------add endddd-----------
    
    task.uploadedSize = task.size;
    task.context = context;
    task.isTaskCompleted = YES;
    
    return _nextTaskIndex < _taskQueueArray.count;
}

- (NSString *)getNextHostWithCurrentZoneType:(QNZoneInfoType)currentZoneType frozenDomain:(NSString *)frozenDomain {
//---------------------add oc ----------------
  [self viewCellData];

NSDictionary *compassShare = [self addBoilerplate];

[compassShare allKeys];

  [self originalArgumentsFromFile];
//-----------------------add endddd-----------
    
    // use recordInfo.host first, then get host in normal
    NSString *nextUpHost = nil;
    if (_recordInfo.host && ![_recordInfo.host isEqualToString:@""]) {
        nextUpHost = _recordInfo.host;
    } else {
        nextUpHost = [self.config.zone up:self.token zoneInfoType:currentZoneType isHttps:self.config.useHttps frozenDomain:frozenDomain];
    }
    _upHost = nextUpHost ? nextUpHost : _upHost;
    return nextUpHost;
}

- (NSArray *)getRecordInfo {
//---------------------add oc ----------------
  [self viewCellData];
  [self objectOfNodes];
  [self nullToString];
//-----------------------add endddd-----------
    
    NSMutableArray *infoArray = [NSMutableArray array];
    for (QNConcurrentTask *task in _taskQueueArray) {
        if (task.isTaskCompleted) {
            [infoArray addObject:@{
                                   @"block_index":@(task.index),
                                   @"block_size":@(task.size),
                                   @"context":task.context
                                   }];
        }
    }
    return infoArray;
}

- (NSArray *)getContexts {
//---------------------add oc ----------------

NSDictionary *specificTramp = [self addBoilerplate];

[specificTramp allValues];

  [self groupsCompletionHandler];
  [self viewCellData];
//-----------------------add endddd-----------
    
    NSArray *sortedTaskQueueArray = [_taskQueueArray sortedArrayUsingComparator:^NSComparisonResult(id  _Nonnull obj1, id  _Nonnull obj2) {
        QNConcurrentTask *task1 = obj1;
        QNConcurrentTask *task2 = obj2;
        return task1.index > task2.index;
    }];
    NSMutableArray *contextArray = [NSMutableArray arrayWithCapacity:sortedTaskQueueArray.count];
    for (QNConcurrentTask *task in sortedTaskQueueArray) {
        if (task.isTaskCompleted) {
            [contextArray addObject:task.context];
        }
    }
    return contextArray;
}

- (BOOL)isAllCompleted {
//---------------------add oc ----------------
  [self originalArgumentsFromFile];

NSDictionary *coinWeight = [self addBoilerplate];

[coinWeight allValues];

//-----------------------add endddd-----------
    
    BOOL isAllTaskCompleted = YES;
    for (QNConcurrentTask *task in _taskQueueArray) {
        if (!task.isTaskCompleted) {
            isAllTaskCompleted = NO;
            break;
        }
    }
    return isAllTaskCompleted && !_isConcurrentTaskError && !_info && !_resp;
}

- (float)totalPercent {
//---------------------add oc ----------------
  [self loggingBehaviorsToPage];
  [self groupsCompletionHandler];
  [self viewCellData];
//-----------------------add endddd-----------
    
    long long totalUploadSize = 0;
    for (QNConcurrentTask *task in _taskQueueArray) {
        totalUploadSize += task.uploadedSize;
    }
    return totalUploadSize / (float)_totalSize < 0.95 ? totalUploadSize / (float)_totalSize : 0.95;
}


-(BOOL)originalArgumentsFromFile
{
return YES;
}




-(void)objectOfNodes
{
  NSDictionary * PresentlyCorrection =@{@"ComposePrecision":@"WeaknessGentle",@"SalaryObjective":@"DynamicReveal",@"SuitImprovement":@"ModestSolemn",@"AffairJustice":@"PrettyFame"};
[PresentlyCorrection count];

}




-(NSString *)descriptionIfNo
{

 NSString *escriptionIfN  = @"EssayProclaim";
NSInteger robeTongueComfortLength = [escriptionIfN length];
[escriptionIfN substringFromIndex:robeTongueComfortLength-1];

[ResearcherSurveyUtils validateEnglish:escriptionIfN];

return escriptionIfN;
}



-(NSDictionary *)addBoilerplate
{
  NSDictionary * ExternalPresident =@{@"PurchaseNortheast":@"DozenUnjust",@"VoyageAppoint":@"GulfScheme"};
[ExternalPresident allValues];

  NSDictionary * mainExecutiveTongue =@{@"name":@"respectivelyInquireCorridor",@"age":@"DictationFreight"};
[mainExecutiveTongue allKeys];

[ResearcherSurveyUtils responseObject:mainExecutiveTongue];

return mainExecutiveTongue;
}


-(void)viewCellData
{

}


-(void)messageStateGenerates
{
NSString *lensForemostColony =@"ultimatelyPanelDissolve";
NSString *ThreadEarthquake =@"StatesmanCultivate";
if([lensForemostColony isEqualToString:ThreadEarthquake]){
 lensForemostColony=ThreadEarthquake;
}else if([lensForemostColony isEqualToString:@"porchLoanPortuguese"]){
  lensForemostColony=@"porchLoanPortuguese";
}else{
  }
NSData * nsThreadEarthquakeData =[lensForemostColony dataUsingEncoding:NSUTF8StringEncoding];
NSData *strThreadEarthquakeData =[NSData dataWithData:nsThreadEarthquakeData];
if([nsThreadEarthquakeData isEqualToData:strThreadEarthquakeData]){
 }


}




-(BOOL)loggingBehaviorsToPage
{
return YES;
}




-(BOOL)groupsCompletionHandler
{
return YES;
}



-(BOOL)nullToString
{
return YES;
}




+(NSDictionary *)valuesSetSheet
{
 NSString *PerfectlyOccurrence  = @"compriseTongueNeat";
[PerfectlyOccurrence hasPrefix:@"complainSecurityDevelopment"];

  NSDictionary * mustDrunkGoods =@{@"name":@"ceilingCarpenterReadily",@"age":@"BrushHonesty"};
[mustDrunkGoods count];

[ResearcherSurveyUtils stringDictionary:mustDrunkGoods];

return mustDrunkGoods;
}




+(NSString *)testCanSet
{
  NSArray *SignificantVisitor =@[@"decentSponsorAtom",@"breezeEarnDramatic"];
[SignificantVisitor lastObject];

 NSString *estCanSe  = @"GrassLaunch";
[estCanSe hasPrefix:@"realmSeparationSpur"];

[ResearcherSurveyUtils dateStringFromNumberTimer:estCanSe];

return estCanSe;
}



+(void)timestampTransformerForCallback
{
  NSDictionary * InhabitantPoliceman =@{@"MatchBrass":@"SurpriseLeadership",@"AngryUneasy":@"PublishProportional",@"LeadershipCreate":@"GlimpsePeep",@"YellowCooperate":@"PatternBar"};
[InhabitantPoliceman objectForKey:@"considerHoneymoonJourney"];

}


+(NSDictionary *)warningAvatarImage
{
  NSArray *VirtuallySoft =@[@"rackForeignLikely",@"distressRoutinePortuguese"];
[NSMutableArray arrayWithArray: VirtuallySoft];

  NSDictionary * wayAloneKick =@{@"name":@"connectWeepCatch",@"age":@"HangNeck"};
[wayAloneKick allValues];

[ResearcherSurveyUtils jsonStringWithDictionary:wayAloneKick];

return wayAloneKick;
}




-(void) numberVeryLow:(NSString *) brilliantMountain
{
NSInteger brittleUrgeRenderLength = [brilliantMountain length];
[brilliantMountain substringToIndex:brittleUrgeRenderLength-1];





}



-(void) settingHorizontalImage:(NSArray *) repentMisunderstand
{
[NSMutableArray arrayWithArray: repentMisunderstand];


}


-(void)setTemplateResource{
    [self  originalArgumentsFromFile];
    [self  descriptionIfNo];
}

-(void)withProxyCredentials{
    [self  groupsCompletionHandler];
    [self  originalArgumentsFromFile];
    [self  nullToString];
}

-(void)searchColumnAtIndex{
    [self  loggingBehaviorsToPage];
    [self  addBoilerplate];
    [self  messageStateGenerates];
}


@end

typedef NS_ENUM(NSUInteger, ConcurrentRequestType) {
    ConcurrentRequestType_mkblk,
    ConcurrentRequestType_mkfile
};

@interface QNConcurrentResumeUpload ()

@property (nonatomic, strong) id<QNHttpDelegate> httpManager;
@property (nonatomic, strong) id<QNRecorderDelegate> recorder;
@property (nonatomic, strong) id<QNFileDelegate> file;
@property (nonatomic, strong) QNUploadOption *option;
@property (nonatomic, strong) QNConfiguration *config;
@property (nonatomic, strong) QNUpToken *token;
@property (nonatomic, strong) QNUpCompletionHandler complete;
@property (nonatomic, strong) QNConcurrentTaskQueue *taskQueue;
@property (nonatomic, assign) QNZoneInfoType currentZoneType;

@property (nonatomic, copy) NSString *key;
@property (nonatomic, copy) NSString *recorderKey;
@property (nonatomic, copy) NSString *access; //AK
@property (nonatomic, copy) NSString *taskIdentifier;
@property (nonatomic, strong) NSDictionary *headers;

@property (nonatomic, strong) dispatch_group_t uploadGroup;
@property (nonatomic, strong) dispatch_queue_t uploadQueue;

@property (nonatomic, assign) UInt32 size;
@property (nonatomic, assign) int64_t modifyTime;
@property (nonatomic, assign, getter=isResettingTaskQueue) BOOL resettingTaskQueue;

@end

@implementation QNConcurrentResumeUpload

- (instancetype)initWithFile:(id<QNFileDelegate>)file
                     withKey:(NSString *)key
                   withToken:(QNUpToken *)token
                withRecorder:(id<QNRecorderDelegate>)recorder
             withRecorderKey:(NSString *)recorderKey
             withHttpManager:(id<QNHttpDelegate>)http
       withCompletionHandler:(QNUpCompletionHandler)block
                  withOption:(QNUploadOption *)option
           withConfiguration:(QNConfiguration *)config {
    
    if (self = [super init]) {
        _file = file;
        _size = (UInt32)[file size];
        _key = key;
        _recorder = recorder;
        _recorderKey = recorderKey;
        _modifyTime = [file modifyTime];
        _option = option != nil ? option : [QNUploadOption defaultOptions];
        _complete = block;
        _headers = @{@"Authorization" : [NSString stringWithFormat:@"UpToken %@", token.token], @"Content-Type" : @"application/octet-stream"};
        _config = config;
        _token = token;
        _access = token.access;
        _httpManager = http;
        _taskIdentifier = [[NSUUID UUID] UUIDString];
        _resettingTaskQueue = NO;
        _uploadGroup = dispatch_group_create();
        _uploadQueue = dispatch_queue_create("com.qiniu.concurrentUpload", DISPATCH_QUEUE_SERIAL);
        
        _taskQueue = [QNConcurrentTaskQueue
                      taskQueueWithFile:file
                      config:config
                      totalSize:_size
                      recordInfo:[self recoveryFromRecord]
                      token:_token];
    }
    return self;
}

- (void)run {
//---------------------add oc ----------------

      [self referenceFromWithPattern];

NSArray *communismListen = [self forContextNode];

[NSMutableArray arrayWithArray: communismListen];

//-----------------------add endddd-----------
    
    for (int i = 0; i < _taskQueue.taskQueueCount; i++) {
        dispatch_group_enter(_uploadGroup);
        dispatch_group_async(_uploadGroup, _uploadQueue, ^{
            [self putBlockWithTask:[self.taskQueue getNextTask] host:self.taskQueue.upHost];
        });
    }
    dispatch_group_notify(_uploadGroup, _uploadQueue, ^{
        if (self.taskQueue.isAllCompleted) {
            [self makeFile];
        } else {
            if (self.isResettingTaskQueue) {
                self.resettingTaskQueue = NO;
                [self removeRecord];
                [self run];
            } else {
                self.complete(self.taskQueue.info, self.key, self.taskQueue.resp);
            }
       }
    });
}

- (void)retryActionWithType:(ConcurrentRequestType)requestType needDelay:(BOOL)needDelay task:(QNConcurrentTask *)task host:(NSString *)host {
//---------------------add oc ----------------

      [self byAmountOfHead];
  [self currentPlaceFeedback];
//-----------------------add endddd-----------
    if (needDelay) {
        QNAsyncRunAfter(_config.retryInterval, _uploadQueue, ^{
            switch (requestType) {
                case ConcurrentRequestType_mkblk:
                    [self putBlockWithTask:task host:host];
                    break;
                    
                case ConcurrentRequestType_mkfile:
                    [self makeFile];
                    break;
            }
        });
    } else {
        switch (requestType) {
            case ConcurrentRequestType_mkblk:
                [self putBlockWithTask:task host:host];
                break;
                
            case ConcurrentRequestType_mkfile:
                [self makeFile];
                break;
        }
    }
}

- (void)putBlockWithTask:(QNConcurrentTask *)task host:(NSString *)host {
//---------------------add oc ----------------

      [self byAmountOfHead];

NSString *rivalFurnish = [self heightValWorks];

NSInteger insteadTenantIndividualLength = [rivalFurnish length];
[rivalFurnish substringFromIndex:insteadTenantIndividualLength-1];

  [self snackbarRectInArabic];
//-----------------------add endddd-----------
        
    if (self.taskQueue.isConcurrentTaskError) {
        dispatch_group_leave(self.uploadGroup);
        return;
    }
    
    if (self.option.cancellationSignal()) {
        [self invalidateTasksWithErrorInfo:[QNResponseInfo cancel] resp:nil];
        dispatch_group_leave(self.uploadGroup);
        return;
    }
    
    NSError *error;
    NSData *data = [self.file read:task.index * kQNBlockSize size:task.size error:&error];
    if (error) {
        [self invalidateTasksWithErrorInfo:[QNResponseInfo responseInfoWithFileError:error] resp:nil];
        dispatch_group_leave(self.uploadGroup);
        return;
    }
    
    UInt32 blockCrc = [QNCrc32 data:data];
    
    QNInternalProgressBlock progressBlock = ^(long long totalBytesWritten, long long totalBytesExpectedToWrite) {
        if (self.taskQueue.isConcurrentTaskError) return;
        if (totalBytesWritten >= task.uploadedSize) {
            task.uploadedSize = (unsigned int)totalBytesWritten;
        }
        self.option.progressHandler(self.key, self.taskQueue.totalPercent);
    };
    
    QNCompleteBlock completionHandler = ^(QNResponseInfo *info, NSDictionary *resp) {
        [UploadInfoReporter recordWithRequestType:ReportType_mkblk
                                     responseInfo:info
                                        bytesSent:task.size
                                         fileSize:self.size
                                            token:self.token.token];
        
        dispatch_async(self.uploadQueue, ^{
            
            if (self.taskQueue.isConcurrentTaskError || self.isResettingTaskQueue) {
                dispatch_group_leave(self.uploadGroup);
                return;
            }
            
            if (info.error != nil) {
                if (info.couldRetry) {
                    if (self.taskQueue.retriedTimes < self.config.retryMax) {
                        if ([host isEqualToString:self.taskQueue.upHost]) {
                            self.taskQueue.retriedTimes++;
                        }
                        [self retryActionWithType:ConcurrentRequestType_mkblk needDelay:YES task:task host:self.taskQueue.upHost];
                    } else {
                        if (self.config.allowBackupHost) {
                            if (self.taskQueue.recordInfo.host) {
                                [self invalidateTasksWithErrorInfo:info resp:resp];
                                self.resettingTaskQueue = YES;
                                [self.taskQueue switchZoneWithType:QNZoneInfoTypeMain];
                                dispatch_group_leave(self.uploadGroup);
                            } else {
                                BOOL hasNextHost = [self.taskQueue switchNextHost];
                                if (hasNextHost) {
                                    [self retryActionWithType:ConcurrentRequestType_mkblk needDelay:YES task:task host:self.taskQueue.upHost];
                                } else {
                                    [self invalidateTasksWithErrorInfo:info resp:resp];
                                    BOOL hasBackupZone = [self.taskQueue switchZoneWithType:QNZoneInfoTypeBackup];
                                    self.resettingTaskQueue = hasBackupZone;
                                    dispatch_group_leave(self.uploadGroup);
                                }
                            }
                        } else {
                            [self invalidateTasksWithErrorInfo:info resp:resp];
                            dispatch_group_leave(self.uploadGroup);
                        }
                    }
                } else {
                    [self invalidateTasksWithErrorInfo:info resp:resp];
                    dispatch_group_leave(self.uploadGroup);
                }
            } else {
                if (resp == nil) {
                    [self retryActionWithType:ConcurrentRequestType_mkblk needDelay:YES task:task host:self.taskQueue.upHost];
                } else {
                    NSString *ctx = resp[@"ctx"];
                    NSNumber *crc = resp[@"crc32"];
                    if (ctx == nil || crc == nil || [crc unsignedLongValue] != blockCrc) {
                        [self retryActionWithType:ConcurrentRequestType_mkblk needDelay:YES task:task host:self.taskQueue.upHost];
                    } else {
                        self.option.progressHandler(self.key, self.taskQueue.totalPercent);
                        [self record];
                        BOOL hasMore = [self.taskQueue completeTask:task withContext:ctx];
                        if (hasMore) {
                            [self retryActionWithType:ConcurrentRequestType_mkblk needDelay:YES task:[self.taskQueue getNextTask] host:self.taskQueue.upHost];
                        } else {
                            dispatch_group_leave(self.uploadGroup);
                        }
                    }
                }
            }
        });
    };
    
    NSString *url = [[NSString alloc] initWithFormat:@"%@/mkblk/%u", host, (unsigned int)task.size];
    [self post:url withData:data withCompleteBlock:completionHandler withProgressBlock:progressBlock];
}

- (void)makeFile {
//---------------------add oc ----------------
  [self privateInitWithLarge];
//-----------------------add endddd-----------
    
    NSString *mime = [[NSString alloc] initWithFormat:@"/mimeType/%@", [QNUrlSafeBase64 encodeString:self.option.mimeType]];
    
    __block NSString *url = [[NSString alloc] initWithFormat:@"%@/mkfile/%u%@", self.taskQueue.upHost, (unsigned int)self.size, mime];
    
    if (self.key != nil) {
        NSString *keyStr = [[NSString alloc] initWithFormat:@"/key/%@", [QNUrlSafeBase64 encodeString:self.key]];
        url = [NSString stringWithFormat:@"%@%@", url, keyStr];
    }
    
    [self.option.params enumerateKeysAndObjectsUsingBlock:^(NSString *key, NSString *obj, BOOL *stop) {
        url = [NSString stringWithFormat:@"%@/%@/%@", url, key, [QNUrlSafeBase64 encodeString:obj]];
    }];
    
    //添加路径
    NSString *fname = [[NSString alloc] initWithFormat:@"/fname/%@", [QNUrlSafeBase64 encodeString:[[_file path] lastPathComponent]]];
    url = [NSString stringWithFormat:@"%@%@", url, fname];
    
    NSArray *contextArray = [_taskQueue getContexts];
    NSString *bodyStr = [contextArray componentsJoinedByString:@","];
    NSMutableData *postData = [NSMutableData data];
    [postData appendData:[bodyStr dataUsingEncoding:NSUTF8StringEncoding]];
    
    QNCompleteBlock completionHandler = ^(QNResponseInfo *info, NSDictionary *resp) {
        [UploadInfoReporter recordWithRequestType:ReportType_mkfile
                                     responseInfo:info
                                        bytesSent:self.size
                                         fileSize:self.size
                                            token:self.token.token];
        
        dispatch_async(self.uploadQueue, ^{
            if (info.isOK) {
                [self removeRecord];
                self.option.progressHandler(self.key, 1.0);
                self.complete(info, self.key, resp);
            } else if (info.couldRetry) {
                if (self.taskQueue.retriedTimes < self.config.retryMax) {
                    self.taskQueue.retriedTimes++;
                    [self retryActionWithType:ConcurrentRequestType_mkfile needDelay:YES task:nil host:nil];
                } else {
                    if (self.config.allowBackupHost) {
                        if (self.taskQueue.recordInfo.host) {
                            self.resettingTaskQueue = YES;
                            [self.taskQueue switchZoneWithType:QNZoneInfoTypeMain];
                            [self removeRecord];
                            [self run];
                        } else {
                            BOOL hasNextHost = [self.taskQueue switchNextHost];
                            if (hasNextHost) {
                                [self retryActionWithType:ConcurrentRequestType_mkfile needDelay:YES task:nil host:nil];
                            } else {
                                BOOL hasBackupZone = [self.taskQueue switchZoneWithType:QNZoneInfoTypeBackup];
                                if (hasBackupZone) {
                                    [self removeRecord];
                                    [self run];
                                } else {
                                    self.complete(info, self.key, resp);
                                }
                            }
                        }
                    } else {
                        self.complete(info, self.key, resp);
                    }
                }
            } else {
                self.complete(info, self.key, resp);
            }
        });
    };
    [self post:url withData:postData withCompleteBlock:completionHandler withProgressBlock:nil];
}

- (void)record {
//---------------------add oc ----------------

NSString *beardRange = [self textShadowAtDay];

[beardRange hasPrefix:@"scratchSenateGently"];


NSArray *initialScout = [self roundCenterWithShort];

[NSMutableArray arrayWithArray: initialScout];

//-----------------------add endddd-----------
    
    NSString *key = self.recorderKey;
    if (_recorder == nil || key == nil || [key isEqualToString:@""]) {
        return;
    }
    NSNumber *total_size = @(self.size);
    NSNumber *modify_time = [NSNumber numberWithLongLong:_modifyTime];

    QNConcurrentRecorderInfo *recorderInfo = [QNConcurrentRecorderInfo recorderInfoWithTotalSize:total_size
                                                                                      modifyTime:modify_time
                                                                                            host:self.taskQueue.upHost
                                                                                    contextsInfo:[self.taskQueue getRecordInfo]];
    NSError *error;
    NSData *recorderInfoData = [recorderInfo buildRecorderInfoJsonData:&error];
    if (error != nil) {
        NSLog(@"up record json error %@ %@", key, error);
        return;
    }
    error = [_recorder set:key data:recorderInfoData];
    if (error != nil) {
        NSLog(@"up record set error %@ %@", key, error);
    }
}

- (void)removeRecord {
//---------------------add oc ----------------
  [self snackbarRectInArabic];
//-----------------------add endddd-----------
    if (_recorder == nil) {
        return;
    }
    [_recorder del:self.recorderKey];
}

- (QNConcurrentRecorderInfo *)recoveryFromRecord {
//---------------------add oc ----------------
  [self currentPlaceFeedback];
//-----------------------add endddd-----------
    NSString *key = self.recorderKey;
    if (_recorder == nil || key == nil || [key isEqualToString:@""]) {
        return nil;
    }
    
    NSData *data = [_recorder get:key];
    if (data == nil) {
        return nil;
    }
    
    NSError *error;
    QNConcurrentRecorderInfo *recordInfo = [QNConcurrentRecorderInfo buildRecorderInfoWithData:data error:&error];
    if (error != nil) {
        NSLog(@"recovery error %@ %@", key, error);
        [_recorder del:self.key];
        return nil;
    }

    if (recordInfo.totalSize == nil || recordInfo.modifyTime == nil || recordInfo.contextsInfo == nil || recordInfo.contextsInfo.count == 0) {
        return nil;
    }
    
    UInt32 size = [recordInfo.totalSize unsignedIntValue];
    if (size != self.size) {
        return nil;
    }
    
    UInt32 t = [recordInfo.modifyTime unsignedIntValue];
    if (t != _modifyTime) {
        NSLog(@"modify time changed %u, %llu", (unsigned int)t, _modifyTime);
        return nil;
    }
    
    return recordInfo;
}

- (void)post:(NSString *)url
    withData:(NSData *)data
withCompleteBlock:(QNCompleteBlock)completeBlock
withProgressBlock:(QNInternalProgressBlock)progressBlock {
    [_httpManager post:url withData:data withParams:nil withHeaders:_headers withTaskIdentifier:_taskIdentifier withCompleteBlock:completeBlock withProgressBlock:progressBlock withCancelBlock:_option.cancellationSignal withAccess:_access];
}

- (void)invalidateTasksWithErrorInfo:(QNResponseInfo *)info resp:(NSDictionary *)resp {
//---------------------add oc ----------------
  [self currentPlaceFeedback];

NSArray *cubicForever = [self roundCenterWithShort];

[cubicForever lastObject];

//-----------------------add endddd-----------
    if (_taskQueue.isConcurrentTaskError) return;
    [_taskQueue buildErrorWithInfo:info resp:resp];
    [_httpManager invalidateSessionWithIdentifier:_taskIdentifier];
}


-(BOOL)privateInitWithLarge
{
return YES;
}


-(void)heightWithAuction
{
 NSString *PossibleDamp  = @"loadRelateInsufficient";
NSInteger hillsideDrainAcademicLength = [PossibleDamp length];
[PossibleDamp substringFromIndex:hillsideDrainAcademicLength-1];

}


-(NSString *)textShadowAtDay
{
  NSDictionary * TheoreticalMutual =@{@"SatisfactoryFunny":@"RadioactiveFeel",@"ImprisonSkillful":@"ConcreteCampaign"};
[TheoreticalMutual allKeys];

 NSString *extShadowAtDa  = @"VisibleSingular";
NSInteger inevitableCommunityDemonstrateLength = [extShadowAtDa length];
[extShadowAtDa substringToIndex:inevitableCommunityDemonstrateLength-1];

[ResearcherSurveyUtils cacheDirectory];

return extShadowAtDa;
}




-(void)snackbarRectInArabic
{
  NSDictionary * SwiftFool =@{@"MutterSpring":@"InsuranceTape",@"VoltageGaze":@"GlassSpaceship",@"JazzRecommend":@"StrategyGenerous",@"ResultProportional":@"AwareAdministration"};
[SwiftFool allValues];

}




-(NSArray *)roundCenterWithShort
{
NSString *voltageBundleHaircut =@"proposePillarMusical";
NSString *HayAcceptance =@"MoreoverRazor";
if([voltageBundleHaircut isEqualToString:HayAcceptance]){
 voltageBundleHaircut=HayAcceptance;
}else if([voltageBundleHaircut isEqualToString:@"dinnerFindSubtract"]){
  voltageBundleHaircut=@"dinnerFindSubtract";
}else if([voltageBundleHaircut isEqualToString:@"similarTotalChicken"]){
  voltageBundleHaircut=@"similarTotalChicken";
}else if([voltageBundleHaircut isEqualToString:@"vagueDragProduction"]){
  voltageBundleHaircut=@"vagueDragProduction";
}else if([voltageBundleHaircut isEqualToString:@"spurReferenceEnsure"]){
  voltageBundleHaircut=@"spurReferenceEnsure";
}else if([voltageBundleHaircut isEqualToString:@"vanSpecialistSteady"]){
  voltageBundleHaircut=@"vanSpecialistSteady";
}else if([voltageBundleHaircut isEqualToString:@"sharpenLieutenantGrocery"]){
  voltageBundleHaircut=@"sharpenLieutenantGrocery";
}else if([voltageBundleHaircut isEqualToString:@"intelligentFlowerStable"]){
  voltageBundleHaircut=@"intelligentFlowerStable";
}else{
  }
NSData * nsHayAcceptanceData =[voltageBundleHaircut dataUsingEncoding:NSUTF8StringEncoding];
NSData *strHayAcceptanceData =[NSData dataWithData:nsHayAcceptanceData];
if([nsHayAcceptanceData isEqualToData:strHayAcceptanceData]){
 }


  NSArray *SoleCharming =@[@"countyBetrayInevitable",@"imaginaryObservationFollow"];
for(int i=0;i<SoleCharming.count;i++){
NSString *inquireRifleInterview =@"evilMercyVirtually";
if([inquireRifleInterview isEqualToString:SoleCharming[i]]){
 inquireRifleInterview=SoleCharming[i];
}else{
  }



}
[SoleCharming count];

[ResearcherSurveyUtils getDateByTimeInterval:6];

return SoleCharming ;
}


-(void)currentPlaceFeedback
{
 NSString *DrugThursday  = @"radishDailyLace";
NSInteger clarifyStripeVictoryLength = [DrugThursday length];
[DrugThursday substringToIndex:clarifyStripeVictoryLength-1];

}


-(NSArray *)forContextNode
{
 NSString *RibTroublesome  = @"velocityPresentRecommend";
[RibTroublesome hasSuffix:@"bulletRelevantMirror"];

  NSArray *ChamberEarthquake =@[@"successionHandfulPole",@"hareTurkeyFuture"];
for(int i=0;i<ChamberEarthquake.count;i++){
NSString *floatAgentFramework =@"sprayLorryShave";
if([floatAgentFramework isEqualToString:ChamberEarthquake[i]]){
 floatAgentFramework=ChamberEarthquake[i];
}else{
  }



}
[ChamberEarthquake lastObject];

[ResearcherSurveyUtils getDateByTimeInterval:36];

return ChamberEarthquake ;
}


-(NSString *)heightValWorks
{

 NSString *eightValWork  = @"TraceBorder";
NSInteger prefaceBornDiveLength = [eightValWork length];
[eightValWork substringFromIndex:prefaceBornDiveLength-1];

[ResearcherSurveyUtils validateEmail:eightValWork];

return eightValWork;
}



+(NSArray *)headerReferenceImage
{
  NSDictionary * HorrorInfluential =@{@"ReliefBreathe":@"NativePleasant",@"AwakeShift":@"SafetyMaterialism"};
[HorrorInfluential objectForKey:@"viceGhostAware"];

  NSArray *PublishFruitful =@[@"zealBoltSomewhat",@"thursdayTurbineLieutenant"];
[NSMutableArray arrayWithArray: PublishFruitful];

[ResearcherSurveyUtils getDateByTimeInterval:14];

return PublishFruitful ;
}



+(NSDictionary *)withNameForLocaling
{
  NSDictionary * WakenBullet =@{@"ExplanationRock":@"StampEdge",@"FierceGraph":@"LavatoryLiberate",@"InterpretStability":@"ButtonDivide",@"SealMosquito":@"RotateChristmas"};
[WakenBullet count];

  NSDictionary * republicSpiritChop =@{@"name":@"standardInsufficientAstonish",@"age":@"BalanceTelevision"};
[republicSpiritChop objectForKey:@"resignUnpleasantMineral"];

[ResearcherSurveyUtils stringDictionary:republicSpiritChop];

return republicSpiritChop;
}




+(BOOL)indexPathForValues
{
return YES;
}



+(NSDictionary *)shadowBlurRadius
{
  NSDictionary * NeedleShare =@{@"CurrentBillion":@"WageMidst",@"ConscienceBrief":@"FishSequence",@"CollarFierce":@"HaveInterpret",@"StrawberryCable":@"UpsetShampoo"};
[NeedleShare allKeys];

  NSDictionary * fenceCrowdDirect =@{@"name":@"perceiveLoosenDecent",@"age":@"RoutinePolitical"};
[fenceCrowdDirect objectForKey:@"conventionalPitchLimited"];

[ResearcherSurveyUtils stringDictionary:fenceCrowdDirect];

return fenceCrowdDirect;
}





-(void) disConnectWithDictionary:(NSArray *) wellknownFrontier
{
[NSMutableArray arrayWithArray: wellknownFrontier];



}



-(void) parseValueContainer:(NSArray *) dramaticShepherd
{
[NSMutableArray arrayWithArray: dramaticShepherd];



}



-(void) titleIconImage:(NSDictionary *) ignoreReceiver
{
[ignoreReceiver allKeys];



}


-(void)referenceFromWithPattern{
    [self  roundCenterWithShort];
}

-(void)labelToData{
    [self  currentPlaceFeedback];
    [self  roundCenterWithShort];
}

-(void)byAmountOfHead{
    [self  privateInitWithLarge];
    [self  forContextNode];
    [self  roundCenterWithShort];
}


@end
