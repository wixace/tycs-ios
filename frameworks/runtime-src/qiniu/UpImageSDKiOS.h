//
//  UpImageSDKiOS.h
//  libhigame
//
//  Created by higame on 2018/11/2.
//  Copyright © 2018年 higame. All rights reserved.
//

#import <Foundation/Foundation.h>

#import "HW_QiNiuSDK.h"
#import "QiniuSDK.h"

class UploadCallback;

@interface UpImageSDKiOS : NSObject
{
    UploadCallback *_UploadCallback;
    int _maxFileSize;
    int _minCompress;
    int _maxWidth;
    int _maxHeight;
    int _compressFormat;
    int _uploadid;
    NSString* _token;
    NSString* _filePath;
    NSTimeInterval _filetime;
    int _zone;
    bool _isChangeZone;
    QNUploadManager* _upManager;
}

- (QNUploadManager *)getUploadMgr;

+ (UpImageSDKiOS *)shareInstance;
-(void)setUploadCallback:(UploadCallback*)callback;
-(void)onUploadCallback:(int)uploadid retCode:(int)retCode key:(NSString*) key;
-(void)uploadImage:(int)uploadid token:(NSString*)token opType:(int)opType filePath:(NSString*)filePath;
-(void)setUploadOptions:(int)maxFileSize minCompress:(int)minCompress maxWidth:(int)maxWidth maxHeight:(int)maxHeight compressFormat:(int)compressFormat;
-(void)cleanUploadData;

@end
