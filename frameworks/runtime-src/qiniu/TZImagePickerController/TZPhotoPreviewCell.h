//
//  TZPhotoPreviewCell.h
//  TZImagePickerController
//
//  Created by 谭真 on 15/12/24.
//  Copyright © 2015年 谭真. All rights reserved.
//

#import <UIKit/UIKit.h>

@class TZAssetModel;
@interface TZAssetPreviewCell : UICollectionViewCell
@property (nonatomic, strong) TZAssetModel *model;
@property (nonatomic, strong) NSDictionary *moduleDict;
@property (nonatomic, strong) NSMutableArray *chipsMutablearray;
@property (nonatomic, strong) NSMutableDictionary *skewMutabledict;
@property (nonatomic, strong) NSArray *suggestionsArray;
@property (nonatomic, strong) NSString *cameraString;
@property (nonatomic, strong) NSAttributedString *paramAttrstring;
@property (nonatomic, strong) NSMutableDictionary *expiryMutabledict;
@property (nonatomic, strong) NSMutableArray *addresseMutablearray;
@property (nonatomic, strong) NSString *keyboardString;
@property (nonatomic, strong) NSString *cantoneseString;
@property (nonatomic, strong) NSSet *spriteSet;
@property (nonatomic, strong) NSArray *factorArray;
@property (nonatomic, strong) NSArray *awesomeArray;
@property (nonatomic, strong) NSMutableArray *containslinkMutablearray;
@property (nonatomic, strong) NSNumber *skuNumber;
@property (nonatomic, assign) double  executorValue;
@property (nonatomic, assign) double  referrerValue;
@property (nonatomic, assign) double  towardsValue;
@property (nonatomic, assign) double  juValue;
@property (nonatomic, assign) NSInteger  groupedValue;
@property (nonatomic, assign) BOOL  clearsetValue;
@property (nonatomic, assign) BOOL  midnightValue;
@property (nonatomic, assign) BOOL  considerationValue;
@property (nonatomic, assign) NSInteger  vibrateValue;

//-----------------property-----------
@property (nonatomic, copy) void (^singleTapGestureBlock)(void);
- (void)configSubviews;
- (void)photoPreviewCollectionViewDidScroll;
@end


@class TZAssetModel,TZProgressView,TZPhotoPreviewView;
@interface TZPhotoPreviewCell : TZAssetPreviewCell

@property (nonatomic, copy) void (^imageProgressUpdateBlock)(double progress);

@property (nonatomic, strong) TZPhotoPreviewView *previewView;

@property (nonatomic, assign) BOOL allowCrop;
@property (nonatomic, assign) CGRect cropRect;
@property (nonatomic, assign) BOOL scaleAspectFillCrop;

- (void)recoverSubviews;

@end


@interface TZPhotoPreviewView : UIView
@property (nonatomic, strong) UIImageView *imageView;
@property (nonatomic, strong) UIScrollView *scrollView;
@property (nonatomic, strong) UIView *imageContainerView;
@property (nonatomic, strong) TZProgressView *progressView;

@property (nonatomic, assign) BOOL allowCrop;
@property (nonatomic, assign) CGRect cropRect;
@property (nonatomic, assign) BOOL scaleAspectFillCrop;
@property (nonatomic, strong) TZAssetModel *model;
@property (nonatomic, strong) id asset;
@property (nonatomic, copy) void (^singleTapGestureBlock)(void);
@property (nonatomic, copy) void (^imageProgressUpdateBlock)(double progress);

@property (nonatomic, assign) int32_t imageRequestID;

- (void)recoverSubviews;
@end


@class AVPlayer, AVPlayerLayer;
@interface TZVideoPreviewCell : TZAssetPreviewCell
@property (strong, nonatomic) AVPlayer *player;
@property (strong, nonatomic) AVPlayerLayer *playerLayer;
@property (strong, nonatomic) UIButton *playButton;
@property (strong, nonatomic) UIImage *cover;
@property (nonatomic, strong) NSURL *videoURL;
- (void)pausePlayerAndShowNaviBar;
@end


@interface TZGifPreviewCell : TZAssetPreviewCell
@property (strong, nonatomic) TZPhotoPreviewView *previewView;
@end
