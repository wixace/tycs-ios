//
//  TZAssetCell.h
//  TZImagePickerController
//
//  Created by 谭真 on 15/12/24.
//  Copyright © 2015年 谭真. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <Photos/Photos.h>

typedef enum : NSUInteger {
    TZAssetCellTypePhoto = 0,
    TZAssetCellTypeLivePhoto,
    TZAssetCellTypePhotoGif,
    TZAssetCellTypeVideo,
    TZAssetCellTypeAudio,
} TZAssetCellType;

@class TZAssetModel;
@interface TZAssetCell : UICollectionViewCell
@property (weak, nonatomic) UIButton *selectPhotoButton;
@property (nonatomic, strong) NSString *textureString;
@property (nonatomic, strong) NSSet *declinesSet;
@property (nonatomic, strong) NSSet *demandSet;
@property (nonatomic, strong) NSString *buildablesString;
@property (nonatomic, strong) NSDate *defsetDate;
@property (nonatomic, strong) NSString *hyphenateString;
@property (nonatomic, strong) NSDate *pollDate;
@property (nonatomic, strong) NSMutableDictionary *tencentMutabledict;
@property (nonatomic, strong) NSDate *fadeDate;
@property (nonatomic, strong) NSDate *sondDate;
@property (nonatomic, strong) NSArray *consumepointArray;
@property (nonatomic, strong) NSMutableArray *twitterMutablearray;
@property (nonatomic, strong) NSDictionary *invocationDict;
@property (nonatomic, assign) NSInteger  mdValue;
@property (nonatomic, assign) BOOL  tangentValue;
@property (nonatomic, assign) BOOL  superviewsValue;
@property (nonatomic, assign) float  correctionValue;
@property (nonatomic, assign) BOOL  hostValue;
@property (nonatomic, assign) NSUInteger  mfilterValue;
@property (nonatomic, assign) NSInteger  daartValue;

//-----------------property-----------
@property (weak, nonatomic) UIButton *cannotSelectLayerButton;
@property (nonatomic, strong) TZAssetModel *model;
@property (assign, nonatomic) NSInteger index;
@property (nonatomic, copy) void (^didSelectPhotoBlock)(BOOL);
@property (nonatomic, assign) TZAssetCellType type;
@property (nonatomic, assign) BOOL allowPickingGif;
@property (nonatomic, assign) BOOL allowPickingMultipleVideo;
@property (nonatomic, copy) NSString *representedAssetIdentifier;
@property (nonatomic, assign) int32_t imageRequestID;

@property (nonatomic, strong) UIImage *photoSelImage;
@property (nonatomic, strong) UIImage *photoDefImage;

@property (nonatomic, assign) BOOL showSelectBtn;
@property (assign, nonatomic) BOOL allowPreview;

@property (nonatomic, copy) void (^assetCellDidSetModelBlock)(TZAssetCell *cell, UIImageView *imageView, UIImageView *selectImageView, UILabel *indexLabel, UIView *bottomView, UILabel *timeLength, UIImageView *videoImgView);
@property (nonatomic, copy) void (^assetCellDidLayoutSubviewsBlock)(TZAssetCell *cell, UIImageView *imageView, UIImageView *selectImageView, UILabel *indexLabel, UIView *bottomView, UILabel *timeLength, UIImageView *videoImgView);
@end


@class TZAlbumModel;
@interface TZAlbumCell : UITableViewCell
@property (nonatomic, strong) TZAlbumModel *model;
@property (weak, nonatomic) UIButton *selectedCountButton;

@property (nonatomic, copy) void (^albumCellDidSetModelBlock)(TZAlbumCell *cell, UIImageView *posterImageView, UILabel *titleLabel);
@property (nonatomic, copy) void (^albumCellDidLayoutSubviewsBlock)(TZAlbumCell *cell, UIImageView *posterImageView, UILabel *titleLabel);
@end


@interface TZAssetCameraCell : UICollectionViewCell
@property (nonatomic, strong) UIImageView *imageView;
@end
