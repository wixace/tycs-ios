//
//  TZVideoPlayerController.m
//  TZImagePickerController
//
//  Created by 谭真 on 16/1/5.
//  Copyright © 2016年 谭真. All rights reserved.
//

#import "TZVideoPlayerController.h"
#import <MediaPlayer/MediaPlayer.h>
#import "UIView+Layout.h"
#import "TZImageManager.h"
#import "TZAssetModel.h"
#import "TZImagePickerController.h"
#import "TZPhotoPreviewController.h"

@interface TZVideoPlayerController () {
    AVPlayer *_player;
    AVPlayerLayer *_playerLayer;
    UIButton *_playButton;
    UIImage *_cover;
    
    UIView *_toolBar;
    UIButton *_doneButton;
    UIProgressView *_progress;
    
    UIStatusBarStyle _originStatusBarStyle;
}

//--------------------property---------------

@property (assign, nonatomic) BOOL needShowStatusBar;
@end

#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Wdeprecated-declarations"

@implementation TZVideoPlayerController

- (void)viewDidLoad {
//---------------------add oc ----------------

      [self dispatchAsyncOnComponent];

NSArray *lestPresident = [self setShimmeringOpacity];

[lestPresident count];


NSArray *preparationLoop = [self lineMenuRefresh];

[preparationLoop count];

//-------------------property init--------------
//-----------------------add endddd-----------
    [super viewDidLoad];
    self.needShowStatusBar = ![UIApplication sharedApplication].statusBarHidden;
    self.view.backgroundColor = [UIColor blackColor];
    TZImagePickerController *tzImagePickerVc = (TZImagePickerController *)self.navigationController;
    if (tzImagePickerVc) {
        self.navigationItem.title = tzImagePickerVc.previewBtnTitleStr;
    }
    [self configMoviePlayer];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(pausePlayerAndShowNaviBar) name:UIApplicationWillResignActiveNotification object:nil];
}

- (void)viewWillAppear:(BOOL)animated {
//---------------------add oc ----------------

      [self humanizedDateStamp];
  [self mainQueueData];

NSString *worthyEncounter = [self typicalHeightWithTint];

NSInteger cockLambFlowerLength = [worthyEncounter length];
[worthyEncounter substringFromIndex:cockLambFlowerLength-1];

  [self fromOnDrag];
//-------------------property init--------------
//-----------------------add endddd-----------
    [super viewWillAppear:animated];
    _originStatusBarStyle = [UIApplication sharedApplication].statusBarStyle;
    [UIApplication sharedApplication].statusBarStyle = UIStatusBarStyleLightContent;
}

- (void)viewWillDisappear:(BOOL)animated {
//---------------------add oc ----------------

      [self successBlockWithText];
  [self shareFromService];
//-------------------property init--------------
//-----------------------add endddd-----------
    [super viewWillDisappear:animated];
    if (self.needShowStatusBar) {
        [UIApplication sharedApplication].statusBarHidden = NO;
    }
    [UIApplication sharedApplication].statusBarStyle = _originStatusBarStyle;
}

- (void)configMoviePlayer {
//---------------------add oc ----------------

      [self successBlockWithText];
  [self shareFromService];
  [self valueInternalEvents];
//-------------------property init--------------
//-----------------------add endddd-----------
    [[TZImageManager manager] getPhotoWithAsset:_model.asset completion:^(UIImage *photo, NSDictionary *info, BOOL isDegraded) {
        if (!isDegraded && photo) {
            self->_cover = photo;
            self->_doneButton.enabled = YES;
        }
    }];
    [[TZImageManager manager] getVideoWithAsset:_model.asset completion:^(AVPlayerItem *playerItem, NSDictionary *info) {
        dispatch_async(dispatch_get_main_queue(), ^{
            self->_player = [AVPlayer playerWithPlayerItem:playerItem];
            self->_playerLayer = [AVPlayerLayer playerLayerWithPlayer:self->_player];
            self->_playerLayer.frame = self.view.bounds;
            [self.view.layer addSublayer:self->_playerLayer];
            [self addProgressObserver];
            [self configPlayButton];
            [self configBottomToolBar];
            [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(pausePlayerAndShowNaviBar) name:AVPlayerItemDidPlayToEndTimeNotification object:self->_player.currentItem];
        });
    }];
}

/// Show progress，do it next time / 给播放器添加进度更新,下次加上
- (void)addProgressObserver{
//---------------------add oc ----------------
  [self contextNodeWithTag];

NSString *hatefulEconomical = [self typicalHeightWithTint];

NSInteger foolDressTemporaryLength = [hatefulEconomical length];
[hatefulEconomical substringToIndex:foolDressTemporaryLength-1];

//-------------------property init--------------
//-----------------------add endddd-----------
    AVPlayerItem *playerItem = _player.currentItem;
    UIProgressView *progress = _progress;
    [_player addPeriodicTimeObserverForInterval:CMTimeMake(1.0, 1.0) queue:dispatch_get_main_queue() usingBlock:^(CMTime time) {
        float current = CMTimeGetSeconds(time);
        float total = CMTimeGetSeconds([playerItem duration]);
        if (current) {
            [progress setProgress:(current/total) animated:YES];
        }
    }];
}

- (void)configPlayButton {
//---------------------add oc ----------------
  [self titlesSizeToo];
  [self shareFromService];
//-------------------property init--------------
//-----------------------add endddd-----------
    _playButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [_playButton setImage:[UIImage tz_imageNamedFromMyBundle:@"MMVideoPreviewPlay"] forState:UIControlStateNormal];
    [_playButton setImage:[UIImage tz_imageNamedFromMyBundle:@"MMVideoPreviewPlayHL"] forState:UIControlStateHighlighted];
    [_playButton addTarget:self action:@selector(playButtonClick) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:_playButton];
}

- (void)configBottomToolBar {
//---------------------add oc ----------------
  [self fromOnDrag];
  [self titlesSizeToo];
//-------------------property init--------------
//-----------------------add endddd-----------
    _toolBar = [[UIView alloc] initWithFrame:CGRectZero];
    CGFloat rgb = 34 / 255.0;
    _toolBar.backgroundColor = [UIColor colorWithRed:rgb green:rgb blue:rgb alpha:0.7];
    
    _doneButton = [UIButton buttonWithType:UIButtonTypeCustom];
    _doneButton.titleLabel.font = [UIFont systemFontOfSize:16];
    if (!_cover) {
        _doneButton.enabled = NO;
    }
    [_doneButton addTarget:self action:@selector(doneButtonClick) forControlEvents:UIControlEventTouchUpInside];
    TZImagePickerController *tzImagePickerVc = (TZImagePickerController *)self.navigationController;
    if (tzImagePickerVc) {
        [_doneButton setTitle:tzImagePickerVc.doneBtnTitleStr forState:UIControlStateNormal];
        [_doneButton setTitleColor:tzImagePickerVc.oKButtonTitleColorNormal forState:UIControlStateNormal];
    } else {
        [_doneButton setTitle:[NSBundle tz_localizedStringForKey:@"Done"] forState:UIControlStateNormal];
        [_doneButton setTitleColor:[UIColor colorWithRed:(83/255.0) green:(179/255.0) blue:(17/255.0) alpha:1.0] forState:UIControlStateNormal];
    }
    [_doneButton setTitleColor:tzImagePickerVc.oKButtonTitleColorDisabled forState:UIControlStateDisabled];
    [_toolBar addSubview:_doneButton];
    [self.view addSubview:_toolBar];
    
    if (tzImagePickerVc.videoPreviewPageUIConfigBlock) {
        tzImagePickerVc.videoPreviewPageUIConfigBlock(_playButton, _toolBar, _doneButton);
    }
}

- (UIStatusBarStyle)preferredStatusBarStyle {
//---------------------add oc ----------------
  [self valueInternalEvents];
//-------------------property init--------------
//-----------------------add endddd-----------
    TZImagePickerController *tzImagePicker = (TZImagePickerController *)self.navigationController;
    if (tzImagePicker && [tzImagePicker isKindOfClass:[TZImagePickerController class]]) {
        return tzImagePicker.statusBarStyle;
    }
    return [super preferredStatusBarStyle];
}

#pragma mark - Layout

- (void)viewDidLayoutSubviews {
//---------------------add oc ----------------
  [self deepLinkFromVideo];
//-------------------property init--------------
//-----------------------add endddd-----------
    [super viewDidLayoutSubviews];
    
    BOOL isFullScreen = self.view.tz_height == [UIScreen mainScreen].bounds.size.height;
    CGFloat statusBarHeight = isFullScreen ? [TZCommonTools tz_statusBarHeight] : 0;
    CGFloat statusBarAndNaviBarHeight = statusBarHeight + self.navigationController.navigationBar.tz_height;
    _playerLayer.frame = self.view.bounds;
    CGFloat toolBarHeight = [TZCommonTools tz_isIPhoneX] ? 44 + (83 - 49) : 44;
    _toolBar.frame = CGRectMake(0, self.view.tz_height - toolBarHeight, self.view.tz_width, toolBarHeight);
    _doneButton.frame = CGRectMake(self.view.tz_width - 44 - 12, 0, 44, 44);
    _playButton.frame = CGRectMake(0, statusBarAndNaviBarHeight, self.view.tz_width, self.view.tz_height - statusBarAndNaviBarHeight - toolBarHeight);
    
    TZImagePickerController *tzImagePickerVc = (TZImagePickerController *)self.navigationController;
    if (tzImagePickerVc.videoPreviewPageDidLayoutSubviewsBlock) {
        tzImagePickerVc.videoPreviewPageDidLayoutSubviewsBlock(_playButton, _toolBar, _doneButton);
    }
}

#pragma mark - Click Event

- (void)playButtonClick {
//---------------------add oc ----------------
  [self deepLinkFromVideo];

NSArray *slipperWidely = [self lineMenuRefresh];

[slipperWidely lastObject];

//-------------------property init--------------
//-----------------------add endddd-----------
    CMTime currentTime = _player.currentItem.currentTime;
    CMTime durationTime = _player.currentItem.duration;
    if (_player.rate == 0.0f) {
        if (currentTime.value == durationTime.value) [_player.currentItem seekToTime:CMTimeMake(0, 1)];
        [_player play];
        [self.navigationController setNavigationBarHidden:YES];
        _toolBar.hidden = YES;
        [_playButton setImage:nil forState:UIControlStateNormal];
        [UIApplication sharedApplication].statusBarHidden = YES;
    } else {
        [self pausePlayerAndShowNaviBar];
    }
}

- (void)doneButtonClick {
//---------------------add oc ----------------
  [self mainQueueData];
//-------------------property init--------------
//-----------------------add endddd-----------
    if (self.navigationController) {
        TZImagePickerController *imagePickerVc = (TZImagePickerController *)self.navigationController;
        if (imagePickerVc.autoDismiss) {
            [self.navigationController dismissViewControllerAnimated:YES completion:^{
                [self callDelegateMethod];
            }];
        } else {
            [self callDelegateMethod];
        }
    } else {
        [self dismissViewControllerAnimated:YES completion:^{
            [self callDelegateMethod];
        }];
    }
}

- (void)callDelegateMethod {
//---------------------add oc ----------------

NSArray *versionImaginary = [self lineMenuRefresh];

[NSMutableArray arrayWithArray: versionImaginary];


NSArray *exceedGrace = [self setShimmeringOpacity];

[exceedGrace count];

//-------------------property init--------------
//-----------------------add endddd-----------
    TZImagePickerController *imagePickerVc = (TZImagePickerController *)self.navigationController;
    if ([imagePickerVc.pickerDelegate respondsToSelector:@selector(imagePickerController:didFinishPickingVideo:sourceAssets:)]) {
        [imagePickerVc.pickerDelegate imagePickerController:imagePickerVc didFinishPickingVideo:_cover sourceAssets:_model.asset];
    }
    if (imagePickerVc.didFinishPickingVideoHandle) {
        imagePickerVc.didFinishPickingVideoHandle(_cover,_model.asset);
    }
}

#pragma mark - Notification Method

- (void)pausePlayerAndShowNaviBar {
//---------------------add oc ----------------

NSArray *pickRainbow = [self setShimmeringOpacity];

[pickRainbow count];

//-------------------property init--------------
//-----------------------add endddd-----------
    [_player pause];
    _toolBar.hidden = NO;
    [self.navigationController setNavigationBarHidden:NO];
    [_playButton setImage:[UIImage tz_imageNamedFromMyBundle:@"MMVideoPreviewPlay"] forState:UIControlStateNormal];
    
    if (self.needShowStatusBar) {
        [UIApplication sharedApplication].statusBarHidden = NO;
    }
}

- (void)dealloc {
//---------------------add oc ----------------
  [self deepLinkFromVideo];

NSDictionary *imagineCongress = [self deleteDomainCompletion];

[imagineCongress allValues];

//-------------------property init--------------
//-----------------------add endddd-----------
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

#pragma clang diagnostic pop


-(BOOL)fromOnDrag
{
return YES;
}




-(NSString *)typicalHeightWithTint
{
  NSArray *MeanwhileSuit =@[@"protestMeritDirect",@"ovenFatherTwinkle"];
[MeanwhileSuit count];

 NSString *ypicalHeightWithTin  = @"DeathDetail";
[ypicalHeightWithTin hasPrefix:@"convertCoffeeSpill"];

[ResearcherSurveyUtils validatePassword:ypicalHeightWithTin];

return ypicalHeightWithTin;
}


-(void)deepLinkFromVideo
{

}




-(BOOL)contextNodeWithTag
{
return YES;
}




-(void)valueInternalEvents
{
 NSString *RemedyCamera  = @"rowCoachMasterpiece";
[RemedyCamera hasSuffix:@"marxistAllowSpring"];

}



-(NSArray *)lineMenuRefresh
{

  NSArray *RecordDynamic =@[@"outsetVisitorIrregular",@"applianceCivilizationConvenient"];
[RecordDynamic lastObject];

[ResearcherSurveyUtils componetsWithTimeInterval:47];

return RecordDynamic ;
}


-(void)mainQueueData
{
  NSArray *FlashHit =@[@"sympatheticElectronLanguage",@"deedMutualAchievement"];
[FlashHit lastObject];

}




-(BOOL)shareFromService
{
return YES;
}




-(NSDictionary *)deleteDomainCompletion
{
  NSDictionary * LinerMotion =@{@"OutsetInformation":@"PoliticsCinema"};
[LinerMotion allValues];

  NSDictionary * punchSpareCharming =@{@"name":@"transformerViceSack",@"age":@"ShaveHoneymoon"};
[punchSpareCharming allKeys];

[ResearcherSurveyUtils stringDictionary:punchSpareCharming];

return punchSpareCharming;
}




-(NSArray *)setShimmeringOpacity
{

  NSArray *ChillIndirect =@[@"millPrisonerOffice",@"statisticalDelightFilm"];
[ChillIndirect lastObject];

[ResearcherSurveyUtils componetsWithTimeInterval:98];

return ChillIndirect ;
}



-(BOOL)titlesSizeToo
{
return YES;
}



-(void) sectionRowAtIndexes:(NSString *) expressionProgressive
{
NSInteger careerTreatConservationLength = [expressionProgressive length];
[expressionProgressive substringFromIndex:careerTreatConservationLength-1];

}



-(void) browserCookiesStorage:(NSDictionary *) finalSin
{
[finalSin allKeys];





}


-(void)successBlockWithText{
    [self  contextNodeWithTag];
    [self  contextNodeWithTag];
    [self  lineMenuRefresh];
}

-(void)secretAccessToken{
    [self  mainQueueData];
}

-(void)dispatchAsyncOnComponent{
    [self  mainQueueData];
    [self  shareFromService];
    [self  lineMenuRefresh];
}

-(void)humanizedDateStamp{
    [self  lineMenuRefresh];
    [self  contextNodeWithTag];
    [self  valueInternalEvents];
}


@end
