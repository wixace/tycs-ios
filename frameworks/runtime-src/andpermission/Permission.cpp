#include "Permission.h"

#if (CC_TARGET_PLATFORM == CC_PLATFORM_ANDROID)
#include "platform/android/jni/JniHelper.h"
#include <jni.h>

using namespace cocos2d;

#define PermissionClassName "com/higame/permission/CppAndpermission"

const std::string Permission::READ_CALENDAR = "android.permission.READ_CALENDAR";
const std::string Permission::WRITE_CALENDAR = "android.permission.WRITE_CALENDAR";

const std::string Permission::CAMERA = "android.permission.CAMERA";

const std::string Permission::READ_CONTACTS = "android.permission.READ_CONTACTS";
const std::string Permission::WRITE_CONTACTS = "android.permission.WRITE_CONTACTS";
const std::string Permission::GET_ACCOUNTS = "android.permission.GET_ACCOUNTS";

const std::string Permission::ACCESS_FINE_LOCATION = "android.permission.ACCESS_FINE_LOCATION";
const std::string Permission::ACCESS_COARSE_LOCATION = "android.permission.ACCESS_COARSE_LOCATION";

const std::string Permission::RECORD_AUDIO = "android.permission.RECORD_AUDIO";

const std::string Permission::READ_PHONE_STATE = "android.permission.READ_PHONE_STATE";
const std::string Permission::CALL_PHONE = "android.permission.CALL_PHONE";
const std::string Permission::READ_CALL_LOG = "android.permission.READ_CALL_LOG";
const std::string Permission::WRITE_CALL_LOG = "android.permission.WRITE_CALL_LOG";
const std::string Permission::ADD_VOICEMAIL = "com.android.voicemail.permission.ADD_VOICEMAIL";
const std::string Permission::USE_SIP = "android.permission.USE_SIP";
const std::string Permission::PROCESS_OUTGOING_CALLS = "android.permission.PROCESS_OUTGOING_CALLS";

const std::string Permission::BODY_SENSORS = "android.permission.BODY_SENSORS";

const std::string Permission::SEND_SMS = "android.permission.SEND_SMS";
const std::string Permission::RECEIVE_SMS = "android.permission.RECEIVE_SMS";
const std::string Permission::READ_SMS = "android.permission.READ_SMS";
const std::string Permission::RECEIVE_WAP_PUSH = "android.permission.RECEIVE_WAP_PUSH";
const std::string Permission::RECEIVE_MMS = "android.permission.RECEIVE_MMS";

const std::string Permission::READ_EXTERNAL_STORAGE = "android.permission.READ_EXTERNAL_STORAGE";
const std::string Permission::WRITE_EXTERNAL_STORAGE = "android.permission.WRITE_EXTERNAL_STORAGE";


const std::string Permission::GROUP_CALENDAR = std::string(Permission::READ_CALENDAR + ";" + Permission::WRITE_CALENDAR);
const std::string Permission::GROUP_CAMERA = std::string(Permission::CAMERA);
const std::string Permission::GROUP_CONTACTS = std::string(Permission::READ_CONTACTS + ";" + Permission::WRITE_CONTACTS + ";" + Permission::GET_ACCOUNTS);
const std::string Permission::GROUP_LOCATION = std::string(Permission::ACCESS_FINE_LOCATION + ";" + Permission::ACCESS_COARSE_LOCATION);
const std::string Permission::GROUP_MICROPHONE = std::string(Permission::RECORD_AUDIO);
const std::string Permission::GROUP_PHONE = std::string(Permission::READ_CALENDAR + ";" + Permission::WRITE_CALENDAR);
const std::string Permission::GROUP_SENSORS = std::string(Permission::READ_CALENDAR + ";" + Permission::WRITE_CALENDAR);
const std::string Permission::GROUP_SMS = std::string(Permission::READ_CALENDAR + ";" + Permission::WRITE_CALENDAR);
const std::string Permission::GROUP_STORAGE = std::string(Permission::READ_EXTERNAL_STORAGE + ";" + Permission::WRITE_EXTERNAL_STORAGE);

#endif





void Permission::requestPermission(std::string permission)
{
#if (CC_TARGET_PLATFORM == CC_PLATFORM_ANDROID)
	JniHelper::callStaticVoidMethod(PermissionClassName, "requestPermission", permission);
#else

#endif
}

bool Permission::hasPermission(std::string permission)
{
#if (CC_TARGET_PLATFORM == CC_PLATFORM_ANDROID)
	return JniHelper::callStaticBooleanMethod(PermissionClassName, "hasPermission", permission);
#else
	return true;
#endif
}
