#include "hwCore_Stream.h"

ChwCore_Stream::ChwCore_Stream(const unsigned char* pBuffer, unsigned long lLen, unsigned long lOffset)
{
	m_pBuffer = pBuffer;
	m_lLen = lLen;
	m_lOffset = lOffset;
	m_version = -1;
}

unsigned long ChwCore_Stream::Size()
{
	return m_lLen;
}

unsigned long ChwCore_Stream::Read(void* pBuffer, unsigned long lLen)
{
	unsigned long lBytes = min(m_lLen - m_lOffset, lLen);
	lBytes = max(lBytes, (unsigned long)0);
	memcpy(pBuffer, m_pBuffer + m_lOffset, lBytes);
	m_lOffset += lBytes;

	return lBytes;
}
void ChwCore_Stream::Seek(long lLen)
{
	m_lOffset += lLen;
}

unsigned int ChwCore_Stream::Version()
{
	if (m_version < 0) {
		//为了兼容之前的格式，把版本号添加到文件末位version=00001；
		int version = 0;
		int versionLen = 14;
		unsigned long curOffset = m_lOffset;
		m_lOffset = m_lLen - versionLen;
		string versionText;
		versionText.resize(versionLen);
		Read(&versionText[0], sizeof(char) * versionLen);
		std::string::size_type pos = versionText.find("version");
		if (pos == std::string::npos)
		{
			m_version = atoi(versionText.substr(8).c_str());
		}
		m_lOffset = curOffset;
	}

	return m_version;
}