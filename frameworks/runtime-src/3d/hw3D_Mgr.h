#pragma once

#include "cocos2d.h"
using namespace cocos2d;

#include "hw3D_C.h"
#include "hw3D_A.h"
#include "hw3D_Camera.h"
#include "hw3d_Material.h"

class Chw3D_Mgr
{
public:
	class CNode
	{
	public:
		float								m_expiryTime;
		unsigned int						m_dwDuplicate;
		void*								m_pData;
		int m_type;

	public:
		CNode();
		virtual								~CNode();
	};

public:
	//std::map<std::string, Chw3D_C*>	m_c3Nodes;
	//std::map<std::string, Chw3D_A*>	m_a3Nodes;
	map<string, CNode>		m_nodes;
	Chw3D_Camera*			m_defaultCamera;
	bool					m_isHardwareSkinMesh;
	bool					m_asyncLoadFile;

	std::map<unsigned int, std::string> m_defaulTypeC3File;
	std::map<unsigned int, std::string> m_defaulTypeA3File;

	std::string				m_defaultMaterialPath;

	std::map<std::string, std::map<int, Vec3*>>	m_cacheVerts;
	float					m_defCacheFileProcessTime;
	float					m_cacheFileExpiryTime;

	bool					m_isDebug;
	std::function<void(std::string)> m_missMaterialHandler;

	std::map<std::string, std::vector<std::function<void(Chw3D_A*)>>> m_a3Callbacks;
	std::map<std::string, std::vector<std::function<void(Chw3D_C*)>>> m_acCallbacks;

public:
	std::map<std::string, Chw3D_Material*> m_reloadMaterials;

public:
	static Chw3D_Mgr* m_instance;
	static Chw3D_Mgr* getInstance();
	static void destroyInstance();

	bool init();
	Chw3D_C* NewCEx(const char* pFilename);
	void DeleteC(Chw3D_C** c3);

	Chw3D_A* NewA(const char* pFilename);
	void DeleteA(Chw3D_A** a3);
	Chw3D_Camera* getDefaultCamera();


	Chw3D_C* queryC3File(const char* pFilename);
	void queryC3FileAsync(const char* pFilename, std::function<void(Chw3D_C*)> callback);

	Chw3D_A* queryA3File(const char* pFilename);
	void queryA3FileAsync(const char* pFilename, std::function<void(Chw3D_A*)> callback);

	void setDefaultTypeFile(unsigned int f3dType, std::string& c3FileName, std::string a3FileName);
	std::string& getDefaultTypeC3File(unsigned int f3dType);
	std::string& getDefaultTypeA3File(unsigned int f3dType);
	bool isHardwareSkinMesh();
	void setHardwareSkinMesh(bool isSkiningMesh);

	std::string getDefaultMaterialPath();
	void setDefaultMaterialPath(const std::string& materialPath);
	void setCacheFileExpiryTime(float time);
	void setCacheFileProcessTime(float time);
	void processCacheFile(float time);
	void removeAllCacheFiles();

	void setDebug(bool isDebug);
	bool isDebug();
	void registerMissMaterialHandler(std::function<void(std::string)> handler);
	void handleMissMaterial(std::string materialPath);
	void reloadMaterial(std::string materialPath);


private:
	Chw3D_Mgr();
	virtual ~Chw3D_Mgr();

};