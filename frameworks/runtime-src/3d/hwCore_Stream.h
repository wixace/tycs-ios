#pragma once

#include "cocos2d.h"
using namespace cocos2d;
#include <vector>
using namespace std;

class ChwCore_Stream
{
protected:
	vector<char>			m_Buffer;
	const unsigned char*	m_pBuffer;
	unsigned long			m_lLen;
	unsigned long			m_lOffset;
	int						m_version;

public:
	ChwCore_Stream(const unsigned char* pBuffer, unsigned long lLen, unsigned long lOffset);

	unsigned long		Read(void* pBuffer, unsigned long lLen);
	void				Seek(long lLen);
	unsigned long		Size();
	unsigned int		Version();
	
};
