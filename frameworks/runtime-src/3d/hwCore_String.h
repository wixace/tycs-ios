#pragma once

#include <vector>
#include <string>
using namespace std;

#include "cocos2d.h"
using namespace cocos2d;

class ChwCore_String : public Ref
{
public:
	vector<unsigned char>		m_Buffer;

public:
	CREATE_FUNC(ChwCore_String)
		public:
	static void					UTF82Unicode(const char* pString, wstring* pUnicode);
	static bool					Unicode2UTF8(void* pString, int nSize, string* pResult);

#ifdef WIN32
	static string				UTF8ToGBK(const std::string& strUTF8);
	static string				GBKToUTF8(const std::string& strGBK);
#endif

public:
								ChwCore_String();
	bool						init(void);

	const unsigned char*		Unicode2UTF8(void* pString, int nSize);
};
