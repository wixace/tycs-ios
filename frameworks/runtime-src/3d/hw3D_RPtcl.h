#pragma once

#include "cocos2d.h"
using namespace cocos2d;

#include "hw3D_Ptcl.h"
#include "hw3D_A.h"

class Chw3D_Sprite;
class Chw3D_RPtcl : public Node
{
public:
	//
	Chw3D_Ptcl*							m_pPtcl;

	unsigned int						m_dwCount;

	vector<Vec3>						m_poses;
	vector<unsigned int>				m_colors;
	vector<Vec2>					m_UVs;

	vector<unsigned short>				m_tris;

	unsigned int						m_dwTracks;

	//
	GLint								m_nUniformColor;
	GLint								m_nUniformUVTrans;

protected:
	void								Build(Chw3D_Sprite* pSprite);

public:
										Chw3D_RPtcl();
	virtual								~Chw3D_RPtcl();

	bool								Create(Chw3D_Ptcl* pPtcl);

	void								draw(Chw3D_Sprite* pSprite, bool bBuild);
};
