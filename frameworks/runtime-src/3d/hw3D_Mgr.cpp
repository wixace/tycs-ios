#include "hw3D_Mgr.h"
#include "hwCore_Stream.h"

Chw3D_Mgr::CNode::CNode():
	m_expiryTime(1000),
	m_dwDuplicate(0),
	m_type(0)
{

}
Chw3D_Mgr::CNode::~CNode()
{
}

Chw3D_Mgr* Chw3D_Mgr::m_instance = nullptr;
Chw3D_Mgr * Chw3D_Mgr::getInstance()
{
	if (!m_instance)
	{
		m_instance = new Chw3D_Mgr();
		m_instance->init();
	}
	return m_instance;
}

void Chw3D_Mgr::destroyInstance()
{
	delete m_instance;
}

Chw3D_Mgr::Chw3D_Mgr() :
	m_defaultCamera(nullptr),
	m_isHardwareSkinMesh(false),
	m_defCacheFileProcessTime(5.0),
	m_missMaterialHandler(nullptr),
	m_isDebug(false)
{

}
Chw3D_Mgr::~Chw3D_Mgr()
{
	if (m_defaultCamera)
	{
		m_defaultCamera->release();
	}

	removeAllCacheFiles();
}

bool Chw3D_Mgr::init()
{
	m_defaultCamera = Chw3D_Camera::create();
	m_defaultCamera->retain();

	auto director = cocos2d::Director::getInstance();
	auto  scheduler = director->getScheduler();

	return true;
}


Chw3D_C* Chw3D_Mgr::NewCEx(const char* pFilename)
{
	/*wpath Path = pFilename;
	wstring Filename = Path.string();
	to_lower(Filename);*/

	// 查找是否已经载入
	map<string, CNode>::iterator it = m_nodes.find(pFilename);
	if (it != m_nodes.end())
	{
		++it->second.m_dwDuplicate;
		return (Chw3D_C*)it->second.m_pData;
	}

	Chw3D_C* pC = new Chw3D_C(pFilename);
	if (pC->LoadFile(pFilename))
	{
		CNode* pNode = &m_nodes[pFilename];
		pNode->m_type = 1;
		pNode->m_pData = pC;
	}
	else
	{
		delete pC;
		pC = nullptr;
	}

	return pC;
}
void Chw3D_Mgr::DeleteC(Chw3D_C** pC)
{
	if (!*pC)
	{
		return;
	}

	map<string, CNode>::iterator it = m_nodes.find((*pC)->m_fileName);
	if (it != m_nodes.end())
	{
		if (it->second.m_dwDuplicate == 0)
		{
			m_nodes.erase((*pC)->m_fileName);
			delete *pC;
		}
		else
		{
			--it->second.m_dwDuplicate;
			*pC = 0;
		}
	}
}

void Chw3D_Mgr::setCacheFileExpiryTime(float time)
{
	m_cacheFileExpiryTime = time;
}

Chw3D_A* Chw3D_Mgr::NewA(const char* pFilename)
{
	/*wpath Path = pFilename;
	wstring Filename = Path.string();
	to_lower(Filename);*/

	// 查找是否已经载入
	map<string, CNode>::iterator it = m_nodes.find(pFilename);
	if(it != m_nodes.end())
	{
		++it->second.m_dwDuplicate;
		return (Chw3D_A*)it->second.m_pData;
	}

	Chw3D_A* pA = new Chw3D_A(pFilename);
	if(pA->LoadFile(pFilename))
	{
		CNode* pNode = &m_nodes[pFilename];
		pNode->m_type = 2;
		pNode->m_pData = pA;
	}
	else
	{
		delete pA;
		pA=nullptr;
	}

	return pA;
}
void Chw3D_Mgr::DeleteA(Chw3D_A** pA)
{
	if (!*pA)
	{
		return;
	}

	map<string, CNode>::iterator it = m_nodes.find((*pA)->m_fileName);
	if (it != m_nodes.end())
	{
		if (it->second.m_dwDuplicate == 0)
		{
			m_nodes.erase((*pA)->m_fileName);
			delete *pA;
		}
		else
		{
			--it->second.m_dwDuplicate;
			*pA = 0;
		}
	}
}


Chw3D_Camera * Chw3D_Mgr::getDefaultCamera()
{
	return m_defaultCamera;
}

bool Chw3D_Mgr::isHardwareSkinMesh()
{
	return m_isHardwareSkinMesh;
}

void Chw3D_Mgr::setHardwareSkinMesh(bool isSkiningMesh)
{
	m_isHardwareSkinMesh = isSkiningMesh;
}

std::string Chw3D_Mgr::getDefaultMaterialPath()
{
	return m_defaultMaterialPath;
}

void Chw3D_Mgr::setDefaultMaterialPath(const std::string& materialPath)
{
	m_defaultMaterialPath = materialPath;
}

void Chw3D_Mgr::setCacheFileProcessTime(float time)
{
	if (m_defCacheFileProcessTime != time)
	{
		m_defCacheFileProcessTime = time;
		auto director = cocos2d::Director::getInstance();
		auto  scheduler = director->getScheduler();
		scheduler->unschedule("3dMgrCacheFileScheduler", this);
		scheduler->schedule(std::bind(&Chw3D_Mgr::processCacheFile, this, std::placeholders::_1),
			this,
			time,
			true,
			"3dMgrCacheFileScheduler");
	}
}

void Chw3D_Mgr::processCacheFile(float time)
{
	float deltaTime = cocos2d::CCDirector::getInstance()->getDeltaTime();
	vector<string> delFiles;
	for (auto iter = m_nodes.begin(); iter != m_nodes.end(); iter++)
	{
		if (iter->second.m_expiryTime < deltaTime)
		{
			delFiles.push_back(iter->first);
		}
	}

	//for (unsigned int n = 0; n < delFiles.size(); ++n)
	//{
	//	std::string fileName = delFiles[n];
	//	auto iter = m_Nodes.find(fileName);
	//	if (iter != m_Nodes.end())
	//	{
	//		delete iter->second.m_pData;
	//		m_Nodes.erase(fileName);
	//	}
	//}
}

void Chw3D_Mgr::removeAllCacheFiles()
{
	for (auto iter = m_nodes.begin(); iter != m_nodes.end(); iter++)
	{
		delete iter->second.m_pData;
	}
	m_nodes.clear();
}

Chw3D_C* Chw3D_Mgr::queryC3File(const char* pFilename)
{

	return NewCEx(pFilename);
}

void Chw3D_Mgr::queryC3FileAsync(const char* pFilename, std::function<void(Chw3D_C*)> callback)
{
	Chw3D_C* pC3 = NewCEx(pFilename);
	if (!pC3->isLoadedData())
	{
		pC3->LoadFileAsync(pFilename, callback);
	}
	else
	{
		callback(pC3);
	}
}

Chw3D_A* Chw3D_Mgr::queryA3File(const char* pFilename)
{
	return NewA(pFilename);
}

void Chw3D_Mgr::queryA3FileAsync(const char* pFilename, std::function<void(Chw3D_A*)> callback)
{
	Chw3D_A* pA3 = NewA(pFilename);
	if (!pA3->isLoadedData())
	{
		pA3->LoadFileAsync(pFilename, callback);
	}
	else
	{
		callback(pA3);
	}
}

void Chw3D_Mgr::registerMissMaterialHandler(std::function<void(std::string)> handler)
{
	m_missMaterialHandler = handler;
}

void Chw3D_Mgr::handleMissMaterial(std::string materialPath)
{
	if (m_missMaterialHandler)
	{
		m_missMaterialHandler(materialPath);
	}
}

void Chw3D_Mgr::reloadMaterial(std::string materialPath)
{

	auto it = m_reloadMaterials.find(materialPath);
	if (it != m_reloadMaterials.end())
	{
		auto textureCache = Director::getInstance()->getTextureCache();
		auto texture = textureCache->addImage(materialPath.c_str());
		it->second->loadTexture(materialPath, texture);
	}
}

void Chw3D_Mgr::setDebug(bool isDebug)
{
	m_isDebug = isDebug;
}

bool Chw3D_Mgr::isDebug()
{
	return m_isDebug;
}

void Chw3D_Mgr::setDefaultTypeFile(unsigned int f3dType, std::string& c3FileName, std::string a3FileName)
{
	m_defaulTypeC3File[f3dType] = c3FileName;
	m_defaulTypeA3File[f3dType] = a3FileName;
}

std::string& Chw3D_Mgr::getDefaultTypeC3File(unsigned int f3dType)
{
	return m_defaulTypeC3File[f3dType];
}

std::string& Chw3D_Mgr::getDefaultTypeA3File(unsigned int f3dType)
{
	return m_defaulTypeA3File[f3dType];
}
