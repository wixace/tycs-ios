#include "hw3D_RPPtcl.h"
#include "hw3D_Camera.h"
#include "hw3D_Sprite.h"

void Chw3D_RPPtcl::Build(Chw3D_Sprite* pSprite)
{
	// 处理现有
	for(vector<Chw3D_RPPtcl_CNode>::iterator it = m_nodes.begin(); it != m_nodes.end();)
	{
		it->m_dAge += pSprite->m_dElapsed;
		if(it->m_dAge >= m_pPPtcl->m_dLife)
		{
			it = m_nodes.erase(it);
			continue;
		}

		Vec3 Temp = it->m_velocity * (float)(m_pPPtcl->m_fSpeed * pSprite->m_dElapsed * pSprite->m_fScale);
		//Vec3Scale(&Temp, &it->m_Velocity, (float)(m_pPPtcl->m_fSpeed * pSprite->m_dElapsed * pSprite->m_fScale));
		Vec3::add(it->m_pos, Temp, &it->m_pos);
		//Vec3Add(&it->m_Pos, &it->m_Pos, &Temp);
	
		it->m_fRadian += (float)(it->m_fRadianSpeed * pSprite->m_dElapsed);
		
		++it;
	}

	// 产生新的
	unsigned int dwCreate = 0;
	m_dTotalSec += pSprite->m_dElapsed;
	double dPer = 1.0 / m_pPPtcl->m_dwRate;
	if(m_dTotalSec >= dPer)
	{
		if(pSprite->m_dElapsed <= 1.0)
		{
			dwCreate = (int)(m_dTotalSec / dPer);
			m_dTotalSec -= (dPer * dwCreate);
		}
		else
		{
			dwCreate = 1;
			m_dTotalSec = 0;
		}

		// 不能超过最多
		dwCreate = min(dwCreate, (unsigned int)(m_dwMaxCount - m_nodes.size()));
	}

	Mat4 Matrix;
	pSprite->FinialMatrix(m_pPPtcl->m_dwNodeIndex, &Matrix);
	//Mat4Multiply(&Matrix, &pSprite->m_Matrix, &Matrix);

	for(unsigned int n = 0; n < dwCreate; ++n)
	{
		Chw3D_RPPtcl_CNode Node;

		Node.m_pos.x = 0; Node.m_pos.y = 0; Node.m_pos.z = 0;
		//Vec3Transform(&Node.m_Pos, &m_pPPtcl->m_Pos, &Matrix);
		Matrix.transformPoint(m_pPPtcl->m_pos, &Node.m_pos);

		Node.m_velocity.x = (rand() % (100 - (-100) + 1) + (-100)) / 100.0f;
		Node.m_velocity.y = (rand() % (100 - (-100) + 1) + (-100)) / 100.0f;
		Node.m_velocity.z = (rand() % (100 - (-100) + 1) + (-100)) / 100.0f;
		//Vec3Normalize(&Node.m_Velocity, &Node.m_Velocity);
		Node.m_velocity.normalize();

		Node.m_dAge = 0;
		Node.m_fRadian = rand() % (m_pPPtcl->m_nDegreeMax - m_pPPtcl->m_nDegreeMin + 1) + m_pPPtcl->m_nDegreeMin;
		Node.m_fRadian = Node.m_fRadian * 3.14159f / 180.0f;
		Node.m_fRadianSpeed = rand() % (m_pPPtcl->m_nRotateMax - m_pPPtcl->m_nRotateMin + 1) + m_pPPtcl->m_nRotateMin;
		Node.m_fRadianSpeed = Node.m_fRadianSpeed *3.14159f / 180.0f;

		m_nodes.push_back(Node);
	}

	// 开始构建顶点
	Chw3D_Camera* pCamera = pSprite->m_pCamera;
	Vec3 XDir, YDir, ZDir;
	pCamera->XDir(&XDir);
	pCamera->YDir(&YDir);
	pCamera->ZDir(&ZDir);

	for(unsigned int n = 0; n < m_nodes.size(); ++n)
	{
		Chw3D_RPPtcl_CNode* pNode = &m_nodes[n];

		// 算最终中心点
		Vec3 Center = pNode->m_pos;

		// 计算颜色
		Vec4 Color(1.0f, 1.0f, 1.0f, 1.0f);
		m_pPPtcl->m_material.m_timeColorTracks.Query(pNode->m_dAge, &Color);
		unsigned int dwColor = ((unsigned int)(255 * Color.w) << 24) | ((unsigned int)(255 * Color.z) << 16) | ((unsigned int)(255 * Color.y) << 8) | (unsigned int)(255 * Color.x);

		// 旋转
		float fRadian = 0;
		if(m_pPPtcl->m_bDirect)
		{
			// 换一种思路
			// 求摄像机视线和方向的垂直向量
			Vec3 Vec;
			//Vec3Cross(&Vec, &ZDir, &pNode->m_Velocity);
			Vec3::cross(ZDir, pNode->m_velocity, &Vec);
			//Vec3Normalize(&Vec, &Vec);
			Vec.normalize();
			// 求和摄像机上方向的夹角
			//fRadian = acos(Vec3Dot(&YDir, &Vec));
			fRadian = acos(Vec3::dot(YDir, Vec));
			fRadian = fRadian - 1.57f;
			// 求是不是大于180度
			//if(Vec3Dot(&Vec, &XDir) > 0.0f)
			if (Vec3::dot(Vec, XDir) > 0.0f)
			{
				fRadian = 3.14f - fRadian;
			}
		}
		else
		{
			// 旋转粒子
			fRadian = pNode->m_fRadian;
		}

		Mat4 RotateMatrix;
		//Mat4RotationAxisAngle(&RotateMatrix, &ZDir, fRadian);
		Mat4::createRotation(ZDir, fRadian, &RotateMatrix);

		Vec3 X, Y;
		//Vec3TransformNormal(&X, &XDir, &RotateMatrix);
		RotateMatrix.transformVector(XDir, &X);

		//Vec3TransformNormal(&Y, &YDir, &RotateMatrix);
		RotateMatrix.transformVector(YDir, &Y);
		// 计算uv
		unsigned int dwImageCount = m_pPPtcl->m_dwImageCol * m_pPPtcl->m_dwImageRow;

		unsigned int dwImageIndex = (unsigned int)(pNode->m_dAge / (1.0f / dwImageCount));
		dwImageIndex = std::min(dwImageIndex, (dwImageCount - 1));

		unsigned int dwCol = dwImageIndex % m_pPPtcl->m_dwImageCol;
		unsigned int dwRow = dwImageIndex / m_pPPtcl->m_dwImageCol;

		float fUStart = (float)dwCol / m_pPPtcl->m_dwImageCol;
		float fUEnd = (float)(dwCol + 1) / m_pPPtcl->m_dwImageCol;
		float fVStart = (float)dwRow / m_pPPtcl->m_dwImageRow;
		float fVEnd = (float)(dwRow + 1) / m_pPPtcl->m_dwImageRow;	

		// 计算位移
		Vec3 XOffset, YOffset;
		float fSize = (float)(m_pPPtcl->m_fSizeStart + (m_pPPtcl->m_fSizeEnd - m_pPPtcl->m_fSizeStart) * (pNode->m_dAge / m_pPPtcl->m_dLife));
		fSize = fSize * pSprite->m_fScale;
		XOffset = X * fSize;
		//Vec3Scale(&XOffset, &X, fSize);
		YOffset = Y * fSize;
		//Vec3Scale(&YOffset, &Y, fSize);

		// 生成四个顶点
		Vec3 Temp;
		unsigned int dwIndex = n * 4;

		// 0
		Vec3::subtract(Center, YOffset, &Temp);
		//Vec3Subtract(&Temp, &Center, &YOffset);

		Vec3::subtract(Temp, XOffset, &m_poses[dwIndex]);
		//Vec3Subtract(&m_Poses[dwIndex], &Temp, &XOffset);
		m_uvs[dwIndex].x = fUStart;
		m_uvs[dwIndex].y = fVEnd;
		m_colors[dwIndex] = dwColor;

		// 1
		//Vec3Add(&m_Poses[dwIndex + 1], &Temp, &XOffset);
		Vec3::add(Temp, XOffset, &m_poses[dwIndex + 1]);
		m_uvs[dwIndex + 1].x = fUEnd;
		m_uvs[dwIndex + 1].y = fVEnd;
		m_colors[dwIndex + 1] = dwColor;

		// 2
		//Vec3Add(&Temp, &Center, &YOffset);
		Vec3::add(Center, YOffset, &Temp);
		//Vec3Add(&m_Poses[dwIndex + 2], &Temp, &XOffset);
		Vec3::add(Temp, XOffset, &m_poses[dwIndex + 2]);
		m_uvs[dwIndex + 2].x = fUEnd;
		m_uvs[dwIndex + 2].y = fVStart;
		m_colors[dwIndex + 2] = dwColor;

		// 3
		Vec3::subtract(Temp, XOffset, &m_poses[dwIndex + 3]);
		//Vec3Subtract(&m_Poses[dwIndex + 3], &Temp, &XOffset);
		m_uvs[dwIndex + 3].x = fUStart;
		m_uvs[dwIndex + 3].y = fVStart;
		m_colors[dwIndex + 3] = dwColor;
	}
}

Chw3D_RPPtcl::Chw3D_RPPtcl()
{
	m_dTotalSec = 0;

	// shader program
	setGLProgram(GLProgramCache::getInstance()->getGLProgram(GLProgram::SHADER_3D_Higame3D));
	m_nUniformColor = glGetUniformLocation(getGLProgram()->getProgram(), "u_color");
	m_nUniformUVTrans = glGetUniformLocation(getGLProgram()->getProgram(), "u_uvtrans");
}
Chw3D_RPPtcl::~Chw3D_RPPtcl()
{
}

bool Chw3D_RPPtcl::Create(Chw3D_PPtcl* pPPtcl)
{
	m_pPPtcl = pPPtcl;

	m_dwMaxCount = (unsigned int)(m_pPPtcl->m_dwRate * (m_pPPtcl->m_dLife + 0.2));
	m_dwMaxCount = max(1, (int)m_dwMaxCount);

	m_poses.resize(m_dwMaxCount * 4);
	m_colors.resize(m_dwMaxCount * 4);
	m_uvs.resize(m_dwMaxCount * 4);

	m_tris.resize(m_dwMaxCount * 6);
	for(unsigned int n = 0; n < m_dwMaxCount; ++n)
	{
		m_tris[n * 6 + 0] = (unsigned short)(n * 4 + 0);
		m_tris[n * 6 + 1] = (unsigned short)(n * 4 + 1);
		m_tris[n * 6 + 2] = (unsigned short)(n * 4 + 2);
		m_tris[n * 6 + 3] = (unsigned short)(n * 4 + 0);
		m_tris[n * 6 + 4] = (unsigned short)(n * 4 + 2);
		m_tris[n * 6 + 5] = (unsigned short)(n * 4 + 3);
	}
	return true;
}

void Chw3D_RPPtcl::draw(Chw3D_Sprite* pSprite, bool bBuild)
{
	CC_PROFILER_START_CATEGORY(kCCProfilerCategorySprite, "Chw3D_RPtcl - draw");

	// 摄像机
	pSprite->m_pCamera->Active();

	// 构建
	if(bBuild)
	{
		Build(pSprite);
	}

	if(m_nodes.size() == 0)
	{
		// 摄像机
		pSprite->m_pCamera->Inactive();
		return;
	}

	// 宏CC_NODE_DRAW_SETUP之中调用getGLProgram()->setUniformsForBuiltins(_modelViewTransform)， 
	// 但由于RMesh的MV矩阵并未保存在_modelViewTransform, 会导致模型视图变换无效，因此不能使用该宏，
	// 而需要使用getGLProgram()->setUniformsForBuiltins(); 
	// CC_NODE_DRAW_SETUP();
	
	do { 
		CCASSERT(getGLProgram(), "No shader program set for this node"); 
		{ 
			getGLProgram()->use();
			getGLProgram()->setUniformsForBuiltins();
		}
	} while(0);

    GL::enableVertexAttribs(GL::VERTEX_ATTRIB_FLAG_POS_COLOR_TEX);
	
	// vertex
	glVertexAttribPointer( GLProgram::VERTEX_ATTRIB_POSITION, 3, GL_FLOAT, GL_FALSE, 0, (void*)&m_poses[0]);
	// texCoods
	glVertexAttribPointer(GLProgram::VERTEX_ATTRIB_TEX_COORD, 2, GL_FLOAT, GL_FALSE, 0, (void*)&m_uvs[0]);
    // color
	glVertexAttribPointer(GLProgram::VERTEX_ATTRIB_COLOR, 4, GL_UNSIGNED_BYTE, GL_TRUE, 0, (void*)&m_colors[0]);

	// texture
	GL::bindTexture2D(m_pPPtcl->m_material.m_pTexture->getName());

	/*
	根据material算uv
	*/
	// UV
	Vec2 UVTrans(0, 0);
	//m_pPPtcl->m_Material.m_UVStepTracks.Query((float)pSprite->m_nFrame, &UVTrans);
	getGLProgram()->setUniformLocationWith2fv(m_nUniformUVTrans, (GLfloat*)&UVTrans, 1);

	/*
	根据material算颜色
	*/
	Vec4 Color(1.0f, 1.0f, 1.0f, 1.0f);
	m_pPPtcl->m_material.m_colorTracks.Query((float)pSprite->m_nFrame, &Color);
	
	// 乘上环境里取出来的值
	/*Color.r = Color.r * CFBGraphics::getSingleton().m_Envionment.m_Color.r;
	Color.g = Color.g * CFBGraphics::getSingleton().m_Envionment.m_Color.g;
	Color.b = Color.b * CFBGraphics::getSingleton().m_Envionment.m_Color.b;
	Color.a = Color.a * CFBGraphics::getSingleton().m_Envionment.m_Color.a;
	m_pFx->m_pEffect->SetFloatArray("g_Color", &Color.r, 4);*/

	getGLProgram()->setUniformLocationWith4fv(m_nUniformColor, (GLfloat*)&Color, 1);

	//
	unsigned int mode = m_pPPtcl->m_dwMode;
	if(Color.w < 1.0f && mode == 0)
	{
		mode = 2;
	}

	// mode
	switch(mode)
	{
	case 0:
		// normal
		glEnable(GL_DEPTH_TEST);
		glDepthMask(GL_TRUE);

		glDisable(GL_BLEND);
		break;
	case 1:
		// colorkey
		glEnable(GL_DEPTH_TEST);
		glDepthMask(GL_TRUE);

		glEnable(GL_BLEND);
		GL::blendFunc(GL_ONE, GL_ONE_MINUS_SRC_ALPHA);
		break;
	case 2:
		// alpha
		glEnable(GL_DEPTH_TEST);
		glDepthMask(GL_FALSE);

		glEnable(GL_BLEND);
		GL::blendFunc(GL_ONE, GL_ONE_MINUS_SRC_ALPHA);
		break;
	case 3:
		// add
		glEnable(GL_DEPTH_TEST);
		glDepthMask(GL_FALSE);

		glEnable(GL_BLEND);
		GL::blendFunc(GL_ONE, GL_ONE);
		break;
	case 4:
		// srcalpha
		glEnable(GL_DEPTH_TEST);
		glDepthMask(GL_FALSE);

		glEnable(GL_BLEND);
		//GL::blendFunc(GL_ONE, GL_ONE_MINUS_SRC_ALPHA);
		glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
		break;
	}

	glDrawElements(GL_TRIANGLES, m_nodes.size() * 6, GL_UNSIGNED_SHORT, (void*)&m_tris[0]);

	CHECK_GL_ERROR_DEBUG();

	// 摄像机
	pSprite->m_pCamera->Inactive();

	// mode
	glDisable(GL_DEPTH_TEST);
	glDepthMask(GL_TRUE);

	glEnable(GL_BLEND);
	GL::blendFunc(GL_ONE, GL_ONE_MINUS_SRC_ALPHA);

	//
    CC_INCREMENT_GL_DRAWS(1);

    CC_PROFILER_STOP_CATEGORY(kCCProfilerCategorySprite, "Chw3D_RPtcl - draw");
}
