#pragma once

#include "cocos2d.h"
using namespace cocos2d;

#include "hwCore_Stream.h"
#include "hw3D_Material.h"

class Chw3D_Ptcl_Frame
{
public:
	unsigned int					m_dwCount;

	vector<Vec3>					m_poses;
	vector<Vec3>					m_velocitys;
	vector<float>					m_ages;
	vector<float>					m_sizes;
	vector<float>					m_radians;

public:
									Chw3D_Ptcl_Frame();
	virtual							~Chw3D_Ptcl_Frame();
};

class Chw3D_Ptcl
{
public:
	string							m_fileName;				// 名字
	unsigned int					m_dwMode;			// 模式
	unsigned int					m_dwNodeIndex;		// 这个Sub使用骨骼索引，同父模型

	unsigned int					m_dwCountMax;
	vector<Chw3D_Ptcl_Frame*>		m_frames;

	float							m_fRotate;			// 1秒旋转多少度（bug这个变量没用了，但是之前导出很多了，先放在这）
	bool							m_bDirect;			// 指向性
	bool							m_bLoop;			// 循环

	unsigned int					m_dwImageCol;		// 一行有多少个 （x 的个数）
	unsigned int					m_dwImageRow;		// 一共有多少行 （y 的个数）

	/*
	下面成员需要手动加载
	*/
	Chw3D_Material					m_material;			// 材质

public:
									Chw3D_Ptcl();
	virtual							~Chw3D_Ptcl();

	void							LoadEx(ChwCore_Stream* pStream, bool isAsync=false);
	Chw3D_Material*					getMaterial();

};
