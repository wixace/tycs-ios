#include "hw3D_RLine.h"
#include "hw3D_Camera.h"
#include "hw3D_Sprite.h"

Chw3D_RLine::Chw3D_RLine()
{
	// shader program
	setGLProgram(GLProgramCache::getInstance()->getGLProgram(GLProgram::SHADER_3D_Higame3D));
	m_nUniformColor = glGetUniformLocation(getGLProgram()->getProgram(), "u_color");
	m_nUniformUVTrans = glGetUniformLocation(getGLProgram()->getProgram(), "u_uvtrans");
}
Chw3D_RLine::~Chw3D_RLine()
{
	for(auto it = m_nodes.begin(); it != m_nodes.end(); ++it)
	{
		delete *it;
	}
	m_nodes.clear();
}

bool Chw3D_RLine::Create(Chw3D_Line* pLine)
{
	m_pLine = pLine;

	m_dwMaxCount = max(2, (int)(m_pLine->m_dMaxLife * 30));

	m_poses.resize(m_dwMaxCount * 2);
	m_colors.resize(m_dwMaxCount * 2);
	m_uvs.resize(m_dwMaxCount * 2);

	m_tris.resize(m_dwMaxCount * 2);
	for(unsigned int n = 0; n < m_dwMaxCount * 2; ++n)
	{
		m_tris[n] = (unsigned short)n;
	}
	return true;
}

void Chw3D_RLine::CalcUV(Chw3D_Sprite* pSprite)
{
	unsigned int dwTile = 0;
	m_pLine->m_material.m_tileTracks.Query(pSprite->m_nFrame, &dwTile);

	unsigned int dwCol = dwTile % m_pLine->m_material.m_dwTileY;
	unsigned int dwRow = dwTile / m_pLine->m_material.m_dwTileX;

	float fUStart = (float)dwCol / m_pLine->m_material.m_dwTileX;
	float fUEnd = (float)(dwCol + 1) / m_pLine->m_material.m_dwTileX;
	float fVStart = (float)dwRow / m_pLine->m_material.m_dwTileY;
	float fVEnd = (float)(dwRow + 1) / m_pLine->m_material.m_dwTileY;

	// 计算每个结点间的距离和总距离
	float fTotalDist = 0;
	vector<float> Dists;
	auto it = m_nodes.begin();
	while(it != m_nodes.end())
	{
		auto itNext = it;
		if(++itNext == m_nodes.end())
		{
			break;
		}

		Vec3 Center0;
		//Vec3Add(&Center0, &(*it)->m_Pos[0], &(*it)->m_Pos[1]);
		Vec3::add((*it)->m_pos[0], (*it)->m_pos[1], &Center0);
		//Vec3Scale(&Center0, &Center0, 0.5f);
		Center0.scale(0.5f);

		Vec3 Center1;
		//Vec3Add(&Center1, &(*itNext)->m_Pos[0], &(*itNext)->m_Pos[1]);
		Vec3::add((*itNext)->m_pos[0], (*itNext)->m_pos[1], &Center1);
		//Vec3Scale(&Center1, &Center1, 0.5f);
		Center1.scale(0.5f);

		Vec3 Dir;
		//Vec3Subtract(&Dir, &Center1, &Center0);
		Vec3::subtract(Center1, Center0, &Dir);
		//float fLength = Vec3Length(&Dir);
		float fLength = Dir.length();

		Dists.push_back(fLength);
		fTotalDist += fLength;

		it = itNext;
	}

	// 防止除0
	fTotalDist = max(fTotalDist, 1.0f);

	// 计算UV
	float fCurU = fUEnd;
	unsigned int dwCounter = 0;
	it = m_nodes.begin();
	while(it != m_nodes.end())
	{
		if(dwCounter > 0)
		{
			// 从第二个开始就算间隔占的比率
			fCurU -= (Dists[dwCounter - 1] / fTotalDist) * (fUEnd - fUStart);
		}

		//Vec2Fill(&(*it)->m_UV[0], fCurU, fVEnd);
		(*it)->m_uv[0].set(fCurU, fVEnd);
		//Vec2Fill(&(*it)->m_UV[1], fCurU, fVStart);
		(*it)->m_uv[1].set(fCurU, fVStart);
		++dwCounter;
		++it;
	}
}
void Chw3D_RLine::Build(Chw3D_Sprite* pSprite)
{
	// 计算生命周期，过了的删掉
	auto it = m_nodes.begin();
	while(it != m_nodes.end())
	{
		(*it)->m_dLife += pSprite->m_dElapsed;
		if((*it)->m_dLife > m_pLine->m_dMaxLife)
		{
			delete *it;
			it = m_nodes.erase(it);
		}
		else
		{
			++it;
		}
	}

	//
	Mat4 Matrix;
	pSprite->FinialMatrix(m_pLine->m_dwNodeIndex, &Matrix);
		
	// 开始处理
	if(m_pLine->m_dMaxLife != 0)
	{
		// 拖尾线体
		// 生成一个新的结点
		if(m_nodes.size() < m_dwMaxCount)
		{
			//
			Chw3D_RLine_Node* pNode = new Chw3D_RLine_Node;
	
			//Vec3Transform(&pNode->m_Pos[0], &m_pLine->m_Knots[0], &Matrix);
			Matrix.transformPoint(m_pLine->m_knots[0], &pNode->m_pos[0]);
			//Vec3Transform(&pNode->m_Pos[1], &m_pLine->m_Knots[1], &Matrix);
			Matrix.transformPoint(m_pLine->m_knots[1], &pNode->m_pos[1]);
			pNode->m_dLife = 0;

			//
			m_nodes.push_back(pNode);
		}
		
		// 至少需要有2段才能继续
		if(m_nodes.size() < 2)
		{
			return;
		}
	}
	else
	{
		// 始终只有一面面向屏幕的线体
		//
		Vec3 Vec[2];
		//Vec3Transform(&Vec[0], &m_pLine->m_Knots[0], &Matrix);
		Matrix.transformPoint(m_pLine->m_knots[0], &Vec[0]);
		//Vec3Transform(&Vec[1], &m_pLine->m_Knots[1], &Matrix);
		Matrix.transformPoint(m_pLine->m_knots[1], &Vec[1]);

		Vec3 Normal;
		//kmVec3Subtract(&Normal, &Vec[0], &Vec[1]);
		Vec3::subtract(Vec[0], Vec[1], &Normal);
		//kmVec3Normalize(&Normal, &Normal);
		Normal = Normal.getNormalized();
		

		Vec3 ZDir;
		pSprite->m_pCamera->ZDir(&ZDir);
		//kmVec3Cross(&Normal, &ZDir, &Normal);
		Vec3::cross(ZDir, Normal, &Normal);
		//Normal.cross(ZDir);
		//kmVec3Normalize(&Normal, &Normal);
		//Normal.normalize();
		Normal = Normal.getNormalized();

		//Vec3Scale(&Normal, &Normal, m_pLine->m_fWidth / 2.0f);
		Normal.scale(m_pLine->m_fWidth / 2.0f);
		//
		Chw3D_RLine_Node* pNode0 = new Chw3D_RLine_Node;

		//Vec3Subtract(&pNode0->m_Pos[0], &Vec[0], &Normal);
		Vec3::subtract(Vec[0], Normal, &pNode0->m_pos[0]);
		//Vec3Subtract(&pNode0->m_Pos[1], &Vec[1], &Normal);
		Vec3::subtract(Vec[1], Normal, &pNode0->m_pos[1]);

		pNode0->m_dLife = 0;

		m_nodes.push_back(pNode0);

		//
		Chw3D_RLine_Node* pNode1 = new Chw3D_RLine_Node;

		//Vec3Add(&pNode1->m_Pos[0], &Vec[0], &Normal);
		Vec3::add(Vec[0], Normal, &pNode1->m_pos[0]);
		//Vec3Add(&pNode1->m_Pos[1], &Vec[1], &Normal);
		Vec3::add(Vec[1], Normal, &pNode1->m_pos[1]);
		pNode1->m_dLife = 0;

		//
		m_nodes.push_back(pNode1);
	}

	// 计算贴图坐标
	CalcUV(pSprite);

	// 填充顶点数据
	unsigned int dwIndex = 0;
	it = m_nodes.begin();
	while(it != m_nodes.end())
	{
		if (dwIndex >= m_dwMaxCount)
		{
			break;
		}
		
		// 生成2个顶点
		// 0
		m_poses[dwIndex * 2 + 0] = (*it)->m_pos[1];
		m_colors[dwIndex * 2 + 0] = 0xFFFFFFFF;
		m_uvs[dwIndex * 2 + 0] = (*it)->m_uv[1];

		// 1
		m_poses[dwIndex * 2 + 1] = (*it)->m_pos[0];
		m_colors[dwIndex * 2 + 1] = 0xFFFFFFFF;
		m_uvs[dwIndex * 2 + 1] = (*it)->m_uv[0];

		++dwIndex;
		++it;
	}
}

void Chw3D_RLine::draw(Chw3D_Sprite* pSprite, bool bBuild)
{
	CC_PROFILER_START_CATEGORY(kCCProfilerCategorySprite, "Chw3D_RLine - draw");

	// 摄像机
	pSprite->m_pCamera->Active();

	// 构建
	if(bBuild)
	{
		Build(pSprite);
	}

	if(m_nodes.size() < 2)
	{
		// 摄像机
		pSprite->m_pCamera->Inactive();
		return;
	}

	// 宏CC_NODE_DRAW_SETUP之中调用getGLProgram()->setUniformsForBuiltins(_modelViewTransform)， 
	// 但由于RMesh的MV矩阵并未保存在_modelViewTransform, 会导致模型视图变换无效，因此不能使用该宏，
	// 而需要使用getGLProgram()->setUniformsForBuiltins(); 
	// CC_NODE_DRAW_SETUP();
		
	do { 
		CCASSERT(getGLProgram(), "No shader program set for this node"); 
		{ 
			getGLProgram()->use();
			getGLProgram()->setUniformsForBuiltins();
		}
	} while(0);

    GL::enableVertexAttribs(GL::VERTEX_ATTRIB_FLAG_POS_COLOR_TEX);
	
	// vertex
	glVertexAttribPointer( GLProgram::VERTEX_ATTRIB_POSITION, 3, GL_FLOAT, GL_FALSE, 0, (void*)&m_poses[0]);
	// texCoods
	glVertexAttribPointer(GLProgram::VERTEX_ATTRIB_TEX_COORD, 2, GL_FLOAT, GL_FALSE, 0, (void*)&m_uvs[0]);
    // color
	glVertexAttribPointer(GLProgram::VERTEX_ATTRIB_COLOR, 4, GL_UNSIGNED_BYTE, GL_TRUE, 0, (void*)&m_colors[0]);

	// texture
	GL::bindTexture2D(m_pLine->m_material.m_pTexture->getName());

	/*
	根据material算uv
	*/
	// UV
	Vec2 UVTrans(0,0);
	//m_pLine->m_Material.m_UVStepTracks.Query((float)pSprite->m_nFrame, &UVTrans);
	getGLProgram()->setUniformLocationWith2fv(m_nUniformUVTrans, (GLfloat*)&UVTrans, 1);

	/*
	根据material算颜色
	*/
	Vec4 Color(1.0f, 1.0f, 1.0f, 1.0f);
	m_pLine->m_material.m_colorTracks.Query((float)pSprite->m_nFrame, &Color);
	
	// 乘上环境里取出来的值
	/*Color.r = Color.r * CFBGraphics::getSingleton().m_Envionment.m_Color.r;
	Color.g = Color.g * CFBGraphics::getSingleton().m_Envionment.m_Color.g;
	Color.b = Color.b * CFBGraphics::getSingleton().m_Envionment.m_Color.b;
	Color.a = Color.a * CFBGraphics::getSingleton().m_Envionment.m_Color.a;
	m_pFx->m_pEffect->SetFloatArray("g_Color", &Color.r, 4);*/

	getGLProgram()->setUniformLocationWith4fv(m_nUniformColor, (GLfloat*)&Color, 1);

	//
	unsigned int mode = m_pLine->m_dwMode;
	if(Color.w < 1.0f && mode == 0)
	{
		mode = 2;
	}

	// mode
	switch(mode)
	{
	case 0:
		// normal
		glEnable(GL_DEPTH_TEST);
		glDepthMask(GL_TRUE);

		glDisable(GL_BLEND);
		break;
	case 1:
		// colorkey
		glEnable(GL_DEPTH_TEST);
		glDepthMask(GL_TRUE);

		glEnable(GL_BLEND);
		GL::blendFunc(GL_ONE, GL_ONE_MINUS_SRC_ALPHA);
		break;
	case 2:
		// alpha
		glEnable(GL_DEPTH_TEST);
		glDepthMask(GL_FALSE);

		glEnable(GL_BLEND);
		GL::blendFunc(GL_ONE, GL_ONE_MINUS_SRC_ALPHA);
		break;
	case 3:
		// add
		glEnable(GL_DEPTH_TEST);
		glDepthMask(GL_FALSE);

		glEnable(GL_BLEND);
		GL::blendFunc(GL_ONE, GL_ONE);
		break;
	case 4:
		// srcalpha
		glEnable(GL_DEPTH_TEST);
		glDepthMask(GL_FALSE);

		glEnable(GL_BLEND);
		//GL::blendFunc(GL_ONE, GL_ONE_MINUS_SRC_ALPHA);
		glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
		break;
	}

	if (m_nodes.size() <= m_dwMaxCount) {
		glDrawElements(GL_TRIANGLE_STRIP, m_nodes.size() * 2, GL_UNSIGNED_SHORT, (void*)&m_tris[0]);
	}
	else {
		CCLOGERROR("RLine Err:%s", m_pLine->m_fileName.c_str());
	}


	CHECK_GL_ERROR_DEBUG();

	// 摄像机
	pSprite->m_pCamera->Inactive();

	// mode
	glDisable(GL_DEPTH_TEST);
	glDepthMask(GL_TRUE);

	glEnable(GL_BLEND);
	GL::blendFunc(GL_ONE, GL_ONE_MINUS_SRC_ALPHA);

	//
    CC_INCREMENT_GL_DRAWS(1);

    CC_PROFILER_STOP_CATEGORY(kCCProfilerCategorySprite, "Chw3D_RLine - draw");
}