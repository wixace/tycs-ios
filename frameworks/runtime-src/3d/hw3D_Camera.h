#pragma once

#include "cocos2d.h"
using namespace cocos2d;

#include "hwCore_Stream.h"

class Chw3D_Camera : public Node
{
public:
	Vec3	m_from;
	Vec3	m_to;
	Vec3	m_up;
	bool	m_viewProjectionDirty;
	Mat4	m_view;
	Mat4	m_projection;
	Mat4	m_viewProjection;
	bool	m_isEnabelGuiDepth;
	Ray*	m_ray;

	float	m_depthRatio;

	float	m_width;
	float	m_height;
	float   m_aspectRatio;
public:
	static Chw3D_Camera*				create(void);

public:
	Chw3D_Camera();
	~Chw3D_Camera();

	bool	Load(const char* pFilename);

	void	Set(float fFromX, float fFromY, float fFromZ, float fToX, float fToY, float fToZ);
	void	Perspective(float fieldOfView, float aspectRatio, float nearPlane, float farPlane);
	void	Perspective(float fFOV, float fWidth, float fHeight, float fNear, float fFar);
	void	Ortho(float fWidth, float fHeight, float fNear, float fFar);

	void	XDir(Vec3* pDir);
	void	YDir(Vec3* pDir); 
	void	ZDir(Vec3* pDir);

	void	Active(void);
	void	Inactive(void);

	Mat4&	getViewProjectionMatrix();

	Vec2	project(const Vec3& src);
	Vec2	projectGL(const Vec3& src);
	Vec3	unproject(const Vec3& src);
	Vec3	unprojectGL(const Vec3& src);

	void	unproject(const Size& size, const Vec3* src, Vec3* dst);
	void	unprojectGL(const Size& size, const Vec3* src, Vec3* dst);
	Vec3	screenPointToWorldPoint(Vec2 screenPoint);

	void	setEnabelGuiDepth(bool isEnabel);
	bool	isEnabelGuiDepth();

	void	setDepthRatio(float depthRatio);
	float	getDepthRatio();
};

