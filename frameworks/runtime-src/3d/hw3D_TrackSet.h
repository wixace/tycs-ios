#pragma once

#include "cocos2d.h"
using namespace cocos2d;

#include "hwCore_Stream.h"

class Chw3D_TrackSet
{
public:
	unsigned int				m_dwKeys;
	Mat4*						m_matrixs;

public:
								Chw3D_TrackSet();
	virtual						~Chw3D_TrackSet();

	virtual void				Load(ChwCore_Stream* pStream);
};

class Chw3D_BoneTrackSet : public Chw3D_TrackSet
{
};
class Chw3D_MeshTrackSet : public Chw3D_TrackSet
{
public:
	virtual void				Load(ChwCore_Stream* pStream);
};

class Chw3D_PtclTrackSet : public Chw3D_TrackSet
{
public:
	virtual void				Load(ChwCore_Stream* pStream);
};

class Chw3D_PPtclTrackSet : public Chw3D_TrackSet
{
};

class Chw3D_LineTrackSet : public Chw3D_TrackSet
{
};

