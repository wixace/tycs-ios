#pragma once

#include "cocos2d.h"
#include <vector>
using namespace std;
using namespace cocos2d;
#include "hw3D_Camera.h"

#include "hw3D_C.h"
#include "hw3D_RMesh.h"
#include "hw3D_RPtcl.h"
#include "hw3D_RPPtcl.h"
#include "hw3D_RLine.h"
#include "3d/CCAABB.h"
#include "hw3D_A.h"


static float G_3D_Sprite_FPS = 1.0f / 30.0f;

enum STATE
{
	S_STOP,
	S_PLAYONCE,
	S_PLAY,
	S_HIDE,
		
	S_LASTFRAME,
	S_PLAYONCEGROUP,
	S_PLAYREPEATGROUP,
};

class Chw3D_Sprite : public Node
{

typedef struct
{
	int startFrame;
	int endFrame;
	int totalPlayTimes;
	int playTimes;
	std::string name;
}action;

public:
	Chw3D_C*								m_pC;
	Chw3D_A*								m_pA;

	vector<Chw3D_RMesh*>					m_rmeshs;
	vector<Chw3D_RPtcl*>					m_rptcls;
	vector<Chw3D_RPPtcl*>					m_rpptcls;
	vector<Chw3D_RLine*>					m_rlines;

	int										m_nFrame;				// 当前帧
	bool									m_bFristBuild;
	Mat4*									m_matrixs;				// 运动矩阵
	bool									m_bSkipFirstFrame;
	Vec3									m_skinCenter;

	double									m_dElapsed;
	double									m_dSec;

	int										m_nRepeatA;
	int										m_nRepeatB;
	int										m_callFunc;

	bool									m_isDirtyTranslate;
	Vec3									m_translate;

	bool									m_isDirtyRotate;
	Vec3									m_RotateAxis;
	float									m_fRotateRadian;
	float									m_fRotateRadianX;
	float									m_fRotateRadianY;
	float									m_fRotateRadianZ;

	bool									m_isDirtyScale;
	float									m_fScale;

	float									m_fShakeRadius;
	double									m_dShakeFreq;
	unsigned int							m_dwShakeTimes;
	double									m_dShakeNow;

	float									m_fShakeX, m_fShakeZ;

	Mat4									m_matrix;
	Chw3D_Sprite*							m_pAttachSprite;
	unsigned int							m_dwAttachIndex;
	EventListenerCustom*					m_attachSpriteListener;

	Chw3D_Camera*							m_pCamera;

	STATE									m_state;

	// 渲染命令
	CustomCommand							m_customCommand;

	// 控制播放速度
	float									m_fTimeScale;

	// 控制渲染使用的颜色
	Vec4									m_color;

	unsigned int							m_playedActionGroupIndex;
	unsigned int							m_playedActionGroupFrames;
	vector<action>							m_actionGroup;									
	float									m_actionGroupEndTime;
	std::function<void()>					m_onCallback;
	Vec4*									m_matrixPalette;
	bool									m_isC3Init;
	bool									m_isA3Init;
	unsigned int							m_3dType;
	bool									m_isHardwareSkinMesh;
	bool									m_isAsync;
	bool									m_isSkipDepthtTest;
	mutable AABB							m_aabb;

	bool									m_isUpdateing;
	bool									m_isLoadDefualt;
	string									m_c3FileName;
	string									m_a3FileName;
	string									m_attachBonename;

	vector<Chw3D_Sprite*>					m_attachChilren;
	bool									m_isAttachSpriteShow;

	string									m_applyCameraName;

public:
	static Chw3D_Sprite*		create(const char* pCFilename, const char* pAFilename, bool bSkipFirstFrame = false, unsigned int i3dType=0);
	static Chw3D_Sprite*		createAsync(const char* pCFilename, const char* pAFilename, bool bSkipFirstFrame = false, unsigned int i3dType = 0);

public:
								Chw3D_Sprite();
	virtual						~Chw3D_Sprite();

	bool						Create(const char* pCFilename, const char* pAFilename, bool bSkipFirstFrame = true);
	bool						LoadData(const char* pCFilename, const char* pAFilename, bool bSkipFirstFrame = true);
	void						CreateAsync(const char* pCFilename, const char* pAFilename, bool bSkipFirstFrame = true);
	bool						LoadC3File(Chw3D_C* pC3, std::string fileName);
	bool						LoadA3File(Chw3D_A* pA3, std::string fileName);
	bool						SetA(const char* pAFilename, bool bSkipFirstFrame = false);
	bool						isInit();
	void						Color(float fR, float fG, float fB, float fA);
	void						Translate(float fX, float fY, float fZ);
	void						Rotate(float fX, float fY, float fZ, float fRadian);
	void						Scale(float fScale);
	void						Shake(float fRadius, double dFerq, unsigned int dwTimes);
	void						Attach(Chw3D_Sprite* pSprite=nullptr, const char* pBonename = 0);

	void						SetRepeatAB(bool bSkipFirstFrame = false);
	void						SetRepeatAB(int nRepeatA, int nRepeatB);
	double						GetRepeatABDuration();
	void						SetState(STATE State, double timeScale=1.0, double dSec = 0.0, const std::function<void()>& callback = nullptr);

	void						FinialMatrix(unsigned int dwIndex, Mat4* pMatrix);

	void						SetCamera(Chw3D_Camera* pCamera);

	bool						Build(double dSec, double dIFS);
	virtual void				update(float fDelta);
	//virtual void				draw(void);
	virtual void draw(Renderer *renderer, const Mat4& transform, uint32_t flags);

	void						onDraw(bool bBuild);
	bool						AddAction(unsigned int startFrame, unsigned int endFrame, unsigned int playTimes);
	bool						CleanActionGroup();
	float						GetActualHeight();
	bool						isHardwareSkinMesh();
	void						setHardwareSkinMesh(bool isSkiningMesh);
	void						set3dType(unsigned int i3dType);
	void						setSkipDepthTest(bool isSkipDepthTest);
	bool						isSkipDepthTest();
	unsigned int				get3dType();
	//const AABB&					getAABB() const;
	bool						reloadFile(std::string& filePath);
	bool						cleanData();
	bool						isC3A3FIleExist();
	void						loadDefault();

	void						addAttachChild(Chw3D_Sprite* sprite);
	void						removeAttachChild(Chw3D_Sprite* sprite);

	void						SetApplyCameraName(std::string& name);

	Chw3D_RMesh*				getRMesh(unsigned int index);
	Chw3D_RPtcl*				getRPtcl(unsigned int index);
	Chw3D_RPPtcl*				getRPPtcl(unsigned int index);
	Chw3D_RLine*				getRLine(unsigned int index);

	Chw3D_C*					getC3();
	Chw3D_A*					getA3();

};
