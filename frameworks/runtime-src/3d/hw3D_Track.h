#pragma once

#include "cocos2d.h"
using namespace cocos2d;

//#include "../kazmath/include/kazmath/vec4.h"

#include <map>
using namespace std;

class Chw3D_DummyTrack
{
public:
	virtual ~Chw3D_DummyTrack()
	{
	}
};

template<class Type>
class Chw3D_BaseTrack : public Chw3D_DummyTrack
{
public:
	map<float, Type>						m_keys;

public:
	virtual ~Chw3D_BaseTrack()
	{
		Dispose();
	}
	
	virtual void Dispose(void)
	{
		m_keys.clear();
	}

	unsigned int Keys(void)
	{
		return (unsigned int)m_keys.size();
	}

	bool operator==(Chw3D_BaseTrack& Right)
	{
		return m_keys == Right.m_keys;
	}
	bool operator!=(Chw3D_BaseTrack& Right)
	{
		return !(*this == Right);
	}
};

template<class Type>
class Chw3D_Track : public Chw3D_BaseTrack<Type>
{
};

template<> class Chw3D_Track<Mat4> : public Chw3D_BaseTrack<Mat4>
{
public:
	bool Query(float fFrame, Mat4* pValue)
	{
		if(m_keys.size() == 0)
		{
			return false;
		}

		map<float, Mat4>::iterator it = m_keys.lower_bound(fFrame);
		if(it == m_keys.end())
		{
			--it;
			*pValue = it->second;
		}
		else if(it == m_keys.begin())
		{
			/*
			if(it->first != fFrame)
			{
				return false;
			}
			*/
			*pValue = it->second;
		}
		else if(it->first == fFrame)
		{
			*pValue = it->second;
		}
		else
		{
			map<float, Mat4>::iterator itPrev = it;
			--itPrev;

			//float fPercent = (fFrame - (float)itPrev->first) / (it->first - itPrev->first);
			//*pValue = itPrev->second * (1.0f - fPercent) + it->second * fPercent;
			*pValue = itPrev->second;
		}
		return true;
	}
};
template<> class Chw3D_Track<Vec4> : public Chw3D_BaseTrack<Vec4>
{
public:
	bool Query(float fFrame, Vec4* pValue)
	{
		if(m_keys.size() == 0)
		{
			return false;
		}

		map<float, Vec4>::iterator it = m_keys.lower_bound(fFrame);
		if(it == m_keys.end())
		{
			--it;
			*pValue = it->second;
		}
		else if(it == m_keys.begin())
		{
			/*
			if(it->first != fFrame)
			{
				return false;
			}
			*/
			*pValue = it->second;
		}
		else if(it->first == fFrame)
		{
			*pValue = it->second;
		}
		else
		{
			map<float, Vec4>::iterator itPrev = it;
			--itPrev;

			float fPercent = (fFrame - (float)itPrev->first) / (it->first - itPrev->first);
			//Vec4Lerp(pValue, &itPrev->second, &it->second, fPercent);
			pValue->x = itPrev->second.x + fPercent * (it->second.x - itPrev->second.x);
			pValue->y = itPrev->second.y + fPercent * (it->second.y - itPrev->second.y);
			pValue->z = itPrev->second.z + fPercent * (it->second.z - itPrev->second.z);
			pValue->w = itPrev->second.w + fPercent * (it->second.w - itPrev->second.w);
		}
		return true;
	}
};

template<> class Chw3D_Track<Vec2> : public Chw3D_BaseTrack<Vec2>
{
public:
	bool Query(float fFrame, Vec2* pValue)
	{
		if (m_keys.size() == 0)
		{
			return false;
		}

		map<float, Vec2>::iterator it = m_keys.lower_bound(fFrame);
		if (it == m_keys.end())
		{
			--it;
			*pValue = it->second;
		}
		else if (it == m_keys.begin())
		{
			/*
			if(it->first != fFrame)
			{
			return false;
			}
			*/
			*pValue = it->second;
		}
		else if (it->first == fFrame)
		{
			*pValue = it->second;
		}
		else
		{
			Vec2 a;
			
			map<float, Vec2>::iterator itPrev = it;
			--itPrev;

			float fPercent = (fFrame - (float)itPrev->first) / (it->first - itPrev->first);
			//Vec2Lerp(pValue, &itPrev->second, &it->second, fPercent);
			pValue->x = itPrev->second.x + fPercent * (it->second.x - itPrev->second.x);
			pValue->y = itPrev->second.y + fPercent * (it->second.y - itPrev->second.y);

		}
		return true;
	}
};

template<> class Chw3D_Track<unsigned int> : public Chw3D_BaseTrack<unsigned int>
{
public:
	bool Query(float fFrame, unsigned int* pValue)
	{
		if(m_keys.size() == 0)
		{
			return false;
		}

		map<float, unsigned int>::iterator it = m_keys.lower_bound(fFrame);
		if(it == m_keys.end())
		{
			--it;
			*pValue = it->second;
		}
		else if(it == m_keys.begin())
		{
			/*
			if(it->first != fFrame)
			{
				return false;
			}
			*/
			*pValue = it->second;
		}
		else
		{
			--it;
			*pValue = it->second;
		}
		return true;
	}
};
template<> class Chw3D_Track<bool> : public Chw3D_BaseTrack<bool>
{
public:
	bool Query(float fFrame, bool* pValue)
	{
		if(m_keys.size() == 0)
		{
			return false;
		}

		map<float, bool>::iterator it = m_keys.lower_bound(fFrame);
		if(it == m_keys.end())
		{
			--it;
			*pValue = it->second;
		}
		else if(it == m_keys.begin())
		{
			/*
			if(it->first != fFrame)
			{
				return false;
			}
			*/
			*pValue = it->second;
		}
		else if(it->first == fFrame)
		{
			*pValue = it->second;
		}
		else
		{
			--it;
			*pValue = it->second;
		}
		return true;
	}
};
