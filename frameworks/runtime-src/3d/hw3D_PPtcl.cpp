#include "hw3D_PPtcl.h"

Chw3D_PPtcl::Chw3D_PPtcl()
{
	m_bDirect = false;

	m_dwImageCol = 1;
	m_dwImageRow = 1;

	m_dwRate = 0;
	m_fSpeed = 0.0f;
	m_dLife = 1.0;
	m_fSizeStart = 1.0f;
	m_fSizeEnd = 1.0f;

	m_nDegreeMin = 0;
	m_nDegreeMax = 0;
	m_nRotateMin = 0;
	m_nRotateMax = 0;
}
Chw3D_PPtcl::~Chw3D_PPtcl()
{
}

void Chw3D_PPtcl::LoadEx(ChwCore_Stream* pStream, bool isAsync)
{
	// 名字
	unsigned int dwLen;
	pStream->Read(&dwLen, sizeof(unsigned int));
	if(dwLen)
	{
		m_fileName.resize(dwLen);
		pStream->Read(&m_fileName[0], sizeof(char) * dwLen);
	}

	// 模式
	pStream->Read(&m_dwMode, sizeof(unsigned int));

	// 这个Sub使用骨骼索引，同父模型
	pStream->Read(&m_dwNodeIndex, sizeof(unsigned int));

	pStream->Read(&m_pos, sizeof(Vec3));

	pStream->Read(&m_bDirect, sizeof(unsigned int));

	pStream->Read(&m_dwImageCol, sizeof(unsigned int));
	pStream->Read(&m_dwImageRow, sizeof(unsigned int));

	pStream->Read(&m_dwRate, sizeof(unsigned int));
	pStream->Read(&m_fSpeed, sizeof(float));
	pStream->Read(&m_dLife, sizeof(double));
	pStream->Read(&m_fSizeStart, sizeof(float));
	pStream->Read(&m_fSizeEnd, sizeof(float));

	pStream->Read(&m_nDegreeMin, sizeof(int));
	pStream->Read(&m_nDegreeMax, sizeof(int));
	pStream->Read(&m_nRotateMin, sizeof(int));
	pStream->Read(&m_nRotateMax, sizeof(int));

	// 材质
	m_material.LoadEx(pStream, isAsync);
}

Chw3D_Material*	Chw3D_PPtcl::getMaterial()
{
	return &m_material;
}