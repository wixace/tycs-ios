#include "hw3D_Mesh.h"

Chw3D_Mesh_Sub::Chw3D_Mesh_Sub()
{
	m_dwVertStride = 0;
	m_dwVerts = 0;
	m_poses = 0;
	m_weights = 0;
	m_blends = 0;
	m_colors = 0;
	m_uvs = 0;

	m_dwTris = 0;
	m_tris = 0;

	m_dwMode = 0;
	m_dwNormalCount = 0;
	m_normals = nullptr;
	m_hasNormal = false;
	m_useProgramName = GLProgram::SHADER_3D_Higame3D;
	//m_pMaterial = 0;
}
Chw3D_Mesh_Sub::~Chw3D_Mesh_Sub()
{
	if(m_poses)
	{
		delete[] m_poses;
	}
	if(m_weights)
	{
		delete[] m_weights;
	}
	if(m_blends)
	{
		delete[] m_blends;
	}
	if(m_colors)
	{
		delete[] m_colors;
	}
	if(m_uvs)
	{
		delete[] m_uvs;
	}
	if(m_tris)
	{
		delete[] m_tris;
	}
	if (m_normals)
	{
		delete[] m_normals;
	}
}

void Chw3D_Mesh_Sub::calcNormal()
{
	if (m_hasNormal)
	{
		Vec3* normals = new Vec3[m_dwVerts];

		vector<Vec3>* normalCools = new vector<Vec3>[m_dwVerts];

		for (size_t i = 0; i < m_dwTris * 3; i += 3)
		{
			Vec3 pos1 = m_poses[m_tris[i]];
			Vec3 pos2 = m_poses[m_tris[i + 1]];
			Vec3 pos3 = m_poses[m_tris[i + 2]];

			pos2.subtract(pos1);
			pos3.subtract(pos1);
			pos2.cross(pos3);
			pos2.normalize();

			normalCools[m_tris[i]].push_back(pos2);
			normalCools[m_tris[i + 1]].push_back(pos2);
			normalCools[m_tris[i + 2]].push_back(pos2);
		}

		for (size_t i = 0; i < m_dwVerts; i++)
		{
			vector<Vec3> single = normalCools[i];
			size_t aa = single.size();
			for (size_t j = single.size(); j > 0; j--)
			{
				for (size_t k = 0; k < j - 1; k++)
				{
					Vec3 normalA = single[k];
					Vec3 normalB = single[k + 1];

					normalA.x += normalB.x;
					normalA.y += normalB.y;
					normalA.z += normalB.z;

					normalA.normalize();

					single[k] = normalA;
				}
			}

			normals[i] = single[0];
			single.clear();
		}

		delete[] normalCools;


		m_normals = normals;
	}

}

Chw3D_Material*	Chw3D_Mesh_Sub::getMaterial()
{
	return &m_material;
}

void Chw3D_Mesh_Sub::LoadEx(ChwCore_Stream* pStream, bool isAsync)
{
	// 顶点结构跨度
	pStream->Read(&m_dwVertStride, sizeof(unsigned int));

	// 顶点
	pStream->Read(&m_dwVerts, sizeof(unsigned int));

//	CCLog(" ------start-- Chw3D_Mesh_Sub::LoadEx:%s, m_dwVerts=%d",&m_Name[0], m_dwVerts);  
	if (pStream->Version() >= 1) {
		pStream->Read(&m_hasNormal, sizeof(bool));
	}
	// 兼容之前的
	if(m_dwVertStride == 44)
	{
		m_poses = new Vec3[m_dwVerts];
		m_weights = new float[m_dwVerts * 4];
		m_blends = new unsigned char[m_dwVerts * 4];
		m_colors = new unsigned int[m_dwVerts];
		m_uvs = new Vec2[m_dwVerts];

		for(unsigned int n = 0; n < m_dwVerts; ++n)
		{
			pStream->Read(&m_poses[n], sizeof(Vec3));
			//if (m_hasNormal) {
				//pStream->Read(&m_Normals[n], sizeof(Vec3));
				//Vec3 a;
				//pStream->Read(&a, sizeof(Vec3));
			//}
			pStream->Read(&m_weights[n * 4], sizeof(float) * 4);
			pStream->Read(&m_blends[n * 4], sizeof(unsigned char) * 4);
			pStream->Read(&m_colors[n], sizeof(unsigned int));
			pStream->Read(&m_uvs[n], sizeof(Vec2));
//			CCLog(" ------in--[44] Chw3D_Mesh_Sub::LoadEx:%s, vert[%d] {m_Poses(%f,%f,%f)} {m_Blends(%d,%d)} {m_Weights(%f,%f)} {m_Colors=%x} {m_UVs(%f,%f)}",&m_Name[0], n, m_Poses[n].x,m_Poses[n].y,m_Poses[n].z, m_Blends[n*4], m_Blends[n*4+1], m_Weights[n*4], m_Weights[n*4+1], m_Colors[n], m_UVs[n].x, m_UVs[n].y ); 
		}
	}
	else
	{
		m_poses = new Vec3[m_dwVerts];
		m_colors = new unsigned int[m_dwVerts];
		m_uvs = new Vec2[m_dwVerts];

		for(unsigned int n = 0; n < m_dwVerts; ++n)
		{
			pStream->Read(&m_poses[n], sizeof(Vec3));

			//if (m_hasNormal) {
				//pStream->Read(&m_Normals[n], sizeof(Vec3));

				//Vec3 a;
				//pStream->Read(&a, sizeof(Vec3));
			//}

			pStream->Read(&m_colors[n], sizeof(unsigned int));
			pStream->Read(&m_uvs[n], sizeof(Vec2));

//			CCLog(" ------in--[24] Chw3D_Mesh_Sub::LoadEx:%s, vert[%d] {m_Poses(%f,%f,%f)} {m_Colors=%x} {m_UVs(%f,%f)}",&m_Name[0], n, m_Poses[n].x,m_Poses[n].y,m_Poses[n].z, m_Colors[n], m_UVs[n].x, m_UVs[n].y ); 

		}
	}

	// 面
	pStream->Read(&m_dwTris, sizeof(unsigned int));

	// 兼容之前的
	m_tris = new unsigned short[m_dwTris * 3];
	pStream->Read(&m_tris[0], sizeof(unsigned short) * m_dwTris * 3);

	//for (unsigned int idx=0; idx<m_dwTris; idx++)
	//{
	//	CCLog(" ------in--[%d] Chw3D_Mesh_Sub::LoadEx:%s, face(%d){%d,%d,%d}", m_dwVertStride, &m_Name[0], idx, m_Tris[idx*3], m_Tris[idx*3 + 1], m_Tris[idx*3 +2] );
	//}

	// 名字
	unsigned int dwLen;
	pStream->Read(&dwLen, sizeof(unsigned int));
	if(dwLen)
	{
		m_fileName.resize(dwLen);
		pStream->Read(&m_fileName[0], sizeof(char) * dwLen);
	}

	// 模式
	pStream->Read(&m_dwMode, sizeof(unsigned int));

	// 这个Sub使用骨骼索引，同父模型
	pStream->Read(&m_dwNodeIndex, sizeof(unsigned int));

	//CCLog(" ------end-- Chw3D_Mesh_Sub::LoadEx:%s, m_dwNodeIndex=%d",&m_Name[0], m_dwNodeIndex);  

	// 材质
	m_material.LoadEx(pStream, isAsync);

	calcNormal();

	if (m_hasNormal) {
		m_useProgramName += "_Speculars";
	}
	if (m_material.m_hasFlowLight) {
		m_useProgramName += "_FlowLight";
	}
}

Chw3D_Mesh::~Chw3D_Mesh()
{
	for(unsigned int n = 0; n < m_subs.size(); ++n)
	{
		delete m_subs[n];
	}
}



void Chw3D_Mesh::LoadEx(ChwCore_Stream* pStream, bool isAsync)
{
	// 名字
	unsigned int dwLen;
	pStream->Read(&dwLen, sizeof(unsigned int));
	if(dwLen)
	{
		m_fileName.resize(dwLen);
		pStream->Read(&m_fileName[0], sizeof(char) * dwLen);
	}

	// 分组
	unsigned int dwSubs;
	pStream->Read(&dwSubs, sizeof(unsigned int));

	//CCLog(" --start-- Chw3D_Mesh::LoadEx:%s, dwSubs=%d", &m_Name[0], dwSubs);  

	for(unsigned int n = 0; n < dwSubs; ++n)
	{
		Chw3D_Mesh_Sub* pSub = new Chw3D_Mesh_Sub;
		pSub->LoadEx(pStream, isAsync);
		m_subs.push_back(pSub);
	}

	// 骨骼索引
	pStream->Read(&m_dwNodeIndex, sizeof(unsigned int));

	//CCLog(" --end-- Chw3D_Mesh::LoadEx:%s, m_dwNodeIndex=%d",&m_Name[0], m_dwNodeIndex);  

}
