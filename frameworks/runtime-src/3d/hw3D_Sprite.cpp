#include "hw3D_Sprite.h"
#include "hw3D_Mgr.h"
#include "CCLuaEngine.h"
#include <iostream>

Chw3D_Sprite* Chw3D_Sprite::create(const char* pCFilename, const char* pAFilename, bool bSkipFirstFrame, unsigned int i3dType)
{
	Chw3D_Sprite* pRet = new Chw3D_Sprite();

	pRet->set3dType(i3dType);
	if(pRet->Create(pCFilename, pAFilename, bSkipFirstFrame))
	{
		pRet->autorelease();
		return pRet;
	}

	CC_SAFE_DELETE(pRet);
	return nullptr;
}

Chw3D_Sprite* Chw3D_Sprite::createAsync(const char* pCFilename, const char* pAFilename, bool bSkipFirstFrame, unsigned int i3dType)
{
	Chw3D_Sprite* pRet = new Chw3D_Sprite();
	pRet->CreateAsync(pCFilename, pAFilename, bSkipFirstFrame);
	pRet->set3dType(i3dType);
	pRet->m_isAsync = true;
	pRet->autorelease();
	return pRet;
}


Chw3D_Sprite::Chw3D_Sprite():
	m_pC(nullptr),
	m_pA(nullptr),
	m_bFristBuild(true),
	m_matrixs(nullptr),
	m_dElapsed(0),
	m_dSec(0),
	m_onCallback(nullptr),
	m_color(1.0,1.0,1.0,1.0),
	m_translate(0, 0, 0),
	m_RotateAxis(0, 1, 0),
	m_skinCenter(0, 0, 0),
	m_fRotateRadian(0),
	m_fScale(1),
	m_fRotateRadianX(0),
	m_fRotateRadianY(0),
	m_fRotateRadianZ(0),
	m_fShakeRadius(0),
	m_dShakeFreq(0),
	m_dwShakeTimes(0),
	m_dShakeNow(0),
	m_fShakeX(0),
	m_fShakeZ(0),
	m_pAttachSprite(0),
	m_dwAttachIndex(UINT_MAX),
	m_fTimeScale(1.0f),
	m_state(S_STOP),
	m_pCamera(Chw3D_Mgr::getInstance()->getDefaultCamera()),
	m_isA3Init(false),
	m_isC3Init(false),
	m_isHardwareSkinMesh(Chw3D_Mgr::getInstance()->isHardwareSkinMesh()),
	m_nRepeatA(0),
	m_nRepeatB(0),
	m_nFrame(0),
	m_bSkipFirstFrame(false),
	m_isAsync(false),
	m_isSkipDepthtTest(false),
	m_isLoadDefualt(false),
	m_isUpdateing(false),
	m_attachSpriteListener(nullptr),
	m_isAttachSpriteShow(true),
	m_3dType(0)
{

}

Chw3D_Sprite::~Chw3D_Sprite()
{
	cleanData();

	if (m_pAttachSprite)
	{
		m_pAttachSprite->removeAttachChild(this);
		m_pAttachSprite = nullptr;
	}

	for (size_t i = 0; i < m_attachChilren.size(); i++)
	{
		m_attachChilren[i]->m_pAttachSprite = nullptr;
	}
	m_attachChilren.clear();

}


bool Chw3D_Sprite::cleanData()
{

	if (m_pC)
	{
		Chw3D_Mgr::getInstance()->DeleteC(&m_pC);
		m_pC = nullptr;
	}
	if (m_pA)
	{
		Chw3D_Mgr::getInstance()->DeleteA(&m_pA);
		m_pA = nullptr;
	}

	for (unsigned int n = 0; n < m_rmeshs.size(); ++n)
	{
		delete m_rmeshs[n];
	}
	m_rmeshs.clear();

	for (unsigned int n = 0; n < m_rptcls.size(); ++n)
	{
		delete m_rptcls[n];
	}
	m_rptcls.clear();

	for (unsigned int n = 0; n < m_rpptcls.size(); ++n)
	{
		delete m_rpptcls[n];
	}
	m_rpptcls.clear();

	for (unsigned int n = 0; n < m_rlines.size(); ++n)
	{
		delete m_rlines[n];
	}
	m_rlines.clear();

	if (m_matrixs)
	{
		delete[] m_matrixs;
		m_matrixs = nullptr;
	}

	auto dispatcher = Director::getInstance()->getEventDispatcher();
	if (m_attachSpriteListener) {
		dispatcher->removeEventListener(m_attachSpriteListener);
	}

	return true;
}

bool Chw3D_Sprite::reloadFile(std::string& filePath)
{

	if (FileUtils::getInstance()->isFileExist(m_c3FileName) && FileUtils::getInstance()->isFileExist(m_a3FileName))
	{

		cleanData();
		Create(m_c3FileName.c_str(), m_a3FileName.c_str(), m_bSkipFirstFrame);

		for (size_t i = 0; i < m_attachChilren.size(); i++)
		{
			if (m_attachChilren[i]->isVisible() == false && m_attachChilren[i]->m_isAttachSpriteShow == false)
			{
				m_attachChilren[i]->setVisible(true);
				m_attachChilren[i]->m_isAttachSpriteShow = true;
			}
		}

		return true;
	}

	return false;
}


bool Chw3D_Sprite::Create(const char* pCFilename, const char* pAFilename, bool bSkipFirstFrame)
{
	m_c3FileName = pCFilename;
	m_a3FileName = pAFilename;

	if (!isC3A3FIleExist())
	{
#if (CC_TARGET_PLATFORM == CC_PLATFORM_WIN32)
		if (Chw3D_Mgr::getInstance()->isDebug())
		{
			if (!FileUtils::getInstance()->isFileExist(m_c3FileName))
			{
				cocos2d::MessageBox(m_c3FileName.c_str(), "Chw3D_Sprite Miss C3 Error");
			}
			if (!FileUtils::getInstance()->isFileExist(m_a3FileName))
			{
				cocos2d::MessageBox(m_a3FileName.c_str(), "Chw3D_Sprite Miss A3 Error");
			}
			
		}
#endif
		
		loadDefault();
		return true;
	}

	LoadData(pCFilename, pAFilename, bSkipFirstFrame);

	
	return true;
}

bool Chw3D_Sprite::LoadData(const char* pCFilename, const char* pAFilename, bool bSkipFirstFrame)
{
	std::string c3FileNme = string(pCFilename);

	auto hw3DMgr = Chw3D_Mgr::getInstance();
	Chw3D_C* pC3 = hw3DMgr->queryC3File(pCFilename);
	LoadC3File(pC3, c3FileNme);

	std::string a3FileNme = string(pAFilename);
	Chw3D_A* pA3 = hw3DMgr->queryA3File(pAFilename);
	LoadA3File(pA3, a3FileNme);

	SetRepeatAB(bSkipFirstFrame);

	if (!m_isUpdateing) {
		m_isUpdateing = true;
		scheduleUpdate();
	}
	return true;
}

void Chw3D_Sprite::CreateAsync(const char* pCFilename, const char* pAFilename, bool bSkipFirstFrame)
{
	auto hw3DMgr = Chw3D_Mgr::getInstance();
	hw3DMgr->queryC3FileAsync(pCFilename, std::bind(&Chw3D_Sprite::LoadC3File, this, std::placeholders::_1, std::string(pCFilename)));

	hw3DMgr->queryA3FileAsync(pAFilename, std::bind(&Chw3D_Sprite::LoadA3File, this, std::placeholders::_1, std::string(pAFilename)));

	SetRepeatAB(bSkipFirstFrame);
	scheduleUpdate();
}

bool Chw3D_Sprite::LoadC3File(Chw3D_C* pC3, std::string fileName)
{ 
	auto hw3DMgr = Chw3D_Mgr::getInstance();
	m_isC3Init = false;
	if (!pC3)
	{
#if (CC_TARGET_PLATFORM == CC_PLATFORM_WIN32)
		if (Chw3D_Mgr::getInstance()->isDebug())
		{
			cocos2d::MessageBox(fileName.c_str(), "Chw3D_Sprite Miss C3 Error");
		}
#endif

		std::string typeFileName = hw3DMgr->getDefaultTypeC3File(m_3dType);
		if (typeFileName != "") {
			pC3 = hw3DMgr->queryC3File(typeFileName.c_str());
			if (pC3) {
			}
		}	
	}
	
	if (pC3)
	{
		m_pC = pC3;
		for (unsigned int n = 0; n < m_pC->m_meshs.size(); ++n)
		{
			Chw3D_RMesh* pRMesh = new Chw3D_RMesh();
			pRMesh->Create(m_pC->m_meshs[n]);
			m_rmeshs.push_back(pRMesh);
		}
		for (unsigned int n = 0; n < m_pC->m_ptcls.size(); ++n)
		{
			Chw3D_RPtcl* pRPtcl = new Chw3D_RPtcl();
			pRPtcl->Create(m_pC->m_ptcls[n]);
			m_rptcls.push_back(pRPtcl);
		}
		for (unsigned int n = 0; n < m_pC->m_pptcls.size(); ++n)
		{
			Chw3D_RPPtcl* pRPPtcl = new Chw3D_RPPtcl();
			pRPPtcl->Create(m_pC->m_pptcls[n]);
			m_rpptcls.push_back(pRPPtcl);
		}
		for (unsigned int n = 0; n < m_pC->m_lines.size(); ++n)
		{
			Chw3D_RLine* pRLine = new Chw3D_RLine();
			pRLine->Create(m_pC->m_lines[n]);
			m_rlines.push_back(pRLine);
		}
		m_isC3Init = true;
		return true;
	}
	else
	{	
		m_isC3Init = false;
		return false;
	}
}

bool Chw3D_Sprite::isC3A3FIleExist()
{
	return FileUtils::getInstance()->isFileExist(m_c3FileName) && FileUtils::getInstance()->isFileExist(m_a3FileName);
}

void Chw3D_Sprite::loadDefault()
{
	auto hw3DMgr = Chw3D_Mgr::getInstance();
	std::string typeC3FileName = hw3DMgr->getDefaultTypeC3File(m_3dType);
	std::string typeA3FileName = hw3DMgr->getDefaultTypeA3File(m_3dType);
	if (typeC3FileName != "" && typeA3FileName != "") {
		m_isLoadDefualt = true;
		LoadData(typeC3FileName.c_str(), typeA3FileName.c_str(), m_bFristBuild);
	}
}

bool Chw3D_Sprite::LoadA3File(Chw3D_A* pA3, std::string fileName)
{
	auto hw3DMgr = Chw3D_Mgr::getInstance();

	m_isA3Init = false;
	m_bFristBuild = true;
	if (m_matrixs)
	{
		delete[] m_matrixs;
		m_matrixs = nullptr;
	}

	if (!pA3)
	{

#if (CC_TARGET_PLATFORM == CC_PLATFORM_WIN32)
		if (Chw3D_Mgr::getInstance()->isDebug())
		{
			cocos2d::MessageBox(fileName.c_str(), "Chw3D_Sprite Miss A3 Error");
		}
#endif

		std::string typeFileName = hw3DMgr->getDefaultTypeA3File(m_3dType);
		if (typeFileName != "") {
			pA3 = hw3DMgr->queryA3File(typeFileName.c_str());
			if (pA3) {
			}
		}
	}

	if (pA3)
	{
		if (m_pA)
		{
			Chw3D_Mgr::getInstance()->DeleteA(&m_pA);
			m_pA = nullptr;
		}
		
		m_pA = pA3;
		m_matrixs = new Mat4[m_pA->m_nodes.size()];
		SetState(m_state);
		if (m_nRepeatA == 0 && m_nRepeatB == 0)
		{
			SetRepeatAB(m_bSkipFirstFrame);
		}
		m_isA3Init = true;
		return true;
	}
	else
	{
		m_isA3Init = false;
		return false;
	}
}

bool Chw3D_Sprite::SetA(const char* pAFilename, bool bSkipFirstFrame)
{
	auto hw3DMgr = Chw3D_Mgr::getInstance();
	m_a3FileName = pAFilename;
	if (!m_isLoadDefualt)
	{
		if (m_isAsync)
		{
			hw3DMgr->queryA3FileAsync(pAFilename, std::bind(&Chw3D_Sprite::LoadA3File, this, std::placeholders::_1, string(pAFilename)));
		}
		else
		{
			std::string a3File = std::string(pAFilename);
			if (FileUtils::getInstance()->isFileExist(a3File)) {

				Chw3D_A* pA3 = hw3DMgr->queryA3File(pAFilename);
				LoadA3File(pA3, string(pAFilename));
			}
		}

		SetRepeatAB(bSkipFirstFrame);
	}

	return true;
}

bool Chw3D_Sprite::isInit()
{
	return m_isC3Init && m_isA3Init;
}

void Chw3D_Sprite::Color(float fR, float fG, float fB, float fA)
{
	m_color.x = fR;
	m_color.y = fG;
	m_color.z = fB;
	m_color.w = fA;
}

void Chw3D_Sprite::Translate(float fX, float fY, float fZ)
{
	m_translate.set(fX, fY, fZ);
	//Vec3Fill(&m_Translate, fX, fY, fZ);
}
void Chw3D_Sprite::Rotate(float fX, float fY, float fZ, float fRadian)
{
	m_RotateAxis.set(fX, fY, fZ);
	m_RotateAxis.normalize();
	m_fRotateRadian = fRadian;

}
void Chw3D_Sprite::Scale(float fScale)
{
	m_fScale= fScale;
}
void Chw3D_Sprite::Shake(float fRadius, double dFreq, unsigned int dwTimes)
{
	m_fShakeRadius = fRadius;
	m_dShakeFreq = dFreq;
	m_dwShakeTimes = dwTimes;
	m_dShakeNow = 0;

	m_fShakeX = 0;
	m_fShakeZ = 0;
}

void Chw3D_Sprite::set3dType(unsigned int i3dType)
{
	m_3dType = i3dType;
}

unsigned int Chw3D_Sprite::get3dType()
{
	return m_3dType;
}


void Chw3D_Sprite::Attach(Chw3D_Sprite* pSprite, const char* pBonename)
{
	
	if (m_pAttachSprite)
	{
		m_pAttachSprite->removeAttachChild(this);
		m_pAttachSprite = nullptr;
	}
	
	Chw3D_Sprite* attachSprite = nullptr;
	m_attachBonename = "";
	m_dwAttachIndex = UINT_MAX;
	if (pBonename)
	{
		m_attachBonename = pBonename;
		if (pSprite)
		{
			if (pSprite->m_pA)
			{
				m_dwAttachIndex = pSprite->m_pA->Query(pBonename);
				if (m_dwAttachIndex != UINT_MAX)
				{
					attachSprite = pSprite;
				}
			}
			else
			{
				attachSprite = pSprite;
			}
		}
	}
	else
	{
		attachSprite = pSprite;
	}

	if (attachSprite)
	{
		attachSprite->addAttachChild(this);
		if (!m_pAttachSprite->isInit())
		{
			this->m_isAttachSpriteShow = false;
			this->setVisible(false);
		}
	}

}

bool Chw3D_Sprite::isHardwareSkinMesh()
{
	return m_isHardwareSkinMesh;
}

void Chw3D_Sprite::setHardwareSkinMesh(bool isSkiningMesh)
{
	m_isHardwareSkinMesh = isSkiningMesh;
}

void Chw3D_Sprite::SetRepeatAB(bool bSkipFirstFrame)
{
	m_bSkipFirstFrame = bSkipFirstFrame;
	if (m_pA)
	{
		SetRepeatAB(bSkipFirstFrame ? m_pA->m_nStartFrame + 1 : m_pA->m_nStartFrame, m_pA->m_nEndFrame);
	}
}
void Chw3D_Sprite::SetRepeatAB(int nRepeatA, int nRepeatB)
{
	m_nRepeatA = nRepeatA;
	m_nRepeatB = nRepeatB;
}


void Chw3D_Sprite::addAttachChild(Chw3D_Sprite* child)
{
	if (child->m_pAttachSprite != this)
	{
		if (m_attachChilren.empty())
		{
			m_attachChilren.reserve(4);
		}
		child->m_pAttachSprite = this;
		m_attachChilren.push_back(child);
	}
}

void Chw3D_Sprite::removeAttachChild(Chw3D_Sprite* child)
{
	if (m_attachChilren.empty())
	{
		return;
	}

	for (auto it = m_attachChilren.begin(); it != m_attachChilren.end(); it++)
	{
		if (*it == child)
		{
			child->m_pAttachSprite = nullptr;
			m_attachChilren.erase(it);
			break;
		}
	}
}

double Chw3D_Sprite::GetRepeatABDuration()
{
	return (m_nRepeatB - m_nRepeatA) * G_3D_Sprite_FPS;
}

bool Chw3D_Sprite::AddAction(unsigned int startFrame, unsigned int endFrame, unsigned int totalPlayTimes)
{
	if(!m_pA || startFrame < m_pA->m_nStartFrame || endFrame > m_pA->m_nEndFrame) {
		CCLOG("Chw3D_Sprite::AddOneAction error");
		return false;
	}

	auto act = action();
	act.startFrame = startFrame;
	act.endFrame = endFrame;
	act.playTimes = 0;
	act.totalPlayTimes = totalPlayTimes;
	m_actionGroup.push_back(act);

	return true;
}

bool Chw3D_Sprite::CleanActionGroup()
{
	m_actionGroupEndTime = 0;
	m_playedActionGroupFrames = 0;
	m_playedActionGroupIndex = 0;
	m_actionGroup.clear();
	return true;
}

void Chw3D_Sprite::SetState(STATE State, double timeScale, double dSec, const std::function<void()>& callback)
{
	if(dSec < 0)
	{
		dSec = 0;
	}

	m_fTimeScale = timeScale;
	if(State != S_LASTFRAME && m_onCallback)
	{
		m_onCallback = nullptr;
	}

	switch(State)
	{
	case S_STOP:
		m_dSec = dSec;
		m_onCallback = nullptr;
		CleanActionGroup();
		break;
	case S_PLAYONCE:
		m_dSec = dSec;
		m_onCallback = callback;
		CleanActionGroup();
		break;
	case S_PLAY:
		m_dSec = dSec;
		m_onCallback = nullptr;
		CleanActionGroup();
		break;
	case S_HIDE:
		m_dSec = dSec;
		m_onCallback = nullptr;
		CleanActionGroup();
		break;
	case S_LASTFRAME:
		//CleanActionGroup();
		break;
	case S_PLAYONCEGROUP:
		{
			m_dSec = dSec;
			m_onCallback = callback;
			m_actionGroupEndTime = 0;
			m_playedActionGroupFrames = 0;
			m_playedActionGroupIndex = 0;
			
			for (size_t i = 0; i < m_actionGroup.size(); i++)
			{
				auto action = m_actionGroup[i];
				m_actionGroupEndTime += (action.endFrame - action.startFrame + 1) * G_3D_Sprite_FPS * action.totalPlayTimes;
			}
			if(m_actionGroup.size()>0)
			{
				auto action = m_actionGroup[m_playedActionGroupIndex];
				SetRepeatAB(action.startFrame, action.endFrame);
			}

		}
		break;
	case S_PLAYREPEATGROUP:
		{
			m_dSec = dSec;
			m_onCallback = callback;
			m_actionGroupEndTime = 0;
			m_playedActionGroupFrames = 0;
			m_playedActionGroupIndex = 0;
			if(m_actionGroup.size()>0)
			{
				auto action = m_actionGroup[m_playedActionGroupIndex];
				SetRepeatAB(action.startFrame, action.endFrame);
			}
		}
		break;
	}
	m_state = State;
}

void Chw3D_Sprite::FinialMatrix(unsigned int dwIndex, Mat4* pMatrix)
{
	if (m_matrixs)
	{
		*pMatrix = m_matrixs[dwIndex];

		//Mat4Multiply(pMatrix, &m_Matrix, pMatrix);
		*pMatrix = (m_matrix) * (*pMatrix);
	}
}

void Chw3D_Sprite::SetCamera(Chw3D_Camera* pCamera)
{
	m_pCamera = pCamera;
}

void Chw3D_Sprite::SetApplyCameraName(std::string& name)
{
	m_applyCameraName = name;
}

bool Chw3D_Sprite::Build(double dSec, double dIFS)
{
	// ����֡λ��
	int nTmpFrame = (dSec / dIFS) - m_playedActionGroupFrames;
	int playFrameCount = m_nRepeatB - m_nRepeatA + 1;
	if (playFrameCount == 0)
	{
		return false;
	}
	int playTimes = nTmpFrame / playFrameCount;
	int nFrame = nTmpFrame % playFrameCount + m_nRepeatA;
	//CCLOG("id %d  %s ------ Build1 dSec %f nFrame %d m_nRepeatA %d  m_nRepeatB %d playTimes %d m_playedActionGroupFrames %d", this->_ID, m_pA->m_Filename.c_str(), dSec, m_nFrame, m_nRepeatA, m_nRepeatB, playTimes, m_playedActionGroupFrames);
	if(m_actionGroup.size() > 0)
	{
		m_playedActionGroupFrames += playTimes * playFrameCount;
		if(m_playedActionGroupIndex < m_actionGroup.size())
		{
			auto action = m_actionGroup[m_playedActionGroupIndex];
			m_actionGroup[m_playedActionGroupIndex].playTimes += playTimes;
			//CCLOG("nTmpFrame %d action playTimes %d action.totalPlayTimes %d", nTmpFrame, m_actionGroup[m_playedActionGroupIndex].playTimes, m_actionGroup[m_playedActionGroupIndex].totalPlayTimes);
			if(m_actionGroup[m_playedActionGroupIndex].playTimes >= m_actionGroup[m_playedActionGroupIndex].totalPlayTimes)
			{
				m_actionGroup[m_playedActionGroupIndex].playTimes = 0;
				m_playedActionGroupIndex += 1;
				if(m_playedActionGroupIndex >= m_actionGroup.size())
				{
					m_playedActionGroupIndex = 0;
				}
				auto newAction = m_actionGroup[m_playedActionGroupIndex];
				SetRepeatAB(newAction.startFrame, newAction.endFrame);
				nTmpFrame = (dSec / dIFS) - m_playedActionGroupFrames;
				playFrameCount = m_nRepeatB - m_nRepeatA + 1;
				playTimes = nTmpFrame / playFrameCount;
				nFrame = nTmpFrame % playFrameCount + m_nRepeatA;
			}
		}
	}	
	
	if(nFrame == m_nFrame && !m_bFristBuild)
	{
		return false;
	}

	auto frameIt = m_pA->m_frameMatrixs.find(nFrame);
	if(frameIt != m_pA->m_frameMatrixs.end())
	{
		memcpy(m_matrixs, frameIt->second, sizeof(float) * 16 * m_pA->m_nodes.size());
	}
	else
	{
		Mat4* matrixs = new Mat4[m_pA->m_nodes.size()];
		// ���������˶�����
		tree<pair<string, unsigned int>>::pre_order_iterator it = m_pA->m_tree.begin();
		++it;
		// �ڽڵ������ǰ��Ӹ��ڵ㵽Ҷ�ڵ������������ģ����Կ��Ա�֤�����ӽڵ�ʱ���ڵ��Ѿ�������ϣ���Ϊ�ӽڵ���õ����ڵ�ı任���ݣ�?
		for(; it != m_pA->m_tree.end(); ++it)
		{
			unsigned int dwIndex = it->second;
			// (1)
			// ��ʼ�����ʾ�Ѷ���ӽڵ�������ؿռ䵽����ռ�ı任�����
			// ����ʼ���󣬾ͱ�ʾ�Ѷ��������ռ䵽�ڵ�������ؿռ�ı任
			//Mat4Inverse(&matrixs[dwIndex], &m_pA->m_Nodes[dwIndex]->m_InitMatrix);
			matrixs[dwIndex] = m_pA->m_nodes[dwIndex]->m_initMatrix.getInversed();

			Chw3D_TrackSet* pTrackSet = m_pA->m_nodes[dwIndex]->m_pTrackSet;

			// ֡����
			int nIndex = min((unsigned int)pTrackSet->m_dwKeys - 1, nFrame - m_pA->m_nStartFrame);
			nIndex = max(0, nIndex);
		
			// (2)
			// �ڸýڵ�������ؿռ�ִ�и�֡�ı�?
			// ���Ͻڵ���˶����󣬣����ö������Žڵ��˶���?

			//Mat4Multiply(
			//	&matrixs[dwIndex], 
			//	&pTrackSet->m_Matrixs[nIndex],
			//	&matrixs[dwIndex]);
			matrixs[dwIndex] = pTrackSet->m_matrixs[nIndex] * matrixs[dwIndex];
			// (3)
			// ִ���걾�ؿռ�ı任֮����Ҫ�任������ռ䣬��ִ�в���(1)�������?
			// ���ϳ�ʼ����


			//Mat4Multiply(&matrixs[dwIndex], &m_pA->m_Nodes[dwIndex]->m_InitMatrix, &matrixs[dwIndex]);
			matrixs[dwIndex] = m_pA->m_nodes[dwIndex]->m_initMatrix * matrixs[dwIndex];
			// �ýڵ�ı任�Ѿ�ִ����ϣ��ٵ����ϸ��ڵ�ı�?���ڵ������ӽڵ����?
			// ���ϸ��˶�����
			unsigned int dwParentIndex = m_pA->m_tree.parent(it)->second;
			if(dwParentIndex != UINT_MAX)
			{
				//Mat4Multiply(&matrixs[dwIndex], &matrixs[dwParentIndex], &matrixs[dwIndex]);
				matrixs[dwIndex] = matrixs[dwParentIndex] * matrixs[dwIndex];
			}
		}

		memcpy(m_matrixs, matrixs, sizeof(float) * 16 * m_pA->m_nodes.size());
		m_pA->m_frameMatrixs[nFrame] = matrixs;
		//delete[] matrixs;

	}
	m_nFrame = nFrame;
	m_bFristBuild = false;

	return true;
}

void Chw3D_Sprite::update(float fDelta)
{

	fDelta *= m_fTimeScale;
	m_dElapsed += fDelta;
	switch(m_state)
	{
	case S_STOP:
		//m_dSec = 0;
		break;
	case S_PLAYONCE:
		{
			m_dSec += fDelta;
			double dTime = (double)(m_nRepeatB - m_nRepeatA) * G_3D_Sprite_FPS;
			if(m_dSec >= dTime)
			{
				m_dSec = dTime;
				SetState(S_LASTFRAME);
			}
		}
		break;
	case S_PLAY:
		m_dSec += fDelta;
		break;
	case S_HIDE:
		m_dSec = 0;
		break;
	case S_LASTFRAME:
		{
			if(m_onCallback)
			{
				m_onCallback();
				//m_onCallback = nullptr;
			}
			else
			{
				SetState(S_STOP, 1, m_dSec);
			}
		}
		break;
	case S_PLAYONCEGROUP:
		{
			m_dSec += fDelta;
			if(m_dSec >= m_actionGroupEndTime)
			{
				m_dSec = m_actionGroupEndTime;
				//CleanActionGroup();
				SetState(S_LASTFRAME);
			}
		}
		break;

	case S_PLAYREPEATGROUP:
		{
			m_dSec += fDelta;
		}
		break;
	}

	m_dShakeNow += fDelta;
	if(m_dShakeNow >= m_dShakeFreq)
	{
		m_dShakeNow = 0;

		if(m_dwShakeTimes > 0)
		{
			m_fShakeX = (rand() % ((int)(m_fShakeRadius * 100) - (int)(-m_fShakeRadius * 100) + 1)) / 100.0f + -m_fShakeRadius;
			m_fShakeZ = (rand() % ((int)(m_fShakeRadius * 100) - (int)(-m_fShakeRadius * 100) + 1)) / 100.0f + -m_fShakeRadius;

			--m_dwShakeTimes;
		}
		else
		{
			m_fShakeX = 0;
			m_fShakeZ = 0;

			m_fShakeRadius = 0;
			m_dShakeFreq = 0;
		}
	}
}

void Chw3D_Sprite::draw(Renderer *renderer, const Mat4& transform, uint32_t flags)
{
	if(m_state == S_HIDE)
	{
		return;
	}

	//if ((m_pCamera != nullptr) && (m_pA != nullptr) && (m_ApplyCameraName.length() > 0)) {
	if ((m_pCamera != nullptr) && (m_pA != nullptr) && (m_pA->m_cameraNodes.size()>0)) {
		
		Chw3D_A_CameraNode* pCameraNode = m_pA->m_cameraNodes[0];
		if (m_applyCameraName.length() > 0) {
			pCameraNode = m_pA->QueryCameraNode(m_applyCameraName.c_str());
		}
		if (pCameraNode != nullptr)
		{
			CameraInfo *cameraInfo = pCameraNode->GetCameraInfo(m_nFrame);
			if (cameraInfo != nullptr)
			{ 
				m_pCamera->Set(cameraInfo->x, cameraInfo->y, cameraInfo->z, cameraInfo->tx, cameraInfo->ty, cameraInfo->tz);
				m_pCamera->Perspective(cameraInfo->focal, m_pCamera->m_aspectRatio, cameraInfo->nearplane, cameraInfo->farplane);
			}
		}
	}

	bool bBuild = false;
	if (isInit())
	{
		bBuild = Build(m_dSec, G_3D_Sprite_FPS);
	}
	
	Mat4 Scale;
	Mat4::createScale(m_fScale, m_fScale, m_fScale, &Scale);
	Mat4 Rotate;

	Mat4::createRotation(m_RotateAxis, m_fRotateRadian, &Rotate);
	Mat4::createScale(m_fScale, m_fScale, m_fScale, &Scale);
	Mat4 Translate;

	unsigned int depth = 0;
	if (m_pCamera && m_pCamera->isEnabelGuiDepth() && !m_pAttachSprite)
	{
		depth = _visitIndex * m_pCamera->getDepthRatio();
	}

	Mat4::createTranslation(m_translate.x + m_fShakeX, m_translate.y, m_translate.z + m_fShakeZ + depth, &Translate);

	m_matrix = Scale;
	m_matrix = Rotate * m_matrix;
	m_matrix = Translate * m_matrix;
	if(m_pAttachSprite)
	{
		if (m_pAttachSprite->isInit())
		{
			setVisible(true);
			if (m_attachBonename != "" && m_dwAttachIndex == UINT_MAX)
			{
				m_dwAttachIndex = m_pAttachSprite->m_pA->Query(m_attachBonename.c_str());
			}
			if (m_dwAttachIndex == UINT_MAX)
			{
				Mat4::createTranslation(m_pAttachSprite->m_skinCenter.x, 0, m_pAttachSprite->m_skinCenter.z, &Translate);
				m_matrix = Translate * m_matrix;
			}
			else
			{
				m_matrix = m_pAttachSprite->m_pA->m_nodes[m_dwAttachIndex]->m_initMatrix * m_matrix;
				m_matrix = m_pAttachSprite->m_matrixs[m_dwAttachIndex] * m_matrix;
				m_matrix = m_pAttachSprite->m_matrix * m_matrix;
			}
		}
		else
		{
			setVisible(false);
		}

	}
	
	// ����Ƥ���ĵ�
	Mat4 Matrix;
	if(m_rmeshs.size() > 0 && m_rmeshs[0]->m_pMesh->m_subs[0]->m_dwVertStride == 44)
	{
		FinialMatrix(m_rmeshs[0]->m_pMesh->m_dwNodeIndex, &Matrix);
	}
	else
	{
		Matrix = m_matrix;
	}
	Vec3 Center(0, 0, 0);
	Matrix.transformPoint(Center, &m_skinCenter);

	//if (m_isC3Init)
	//{
		this->retain();
		m_customCommand.init(_globalZOrder);
		m_customCommand.func = CC_CALLBACK_0(Chw3D_Sprite::onDraw, this, bBuild);
		renderer->addCommand(&m_customCommand);
	//}
	//

}

float Chw3D_Sprite::GetActualHeight()
{
	return m_skinCenter.y;
}

void Chw3D_Sprite::setSkipDepthTest(bool isSkipDepthTest)
{
	m_isSkipDepthtTest = isSkipDepthTest;
}

bool Chw3D_Sprite::isSkipDepthTest()
{
	return m_isSkipDepthtTest;
}

//const AABB& Chw3D_Sprite::getAABB() const
//{
//	return AABB();
//}


void Chw3D_Sprite::onDraw(bool bBuild)
{
	glEnable(GL_CULL_FACE);
	glCullFace(GL_BACK);

	// 
	for(unsigned int n = 0; n < m_rmeshs.size(); ++n)
	{
		m_rmeshs[n]->draw(this, bBuild, m_color);
	}
	for(unsigned int n = 0; n < m_rptcls.size(); ++n)
	{
		m_rptcls[n]->draw(this, bBuild);
	}
	for(unsigned int n = 0; n < m_rpptcls.size(); ++n)
	{
		m_rpptcls[n]->draw(this, bBuild);
	}
	for(unsigned int n = 0; n < m_rlines.size(); ++n)
	{
		m_rlines[n]->draw(this, bBuild);
	}

	if(bBuild)
	{
		m_dElapsed = 0;
	}

	glDisable(GL_CULL_FACE);
	this->release();
}


Chw3D_RMesh* Chw3D_Sprite::getRMesh(unsigned int index)
{
	if (m_rmeshs.size() > 0 && (0 < index && index < m_rmeshs.size())) {
		return m_rmeshs[index];
	}

	return nullptr;
}

Chw3D_RPtcl* Chw3D_Sprite::getRPtcl(unsigned int index)
{
	if (m_rptcls.size() > 0 && (0 < index && index < m_rptcls.size())) {
		return m_rptcls[index];
	}

	return nullptr;
}

Chw3D_RPPtcl* Chw3D_Sprite::getRPPtcl(unsigned int index)
{
	if (m_rpptcls.size() > 0 && (0 < index && index < m_rpptcls.size())) {
		return m_rpptcls[index];
	}

	return nullptr;
}
Chw3D_RLine* Chw3D_Sprite::getRLine(unsigned int index)
{
	if (m_rlines.size() > 0 && m_rlines.size() <= index) {
		return m_rlines[index];
	}

	return nullptr;
}

Chw3D_C* Chw3D_Sprite::getC3()
{
	return m_pC;
}

Chw3D_A* Chw3D_Sprite::getA3()
{
	return m_pA;
}
