#pragma once

#include "cocos2d.h"
using namespace cocos2d;

#include "tree.hh"

#include "hwCore_Stream.h"
#include "hw3D_TrackSet.h"

class Chw3D_A_Node
{
public:
	enum TYPE
	{
		T_BONE			= 0,
		T_MESH,
		T_PTCL,
		T_LINE,
		T_PPTCL,
	};

public:
	TYPE											m_type;
	string											m_name;
	Chw3D_TrackSet*									m_pTrackSet;
	Mat4											m_initMatrix;

public:
							Chw3D_A_Node();
	virtual					~Chw3D_A_Node();

	void					Load(ChwCore_Stream* pStream);

};

typedef struct _CameraInfo {
	float x;
	float y;
	float z;
	float tx;
	float ty;
	float tz;
	float bank;
	float focal;
	unsigned short flags;
	float nearplane;
	float farplane;
	void *appdata;
}CameraInfo;

class Chw3D_A_CameraNode
{
public:
	string											m_fileName;
	float											m_aspectRatio;
	unsigned int									m_dwKeys;
	CameraInfo*										m_pCameraTrackSet;

public:
	Chw3D_A_CameraNode();
	virtual					~Chw3D_A_CameraNode();
	void					Load(ChwCore_Stream* pStream);
	CameraInfo*				GetCameraInfo(unsigned int nframe);
};

class Chw3D_A : public Node
{
public:

	unsigned int										m_nStartFrame;
	unsigned int										m_nEndFrame;
	unsigned int										m_bonesCount;
	std::map<int, Mat4*>								m_frameMatrixs;				// 运动矩阵
	vector<Chw3D_A_Node*>								m_nodes;				// 结点
	vector<Chw3D_A_CameraNode*>							m_cameraNodes;			// 摄像机结点
	tree<pair<string, unsigned int>>					m_tree;					// 结点树

	std::string											m_fileName;				// 文件名
	bool												m_isLoadedData;

public:
	void						TreeLoad(tree<pair<string, unsigned int>>::iterator it, ChwCore_Stream* pStream);

public:
								Chw3D_A();
								Chw3D_A(const char*  fileName);
	virtual						~Chw3D_A();

	bool						LoadFile(const char* pFilename);
	void						LoadFileAsync(const char* pFilename, std::function<void(Chw3D_A*)>& callback);
	void						LoadData(cocos2d::Data* data);
	bool						isLoadedData();
	unsigned int				getBonesCount();
	unsigned int				Query(const char* pName);
	Chw3D_A_CameraNode*			QueryCameraNode(const char* pName);
};
