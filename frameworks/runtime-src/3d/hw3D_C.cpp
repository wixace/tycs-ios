#include "hw3D_C.h"
#include "hwCore_String.h"

Chw3D_C::Chw3D_C(const char* fileName):
	m_fileName(fileName),
	m_isLoadedData(false)
{
}

Chw3D_C::Chw3D_C():
	m_isLoadedData(false)
{
}
Chw3D_C::~Chw3D_C()
{
	for(unsigned int n = 0; n < m_meshs.size(); ++n)
	{
		delete m_meshs[n];
	}
	for(unsigned int n = 0; n < m_ptcls.size(); ++n)
	{
		delete m_ptcls[n];
	}
	for(unsigned int n = 0; n < m_pptcls.size(); ++n)
	{
		delete m_pptcls[n];
	}
	for(unsigned int n = 0; n < m_lines.size(); ++n)
	{
		delete m_lines[n];
	}
	//m_Bones.Release();
}



void Chw3D_C::LoadData(cocos2d::Data* data, bool isAsync)
{
	ChwCore_Stream Stream(data->getBytes(), data->getSize(), 0);
	
	unsigned int dwMeshs;
	Stream.Read(&dwMeshs, sizeof(unsigned int));
	//CCLOG("Chw3D_C::LoadEx:%s, dwMeshs=%d", pFilename, dwMeshs);  
	for (unsigned int n = 0; n < dwMeshs; n++)
	{
		Chw3D_Mesh* pMesh = new Chw3D_Mesh;
		pMesh->LoadEx(&Stream, isAsync);
		m_meshs.push_back(pMesh);
	}

	unsigned int dwPtcls;
	Stream.Read(&dwPtcls, sizeof(unsigned int));
	//CCLOG("Chw3D_C::LoadEx:%s, dwPtcls=%d", pFilename, dwPtcls);  
	for (unsigned int n = 0; n < dwPtcls; n++)
	{
		Chw3D_Ptcl* pPtcl = new Chw3D_Ptcl;
		pPtcl->LoadEx(&Stream, isAsync);
		m_ptcls.push_back(pPtcl);
	}

	unsigned int dwPPtcls;
	Stream.Read(&dwPPtcls, sizeof(unsigned int));
	//CCLOG("Chw3D_C::LoadEx:%s, dwPPtcls=%d", pFilename, dwPPtcls);  
	for (unsigned int n = 0; n < dwPPtcls; n++)
	{
		Chw3D_PPtcl* pPPtcl = new Chw3D_PPtcl;
		pPPtcl->LoadEx(&Stream, isAsync);
		m_pptcls.push_back(pPPtcl);
	}

	unsigned int dwLines;
	Stream.Read(&dwLines, sizeof(unsigned int));
	//CCLOG("Chw3D_C::LoadEx:%s, dwLines=%d", pFilename, dwLines);  
	for (unsigned int n = 0; n < dwLines; n++)
	{
		Chw3D_Line* pLine = new Chw3D_Line;
		pLine->LoadEx(&Stream, isAsync);
		m_lines.push_back(pLine);
	}

	// 兼容之前版本数据接口, 只能在最后添加数据
	unsigned int meshUVStepCount = 0;
	Stream.Read(&meshUVStepCount, sizeof(unsigned int));
	for (size_t i = 0; i < meshUVStepCount; i++)
	{
		unsigned int meshIndex = 0;
		Stream.Read(&meshIndex, sizeof(unsigned int));
		Chw3D_Mesh * pMesh = m_meshs[meshIndex];
		if (pMesh) {

			for (size_t j = 0; j < pMesh->m_subs.size(); j++)
			{
				Chw3D_Mesh_Sub* pSub = pMesh->m_subs[j];
				unsigned int dwUVSteps = 0;
				Stream.Read(&dwUVSteps, sizeof(unsigned int));
				for (unsigned int n = 0; n < dwUVSteps; ++n)
				{
					float fFrame;
					Stream.Read(&fFrame, sizeof(float));

					Vec2 uvTrans;
					Stream.Read(&uvTrans, sizeof(Vec2));
					pSub->m_material.m_uvStepTracks.m_keys[fFrame] = uvTrans;
				}

				if (dwUVSteps > 0) {

					pSub->m_material.m_texParams = { GL_LINEAR, GL_LINEAR, GL_CLAMP_TO_EDGE, GL_CLAMP_TO_EDGE };
					if (pSub->m_material.m_pTexture)
					{
						pSub->m_material.m_pTexture->setTexParameters(pSub->m_material.m_texParams);
					}
				}
			}
		}
	}
}

void Chw3D_C::LoadFileAsync(const char* pFilename, std::function<void(Chw3D_C*)>& callback)
{
	m_fileName = pFilename;
	m_isLoadedData = true;
	FileUtils::getInstance()->getDataFromFile(pFilename, [=](Data data)
	{
		if (data.getSize() == 0)
		{
			delete this;
			callback(nullptr);
		}
		else
		{
			m_fileName = pFilename;
			LoadData(&data, true);
			callback(this);
		}
	});
}


bool Chw3D_C::LoadFile(const char* pFilename)
{
	m_fileName = pFilename;
	Data data = FileUtils::getInstance()->getDataFromFile(pFilename);
	if(data.getSize() == 0)
	{
		//cocos2d::MessageBox(pFilename, "Chw3D_C File Miss Fatal Error");
		return false;
	}
	LoadData(&data);
	m_isLoadedData = true;
	return true;
}

bool Chw3D_C::isLoadedData()
{
	return m_isLoadedData;
}