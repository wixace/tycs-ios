#pragma once

#include "cocos2d.h"
using namespace cocos2d;

#include "hwCore_Stream.h"
#include "hw3D_Track.h"

class Chw3D_Material
{
public:
	enum class MaterialmTexType
	{
		Anbient,
		Diffuse,
		Specular,
		Glossiness,
		SpecularLv,
		SelfLumination,
		Opacity,
		Filter,
		Bump
	};

public:
	Texture2D*									m_pTexture;
	Texture2D*									m_pSpecularTexture;
	Texture2D*									m_pOpacityTexture;
	Texture2D*									m_pFilterTexture;
	Texture2D*									m_pFlowLightTexture;
	map<string,MaterialmTexType>				m_fileMaterialmTexTypes;
	std::string									m_textureFileName;
	Chw3D_Track<bool>							m_drawTracks;				// 绘制轨迹（外部使用）
	Chw3D_Track<Vec4>							m_colorTracks;				// 颜色轨迹
	Chw3D_Track<Vec4>							m_timeColorTracks;			// 颜色轨迹
	Vec2										m_uvTrans;					// UV移动
	unsigned int								m_dwTileX, m_dwTileY;		// XY方向格子数
	Chw3D_Track<unsigned int>					m_tileTracks;				// 格子轨迹
	Chw3D_Track<Vec2>							m_uvStepTracks;				// 流体UV

	bool										m_hasFlowLight;				// 流光特效
	Vec4										m_lightColor;				// 流光颜色
	Vec2										m_lightSpeed;				// 流光颜色
	float										m_lightWidth;				// 流光宽度

	float										m_specular;					// 高光系数

	Texture2D::TexParams						m_texParams;
public:
										Chw3D_Material();
	virtual								~Chw3D_Material();

	void								LoadEx(ChwCore_Stream* pStream, bool isAsync=false);
	void								loadTexture(MaterialmTexType type, Texture2D* texture);
	void								loadTexture(std::string& texFileName, bool isAsync);
	Texture2D*							getTextureWithType(MaterialmTexType type);
	void								setTextureWithType(MaterialmTexType type, Texture2D* tex);
};
