#pragma once

#include "cocos2d.h"
using namespace cocos2d;

#include "hwCore_Stream.h"
#include "hw3D_Material.h"

class Chw3D_Mesh_Sub
{
public:
	unsigned int										m_dwVertStride;		// 顶点结构跨度
	unsigned int										m_dwVerts;			// 顶点数
	Vec3*												m_poses;
	float*												m_weights;
	unsigned char*										m_blends;
	unsigned int*										m_colors;
	Vec2*												m_uvs;

	unsigned int										m_dwTris;			// 面数
	unsigned short*										m_tris;				// 

	string												m_fileName;				// 名字
	unsigned int										m_dwMode;			// 模式
	unsigned int										m_dwNodeIndex;		// 这个Sub使用骨骼索引，同父模型

	bool												m_hasNormal;
	unsigned int										m_dwNormalCount;		// 法线个数
	Vec3*												m_normals;			// 法线数组

	map<string, map<int, Vec3*>>						m_cacheVerts;

	/*
	下面成员需要手动加载
	*/
	Chw3D_Material										m_material;			// 材质
	std::string											m_useProgramName;
public:
										Chw3D_Mesh_Sub();
	virtual								~Chw3D_Mesh_Sub();

	virtual void						LoadEx(ChwCore_Stream* pStream, bool isAsync=false);
	void								calcNormal();
	Chw3D_Material*						getMaterial();
	//void								Prepare(Chw3D_RA* pRA);
	//void								Draw(Chw3D_RA* pRA, unsigned int dwIndex0, unsigned int dwIndex1);
};
	
class Chw3D_Mesh
{
public:
	string											m_fileName;					// 名字

	vector<Chw3D_Mesh_Sub*>							m_subs;					

	unsigned int									m_dwNodeIndex;			// 这个模型使用骨骼索引

public:
	virtual				~Chw3D_Mesh();

	void				LoadEx(ChwCore_Stream* pStream, bool isAsync = false);

};

