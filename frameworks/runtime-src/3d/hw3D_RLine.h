#pragma once

#include "cocos2d.h"
using namespace cocos2d;

#include "hw3D_Line.h"

class Chw3D_Sprite;
class Chw3D_RLine_Node
{
public:
	Vec3								m_pos[2];
	Vec2								m_uv[2];
	double								m_dLife;				// 生命
};

class Chw3D_RLine : public Node
{
public:
	//
	Chw3D_Line*									m_pLine;

	unsigned int								m_dwMaxCount;			// 最多的面数

	vector<Vec3>								m_poses;
	vector<unsigned int>						m_colors;
	vector<Vec2>								m_uvs;

	vector<unsigned short>						m_tris;

	list<Chw3D_RLine_Node*>						m_nodes;				// 当前有效段数

	//
	GLint										m_nUniformColor;
	GLint										m_nUniformUVTrans;

public:
	void								CalcUV(Chw3D_Sprite* pSprite);
	void								Build(Chw3D_Sprite* pSprite);

public:
										Chw3D_RLine();
	virtual								~Chw3D_RLine();

	bool								Create(Chw3D_Line* pLine);

	void								draw(Chw3D_Sprite* pSprite, bool bBuild);
};