#include "hw3D_TrackSet.h"

Chw3D_TrackSet::Chw3D_TrackSet()
{
	m_matrixs = 0;
}
Chw3D_TrackSet::~Chw3D_TrackSet()
{
	if(m_matrixs)
	{
		delete[] m_matrixs;
	}
}

void Chw3D_TrackSet::Load(ChwCore_Stream* pStream)
{
	pStream->Read(&m_dwKeys, sizeof(unsigned int));

	//CCLOG("\tChw3D_TrackSet::Load:, m_dwKeys=%d", m_dwKeys); 

	m_matrixs = new Mat4[m_dwKeys];
	for(unsigned int n = 0; n < m_dwKeys; ++n)
	{
		pStream->Read(&m_matrixs[n], sizeof(Mat4));


		//CCLOG( "frame=%d:\n" , n );

		//CCLOG( "[" );

		//for(int x=0; x<4; x++)
		//{


		//		CCLOG( "%f,%f,%f,%f\n", m_Matrixs[n].m[x*4+0], m_Matrixs[n].m[x*4+1], m_Matrixs[n].m[x*4+2], m_Matrixs[n].m[x*4+3] );

		//	
		//	if (x==3)
		//	{
		//		CCLOG( "]\n" );
		//	}

		//}

	}
}

void Chw3D_MeshTrackSet::Load(ChwCore_Stream* pStream)
{
	Chw3D_TrackSet::Load(pStream);
}

void Chw3D_PtclTrackSet::Load(ChwCore_Stream* pStream)
{
	Chw3D_TrackSet::Load(pStream);
}
