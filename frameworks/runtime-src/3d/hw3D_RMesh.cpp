#include "hw3D_RMesh.h"
#include "hw3D_Mesh.h"
#include "hw3D_Camera.h"
#include "hw3D_Sprite.h"


Chw3D_RMesh_Sub::Chw3D_RMesh_Sub():
	m_Poses(nullptr),
	m_Normals(nullptr)
{

}


Chw3D_RMesh_Sub::~Chw3D_RMesh_Sub()
{
	if(m_Poses)
	{
		delete[] m_Poses;
		m_Poses = nullptr;
	}

	if (m_Normals)
	{
		delete[] m_Normals;
		m_Normals = nullptr;
	}
}

Chw3D_RMesh::Chw3D_RMesh():
	m_isInitPoses(false)
{
	// frag
/*	auto fileUtiles = FileUtils::getInstance();
	auto fragmentFilePath = fileUtiles->fullPathForFilename("../../frameworks/cocos2d-x/cocos/renderer/ccShader_Higame3D.vert");
	auto fragSource = fileUtiles->getStringFromFile(fragmentFilePath);

	// vert
	auto vertexFilePath = fileUtiles->fullPathForFilename("../../frameworks/cocos2d-x/cocos/renderer/ccShader_Higame3D.vert");
	auto vertSource = fileUtiles->getStringFromFile(vertexFilePath);

	m_glProgram = GLProgram::createWithByteArrays(vertSource.c_str(), fragSource.c_str());
	auto glprogramstate = GLProgramState::getOrCreateWithGLProgram(m_glProgram);
	setGLProgramState(glprogramstate);*/
}
Chw3D_RMesh::~Chw3D_RMesh()
{
	for(unsigned int n = 0; n < m_subs.size(); ++n)
	{
		delete m_subs[n];
	}
	m_subs.clear();
}

bool Chw3D_RMesh::Create(Chw3D_Mesh* pMesh)
{
	m_pMesh = pMesh;

	if(m_pMesh->m_subs[0]->m_dwVertStride == 44)
	{
		for(unsigned int n = 0; n < m_pMesh->m_subs.size(); ++n)
		{
			Chw3D_Mesh_Sub* pSub = m_pMesh->m_subs[n];
			Chw3D_RMesh_Sub* pRSub = new Chw3D_RMesh_Sub();
			pRSub->m_Poses = new Vec3[pSub->m_dwVerts];
			if (pSub->m_hasNormal)
			{
				pRSub->m_Normals = new Vec3[pSub->m_dwVerts];
			}
			
			m_subs.push_back(pRSub);
		}
	}

	return true;
}


Vec4 Chw3D_RMesh::getPosition(Vec3 position, Vec4 weight, Vec4 Indexs, Vec4* matrixPalette)
{
	float blendWeight = weight.x;

	int matrixIndex = Indexs.x;
	Vec4 matrixPalette1 = matrixPalette[matrixIndex] * blendWeight;
	Vec4 matrixPalette2 = matrixPalette[matrixIndex + 1] * blendWeight;
	Vec4 matrixPalette3 = matrixPalette[matrixIndex + 2] * blendWeight;

	blendWeight = weight.y;
	if (blendWeight > 0.0)
	{
		matrixIndex = Indexs.y;
		matrixPalette1 += matrixPalette[matrixIndex] * blendWeight;
		matrixPalette2 += matrixPalette[matrixIndex + 1] * blendWeight;
		matrixPalette3 += matrixPalette[matrixIndex + 2] * blendWeight;
	}

	Vec4 _skinnedPosition;
	Vec4 cposition = Vec4(position.x, position.y, position.z, 1.0);
	_skinnedPosition.x = Vec4::dot(cposition, matrixPalette1);
	_skinnedPosition.y = Vec4::dot(cposition, matrixPalette2);
	_skinnedPosition.z = Vec4::dot(cposition, matrixPalette3);
	_skinnedPosition.w = cposition.w;

	return _skinnedPosition;
}

void Chw3D_RMesh::cacleVertPos(Vec3* srcPos, Vec3* outPos, float* weight, unsigned char* index, Mat4* matrixs)
{
	Mat4* pM;
	float fW = 0;
	Vec3 v(0,0,0);
	Vec3* pSrc = srcPos;

	float fWeight1 = weight[0];
	// 找到影响该顶点的骨骼节点1，乘以骨骼的变换矩阵
	if (fWeight1 > 0.0f)
	{
		unsigned char index1 = index[0];
		pM = &matrixs[index1];
		fW = pSrc->x * pM->m[3] + pSrc->y * pM->m[7] + pSrc->z * pM->m[11] + pM->m[15];
		v.x = (pSrc->x * pM->m[0] + pSrc->y * pM->m[4] + pSrc->z * pM->m[8] + pM->m[12]) / fW * fWeight1;
		v.y = (pSrc->x * pM->m[1] + pSrc->y * pM->m[5] + pSrc->z * pM->m[9] + pM->m[13]) / fW * fWeight1;
		v.z = (pSrc->x * pM->m[2] + pSrc->y * pM->m[6] + pSrc->z * pM->m[10] + pM->m[14]) / fW * fWeight1;

	}

	float fWeight2 = weight[1];
	if (fWeight2 > 0.0f)
	{
		unsigned char index2 = index[1];
		pM = &matrixs[index2];
		fW = pSrc->x * pM->m[3] + pSrc->y * pM->m[7] + pSrc->z * pM->m[11] + pM->m[15];
		v.x += (pSrc->x * pM->m[0] + pSrc->y * pM->m[4] + pSrc->z * pM->m[8] + pM->m[12]) / fW * fWeight2;
		v.y += (pSrc->x * pM->m[1] + pSrc->y * pM->m[5] + pSrc->z * pM->m[9] + pM->m[13]) / fW * fWeight2;
		v.z += (pSrc->x * pM->m[2] + pSrc->y * pM->m[6] + pSrc->z * pM->m[10] + pM->m[14]) / fW * fWeight2;
	}

	// 根据混合权重计算节点的最终位置
	outPos->x = v.x;
	outPos->y = v.y;
	outPos->z = v.z;

}


Vec3* Chw3D_RMesh::getNormalLines(size_t count, Chw3D_Mesh_Sub* pSub, Vec3* pPoses)
{
	Vec3* normals = new Vec3[pSub->m_dwVerts];

	vector<Vec3>* normalCools = new vector<Vec3>[pSub->m_dwVerts];

	for (size_t i = 0; i < count; i += 3)
	{
		Vec3 pos1 = pPoses[pSub->m_tris[i]];
		Vec3 pos2 = pPoses[pSub->m_tris[i + 1]];
		Vec3 pos3 = pPoses[pSub->m_tris[i + 2]];

		pos2.subtract(pos1);
		pos3.subtract(pos1);
		pos2.cross(pos3);
		pos2.normalize();

		normalCools[pSub->m_tris[i]].push_back(pos2);
		normalCools[pSub->m_tris[i + 1]].push_back(pos2);
		normalCools[pSub->m_tris[i + 2]].push_back(pos2);
	}

	for (size_t i = 0; i < pSub->m_dwVerts; i++)
	{
		vector<Vec3> single = normalCools[i];
		size_t aa = single.size();
		for (size_t j = single.size(); j > 0; j--)
		{
			for (size_t k = 0; k < j - 1; k++)
			{
				Vec3 normalA = single[k];
				Vec3 normalB = single[k + 1];

				normalA.x += normalB.x;
				normalA.y += normalB.y;
				normalA.z += normalB.z;

				normalA.normalize();

				single[k] = normalA;
			}
		}

		normals[i] = single[0];
		single.clear();
	}

	delete[] normalCools;

	return normals;
}

void Chw3D_RMesh::skiningMesh(Chw3D_Sprite* pSprite, Chw3D_Mesh_Sub* pSub, Chw3D_RMesh_Sub* pRSub)
{
	if (pSprite->m_pA && pSprite->isInit())
	{
		
		if (pSprite->isHardwareSkinMesh() && pSprite->isInit() && pSprite->m_pA->getBonesCount() > 0)
		{
			// 混合蒙皮
			if (pSprite->m_pA->m_nodes.size() > SKINNING_JOINT_COUNT)
			{
				for (unsigned int v = 0; v < pSub->m_dwVerts; ++v)
				{
					float index1 = pSub->m_blends[v * 4 + 0];
					float index2 = pSub->m_blends[v * 4 + 1];
					// 骨骼数组有限, 前半部分GPU蒙皮, 后半部分CUP蒙皮
					if (index1 < SKINNING_JOINT_COUNT && index2 < SKINNING_JOINT_COUNT)
					{
						pRSub->m_Poses[v].x = pSub->m_poses[v].x;
						pRSub->m_Poses[v].y = pSub->m_poses[v].y;
						pRSub->m_Poses[v].z = pSub->m_poses[v].z;
					}
					else
					{
						cacleVertPos(&pSub->m_poses[v], &pRSub->m_Poses[v], &pSub->m_weights[v * 4], &pSub->m_blends[v * 4], pSprite->m_matrixs);
					}
				}
			}
			else
			{
				// 纯GPU蒙皮
				memcpy(&pRSub->m_Poses[0], &pSub->m_poses[0], sizeof(Vec3) * pSub->m_dwVerts);

			}
		}
		else
		{
						
			// cpu蒙皮
			if (pRSub->m_Normals) {


				for (unsigned int v = 0; v < pSub->m_dwVerts; ++v)
				{
					cacleVertPos(&pSub->m_poses[v], &pRSub->m_Poses[v], &pSub->m_weights[v * 4], &pSub->m_blends[v * 4], pSprite->m_matrixs);
					cacleVertPos(&pSub->m_normals[v], &pRSub->m_Normals[v], &pSub->m_weights[v * 4], &pSub->m_blends[v * 4], pSprite->m_matrixs);
					//memcpy(&pRSub->m_Normals[0], &pSub->m_Normals[0], sizeof(Vec3) * pSub->m_dwVerts);
					pRSub->m_Normals[v].normalize();
				}
			}
			else {

				for (unsigned int v = 0; v < pSub->m_dwVerts; ++v)
				{
					cacleVertPos(&pSub->m_poses[v], &pRSub->m_Poses[v], &pSub->m_weights[v * 4], &pSub->m_blends[v * 4], pSprite->m_matrixs);

				}
			}
		}
	}
}

void Chw3D_RMesh::draw(Chw3D_Sprite* pSprite, bool bBuild, Vec4 color)
{
	CC_PROFILER_START_CATEGORY(kCCProfilerCategorySprite, "Chw3D_RMesh - draw");

	// 摄像机
	pSprite->m_pCamera->Active();

	// m_Matrix表示Sprite自身的变换，M（Node）表示影响模型的骨骼的变换矩阵，
	// 则 这行语句执行 m_Matrix * M(Node)，将Sprite的变换加上骨骼对它的影响
	// 关于M（Node）的计算过程，详见Chw3D_Sprite::Build()
	Mat4 Matrix;
	pSprite->FinialMatrix(m_pMesh->m_dwNodeIndex, &Matrix);
	//kmGLMultMatrix(&Matrix);
	Director::getInstance()->multiplyMatrix(MATRIX_STACK_TYPE::MATRIX_STACK_MODELVIEW, Matrix);
	/*kmGLMultMatrix(pMatrix);
	kmGLMultMatrix(&pSprite->m_Matrixs[m_pMesh->m_dwNodeIndex]);*/
	auto __renderer__ = Director::getInstance()->getRenderer();
	for(unsigned int n = 0; n < m_pMesh->m_subs.size(); ++n)
	{
		Chw3D_Mesh_Sub* pSub = m_pMesh->m_subs[n];
		if (!pSub)
		{
			break;
		}
		__renderer__->addDrawnVertices(pSub->m_dwVerts);
		// 顶点蒙皮
		if (m_subs.size() > n)
		{
			skiningMesh(pSprite, pSub, m_subs[n]);
		}
		
		//CHECK_GL_ERROR_DEBUG();
		if (pSprite->isHardwareSkinMesh() && pSprite->isInit() && pSprite->m_pA->getBonesCount() > 0)
		{
			setGLProgram(GLProgramCache::getInstance()->getGLProgram(GLProgram::SHADER_3D_Higame3D_Skin));
			getGLProgram()->use();
			getGLProgram()->setUniformsForBuiltins();
			
			GL::enableVertexAttribs(GL::VERTEX_ATTRIB_FLAG_POS_COLOR_TEX);
			glEnableVertexAttribArray(GLProgram::VERTEX_ATTRIB_BLEND_WEIGHT);
			glEnableVertexAttribArray(GLProgram::VERTEX_ATTRIB_BLEND_INDEX);
			//CHECK_GL_ERROR_DEBUG();
			glVertexAttribPointer(GLProgram::VERTEX_ATTRIB_BLEND_WEIGHT, 4, GL_FLOAT, GL_TRUE, 0, (void*)&pSub->m_weights[0]);
			glVertexAttribPointer(GLProgram::VERTEX_ATTRIB_BLEND_INDEX, 4, GL_UNSIGNED_BYTE, GL_FALSE, 0, (void*)&pSub->m_blends[0]);
			//CHECK_GL_ERROR_DEBUG();
			auto boneUni = getGLProgram()->getUniformLocation("u_bones");
			//CHECK_GL_ERROR_DEBUG();
			int boneCount = std::min((unsigned int)pSprite->m_pA->m_nodes.size(), SKINNING_JOINT_COUNT);
			getGLProgram()->setUniformLocationWithMatrix4fv(boneUni, (GLfloat*)&pSprite->m_matrixs[0], boneCount);
			//CHECK_GL_ERROR_DEBUG();
		}
		else
		{
			setGLProgram(GLProgramCache::getInstance()->getGLProgram(pSub->m_useProgramName));
			//setGLProgram(m_glProgram);
			getGLProgram()->use();
			getGLProgram()->setUniformsForBuiltins();
			GL::enableVertexAttribs(GL::VERTEX_ATTRIB_FLAG_POS_COLOR_TEX);
			CHECK_GL_ERROR_DEBUG();
		}

		// texture
		GL::bindTexture2D(pSub->m_material.m_pTexture->getName());

		if (pSub->m_material.m_pSpecularTexture) {

		
			GL::bindTexture2DN(1, pSub->m_material.m_pSpecularTexture->getName());

			GLint u_specular = glGetUniformLocation(getGLProgram()->getProgram(), "u_specular");
			getGLProgram()->setUniformLocationWith1f(u_specular, pSub->m_material.m_specular);

		}

		if (pSub->m_material.m_hasFlowLight) {

			if (pSub->m_material.m_pFlowLightTexture) {
				GL::bindTexture2DN(2, pSub->m_material.m_pFlowLightTexture->getName());
			}
			GLint u_width = glGetUniformLocation(getGLProgram()->getProgram(), "u_light_width");
			getGLProgram()->setUniformLocationWith1f(u_width, pSub->m_material.m_lightWidth);

			GLint u_speed = glGetUniformLocation(getGLProgram()->getProgram(), "u_light_speed");
			getGLProgram()->setUniformLocationWith2fv(u_speed, (GLfloat*)&pSub->m_material.m_lightSpeed, 1);

			GLint u_time = glGetUniformLocation(getGLProgram()->getProgram(), "u_light_time");
			getGLProgram()->setUniformLocationWith1f(u_time, pSprite->m_dSec);

			GLint u_color = glGetUniformLocation(getGLProgram()->getProgram(), "u_flow_light_color");
			getGLProgram()->setUniformLocationWith4fv(u_color, (GLfloat*)&pSub->m_material.m_lightColor, 1);

		}
		/*
		根据material算颜色
		*/
		Vec4 Color(1.0f, 1.0f, 1.0f, 1.0f);
		pSub->m_material.m_colorTracks.Query((float)pSprite->m_nFrame, &Color);
		Color.x *= color.x;
		Color.y *= color.y;
		Color.z *= color.z;
		Color.w *= color.w;

		// 乘上环境里取出来的值
		/*Color.r = Color.r * CFBGraphics::getSingleton().m_Envionment.m_Color.r;
		Color.g = Color.g * CFBGraphics::getSingleton().m_Envionment.m_Color.g;
		Color.b = Color.b * CFBGraphics::getSingleton().m_Envionment.m_Color.b;
		Color.a = Color.a * CFBGraphics::getSingleton().m_Envionment.m_Color.a;
		m_pFx->m_pEffect->SetFloatArray("g_Color", &Color.r, 4);*/

		//
		auto hasColorKeyUni = glGetUniformLocation(getGLProgram()->getProgram(), "u_has_colorkey");
		unsigned int mode = pSub->m_dwMode;
		if(Color.w < 1.0f && mode == 0)
		{
			mode = 2;
		}

		// mode
		switch(mode)
		{
		case 0:
			// normal
			glEnable(GL_DEPTH_TEST);
			glDepthMask(GL_TRUE);
			glDisable(GL_BLEND);
			glUniform1i(hasColorKeyUni, 0);

			break;
		case 1:
			// colorkey
			glEnable(GL_DEPTH_TEST);
			glDepthMask(GL_TRUE);

			glEnable(GL_BLEND);
			GL::blendFunc(GL_ONE, GL_ONE_MINUS_SRC_ALPHA);
			glUniform1i(hasColorKeyUni, 1);
			break;
		case 2:
			// alpha
			glEnable(GL_DEPTH_TEST);
			glDepthMask(GL_FALSE);

			glEnable(GL_BLEND);
			GL::blendFunc(GL_ONE, GL_ONE_MINUS_SRC_ALPHA);
			//glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
			glUniform1i(hasColorKeyUni, 0);
			break;
		case 3:
			// add
			glEnable(GL_DEPTH_TEST);
			glDepthMask(GL_FALSE);

			glEnable(GL_BLEND);
			GL::blendFunc(GL_ONE, GL_ONE);
			glUniform1i(hasColorKeyUni, 0);
			break;
		case 4:
			// srcalpha
			glEnable(GL_DEPTH_TEST);
			glDepthMask(GL_FALSE);

			glEnable(GL_BLEND);
			//GL::blendFunc(GL_ONE, GL_ONE_MINUS_SRC_ALPHA);
			glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
			glUniform1i(hasColorKeyUni, 0);
			break;
		}


		// position
		if (m_subs.size() > n)
		{
			glVertexAttribPointer(GLProgram::VERTEX_ATTRIB_POSITION, 3, GL_FLOAT, GL_FALSE, 0, (void*)&m_subs[n]->m_Poses[0]);
		}
		else
		{
			glVertexAttribPointer(GLProgram::VERTEX_ATTRIB_POSITION, 3, GL_FLOAT, GL_FALSE, 0, (void*)&pSub->m_poses[0]);
		}

		// texCoods
		//getGLProgramState()->setVertexAttribPointer(s_attributeNames[GLProgram::VERTEX_ATTRIB_TEX_COORD], 2, GL_FLOAT, GL_FALSE, 0, (GLvoid*)&pSub->m_UVs[0]);
		glVertexAttribPointer(GLProgram::VERTEX_ATTRIB_TEX_COORD, 2, GL_FLOAT, GL_FALSE, 0, (void*)&pSub->m_uvs[0]);
 		// color
		//getGLProgramState()->setVertexAttribPointer(s_attributeNames[GLProgram::VERTEX_ATTRIB_COLOR], 4, GL_UNSIGNED_BYTE, GL_TRUE, 0, (GLvoid*)&pSub->m_Colors[0]);
		glVertexAttribPointer(GLProgram::VERTEX_ATTRIB_COLOR, 4, GL_UNSIGNED_BYTE, GL_TRUE, 0, (void*)&pSub->m_colors[0]);


		
		if (pSub->m_hasNormal) {

			Vec3* normals;

			glEnableVertexAttribArray(GLProgram::VERTEX_ATTRIB_NORMAL);

			if (m_subs.size() > n){

				/*if (m_pMesh->m_Subs[n]->m_dwNormalCount == 0) {

					m_pMesh->m_Subs[n]->m_Normals = getNormalLines(m_pMesh->m_Subs[n]->m_dwTris * 3, pSub, m_Subs[n]->m_Poses);
					m_pMesh->m_Subs[n]->m_dwNormalCount = pSub->m_dwVerts;
					
				}*/
				//m_pMesh->m_Subs[n]->m_Normals = getNormalLines(m_pMesh->m_Subs[n]->m_dwTris * 3, pSub, m_Subs[n]->m_Poses);
				normals = m_subs[n]->m_Normals;
			}
			else {

				/*if (pSub->m_dwNormalCount == 0) {
					pSub->m_Normals = getNormalLines(pSub->m_dwTris * 3, pSub, pSub->m_Poses);
					pSub->m_dwNormalCount = pSub->m_dwVerts;
					
				}*/
				//pSub->m_Normals = getNormalLines(pSub->m_dwTris * 3, pSub, pSub->m_Poses);
				normals = pSub->m_normals;
			}

			glVertexAttribPointer(GLProgram::VERTEX_ATTRIB_NORMAL, 3, GL_FLOAT, GL_FALSE, 0, (void*)&normals[0]);

		}
//		else
//			normals = getNormalLines(m_pMesh->m_Subs[n]->m_dwTris * 3, pSub, pSub->m_Poses);

		

		//
		m_nUniformColor = glGetUniformLocation(getGLProgram()->getProgram(), "u_color");
		m_nUniformUVTrans = glGetUniformLocation(getGLProgram()->getProgram(), "u_uvtrans");

		/*
		根据material算uv
		*/
		// UV
		Vec2 UVTrans(0,0);
		if (pSub->m_material.m_uvStepTracks.Keys() > 0) {
			pSub->m_material.m_uvStepTracks.Query((float)pSprite->m_nFrame, &UVTrans);
		}
		else {
			//Vec2Scale(&UVTrans, &pSub->m_Material.m_UVTrans, pSprite->m_dSec);
			UVTrans = pSub->m_material.m_uvTrans * pSprite->m_dSec;
		}
	
		getGLProgram()->setUniformLocationWith2fv(m_nUniformUVTrans, (GLfloat*)&UVTrans, 1);

		//
		getGLProgram()->setUniformLocationWith4fv(m_nUniformColor, (GLfloat*)&Color, 1);

		//
		//glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
		glDrawElements(GL_TRIANGLES, m_pMesh->m_subs[n]->m_dwTris * 3, GL_UNSIGNED_SHORT, (void*)&pSub->m_tris[0]);
		//glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);

		CHECK_GL_ERROR_DEBUG();
	}

	// 摄像机
	pSprite->m_pCamera->Inactive();

	// mode
	glDisable(GL_DEPTH_TEST);
	glDepthMask(GL_TRUE);

	glEnable(GL_BLEND);
	GL::blendFunc(GL_ONE, GL_ONE_MINUS_SRC_ALPHA);

	//
    CC_INCREMENT_GL_DRAWS(1);

    CC_PROFILER_STOP_CATEGORY(kCCProfilerCategorySprite, "Chw3D_RMesh - draw");
}
