#include "hw3D_Line.h"

Chw3D_Line::Chw3D_Line()
{
}
Chw3D_Line::~Chw3D_Line()
{
}

void Chw3D_Line::LoadEx(ChwCore_Stream* pStream, bool isAsync)
{
	// 名字
	unsigned int dwLen;
	pStream->Read(&dwLen, sizeof(unsigned int));
	if(dwLen)
	{
		m_fileName.resize(dwLen);
		pStream->Read(&m_fileName[0], sizeof(char) * dwLen);
	}

	// 模式
	pStream->Read(&m_dwMode, sizeof(unsigned int));

	// 这个Sub使用骨骼索引，同父模型
	pStream->Read(&m_dwNodeIndex, sizeof(unsigned int));

	//
	pStream->Read(m_knots, sizeof(Vec3) * 2);
	pStream->Read(&m_dMaxLife, sizeof(double));
	pStream->Read(&m_fWidth, sizeof(float));

	// 材质
	m_material.LoadEx(pStream, isAsync);
}

Chw3D_Material*	Chw3D_Line::getMaterial()
{
	return &m_material;
}