#ifndef _HiGameSdkAndroid_H_
#define _HiGameSdkAndroid_H_

#if (CC_TARGET_PLATFORM == CC_PLATFORM_ANDROID)
//接口定义
#include "cocos2d.h"

class HiGameSdkAndroid:public cocos2d::Ref
{

public:

	typedef std::function<void(int callbackType, int resultCode, std::string& data)> SDKCallback;

	HiGameSdkAndroid();
	virtual ~HiGameSdkAndroid();

	static HiGameSdkAndroid* getInstance();
	static void destroyInstance();

	void initSDK();


	void onCallback(int callbackType, int resutlCode, std::string& data);
	void setSDKCallback(SDKCallback _callback);

	void login(std::string& params);
	void logout(std::string& params);
	void switchAccount(std::string& params);
	void exit();
	void deal(std::string& params);
	void share(std::string& params);
	void callOtherFunction(int type, std::string& params);
	void submitExtendData(std::string& params);
	std::string getValue(std::string& key);
	bool isLogined();
	bool isInitSDK();
	std::string getUserID();
	std::string getAppInfo();

	std::string getMainChannel();
	std::string getSubChannel();
	int getLogicChannel();
	int getAppID();

	SDKCallback _sdkCallback;

};

#endif /* #if (CC_TARGET_PLATFORM == CC_PLATFORM_ANDROID) */
#endif // _HiGameSdkAndroid_H_
