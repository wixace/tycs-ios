#ifndef _HiGameSDK_H_
#define _HiGameSDK_H_

#define HIGAME_COCOS2DX_VERSION "1.0"
//接口定义
#include "cocos2d.h"

class CocosGameCallback {
    
public:
    
    typedef std::function<void(int, int, std::string&)> onCocosCallback;
    
    CocosGameCallback(const onCocosCallback& callback);
    void onCallback(int callbackCode, int resultCode, const char *data);

private:
    onCocosCallback m_callback;
};


class HiGameSDK:public cocos2d::Ref
{
public:

	enum class SDKCallbackType
	{
		Init,
	};


	typedef std::function<void(int callbackType, int resultCode, std::string& data)> SDKCallback;

	HiGameSDK();
	virtual ~HiGameSDK();

	static HiGameSDK* getInstance();
	static void destroyInstance();


	const std::string& getConfig();

	void setCallback(SDKCallback callback);
	void onCallback(int callbackType, int resultCode, std::string& data);

	void initSDK();
	void login(std::string& params);
	void logout(std::string& params);
	void switchAccount(std::string& params);
	void exit();
	void deal(std::string& params);
	void share(std::string& params);
	void callOtherFunction(int type, std::string& params);



	void submitExtendData(std::string& params);
	const std::string& getValue(std::string& key);
	bool isLogined();
	const std::string& getUserID();
	std::string getAppInfo();
	std::string getMainChannel();
	std::string getSubChannel();
	int getLogicChannel();
	int getAppID();
	bool isInitSDK();
public:
	std::map<int, unsigned int>	_callbackFuncs;
	SDKCallback _callback;
	std::string _config;
	std::string _tmpValue;
	std::string _userID;
    CocosGameCallback* _bridgeCallback;
};


#endif // _HiGameSDK_H_
