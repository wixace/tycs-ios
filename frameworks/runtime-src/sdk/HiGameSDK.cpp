#include "HiGameSDK.h"
#include "CCLuaEngine.h"
#include <stdlib.h>

#if (CC_TARGET_PLATFORM == CC_PLATFORM_ANDROID)
#include "HiGameSDK_Android.h"
#elif (CC_TARGET_PLATFORM == CC_PLATFORM_IOS)
#include "GameSDKHandle_iOS.h"
#endif

CocosGameCallback::CocosGameCallback(const onCocosCallback& callback)
{
    m_callback = callback;
}

void CocosGameCallback::onCallback(int callbackCode, int resultCode, const char *data)
{
    if(m_callback){
        std::string d = std::string(data);
        m_callback(callbackCode, resultCode, d);
    }
}

static HiGameSDK* sharedSDK = nullptr;
HiGameSDK* HiGameSDK::getInstance()
{
	if (!sharedSDK) {
		sharedSDK = new (std::nothrow) HiGameSDK();
	}
	return sharedSDK;
}

void HiGameSDK::destroyInstance()
{
    if(sharedSDK != nullptr)
    {
        CC_SAFE_DELETE(sharedSDK);
    }

}

HiGameSDK::HiGameSDK():
	_callback(nullptr),
    _bridgeCallback(nullptr)
{
}

HiGameSDK::~HiGameSDK()
{
    if(_bridgeCallback){
        delete _bridgeCallback;
    }
}


const std::string& HiGameSDK::getConfig()
{
	if (cocos2d::FileUtils::getInstance()->isFileExist(std::string("game.json")))
	{
		_config = cocos2d::FileUtils::getInstance()->getStringFromFile(std::string("game.json"));
	}
	else
	{

		_config = std::string("");

	}
	return _config;
} 

void HiGameSDK::setCallback(SDKCallback callback)
{
	_callback = callback;
#if (CC_TARGET_PLATFORM == CC_PLATFORM_ANDROID)

	HiGameSdkAndroid::getInstance()->setSDKCallback([=](int callbackType, int resultCode, std::string& data)
	{
		onCallback(callbackType, resultCode, data);
	});

	if (HiGameSdkAndroid::getInstance()->isInitSDK())
	{
		std::string data = std::string("");
		onCallback(0, 1, data);
	}

#elif (CC_TARGET_PLATFORM == CC_PLATFORM_IOS)
    
    if(_bridgeCallback == nullptr)
    {
        _bridgeCallback = new CocosGameCallback([=](int callbackType, int resultCode, std::string& data)
        {
            onCallback(callbackType, resultCode, data);
        });
    }
    GameSDKHandleiOS::setSDKCalllback(_bridgeCallback);
    if (GameSDKHandleiOS::isInitSDK())
    {
        std::string data = std::string("");
        onCallback(0, 1, data);
    }
    //GameSDKHandleiOS::initSDK();
    
#else

#endif
}


void HiGameSDK::onCallback(int callbackType, int resultCode, std::string& data)
{
	if (_callback)
	{
		_callback(callbackType, resultCode, data);
	}
}

void HiGameSDK::initSDK()
{
#if (CC_TARGET_PLATFORM == CC_PLATFORM_ANDROID)

	HiGameSdkAndroid::getInstance()->initSDK();

#elif (CC_TARGET_PLATFORM == CC_PLATFORM_IOS)
    GameSDKHandleiOS::initSDK();
#else
	std::string data = std::string("");
	onCallback(0, 1, data);
#endif
}

void HiGameSDK::login(std::string& params)
{
#if (CC_TARGET_PLATFORM == CC_PLATFORM_ANDROID)
	HiGameSdkAndroid::getInstance()->login(params);
#elif (CC_TARGET_PLATFORM == CC_PLATFORM_IOS)
    GameSDKHandleiOS::login(params);
#else

#endif
}

void HiGameSDK::logout(std::string& params)
{
#if (CC_TARGET_PLATFORM == CC_PLATFORM_ANDROID)
	HiGameSdkAndroid::getInstance()->logout(params);
#elif (CC_TARGET_PLATFORM == CC_PLATFORM_IOS)
    GameSDKHandleiOS::logout(params);
#else

#endif
}

void HiGameSDK::switchAccount(std::string& params)
{
#if (CC_TARGET_PLATFORM == CC_PLATFORM_ANDROID)
	HiGameSdkAndroid::getInstance()->switchAccount(params);
#elif (CC_TARGET_PLATFORM == CC_PLATFORM_IOS)
    GameSDKHandleiOS::switchAccount(params);
#else

#endif
}

void HiGameSDK::exit()
{
#if (CC_TARGET_PLATFORM == CC_PLATFORM_ANDROID)
	HiGameSdkAndroid::getInstance()->exit();
#elif (CC_TARGET_PLATFORM == CC_PLATFORM_IOS)
    GameSDKHandleiOS::exit();
#else

#endif
}

void HiGameSDK::deal(std::string& params)
{
#if (CC_TARGET_PLATFORM == CC_PLATFORM_ANDROID)
	HiGameSdkAndroid::getInstance()->deal(params);
#elif (CC_TARGET_PLATFORM == CC_PLATFORM_IOS)
    GameSDKHandleiOS::deal(params);
#else

#endif
}

void HiGameSDK::share(std::string& params)
{
#if (CC_TARGET_PLATFORM == CC_PLATFORM_ANDROID)
	HiGameSdkAndroid::getInstance()->share(params);
#elif (CC_TARGET_PLATFORM == CC_PLATFORM_IOS)
    GameSDKHandleiOS::share(params);
#else

#endif
}

void HiGameSDK::callOtherFunction(int type, std::string& params)
{
#if (CC_TARGET_PLATFORM == CC_PLATFORM_ANDROID)
	HiGameSdkAndroid::getInstance()->callOtherFunction(type, params);
#elif (CC_TARGET_PLATFORM == CC_PLATFORM_IOS)
    GameSDKHandleiOS::callOtherFunction(type, params);
#else

#endif
}

void HiGameSDK::submitExtendData(std::string& params)
{
#if (CC_TARGET_PLATFORM == CC_PLATFORM_ANDROID)
	HiGameSdkAndroid::getInstance()->submitExtendData(params);
#elif (CC_TARGET_PLATFORM == CC_PLATFORM_IOS)
    GameSDKHandleiOS::submitExtendData(params);
#else

#endif
}

const std::string& HiGameSDK::getValue(std::string& key)
{
	#if (CC_TARGET_PLATFORM == CC_PLATFORM_ANDROID)
		_tmpValue = HiGameSdkAndroid::getInstance()->getValue(key);
	#elif (CC_TARGET_PLATFORM == CC_PLATFORM_IOS)
        _tmpValue = GameSDKHandleiOS::getValue(key);
	#else
		_tmpValue = std::string("");
	#endif

	return _tmpValue;
}

bool HiGameSDK::isLogined()
{
	#if (CC_TARGET_PLATFORM == CC_PLATFORM_ANDROID)
		return HiGameSdkAndroid::getInstance()->isLogined();
	#elif (CC_TARGET_PLATFORM == CC_PLATFORM_IOS)
        return GameSDKHandleiOS::isLogined();
	#else
		return true;
	#endif
}

const std::string& HiGameSDK::getUserID()
{
	#if (CC_TARGET_PLATFORM == CC_PLATFORM_ANDROID)
		_userID = HiGameSdkAndroid::getInstance()->getUserID();
	#elif (CC_TARGET_PLATFORM == CC_PLATFORM_IOS)
        _userID = GameSDKHandleiOS::getUserID();
	#else
		_userID = std::string("");                                                                                                               
	#endif

	return _userID;
}

std::string HiGameSDK::getAppInfo()
{
#if (CC_TARGET_PLATFORM == CC_PLATFORM_ANDROID)
	return HiGameSdkAndroid::getInstance()->getAppInfo();
#elif (CC_TARGET_PLATFORM == CC_PLATFORM_IOS)
    return GameSDKHandleiOS::getAppInfo();
#else
	return std::string("");
#endif
}

std::string HiGameSDK::getMainChannel()
{
#if (CC_TARGET_PLATFORM == CC_PLATFORM_ANDROID)
	return HiGameSdkAndroid::getInstance()->getMainChannel();
#elif (CC_TARGET_PLATFORM == CC_PLATFORM_IOS)
    return GameSDKHandleiOS::getMainChannel();
#else
	return std::string("higame");
#endif
}

std::string HiGameSDK::getSubChannel()
{
#if (CC_TARGET_PLATFORM == CC_PLATFORM_ANDROID)
	return HiGameSdkAndroid::getInstance()->getSubChannel();
#elif (CC_TARGET_PLATFORM == CC_PLATFORM_IOS)
    return GameSDKHandleiOS::getSubChannel();
#else
	return std::string("0");
#endif
}

int HiGameSDK::getLogicChannel()
{
#if (CC_TARGET_PLATFORM == CC_PLATFORM_ANDROID)
	return HiGameSdkAndroid::getInstance()->getLogicChannel();
#elif (CC_TARGET_PLATFORM == CC_PLATFORM_IOS)
    return GameSDKHandleiOS::getLogicChannel();
#else
	return 0;
#endif
}


int HiGameSDK::getAppID()
{
#if (CC_TARGET_PLATFORM == CC_PLATFORM_ANDROID)
	return HiGameSdkAndroid::getInstance()->getAppID();
#elif (CC_TARGET_PLATFORM == CC_PLATFORM_IOS)
    return GameSDKHandleiOS::getAppID();
#else
	return 0;
#endif
}

bool HiGameSDK::isInitSDK()
{
#if (CC_TARGET_PLATFORM == CC_PLATFORM_ANDROID)
	return HiGameSdkAndroid::getInstance()->isInitSDK();
#elif (CC_TARGET_PLATFORM == CC_PLATFORM_IOS)
    return GameSDKHandleiOS::isInitSDK();
#else
	return 0;
#endif

}
