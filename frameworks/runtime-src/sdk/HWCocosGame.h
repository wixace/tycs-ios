//
//  HWCocosGame.h
//  GameBridgeSDK
//
//  Created by higame on 2018/8/23.
//  Copyright © 2018年 higame. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "HiGameSDK.h"


class CocosGameCallback;
@interface HWCocosGame : NSObject
{
    CocosGameCallback *_cocos2dCallback;
}
+ (HWCocosGame *)shareInstance;
-(void)addNotifications;
-(void) HWCocosGameonCallback:(NSNotification *)notify;
-(void) HWCocosGamesetSDKCalllback:(CocosGameCallback*)callback;
-(void) HWCocosGameinitSDK;
-(void) HWCocosGamelogin:(NSString*)params;
-(void) HWCocosGamelogout:(NSString*)params;
-(void) HWCocosGameswitchAccount:(NSString*)params;
-(void) HWCocosGameexit;
-(void) HWCocosGamedeal:(NSString*)params;
-(void) HWCocosGameshare:(NSString*)params;
-(void) HWCocosGamecallOtherFunction:(int)type params:(NSString*)params;
-(void) HWCocosGamesubmitExtendData:(NSString*)params;
-(NSString*) HWCocosGamegetValue:(NSString*)params;
-(bool) HWCocosGameisLogined;
-(bool) HWCocosGameisInitSDK;
-(NSString*) HWCocosGamegetUserID;
-(NSString*) HWCocosGamegetAppInfo;

-(NSString*) HWCocosGamegetMainChannel;
-(NSString*) HWCocosGamegetSubChannel;
-(int) HWCocosGamegetLogicChannel;
-(int) HWCocosGamegetAppID;

@end
