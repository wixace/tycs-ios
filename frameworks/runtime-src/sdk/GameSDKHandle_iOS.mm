#include "GameSDKHandle_iOS.h"

#if (CC_TARGET_PLATFORM == CC_PLATFORM_IOS)

#include "json/rapidjson.h"
#include "json/writer.h"
#include "json/document.h"
#include "json/prettywriter.h"
#include "json/stringbuffer.h"
#include <stdlib.h>
#include<iostream>
#include<sstream>
#include "base/ccUTF8.h"

#include "HiGameSDK.h"
#include "CCLuaEngine.h"


#include "HWCocosGame.h"

using namespace rapidjson;
using namespace cocos2d;



void GameSDKHandleiOS::initSDK()
{
    [[HWCocosGame shareInstance] HWCocosGameinitSDK];
}

void GameSDKHandleiOS::setSDKCalllback(CocosGameCallback* callback)
{
    [[HWCocosGame shareInstance] HWCocosGamesetSDKCalllback:callback];
}

void GameSDKHandleiOS::login(std::string& params)
{
    [[HWCocosGame shareInstance] HWCocosGamelogin:[NSString stringWithCString:params.c_str() encoding:NSUTF8StringEncoding]];
}

void GameSDKHandleiOS::logout(std::string& params)
{
    [[HWCocosGame shareInstance] HWCocosGamelogout:[NSString stringWithCString:params.c_str() encoding:NSUTF8StringEncoding]];
}

void GameSDKHandleiOS::switchAccount(std::string& params)
{
    [[HWCocosGame shareInstance] HWCocosGameswitchAccount:[NSString stringWithCString:params.c_str() encoding:NSUTF8StringEncoding]];
}

void GameSDKHandleiOS::exit()
{
    [[HWCocosGame shareInstance] HWCocosGameexit];
}

void GameSDKHandleiOS::deal(std::string& params)
{
    [[HWCocosGame shareInstance] HWCocosGamedeal:[NSString stringWithCString:params.c_str() encoding:NSUTF8StringEncoding]];
}

void GameSDKHandleiOS::share(std::string& params)
{
    [[HWCocosGame shareInstance] HWCocosGameshare:[NSString stringWithCString:params.c_str() encoding:NSUTF8StringEncoding]];
}

void GameSDKHandleiOS::callOtherFunction(int funcType, std::string& params)
{
    [[HWCocosGame shareInstance] HWCocosGamecallOtherFunction:funcType params:[NSString stringWithCString:params.c_str() encoding:NSUTF8StringEncoding]];
}

void GameSDKHandleiOS::submitExtendData(std::string& params)
{
    [[HWCocosGame shareInstance] HWCocosGamesubmitExtendData:[NSString stringWithCString:params.c_str() encoding:NSUTF8StringEncoding]];
}

std::string GameSDKHandleiOS::getValue(std::string& key)
{
    return std::string([[[HWCocosGame shareInstance] HWCocosGamegetValue:[NSString stringWithCString:key.c_str() encoding:NSUTF8StringEncoding]] UTF8String]);
}

bool GameSDKHandleiOS::isLogined()
{
    return [[HWCocosGame shareInstance] HWCocosGameisLogined];
}

bool GameSDKHandleiOS::isInitSDK()
{
    return [[HWCocosGame shareInstance] HWCocosGameisInitSDK];
}

std::string GameSDKHandleiOS::getUserID()
{
    return std::string([[[HWCocosGame shareInstance] HWCocosGamegetUserID] UTF8String]);
}

std::string GameSDKHandleiOS::getAppInfo()
{
    return std::string([[[HWCocosGame shareInstance] HWCocosGamegetAppInfo] UTF8String]);
}

std::string GameSDKHandleiOS::getMainChannel()
{
    return std::string([[[HWCocosGame shareInstance] HWCocosGamegetMainChannel] UTF8String]);
}

std::string GameSDKHandleiOS::getSubChannel()
{
    return std::string([[[HWCocosGame shareInstance] HWCocosGamegetSubChannel] UTF8String]);
}

int GameSDKHandleiOS::getLogicChannel()
{
    return [[HWCocosGame shareInstance] HWCocosGamegetLogicChannel];
}

int GameSDKHandleiOS::getAppID()
{
     return [[HWCocosGame shareInstance] HWCocosGamegetAppID];
}

#endif /* #if (CC_TARGET_PLATFORM == CC_PLATFORM_ANDROID) */
