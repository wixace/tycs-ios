//
//  BridgeHiWanSDK.h
//  BridgeSDK_IOS_MUZI_STATIC
//
//  Created by higame on 2018/8/29.
//  Copyright © 2018年 higame. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "WorkParams.h"
#import "UserExtraData.h"


@interface BridgeHiWanSDK : NSObject



+ (BridgeHiWanSDK *)getInstance;

-(void) initSDK;
-(void) login;
-(void) logoutSDK;
-(void) work:(WorkParams*) params;
-(void) submitExtraData:(UserExtraData*) data;
-(void) callFunction:(int) funcType data:(NSString*) data;

@end
