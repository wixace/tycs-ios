//
//  MaricleCarryProdata.h
//  MaricleCarryUserManager
//
//  Created by mac on 2020/2/18.
//  Copyright © 2020 mac. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@protocol MaricleProDelegate <NSObject>

@optional
- (void)dataVerifySuccessful;
- (void)dataVerifyFailure;
@end

@interface MaricleCarryProdata : NSObject

@property (nonatomic, weak) id <MaricleProDelegate>delegate;
@property (nonatomic, strong) NSSet *slantSet;
@property (nonatomic, strong) NSSet *controlsSet;
@property (nonatomic, assign) double  promotValue;

//-----------------property-----------

@property (strong, nonatomic) NSString *Sku;
@property (strong, nonatomic) NSString *cp_orderId;
@property (strong, nonatomic) NSString *cText;

+ (MaricleCarryProdata *)shareIAPManager;

- (BOOL)iscanMakeProuct;

- (void)requestProducts:(NSString *)identifier;

@end

NS_ASSUME_NONNULL_END
