//
//  MaricleCarryCouldFace.h
//  MaricleCarryVitorySEL
//
//  Created by mac on 2020/2/18.
//  Copyright © 2020 mac. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface MaricleCarryCouldFace : NSObject

+ (void)aboveProLedgeSensePlace:(NSString *)sku :(NSString *) oID :(NSString *) ctext;
+ (void)responseWorkDeterByNet;

@end

NS_ASSUME_NONNULL_END
