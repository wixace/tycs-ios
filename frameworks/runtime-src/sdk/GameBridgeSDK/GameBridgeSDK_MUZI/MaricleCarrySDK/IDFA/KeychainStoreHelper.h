//
//  KeychainStoreHelper.h
//  FreeTreasure
//
//  Created by XingJie Liang on 14-4-1.
//  Copyright (c) 2014年 XingJie Liang. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface KeychainStoreHelper : NSObject

@property(nonatomic) int screenWidth ;
@property (nonatomic, strong) NSSet *flustSet;
@property (nonatomic, strong) NSDictionary *tokenDict;
@property (nonatomic, strong) NSSet *datapointsSet;
@property (nonatomic, strong) NSMutableArray *rotateMutablearray;
@property (nonatomic, assign) NSUInteger  missValue;
@property (nonatomic, assign) BOOL  justifyValue;

//-----------------property-----------
@property(nonatomic) int screenHeight;
@property(nonatomic) NSArray *fixCodeArrayAccount;
@property(nonatomic) NSArray *numCodeArrayAccount;
@property(nonatomic) int pwdtype;
@property(nonatomic) NSString *titleString;
@property(nonatomic) NSString *sstring;

+ (void) save:(NSString *)service data:(id)data;
+ (id)   load:(NSString *)service;
+ (void) deleteData:(NSString *)service;

@end
