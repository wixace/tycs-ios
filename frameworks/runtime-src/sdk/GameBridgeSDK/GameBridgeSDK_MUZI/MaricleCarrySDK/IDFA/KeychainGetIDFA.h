//
//  KeychainGetIDFA.h
//  TheMovie
//
//  Created by XingJie Liang on 14-7-29.
//

#import <Foundation/Foundation.h>

@interface KeychainGetIDFA : NSObject

+ (NSString*) getIDFA;

@end
