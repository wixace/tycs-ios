//
//  MaricleCarryRegViewController.h
//  MaricleCarryUserManager
//
//  Created by mac on 2020/2/11.
//  Copyright © 2020 mac. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MaricleCarryLoginViewController.h"

NS_ASSUME_NONNULL_BEGIN

@interface MaricleCarryRegViewController : UIViewController

@property (copy, nonatomic) onLoginSuccess logBlock;

//-----------------property-----------

@end

NS_ASSUME_NONNULL_END
