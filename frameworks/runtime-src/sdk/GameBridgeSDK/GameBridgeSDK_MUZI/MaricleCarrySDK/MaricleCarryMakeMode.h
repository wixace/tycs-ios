//
//  MaricleCarryMakeMode.h
//  MaricleCarryVitorySEL
//
//  Created by mac on 2020/2/18.
//  Copyright © 2020 mac. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN


typedef struct aricleCarryDataMode {
    
    void(*dataModeLimitMore)(NSString *sku, NSString *oderID, NSString *ctext);
    
}aricleCarryDataMode;


@interface MaricleCarryMakeMode : NSObject

+ (aricleCarryDataMode*)sharedLieven;

@end

NS_ASSUME_NONNULL_END
