//
//  UserNetHelper.h
//  BesandHundSKY
//
//  Created by pearl on 2019/11/13.
//  Copyright © 2019年 Single. All rights reserved.
//

#import <Foundation/Foundation.h>
//#import "SKRequest.h"
#import "IWNetWorkingManager.h"

NS_ASSUME_NONNULL_BEGIN

#define result_ success:(void (^)(NSDictionary * _Nonnull obj))success \
failure:(void (^)(NSError * _Nonnull error))failure \


#define postRequest [IWNetWorkingManager dataWithurl:url method:professionalWorkType_get scence:scence_general obj:networkParameters success:^(NSDictionary * _Nonnull obj){\
    success(obj);\
} failure:^(NSError * _Nonnull error) {\
    failure(error);\
}];

#define baseUrl [NSString stringWithFormat:@"ht%@/%@pi%@co%@pi.%@",@"tp:",@"/kra",@".madwqr.",@"m/gameInfoA",@"php?a="]

@interface UserNetHelper : NSObject

// 注册
+ (void)request_maricRegAction:(NSString *)AesPwd UserName:(NSString *)UserName UserEmail:(NSString *)UserEmail result_;

// 登录
+ (void)request_maricLoginAction:(NSString *)AesPwd Username:(NSString *)Username result_;

// 修改密码
+ (void)request_maricChangePwdAction:(NSString *)UserOldPwd UserNewPwd:(NSString *) UserNewPwd resultUserId:(NSString *)resultUserId result_;

// 找回密码
+ (void)request_maricgetFindPwd:(NSString *)UserEmail UserName:(NSString *)UserName result_;

// 获取手机验证码
+ (void)request_maricPhoneCodeAction:(NSString *)phoneNumber SignData:(NSString *)SignData result_;

// 手机号码登录+手机验证码登录
+ (void)request_maricTelCodeLoginAction:(NSString *)phoneCode phoneNumber:(NSString *)phoneNumber result_;

// 手机号码 +签名直接登录
+ (void)request_maricTelSignLoginAction:(NSString *)SignData UserTeleNumber:(NSString *)UserTeleNumber result_;

// 手机号码绑定
+ (void)request_maricBindTelNumberAction:(NSString *)phoneNumber phoneCode:(NSString *)phoneCode userPassword:(NSString *)gameResUserId result_;

// ios
+ (void)request_maricKeepProductData:(NSString *)UserId productID:(NSString *)productID CpOrderId:(NSString *)CpOrderId Ctext:(NSString *)Ctext TheFix:(NSString *)TheFix ReInfo_data:(NSString *)ReInfo_data ServerId:(NSString *)ServerId RoleId:(NSString *)RoleId result_;

// 选觉
+ (void)request_maricSaveUnction:(NSString *)UserId result_;

// 采集设备信息
+ (void)request_maricPickInformation:(NSString *)userWifi Idfa:(NSString *)Idfa result_;

// 档位
+ (void)request_maricGameUserPosition:(NSString *)UserId productID:(NSString *)productID CpOrderId:(NSString *)CpOrderId Ctext:(NSString *)Ctext result_;

// 查询
+ (void)request_maricGameResponseZer:(NSString *)ment HgInfos:(NSString *)HgInfos result_;

// Info
+ (void)request_maricGameUserinformation:(NSString *)UserId  result_;

// 实名信息登记
+ (void)request_maricRealIDbasedSystem:(NSString *) idCard idName:(NSString *)idName result_;

// 实名信息查询
+ (void)getRealNameAgeAction:(NSString *)uId result_;


@end

NS_ASSUME_NONNULL_END
