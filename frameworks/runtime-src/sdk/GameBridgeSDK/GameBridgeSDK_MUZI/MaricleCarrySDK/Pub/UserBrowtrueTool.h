//
//  UserBrowtrueTool.h
//  BesandHundSKY
//
//  Created by pearl on 2019/11/13.
//  Copyright © 2019年 Single. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import <SystemConfiguration/CaptiveNetwork.h>


//AES加密的key
#define kAESKey @"hads^4327849##3AES#11"
//@"p&*Aauth^&5*1235"

NS_ASSUME_NONNULL_BEGIN

@interface UserBrowtrueTool : NSObject

+ (NSString *)getCurrentIDFA;
+ (NSString *)getKeychainIDFA;
+ (NSString *)strFromMD5:(NSString *)str;
+ (NSString *)aesEncryptFromString:(NSString *)str;
+ (NSString *)aesDecryptFromString:(NSString *)str;
+ (NSString *)getWifi;
+ (BOOL)isMessage;
+ (UIImage *)getImageFromLocalTrue:(NSString *)str;
- (void)pauseTimer;
- (void)resumeTimer;
- (void)resumeTimerAfterTimeInterval:(NSTimeInterval)interval;
//电话号码中间4位*显示
+ (NSString*) getSecrectStringWithPhoneNumber:(NSString*)phoneNum;

//银行卡号中间8位*显示
+ (NSString*) getSecrectStringWithAccountNo:(NSString*)accountNo;

//转为手机格式，默认为-
 + (NSString*) stringMobileFormat:(NSString*)mobile;

//金额数字添加单位（暂时写了万和亿，有更多的需求请参考写法来自行添加）
+ (NSString*) stringChineseFormat:(double)value;

//添加数字的千位符
+ (NSString*) countNumAndChangeformat:(NSString *)num;

// NSString转为NSNumber
- (NSNumber*) toNumber;

//计算文字高度
- (CGFloat  ) heightWithFontSize:(CGFloat)fontSize width:(CGFloat)width;

//计算文字宽度
- (CGFloat  ) widthWithFontSize:(CGFloat)fontSize height:(CGFloat)maxHeight;

//抹除小数末尾的0
- (NSString*) removeUnwantedZero;

//去掉前后空格
- (NSString*) trimmedString;

//通过时间戳计算时间差（几小时前、几天前）
+ (NSString *) compareCurrentTime:(NSTimeInterval) compareDate;

//通过时间戳得出显示时间
+ (NSString *) getDateStringWithTimestamp:(NSTimeInterval)timestamp;

//通过时间戳和格式显示时间
+ (NSString *) getStringWithTimestamp:(NSTimeInterval)timestamp formatter:(NSString*)formatter;

@end

NS_ASSUME_NONNULL_END
