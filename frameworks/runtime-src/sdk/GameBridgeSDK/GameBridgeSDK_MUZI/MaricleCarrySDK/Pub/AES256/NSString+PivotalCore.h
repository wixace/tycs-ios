#import <Foundation/NSString.h>

@interface NSString (PivotalCore)

/*
 Overrides the framework version of stringbyAddingPercentEscapesUsingEncoding,
 because the framework version does not escape several characters.  See the blog
 post at http://simonwoodside.com/weblog/2009/4/22/how_to_really_url_encode/ for
 details.
 */
- (NSString *)stringByAddingPercentEscapesUsingEncoding:(NSStringEncoding)encoding includeAll:(BOOL)includeAll;
@end
