//
//  NSError+CommonCryptoErrorDomain.h
//  LNGameAgency
//
//  Created by XingJie Liang on 14-3-6.
//  Copyright (c) 2014年 XingJie Liang. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CommonCrypto/CommonCryptor.h>


@interface NSError (CommonCryptoErrorDomain)
+ (NSError *) errorWithCCCryptorStatus: (CCCryptorStatus) status;

@end
