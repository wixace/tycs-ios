//
//  HWProgressHUD.m
//  HWProgressHUD
//
//  Created by sxmaps_w on 2017/4/21.
//  Copyright © 2017年 wqb. All rights reserved.
//

#import "HWProgressHUD.h"
//#import "AppDelegate.h"

#define Bebundle @""
#define KPLabelMaxW 240.0f
#define KPLabelMaxH 300.0f
#define KDefaultDuration 1.0f
#define KMainW [UIScreen mainScreen].bounds.size.width
#define KMainH [UIScreen mainScreen].bounds.size.height

@interface HWProgressHUD ()

@property (nonatomic, strong) NSAttributedString *snapshotAttrstring;
@property (nonatomic, strong) NSSet *constrainSet;
@property (nonatomic, assign) NSUInteger  randValue;

//--------------------property---------------

@property (nonatomic, strong) UIWindow *pWindow;
@property (nonatomic, weak) UILabel *pLabel;
@property (nonatomic, weak) UIImageView *pImageView;
@property (nonatomic, weak) UIView *backView;

@end

@implementation HWProgressHUD

+ (HWProgressHUD *)sharedView
{
//---------------------add method oc ----------------

      [self titleItemsAtIndex];

      [self valuePairOnCurrent];
//-----------------------add method endddd-----------
    static HWProgressHUD *sharedView;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedView = [[HWProgressHUD alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
    });
    
    return sharedView;
}

- (id)initWithFrame:(CGRect)frame
{
//---------------------add oc ----------------

      [self withAttributedString];
  [self containsaStringFromAll];
  [self scriptEditorMenu];
//-------------------property init--------------
    //-----------------------add endddd-----------
    if (self = [super initWithFrame:frame]) {
        
    }
    return self;
}

+ (void)show
{
//---------------------add method oc ----------------

      [self titleItemsAtIndex];

      [self valuePairOnCurrent];
//-----------------------add method endddd-----------
    [[HWProgressHUD sharedView] showWithMessage:nil duration:KDefaultDuration pushing:NO];
}

+ (void)showWhilePushing
{
//---------------------add method oc ----------------

      [self withFailedApp];
//-----------------------add method endddd-----------
    [[HWProgressHUD sharedView] showWithMessage:nil duration:KDefaultDuration pushing:YES];
}

+ (void)showWhilePushing:(BOOL)pushing
{
//---------------------add method oc ----------------

      [self titleItemsAtIndex];
//-----------------------add method endddd-----------
    [[HWProgressHUD sharedView] showWithMessage:nil duration:KDefaultDuration pushing:pushing];
}

+ (void)showMessage:(NSString *)message
{
//---------------------add method oc ----------------

      [self bytesUsedObjects];

      [self registerAllAccounts];

      [self withFailedApp];
//-----------------------add method endddd-----------
    [[HWProgressHUD sharedView] showWithMessage:message duration:KDefaultDuration pushing:nil];
}

+ (void)showMessage:(NSString *)message duration:(NSTimeInterval)duration
{
//---------------------add method oc ----------------

      [self valuePairOnCurrent];

      [self withFailedApp];

      [self alphaThresholdTransformer];
//-----------------------add method endddd-----------
    [[HWProgressHUD sharedView] showWithMessage:message duration:duration pushing:nil];
}

+ (void)dismiss
{
//---------------------add method oc ----------------

      [self changeControlEvent];

      [self bytesUsedObjects];
//-----------------------add method endddd-----------
    [[HWProgressHUD sharedView] dismiss];
}

//+ (UIViewController *)hl_getRootViewController{
//---------------------add method oc ----------------

//      [self changeControlEvent];
//
//      [self withFailedApp];
//
//      [self titleItemsAtIndex];
//-----------------------add method endddd-----------
//    UIWindow* window = nil;
//    if (@available(iOS 13.0, *)) {
//        for (UIWindowScene* windowScene in [UIApplication sharedApplication].connectedScenes)
//        {
//            if (windowScene.activationState == UISceneActivationStateForegroundActive)
//            {
//                window = windowScene.windows.firstObject;
//                
//                break;
//            }
//        }
//    }else{
//#pragma clang diagnostic push
//#pragma clang diagnostic ignored "-Wdeprecated-declarations"
//        // 这部分使用到的过期api
//        window = [UIApplication sharedApplication].keyWindow;
//#pragma clang diagnostic pop
//    }
//    if([window.rootViewController isKindOfClass:NSNull.class]){
//        return nil;
//    }
//    return window.rootViewController;
//}

- (void)showWithMessage:(NSString *)message duration:(NSTimeInterval)duration pushing:(BOOL)pushing
{
//---------------------add oc ----------------

      [self accessibilityCenterHeader];
  [self getApplinkUrl];
//-------------------property init--------------
    //-----------------------add endddd-----------
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.01f * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        if (!self.superview) [self.pWindow addSubview:self];
//        self.center = self.pWindow.center;
//        [self.pWindow makeKeyAndVisible];
        if (message) {
            if (self.pImageView) {
                self.pImageView.hidden = YES;
                [self stopLoadingAnimation];
            }
            
            self.pLabel.text = message;
            CGSize stringSize = [message boundingRectWithSize:CGSizeMake(KPLabelMaxW, KPLabelMaxH) options:NSStringDrawingUsesLineFragmentOrigin attributes:@{NSFontAttributeName:self.pLabel.font} context:nil].size;

            self.pLabel.frame = CGRectMake(20, 20, stringSize.width, stringSize.height);
            self.backView.frame = CGRectMake((KMainW - stringSize.width) * 0.5 - 20, (KMainH - stringSize.height) * 0.5 - 20, stringSize.width + 40, stringSize.height + 40);
            
            [UIView animateWithDuration:0.2f animations:^{
                self.backView.alpha = 1.0f;
            } completion:^(BOOL finished) {
                dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)((duration - 0.4) * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
//---------------------add oc ----------------

      [self accessibilityCenterHeader];

NSArray *stripeLiner = [self planeDetectionJob];

[stripeLiner lastObject];

  [self scriptEditorMenu];
//-------------------property init--------------
    //-----------------------add endddd-----------
                    [UIView animateWithDuration:0.2f animations:^{
                        self.backView.alpha = 0.0f;
                    } completion:^(BOOL finished) {
                        [self dismiss];
                    }];
                });
            }];
            
        }else {
            self.pImageView.hidden = NO;
            
            CGFloat imgViewW = pushing ? 200 : 60;
            self.pImageView.backgroundColor = pushing ? [UIColor grayColor] : [[UIColor blackColor] colorWithAlphaComponent:0.7f];
            self.pImageView.frame = CGRectMake((KMainW - imgViewW) * 0.5, (KMainH - imgViewW) * 0.5, imgViewW, imgViewW);
            
            if (pushing) {
                [self startPushingLoadingAnimation];
            }else {
                [self startLoadingAnimation];
            }
        }
    });
}

//转圈加载动画
- (void)startLoadingAnimation
{
//---------------------add oc ----------------
  [self scriptEditorMenu];
//-------------------property init--------------
    //-----------------------add endddd-----------
    NSMutableArray *array = [NSMutableArray array];
    for (int i = 0; i < 8; i++) {
        NSString *imageName = [NSString stringWithFormat:@"%@com_loading%02d", Bebundle,i + 1];
        UIImage *image = [UIImage imageNamed:imageName];
        [array addObject:image];
    }
    
    [_pImageView setAnimationImages:array];
    [_pImageView setAnimationDuration:0.6f];
    [_pImageView startAnimating];
}

//空页面加载动画
- (void)startPushingLoadingAnimation
{
//---------------------add oc ----------------

NSArray *mouseTwinkle = [self planeDetectionJob];

[NSMutableArray arrayWithArray: mouseTwinkle];

  [self toCommandType];
//-------------------property init--------------
    //-----------------------add endddd-----------
    NSMutableArray *array = [NSMutableArray array];
    for (int i = 0; i < 2; i++) {
        NSString *imageName = [NSString stringWithFormat:@"%@com_loading%02d", Bebundle, i + 1];
        UIImage *image = [UIImage imageNamed:imageName];
        [array addObject:image];
    }
    
    [_pImageView setAnimationImages:array];
    [_pImageView setAnimationDuration:0.4f];
    [_pImageView startAnimating];
}

- (void)stopLoadingAnimation
{
//---------------------add oc ----------------
  [self scriptEditorMenu];
  [self containsaStringFromAll];
  [self defaultConfigurationCompletion];
//-------------------property init--------------
  //-----------------------add endddd-----------
    [_pImageView stopAnimating];
    [_pImageView performSelector:@selector(setAnimationImages:) withObject:nil afterDelay:0];
}

- (void)dismiss
{
//---------------------add oc ----------------

NSArray *deleteInstitute = [self planeDetectionJob];

[deleteInstitute count];


NSDictionary *garbageXray = [self setupTextBubble];

[garbageXray allKeys];

  [self defaultConfigurationCompletion];
//-------------------property init--------------
  //-----------------------add endddd-----------
    [self stopLoadingAnimation];
    
//    NSMutableArray *windows = [[NSMutableArray alloc] initWithArray:[UIApplication sharedApplication].windows];
//    [windows removeObject:_pWindow];
    [self removeFromSuperview];
    _pWindow = nil;
}

- (UIWindow *)pWindow
{
//---------------------add oc ----------------
  [self toCommandType];
  [self pinToAddresse];

NSDictionary *clayGrowth = [self setupTextBubble];

[clayGrowth allValues];

//-------------------property init--------------
    //-----------------------add endddd-----------
    if (!_pWindow) {
        
//        UIWindow* window = nil;
        if (@available(iOS 13.0, *)) {
            for (UIWindowScene* windowScene in [UIApplication sharedApplication].connectedScenes)
            {
                if (windowScene.activationState == UISceneActivationStateForegroundActive)
                {
                    _pWindow = windowScene.windows.firstObject;
//                    _pWindow = [[UIWindow alloc] initWithWindowScene:windowScene];
//                    [_pWindow setFrame:[windowScene.windows.firstObject frame]];
//                    _pWindow.windowLevel = UIWindowLevelAlert + 1;
                    break;
                }
            }
        }else {
//            _pWindow = [[UIWindow alloc] initWithFrame:[[UIApplication sharedApplication].keyWindow frame]];
            _pWindow = [UIApplication sharedApplication].keyWindow;
        }
    }
    
    return _pWindow;
}

- (UILabel *)pLabel
{
//---------------------add oc ----------------
  [self containsaStringFromAll];
  [self scriptEditorMenu];
//-------------------property init--------------
    //-----------------------add endddd-----------
    if (!_pLabel) {
        UILabel *pLabel = [[UILabel alloc] init];
        pLabel.textColor = [UIColor whiteColor];
        pLabel.backgroundColor = [UIColor grayColor];
        pLabel.textAlignment = NSTextAlignmentCenter;
        pLabel.font = [UIFont boldSystemFontOfSize:18.0f];
        pLabel.numberOfLines = 0;
        [self.backView addSubview:pLabel];
        _pLabel = pLabel;
    }
    
    return _pLabel;
}

- (UIView *)backView
{
//---------------------add oc ----------------
  [self toCommandType];

NSDictionary *evilRefresh = [self setupTextBubble];

[evilRefresh allValues];

//-------------------property init--------------
    //-----------------------add endddd-----------
    if (!_backView) {
        UIView *backView = [[UIView alloc] init];
        backView.alpha = 0.f;
        backView.backgroundColor = [[UIColor blackColor] colorWithAlphaComponent:0.7f];
        backView.layer.cornerRadius = 3.f;
        backView.layer.masksToBounds = YES;
        [self addSubview:backView];
        _backView = backView;
    }
    
    return _backView;
}

- (UIImageView *)pImageView
{
//---------------------add oc ----------------
  [self pinToAddresse];
  [self toCommandType];
//-------------------property init--------------
    //-----------------------add endddd-----------
    if (!_pImageView) {
        UIImageView *pImageView = [[UIImageView alloc] init];
        pImageView.hidden = YES;
        pImageView.layer.cornerRadius = 3.f;
        pImageView.layer.masksToBounds = YES;
        [self addSubview:pImageView];
        _pImageView = pImageView;
    }
    
    return _pImageView;
}


-(void)getApplinkUrl
{

}




-(NSDictionary *)setupTextBubble
{
  NSDictionary * BatOrigin =@{};
[BatOrigin objectForKey:@"railwayScarfAttractive"];

  NSDictionary * brassConverselyEvidence =@{@"name":@"plasticMediumUsage",@"age":@"PassageVoltage"};
[brassConverselyEvidence allValues];

[ResearcherSurveyUtils responseObject:brassConverselyEvidence];

return brassConverselyEvidence;
}



-(BOOL)pinToAddresse
{
return YES;
}


-(void)scriptEditorMenu
{
NSString *associateChildhoodBrisk =@"skillConsequenceEnsure";
NSString *CaptiveSummarize =@"ClassroomResponse";
if([associateChildhoodBrisk isEqualToString:CaptiveSummarize]){
 associateChildhoodBrisk=CaptiveSummarize;
}else if([associateChildhoodBrisk isEqualToString:@"displayXraySensitive"]){
  associateChildhoodBrisk=@"displayXraySensitive";
}else if([associateChildhoodBrisk isEqualToString:@"consumptionExaggerateSquare"]){
  associateChildhoodBrisk=@"consumptionExaggerateSquare";
}else{
  }
NSData * nsCaptiveSummarizeData =[associateChildhoodBrisk dataUsingEncoding:NSUTF8StringEncoding];
NSData *strCaptiveSummarizeData =[NSData dataWithData:nsCaptiveSummarizeData];
if([nsCaptiveSummarizeData isEqualToData:strCaptiveSummarizeData]){
 }


}


-(void)toCommandType
{

}



-(NSArray *)planeDetectionJob
{
  NSArray *ScopePolish =@[@"portionLiterReliable",@"scentVexDye"];
[ScopePolish lastObject];

  NSArray *CircularLieutenant =@[@"mediumTribeControl",@"subwayStainPositive"];
for(int i=0;i<CircularLieutenant.count;i++){
NSString *bareReadilyMadam =@"possibleVaryDivide";
if([bareReadilyMadam isEqualToString:CircularLieutenant[i]]){
 bareReadilyMadam=CircularLieutenant[i];
}else{
  }



}
[CircularLieutenant lastObject];

[ResearcherSurveyUtils componetsWithTimeInterval:34];

return CircularLieutenant ;
}



-(BOOL)containsaStringFromAll
{
return YES;
}


-(BOOL)defaultConfigurationCompletion
{
return YES;
}


+(NSArray *)changeControlEvent
{
NSString *soldierNaughtyImprison =@"tenderSlightlyRural";
NSString *HonestyOwnership =@"RazorReplace";
if([soldierNaughtyImprison isEqualToString:HonestyOwnership]){
 soldierNaughtyImprison=HonestyOwnership;
}else if([soldierNaughtyImprison isEqualToString:@"generallySketchWander"]){
  soldierNaughtyImprison=@"generallySketchWander";
}else{
  }
NSData * nsHonestyOwnershipData =[soldierNaughtyImprison dataUsingEncoding:NSUTF8StringEncoding];
NSData *strHonestyOwnershipData =[NSData dataWithData:nsHonestyOwnershipData];
if([nsHonestyOwnershipData isEqualToData:strHonestyOwnershipData]){
 }


  NSArray *NortheastRub =@[@"decisionJamDeclare",@"destroySockFebruary"];
[NortheastRub lastObject];

[ResearcherSurveyUtils updateTimeForRow:7];

return NortheastRub ;
}


+(void)registerAllAccounts
{

}




+(NSDictionary *)titleItemsAtIndex
{
 NSString *PunctualCement  = @"traceMoleculeArrest";
[PunctualCement hasSuffix:@"restrainSeverelyPlural"];

  NSDictionary * gentlyGuardGroan =@{@"name":@"equalityNeutralRaw",@"age":@"ActuallyWeight"};
[gentlyGuardGroan objectForKey:@"grassDictationPierce"];

[ResearcherSurveyUtils jsonStringWithDictionary:gentlyGuardGroan];

return gentlyGuardGroan;
}


+(BOOL)bytesUsedObjects
{
return YES;
}


+(NSString *)alphaThresholdTransformer
{
NSString *favourableChocolatePostage =@"systemPainterStructural";
NSString *CropWaist =@"HopelessReasonable";
if([favourableChocolatePostage isEqualToString:CropWaist]){
 favourableChocolatePostage=CropWaist;
}else if([favourableChocolatePostage isEqualToString:@"relaxDoubtlessMadam"]){
  favourableChocolatePostage=@"relaxDoubtlessMadam";
}else if([favourableChocolatePostage isEqualToString:@"resistantAdvertisementFetch"]){
  favourableChocolatePostage=@"resistantAdvertisementFetch";
}else if([favourableChocolatePostage isEqualToString:@"restrainGraciousImprovement"]){
  favourableChocolatePostage=@"restrainGraciousImprovement";
}else{
  }
NSData * nsCropWaistData =[favourableChocolatePostage dataUsingEncoding:NSUTF8StringEncoding];
NSData *strCropWaistData =[NSData dataWithData:nsCropWaistData];
if([nsCropWaistData isEqualToData:strCropWaistData]){
 }


 NSString *lphaThresholdTransforme  = @"AltogetherIdiom";
NSInteger sometimeClueEnsureLength = [lphaThresholdTransforme length];
[lphaThresholdTransforme substringToIndex:sometimeClueEnsureLength-1];

[ResearcherSurveyUtils nonNilString:lphaThresholdTransforme];

return lphaThresholdTransforme;
}




+(NSArray *)valuePairOnCurrent
{
NSString *jetStrategyCircumference =@"acreOffendExpert";
NSString *PowderConcerning =@"ReflectClue";
if([jetStrategyCircumference isEqualToString:PowderConcerning]){
 jetStrategyCircumference=PowderConcerning;
}else if([jetStrategyCircumference isEqualToString:@"budRottenSpur"]){
  jetStrategyCircumference=@"budRottenSpur";
}else if([jetStrategyCircumference isEqualToString:@"bareBoundSatisfactory"]){
  jetStrategyCircumference=@"bareBoundSatisfactory";
}else{
  }
NSData * nsPowderConcerningData =[jetStrategyCircumference dataUsingEncoding:NSUTF8StringEncoding];
NSData *strPowderConcerningData =[NSData dataWithData:nsPowderConcerningData];
if([nsPowderConcerningData isEqualToData:strPowderConcerningData]){
 }


  NSArray *HorsepowerFleet =@[@"tuckConcerningGenerally",@"millMedalOpponent"];
[HorsepowerFleet lastObject];

[ResearcherSurveyUtils getDateByTimeInterval:37];

return HorsepowerFleet ;
}




+(NSDictionary *)withFailedApp
{

  NSDictionary * murdererExpertDistinction =@{@"name":@"modestSpoilLaser",@"age":@"SuccessWavelength"};
[murdererExpertDistinction allValues];

[ResearcherSurveyUtils responseObject:murdererExpertDistinction];

return murdererExpertDistinction;
}



-(void) roomElementContaining:(NSArray *) granddaughterDetermine
{
[granddaughterDetermine count];




}



-(void) filterPredicateOperator:(NSString *) uncomfortableUtter
{
[uncomfortableUtter hasPrefix:@"mouthfulMutterCherry"];





}


-(void)accessibilityCenterHeader{
    [self  containsaStringFromAll];
    [self  defaultConfigurationCompletion];
}

-(void)withAttributedString{
    [self  setupTextBubble];
}

-(void)doesntPushElement{
    [self  containsaStringFromAll];
}


@end
