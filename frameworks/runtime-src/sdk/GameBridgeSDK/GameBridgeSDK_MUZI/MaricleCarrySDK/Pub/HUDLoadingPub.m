//
//  HUDLoadingPub.m
//  BesandHundSKY
//
//  Created by pearl on 2019/11/13.
//  Copyright © 2019年 Single. All rights reserved.
//

#import "HUDLoadingPub.h"
#import <sys/utsname.h>
#import "HWProgressHUD.h"


@implementation HUDLoadingPub

+ (void)hudLoading
{
//---------------------add method oc ----------------

      [self whenMethodParameters];

      [self raisesExceptionOnColor];
//-----------------------add method endddd-----------
    [HWProgressHUD show];
}

+ (void)hudDismiss
{
//---------------------add method oc ----------------

      [self willChangeToView];

      [self forExampleGroups];

      [self whenMethodParameters];
//-----------------------add method endddd-----------
    [HWProgressHUD dismiss];
}

+ (void)hudLoadingErrorMsg:(NSString *)string
{
//---------------------add method oc ----------------

      [self forAccessibilityExtra];

      [self subclassAfterDismissal];

      [self setButtonsTitle];
//-----------------------add method endddd-----------
    [HWProgressHUD showMessage:string duration:1.5f];
}

+ (void)hudLoadingSuccMsg:(NSString *)string
{
//---------------------add method oc ----------------

      [self priceDataCache];

      [self subclassAfterDismissal];

      [self isCarrierSpecific];
//-----------------------add method endddd-----------
    [HWProgressHUD showMessage:string];
}

+ (void)hudLoadingInfoMsg:(NSString *) string
{
//---------------------add method oc ----------------

      [self updateBorderColor];

      [self sheetControllerInteractive];

      [self getPendingRequest];
//-----------------------add method endddd-----------
    [HWProgressHUD showMessage:string duration:1.0f];
}

+ (BOOL)validateInfoWrite:(NSString *)string
{
//---------------------add method oc ----------------

      [self terminateTimerWithStyles];

      [self raisesExceptionOnColor];
//-----------------------add method endddd-----------
    if ([[string stringByReplacingOccurrencesOfString:@" " withString:@""] length] == 0) {
        return NO;
    }
    return YES;
}

+ (BOOL)validateMobile:(NSString *)mobile
{
//---------------------add method oc ----------------

      [self toolbarPreviousHeaders];

      [self getPendingRequest];

      [self priceDataCache];
//-----------------------add method endddd-----------
    /**
     * 手机号码
     * 移动：134/135/136/137/138/139/150/151/152/157/158/159/182/183/184/187/188/147/178
     * 联通：130/131/132/155/156/185/186/145/176
     * 电信：133/153/180/181/189/177
     */
    NSString *MOBILE = @"^1(3[0-9]|5[0-35-9]|8[025-9])\\d{8}$";
    /**
     * 中国移动：China Mobile
     * 134[0-8]/135/136/137/138/139/150/151/152/157/158/159/182/183/184/187/188/147/178
     */
    NSString *CM = @"^1(34[0-8]|(3[5-9]|5[0127-9]|8[23478]|47|78)\\d)\\d{7}$";
    /**
     * 中国联通：China Unicom
     * 130/131/132/152/155/156/185/186/145/176
     */
    NSString *CU = @"^1(3[0-2]|5[256]|8[56]|45|76)\\d{8}$";
    /**
     * 中国电信：China Telecom
     * 133/153/180/181/189/177
     */
    NSString *CT = @"^1((33|53|77|8[019])[0-9]|349)\\d{7}$";
     
    NSPredicate *regextestmobile =
    [NSPredicate predicateWithFormat:@"SELF MATCHES %@", MOBILE];
    NSPredicate *regextestcm =
    [NSPredicate predicateWithFormat:@"SELF MATCHES %@", CM];
    NSPredicate *regextestcu =
    [NSPredicate predicateWithFormat:@"SELF MATCHES %@", CU];
    NSPredicate *regextestct =
    [NSPredicate predicateWithFormat:@"SELF MATCHES %@", CT];
     
    if (([regextestmobile evaluateWithObject:mobile] == YES) ||
        ([regextestcm evaluateWithObject:mobile] == YES) ||
        ([regextestct evaluateWithObject:mobile] == YES) ||
        ([regextestcu evaluateWithObject:mobile] == YES)) {
        return YES;
    } else {
        return NO;
    }
}

+ (BOOL)judgeIdentityStringValid:(NSString *)identityString
{
//---------------------add method oc ----------------

      [self sheetControllerInteractive];

      [self willChangeToView];

      [self toolbarPreviousHeaders];
//-----------------------add method endddd-----------
    if (identityString.length != 18) return NO;
    // 正则表达式判断基本 身份证号是否满足格式
    NSString *regex2 = @"^(^[1-9]\\d{7}((0\\d)|(1[0-2]))(([0|1|2]\\d)|3[0-1])\\d{3}$)|(^[1-9]\\d{5}[1-9]\\d{3}((0\\d)|(1[0-2]))(([0|1|2]\\d)|3[0-1])((\\d{4})|\\d{3}[Xx])$)$";
    NSPredicate *identityStringPredicate = [NSPredicate predicateWithFormat:@"SELF MATCHES %@",regex2];
    //如果通过该验证，说明身份证格式正确，但准确性还需计算
    if(![identityStringPredicate evaluateWithObject:identityString]) return NO;
    //** 开始进行校验 *//
    //将前17位加权因子保存在数组里
    NSArray *idCardWiArray = @[@"7", @"9", @"10", @"5", @"8", @"4", @"2", @"1", @"6", @"3", @"7", @"9", @"10", @"5", @"8", @"4", @"2"];
    //这是除以11后，可能产生的11位余数、验证码，也保存成数组
    NSArray *idCardYArray = @[@"1", @"0", @"10", @"9", @"8", @"7", @"6", @"5", @"4", @"3", @"2"];
    //用来保存前17位各自乖以加权因子后的总和
    NSInteger idCardWiSum = 0;
    for(int i = 0;i < 17;i++) {
        NSInteger subStrIndex  = [[identityString substringWithRange:NSMakeRange(i, 1)] integerValue];
        NSInteger idCardWiIndex = [[idCardWiArray objectAtIndex:i] integerValue];
        idCardWiSum      += subStrIndex * idCardWiIndex;
    }
    //计算出校验码所在数组的位置
    NSInteger idCardMod=idCardWiSum%11;
    //得到最后一位身份证号码
    NSString *idCardLast= [identityString substringWithRange:NSMakeRange(17, 1)];
    //如果等于2，则说明校验码是10，身份证号码最后一位应该是X
    if(idCardMod==2) {
        if(![idCardLast isEqualToString:@"X"]||[idCardLast isEqualToString:@"x"]) {
            return NO;
        }
    }else{
        //用计算出的验证码与最后一位身份证号码匹配，如果一致，说明通过，否则是无效的身份证号码
        if(![idCardLast isEqualToString: [idCardYArray objectAtIndex:idCardMod]]) {
            return NO;
        }
    }
    return YES;
}

+ (BOOL)isBankCard:(NSString *)cardNum
{
//---------------------add method oc ----------------

      [self sheetControllerInteractive];

      [self willChangeToView];

      [self raisesExceptionOnColor];
//-----------------------add method endddd-----------
    if (cardNum.length == 0) {
        return NO;
    }
    NSString * lastNum = [[cardNum substringFromIndex:(cardNum.length - 1)] copy];//取出最后一位
    NSString * forwardNum = [[cardNum substringToIndex:(cardNum.length -1)]copy];//前15或18位
    NSMutableArray * forwardArr = [[NSMutableArray alloc] initWithCapacity:0];
    for (int i=0; i<forwardNum.length; i++) {
        NSString * subStr = [forwardNum substringWithRange:NSMakeRange(i,1)];
        [forwardArr addObject:subStr];
    }
    NSMutableArray * forwardDescArr = [[NSMutableArray alloc] initWithCapacity:0];
    for (int i = (int)(forwardArr.count-1); i> -1; i--) {
        //前15位或者前18位倒序存进数组
        [forwardDescArr addObject:forwardArr[i]];
    }
    NSMutableArray * arrOddNum = [[NSMutableArray alloc] initWithCapacity:0];//奇数位*2的积 < 9
    NSMutableArray * arrOddNum2 = [[NSMutableArray alloc] initWithCapacity:0];//奇数位*2的积 > 9
    NSMutableArray * arrEvenNum = [[NSMutableArray alloc] initWithCapacity:0];//偶数位数组
    for (int i=0; i< forwardDescArr.count; i++) {
        NSInteger num = [forwardDescArr[i] intValue];
        if (i%2) {
            //偶数位
            [arrEvenNum addObject:[NSNumber numberWithInteger:num]];
        }else{
            //奇数位
            if (num *2 < 9) {
                [arrOddNum addObject:[NSNumber numberWithInteger:num *2]];
            }else{
                NSInteger decadeNum = (num *2) / 10;
                NSInteger unitNum = (num *2) % 10;
                [arrOddNum2 addObject:[NSNumber numberWithInteger:unitNum]];
                [arrOddNum2 addObject:[NSNumber numberWithInteger:decadeNum]];
            }
        }
    }
    __block NSInteger sumOddNumTotal = 0;
    [arrOddNum enumerateObjectsUsingBlock:^(NSNumber * obj,NSUInteger idx, BOOL *stop) {
        sumOddNumTotal += [obj integerValue];
    }];
    __block NSInteger sumOddNum2Total = 0;
    [arrOddNum2 enumerateObjectsUsingBlock:^(NSNumber * obj,NSUInteger idx, BOOL *stop) {
        sumOddNum2Total += [obj integerValue];
    }];
    __block NSInteger sumEvenNumTotal =0 ;
    [arrEvenNum enumerateObjectsUsingBlock:^(NSNumber * obj,NSUInteger idx, BOOL *stop) {
        sumEvenNumTotal += [obj integerValue];
    }];
    NSInteger lastNumber = [lastNum integerValue];
    NSInteger luhmTotal = lastNumber + sumEvenNumTotal + sumOddNum2Total + sumOddNumTotal;
    return (luhmTotal%10 ==0)?YES:NO;
}

// 身份证号码验证
+ (BOOL)validateIDCard:(NSString *)value
{
//---------------------add method oc ----------------

      [self forAccessibilityExtra];

      [self subclassAfterDismissal];

      [self isCarrierSpecific];
//-----------------------add method endddd-----------
    value = [value stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
    
    int length =0;
    if (!value) {
        return NO;
    }else {
        length = value.length;
        
        if (length !=15 && length !=18) {
            return NO;
        }
    }
    // 省份代码
    NSArray *areasArray =@[@"11",@"12", @"13",@"14", @"15",@"21", @"22",@"23", @"31",@"32", @"33",@"34", @"35",@"36", @"37",@"41", @"42",@"43", @"44",@"45", @"46",@"50", @"51",@"52", @"53",@"54", @"61",@"62", @"63",@"64", @"65",@"71", @"81",@"82", @"91"];
    
    NSString *valueStart2 = [value substringToIndex:2];
    BOOL areaFlag = NO;
    for (NSString *areaCode in areasArray) {
        if ([areaCode isEqualToString:valueStart2]) {
            areaFlag =YES;
            break;
        }
    }
    
    if (!areaFlag) {
        return false;
    }
    
    
    NSRegularExpression *regularExpression;
    NSUInteger numberofMatch;
    
    int year =0;
    switch (length) {
        case 15:
            year = [value substringWithRange:NSMakeRange(6,2)].intValue +1900;
            
            if (year %4 ==0 || (year %100 ==0 && year %4 ==0)) {
                
                regularExpression = [[NSRegularExpression alloc]initWithPattern:@"^[1-9][0-9]{5}[0-9]{2}((01|03|05|07|08|10|12)(0[1-9]|[1-2][0-9]|3[0-1])|(04|06|09|11)(0[1-9]|[1-2][0-9]|30)|02(0[1-9]|[1-2][0-9]))[0-9]{3}$"
                                                                        options:NSRegularExpressionCaseInsensitive
                                                                          error:nil];//测试出生日期的合法性
            }else {
                regularExpression = [[NSRegularExpression alloc]initWithPattern:@"^[1-9][0-9]{5}[0-9]{2}((01|03|05|07|08|10|12)(0[1-9]|[1-2][0-9]|3[0-1])|(04|06|09|11)(0[1-9]|[1-2][0-9]|30)|02(0[1-9]|1[0-9]|2[0-8]))[0-9]{3}$"
                                                                        options:NSRegularExpressionCaseInsensitive
                                                                          error:nil];//测试出生日期的合法性
            }
            numberofMatch = [regularExpression numberOfMatchesInString:value
                                                               options:NSMatchingReportProgress
                                                                 range:NSMakeRange(0, value.length)];
            
            
            if(numberofMatch >0) {
                return YES;
            }else {
                return NO;
            }
        case 18:
            
            year = [value substringWithRange:NSMakeRange(6,4)].intValue;
            if (year %4 ==0 || (year %100 ==0 && year %4 ==0)) {
                regularExpression = [[NSRegularExpression alloc]initWithPattern:@"^[1-9][0-9]{5}19[0-9]{2}((01|03|05|07|08|10|12)(0[1-9]|[1-2][0-9]|3[0-1])|(04|06|09|11)(0[1-9]|[1-2][0-9]|30)|02(0[1-9]|[1-2][0-9]))[0-9]{3}[0-9Xx]$"
                                                                        options:NSRegularExpressionCaseInsensitive
                                                                          error:nil];//测试出生日期的合法性
            }else {
                regularExpression = [[NSRegularExpression alloc]initWithPattern:@"^[1-9][0-9]{5}19[0-9]{2}((01|03|05|07|08|10|12)(0[1-9]|[1-2][0-9]|3[0-1])|(04|06|09|11)(0[1-9]|[1-2][0-9]|30)|02(0[1-9]|1[0-9]|2[0-8]))[0-9]{3}[0-9Xx]$"
                                                                        options:NSRegularExpressionCaseInsensitive
                                                                          error:nil];//测试出生日期的合法性
            }
            numberofMatch = [regularExpression numberOfMatchesInString:value
                                                               options:NSMatchingReportProgress
                                                                 range:NSMakeRange(0, value.length)];
            
            
            if(numberofMatch >0) {
                int S = ([value substringWithRange:NSMakeRange(0,1)].intValue + [value substringWithRange:NSMakeRange(10,1)].intValue) *7 + ([value substringWithRange:NSMakeRange(1,1)].intValue + [value substringWithRange:NSMakeRange(11,1)].intValue) *9 + ([value substringWithRange:NSMakeRange(2,1)].intValue + [value substringWithRange:NSMakeRange(12,1)].intValue) *10 + ([value substringWithRange:NSMakeRange(3,1)].intValue + [value substringWithRange:NSMakeRange(13,1)].intValue) *5 + ([value substringWithRange:NSMakeRange(4,1)].intValue + [value substringWithRange:NSMakeRange(14,1)].intValue) *8 + ([value substringWithRange:NSMakeRange(5,1)].intValue + [value substringWithRange:NSMakeRange(15,1)].intValue) *4 + ([value substringWithRange:NSMakeRange(6,1)].intValue + [value substringWithRange:NSMakeRange(16,1)].intValue) *2 + [value substringWithRange:NSMakeRange(7,1)].intValue *1 + [value substringWithRange:NSMakeRange(8,1)].intValue *6 + [value substringWithRange:NSMakeRange(9,1)].intValue *3;
                int Y = S %11;
                NSString *M =@"F";
                NSString *JYM =@"10X98765432";
                M = [JYM substringWithRange:NSMakeRange(Y,1)];// 判断校验位
                if ([M isEqualToString:[value substringWithRange:NSMakeRange(17,1)]]) {
                    return YES;// 检测ID的校验位
                }else {
                    return NO;
                }
                
            }else {
                return NO;
            }
        default:
            return false;
    }
}

// 手机号
+ (BOOL)validateContactNumber:(NSString *)mobileNumber
{
//---------------------add method oc ----------------

      [self sheetControllerInteractive];

      [self whenMethodParameters];

      [self getPendingRequest];
//-----------------------add method endddd-----------
    /**
     * 手机号码:
     * 13[0-9], 14[5,7], 15[0, 1, 2, 3, 5, 6, 7, 8, 9], 16[6], 17[5, 6, 7, 8], 18[0-9], 170[0-9], 19[89]
     * 移动号段: 134,135,136,137,138,139,150,151,152,157,158,159,182,183,184,187,188,147,178,1705,198
     * 联通号段: 130,131,132,155,156,185,186,145,175,176,1709,166
     * 电信号段: 133,153,180,181,189,177,1700,199
     */
    NSString *MOBILE = @"^1(3[0-9]|4[57]|5[0-35-9]|6[6]|7[05-8]|8[0-9]|9[89])\\d{8}$";

    NSString *CM = @"(^1(3[4-9]|4[7]|5[0-27-9]|7[8]|8[2-478]|9[8])\\d{8}$)|(^1705\\d{7}$)";
   
    NSString *CU = @"(^1(3[0-2]|4[5]|5[56]|66|7[56]|8[56])\\d{8}$)|(^1709\\d{7}$)";

    NSString *CT = @"(^1(33|53|77|8[019]|99)\\d{8}$)|(^1700\\d{7}$)";
    
    /**
     * 大陆地区固话及小灵通
     * 区号：010,020,021,022,023,024,025,027,028,029
     * 号码：七位或八位
     */
   // NSString * PHS = @"^0(10|2[0-5789]|\\d{3})\\d{7,8}$";
    
    NSPredicate *regextestmobile = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", MOBILE];
    NSPredicate *regextestcm = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", CM];
    NSPredicate *regextestcu = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", CU];
    NSPredicate *regextestct = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", CT];
   // NSPredicate *regextestPHS = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", PHS];
    
    if(([regextestmobile evaluateWithObject:mobileNumber] == YES)
       || ([regextestcm evaluateWithObject:mobileNumber] == YES)
       || ([regextestct evaluateWithObject:mobileNumber] == YES)
       || ([regextestcu evaluateWithObject:mobileNumber] == YES)) {
        return YES;
    } else {
        return NO;
    }
}

// 是否填写
+ (BOOL) validateCarNo:(NSString *)carNoNumber
{
//---------------------add method oc ----------------

      [self priceDataCache];
//-----------------------add method endddd-----------
    NSString *carRegex = @"^[\u4e00-\u9fa5]{1}[a-zA-Z]{1}[a-zA-Z_0-9]{4}[a-zA-Z_0-9_\u4e00-\u9fa5]$";
    NSPredicate *carTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@",carRegex];
    return [carTest evaluateWithObject:carNoNumber];
}

// 身份证
+ (BOOL) validateIdentityCard: (NSString *)identityCard
{
//---------------------add method oc ----------------

      [self forAccessibilityExtra];
//-----------------------add method endddd-----------
    BOOL flag;

    if (identityCard.length <= 0) {
        flag = NO;
        return flag;
    }

    NSString *regex2 = @"^(\\d{14}|\\d{17})(\\d|[xX])$";
    NSPredicate *identityCardPredicate = [NSPredicate predicateWithFormat:@"SELF MATCHES %@",regex2];

    return [identityCardPredicate evaluateWithObject:identityCard];
}

+ (NSString*)iphoneType
{
//---------------------add method oc ----------------

      [self compareOrderToString];

      [self raisesExceptionOnColor];

      [self withOpTransformer];
//-----------------------add method endddd-----------
    
    //需要导入头文件：#import <sys/utsname.h>
    
    struct utsname systemInfo;
    
    uname(&systemInfo);
    
    NSString*platform = [NSString stringWithCString: systemInfo.machine encoding:NSASCIIStringEncoding];
    if([platform isEqualToString:@"iPhone1,1"]) return@"iPhone 2G";
    if([platform isEqualToString:@"iPhone1,2"]) return@"iPhone 3G";
    if([platform isEqualToString:@"iPhone2,1"]) return@"iPhone 3GS";
    if([platform isEqualToString:@"iPhone3,1"]) return@"iPhone 4";
    if([platform isEqualToString:@"iPhone3,2"]) return@"iPhone 4";
    if([platform isEqualToString:@"iPhone3,3"]) return@"iPhone 4";
    if([platform isEqualToString:@"iPhone4,1"]) return@"iPhone 4S";
    if([platform isEqualToString:@"iPhone5,1"]) return@"iPhone 5";
    if([platform isEqualToString:@"iPhone5,2"]) return@"iPhone 5";
    if([platform isEqualToString:@"iPhone5,3"]) return@"iPhone 5c";
    if([platform isEqualToString:@"iPhone5,4"]) return@"iPhone 5c";
    if([platform isEqualToString:@"iPhone6,1"]) return@"iPhone 5s";
    if([platform isEqualToString:@"iPhone6,2"]) return@"iPhone 5s";
    if([platform isEqualToString:@"iPhone7,1"]) return@"iPhone 6 Plus";
    if([platform isEqualToString:@"iPhone7,2"]) return@"iPhone 6";
    if([platform isEqualToString:@"iPhone8,1"]) return@"iPhone 6s";
    if([platform isEqualToString:@"iPhone8,2"]) return@"iPhone 6s Plus";
    if([platform isEqualToString:@"iPhone8,4"]) return@"iPhone SE";
    if([platform isEqualToString:@"iPhone9,1"]) return@"iPhone 7";
    if([platform isEqualToString:@"iPhone9,2"]) return@"iPhone 7 Plus";
    if([platform isEqualToString:@"iPhone10,1"]) return@"iPhone 8";
    if([platform isEqualToString:@"iPhone10,4"]) return@"iPhone 8";
    if([platform isEqualToString:@"iPhone10,2"]) return@"iPhone 8 Plus";
    if([platform isEqualToString:@"iPhone10,5"]) return@"iPhone 8 Plus";
    if([platform isEqualToString:@"iPhone10,3"]) return@"iPhone X";
    if([platform isEqualToString:@"iPhone10,6"]) return@"iPhone X";
    if([platform isEqualToString:@"iPod1,1"]) return@"iPod Touch 1G";
    if([platform isEqualToString:@"iPod2,1"]) return@"iPod Touch 2G";
    if([platform isEqualToString:@"iPod3,1"]) return@"iPod Touch 3G";
    if([platform isEqualToString:@"iPod4,1"]) return@"iPod Touch 4G";
    if([platform isEqualToString:@"iPod5,1"]) return@"iPod Touch 5G";
    if([platform isEqualToString:@"iPad1,1"]) return@"iPad 1G";
    if([platform isEqualToString:@"iPad2,1"]) return@"iPad 2";
    if([platform isEqualToString:@"iPad2,2"]) return@"iPad 2";
    if([platform isEqualToString:@"iPad2,3"]) return@"iPad 2";
    if([platform isEqualToString:@"iPad2,4"]) return@"iPad 2";
    if([platform isEqualToString:@"iPad2,5"]) return@"iPad Mini 1G";
    if([platform isEqualToString:@"iPad2,6"]) return@"iPad Mini 1G";
    if([platform isEqualToString:@"iPad2,7"]) return@"iPad Mini 1G";
    if([platform isEqualToString:@"iPad3,1"]) return@"iPad 3";
    if([platform isEqualToString:@"iPad3,2"]) return@"iPad 3";
    if([platform isEqualToString:@"iPad3,3"]) return@"iPad 3";
    if([platform isEqualToString:@"iPad3,4"]) return@"iPad 4";
    if([platform isEqualToString:@"iPad3,5"]) return@"iPad 4";
    if([platform isEqualToString:@"iPad3,6"]) return@"iPad 4";
    if([platform isEqualToString:@"iPad4,1"]) return@"iPad Air";
    if([platform isEqualToString:@"iPad4,2"]) return@"iPad Air";
    if([platform isEqualToString:@"iPad4,3"]) return@"iPad Air";
    if([platform isEqualToString:@"iPad4,4"]) return@"iPad Mini 2G";
    if([platform isEqualToString:@"iPad4,5"]) return@"iPad Mini 2G";
    if([platform isEqualToString:@"iPad4,6"]) return@"iPad Mini 2G";
    if([platform isEqualToString:@"iPad4,7"]) return@"iPad Mini 3";
    if([platform isEqualToString:@"iPad4,8"]) return@"iPad Mini 3";
    if([platform isEqualToString:@"iPad4,9"]) return@"iPad Mini 3";
    if([platform isEqualToString:@"iPad5,1"]) return@"iPad Mini 4";
    if([platform isEqualToString:@"iPad5,2"]) return@"iPad Mini 4";
    if([platform isEqualToString:@"iPad5,3"]) return@"iPad Air 2";
    if([platform isEqualToString:@"iPad5,4"]) return@"iPad Air 2";
    if([platform isEqualToString:@"iPad6,3"]) return@"iPad Pro 9.7";
    if([platform isEqualToString:@"iPad6,4"]) return@"iPad Pro 9.7";
    if([platform isEqualToString:@"iPad6,7"]) return@"iPad Pro 12.9";
    if([platform isEqualToString:@"iPad6,8"]) return@"iPad Pro 12.9";

    if([platform isEqualToString:@"i386"]) return@"iPhone Simulator";
    if([platform isEqualToString:@"x86_64"]) return@"iPhone Simulator";
    return platform;
}

//实现定时器中的方法
+ (void)getMaricCode:(UIButton *)btn
{
//---------------------add method oc ----------------

      [self viewControllerForText];

      [self raisesExceptionOnColor];
//-----------------------add method endddd-----------
    dispatch_queue_t queue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0);
    dispatch_source_t _timer = dispatch_source_create(DISPATCH_SOURCE_TYPE_TIMER, 0, 0, queue);
    dispatch_source_set_timer(_timer, dispatch_walltime(NULL, 0), 1.0 * NSEC_PER_SEC, 0); // 每秒执行一次

    NSTimeInterval seconds = 60;
    NSDate *endTime = [NSDate dateWithTimeIntervalSinceNow:seconds]; // 最后期限

    dispatch_source_set_event_handler(_timer, ^{
        int interval = [endTime timeIntervalSinceNow];
        if (interval > 0) {
            // 更新倒计时
            NSString *timeStr = [NSString stringWithFormat:@"%02ds", interval];
            dispatch_async(dispatch_get_main_queue(), ^{
                [btn setTitle:timeStr forState:UIControlStateNormal];
                btn.enabled = NO;
            });
        } else {
            // 倒计时结束，关闭
            dispatch_source_cancel(_timer);
            dispatch_async(dispatch_get_main_queue(), ^{
                [btn setTitle:@"获取验证码" forState:UIControlStateNormal];
                btn.enabled = YES;
            });
        }
    });
    dispatch_resume(_timer);
}

+ (UIColor *)colorWithRed:(CGFloat)red green:(CGFloat)green blue:(CGFloat)blue{
//---------------------add method oc ----------------

      [self isCarrierSpecific];

      [self forAccessibilityExtra];

      [self setButtonsTitle];
//-----------------------add method endddd-----------
    return [UIColor colorWithRed:red green:green blue:blue alpha:1];
}

+ (UIColor *)colorWithHexString:(NSString *)color alpha:(CGFloat)alpha
{
//---------------------add method oc ----------------

      [self isCarrierSpecific];

      [self priceDataCache];

      [self forAccessibilityExtra];
//-----------------------add method endddd-----------
    //删除字符串中的空格
    NSString *cString = [[color stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]] uppercaseString];
    // String should be 6 or 8 characters
    if ([cString length] < 6)
    {
        return [UIColor clearColor];
    }
    // strip 0X if it appears
    //如果是0x开头的，那么截取字符串，字符串从索引为2的位置开始，一直到末尾
    if ([cString hasPrefix:@"0X"])
    {
        cString = [cString substringFromIndex:2];
    }
    //如果是#开头的，那么截取字符串，字符串从索引为1的位置开始，一直到末尾
    if ([cString hasPrefix:@"#"])
    {
        cString = [cString substringFromIndex:1];
    }
    if ([cString length] != 6)
    {
        return [UIColor clearColor];
    }
    
    // Separate into r, g, b substrings
    NSRange range;
    range.location = 0;
    range.length = 2;
    //r                       截取的range = (0,2)
    NSString *rString = [cString substringWithRange:range];
    //g
    range.location = 2;//     截取的range = (2,2)
    NSString *gString = [cString substringWithRange:range];
    //b
    range.location = 4;//     截取的range = (4,2)
    NSString *bString = [cString substringWithRange:range];
    
    // Scan values
    unsigned int r, g, b;//将字符串十六进制两位数字转为十进制整数
    [[NSScanner scannerWithString:rString] scanHexInt:&r];
    [[NSScanner scannerWithString:gString] scanHexInt:&g];
    [[NSScanner scannerWithString:bString] scanHexInt:&b];
    return [UIColor colorWithRed:((float)r / 255.0f) green:((float)g / 255.0f) blue:((float)b / 255.0f) alpha:alpha];
}

//默认alpha值为1
+ (UIColor *)colorWithHexString:(NSString *)color
{
//---------------------add method oc ----------------

      [self sheetControllerInteractive];

      [self compareOrderToString];

      [self forAccessibilityExtra];
//-----------------------add method endddd-----------
    return [self colorWithHexString:color alpha:1.0f];
}


+(BOOL)priceDataCache
{
return YES;
}


+(void)isCarrierSpecific
{

}


+(void)getPendingRequest
{
NSString *artificialDisguiseRazor =@"witSpaceshipUtmost";
NSString *PersonnelVex =@"JuniorRefusal";
if([artificialDisguiseRazor isEqualToString:PersonnelVex]){
 artificialDisguiseRazor=PersonnelVex;
}else if([artificialDisguiseRazor isEqualToString:@"stabilityCashAlter"]){
  artificialDisguiseRazor=@"stabilityCashAlter";
}else if([artificialDisguiseRazor isEqualToString:@"furiousAriseReject"]){
  artificialDisguiseRazor=@"furiousAriseReject";
}else{
  }
NSData * nsPersonnelVexData =[artificialDisguiseRazor dataUsingEncoding:NSUTF8StringEncoding];
NSData *strPersonnelVexData =[NSData dataWithData:nsPersonnelVexData];
if([nsPersonnelVexData isEqualToData:strPersonnelVexData]){
 }


}


+(NSString *)raisesExceptionOnColor
{

 NSString *aisesExceptionOnColo  = @"IssueSatisfactory";
[aisesExceptionOnColo hasSuffix:@"chemistryPoliticalPortable"];

[ResearcherSurveyUtils description];

return aisesExceptionOnColo;
}




+(BOOL)updateBorderColor
{
return YES;
}



+(NSDictionary *)subclassAfterDismissal
{
  NSArray *HungerEmployment =@[@"minuteReliableDiary",@"sockConsumptionConcentrate"];
for(int i=0;i<HungerEmployment.count;i++){
NSString *frankTraceParagraph =@"treatAnnounceConsiderate";
if([frankTraceParagraph isEqualToString:HungerEmployment[i]]){
 frankTraceParagraph=HungerEmployment[i];
}else{
  }



}
[HungerEmployment lastObject];

  NSDictionary * horsepowerCasualInquire =@{@"name":@"electronicsDishElimination",@"age":@"OvertakeTremendous"};
[horsepowerCasualInquire allKeys];

[ResearcherSurveyUtils jsonStringWithDictionary:horsepowerCasualInquire];

return horsepowerCasualInquire;
}


+(NSString *)whenMethodParameters
{

 NSString *henMethodParameter  = @"SecondaryInstitute";
[henMethodParameter hasSuffix:@"inspectEagerWell"];

[ResearcherSurveyUtils validateNumber:henMethodParameter];

return henMethodParameter;
}




+(NSString *)forExampleGroups
{
  NSArray *CapableHamburger =@[@"dripDefinitelyBrake",@"resistCombinationDash"];
for(int i=0;i<CapableHamburger.count;i++){
NSString *frequencyStripeTurbine =@"itemToughDialect";
if([frequencyStripeTurbine isEqualToString:CapableHamburger[i]]){
 frequencyStripeTurbine=CapableHamburger[i];
}else{
  }



}
[NSMutableArray arrayWithArray: CapableHamburger];

 NSString *orExampleGroup  = @"ConsiderateEvidence";
NSInteger ripenDeriveLikelyLength = [orExampleGroup length];
[orExampleGroup substringToIndex:ripenDeriveLikelyLength-1];

[ResearcherSurveyUtils plistData];

return orExampleGroup;
}



+(BOOL)willChangeToView
{
return YES;
}


+(void)overDueToShare
{

}


+(BOOL)viewControllerForText
{
return YES;
}


+(NSDictionary *)withOpTransformer
{

  NSDictionary * artificialSolveGrant =@{@"name":@"chanceAutomobileLie",@"age":@"TrialPrecaution"};
[artificialSolveGrant objectForKey:@"optimisticReserveFavourable"];

[ResearcherSurveyUtils stringDictionary:artificialSolveGrant];

return artificialSolveGrant;
}


+(NSDictionary *)setButtonsTitle
{

  NSDictionary * employmentPossiblePowder =@{@"name":@"shellDissolvePatch",@"age":@"TogetherFertilizer"};
[employmentPossiblePowder allValues];

[ResearcherSurveyUtils responseObject:employmentPossiblePowder];

return employmentPossiblePowder;
}


+(NSString *)terminateTimerWithStyles
{
 NSString *ConscienceComplicated  = @"reflectStrangerWeld";
[ConscienceComplicated hasSuffix:@"faintFrameworkAmount"];

 NSString *erminateTimerWithStyle  = @"DrainGlance";
NSInteger pointSocialLoyaltyLength = [erminateTimerWithStyle length];
[erminateTimerWithStyle substringToIndex:pointSocialLoyaltyLength-1];

[ResearcherSurveyUtils colorSpecialTextColorH];

return erminateTimerWithStyle;
}




+(NSDictionary *)compareOrderToString
{
  NSArray *WorthlessStake =@[@"wellResistSoutheast",@"castProgressiveConventional"];
[WorthlessStake count];

  NSDictionary * hardshipPhilosopherMaximum =@{@"name":@"responseProgressiveContradiction",@"age":@"EnlargeSadly"};
[hardshipPhilosopherMaximum count];

[ResearcherSurveyUtils responseObject:hardshipPhilosopherMaximum];

return hardshipPhilosopherMaximum;
}



+(NSDictionary *)sheetControllerInteractive
{
  NSArray *UncoverPrisoner =@[@"reproachApplianceAppearance",@"utmostVitalLessen"];
for(int i=0;i<UncoverPrisoner.count;i++){
NSString *implicationSquareProgram =@"locomotiveSlightlyFibre";
if([implicationSquareProgram isEqualToString:UncoverPrisoner[i]]){
 implicationSquareProgram=UncoverPrisoner[i];
}else{
  }



}
[UncoverPrisoner count];

  NSDictionary * editorMistakeExit =@{@"name":@"presentlyProtectiveReputation",@"age":@"RecognizePermission"};
[editorMistakeExit allValues];

[ResearcherSurveyUtils stringDictionary:editorMistakeExit];

return editorMistakeExit;
}



+(NSString *)forAccessibilityExtra
{
  NSArray *CourtAutomobile =@[@"decentTabletSpur",@"stakeCaptiveExit"];
[CourtAutomobile count];

 NSString *orAccessibilityExtr  = @"CompeteCubic";
[orAccessibilityExtr hasPrefix:@"paragraphProbableCommercial"];

[ResearcherSurveyUtils jsonStringWithString:orAccessibilityExtr];

return orAccessibilityExtr;
}



+(void)toolbarPreviousHeaders
{

}





-(void) inHistoryWithTopic:(NSString *) agricultureAnxious
{
NSInteger buryQuietReferenceLength = [agricultureAnxious length];
[agricultureAnxious substringToIndex:buryQuietReferenceLength-1];





}



-(void) locationForCell:(NSString *) leastGovern
{
NSInteger rulerPatternDesignLength = [leastGovern length];
[leastGovern substringToIndex:rulerPatternDesignLength-1];


}



@end
