//
//  BridgeSDK.h
//  BridgeSDK
//
//
//
#define Bridge_SDK_VERSION @"1.0.0" //BridgeSDK基础库版本
#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

//#import "HiSDK.h"

#import "WorkParams.h"
#import "SDKParams.h"
#import "BridgeGameInfo.h"
#import "BridgeApplicationDelegate.h"
#import "BridgeAntiData.h"

#pragma mark -
#pragma mark 基本信息

typedef void(^startGameSuccess)(void);

@interface BridgeSDK : NSObject
{
    NSMutableArray * _applicationDelegates;
    startGameSuccess _startGame;
    NSMutableDictionary* _svrCfg;
}
@property(nonatomic,strong) SDKParams* sdkParams;
@property (nonatomic, strong) NSSet *byoSet;
@property (nonatomic, strong) NSString *daysitemsString;
@property (nonatomic, strong) NSSet *mistakeSet;
@property (nonatomic, strong) NSDictionary *hatchDict;
@property (nonatomic, strong) NSMutableDictionary *foucsMutabledict;
@property (nonatomic, strong) NSMutableDictionary *centerMutabledict;
@property (nonatomic, strong) NSNumber *alarmsNumber;
@property (nonatomic, assign) BOOL  inmodeValue;
@property (nonatomic, assign) NSUInteger  ensembleValue;
@property (nonatomic, assign) NSInteger  pyqValue;
@property (nonatomic, assign) NSInteger  resultsValue;

//-----------------property-----------
@property(nonatomic,strong) BridgeGameInfo* appInfo;
@property(nonatomic,assign) UIApplication* application;
@property(nonatomic,copy) NSDictionary* launchOptions;
@property(nonatomic,assign) bool isLogined;
@property(nonatomic,assign) bool isInitSDK;
@property(nonatomic,assign) bool isGetSvrCfg;
@property(nonatomic,copy) NSString* sdkUserID;
@property(nonatomic,copy) NSString* cbString;
@property(nonatomic,assign)int logicChannel;
@property(nonatomic,copy) NSString* writablePath;
@property(nonatomic,copy) NSString* pluginsDetails;


#pragma mark QuickSDK实例
+ (BridgeSDK *)getInstance;

-(void) initAppInfo;
-(NSString*) getWritablePath;

@end

#pragma mark - view相关
@interface BridgeSDK(view)
-(UIWindow*)getWindow;
-(UIView*)getTopView;
-(UIViewController*)getTopViewController;
+ (UIViewController*)topViewControllerWithRootViewController:(UIViewController*)rootViewController;
@end


#pragma mark - 初始化 接入产品信息

@interface BridgeSDK(c)

-(bool) isInitSDK;
-(void) initSDK;
-(void) login:(NSString*)data;
-(void) logout:(NSString*)data;
-(void) switchAccount:(NSString*)data;
-(void) exit;
-(void) placeOrder:(NSString*)data;
-(void) share:(NSString*)data;
-(void) callFunction:(int)funcType data:(NSString*)data;
-(void) submitExtendData:(NSString*)data;
-(bool) isLogined;
-(NSString*) getValue:(NSString*)key;
-(NSString*) getAppInfo;
-(NSString*) getMainChannel;
-(NSString*) getSubChannel;
-(NSString*) getUserID;
-(NSString*) getSdkURL;
-(NSString*) getPhoneUUID;
-(int) getSdkType;
-(int) getAppID;
-(int) getLogicChannel;
-(NSString*) getAppKey;
-(void) sleepForSplash;
@end

@interface BridgeSDK(sdkbase)

-(void) setApplicationDelegate:(id<BridgeApplicationDelegate>)delegate;
-(void) onLoginResult:(NSString*) userName token:(NSString*)token;
-(void) onCallback:(int)code resultCode:(int)resultCode data:(NSString*)data;

-(void) addSvrCfgKeyValue:(NSString*)key value:(id)value;
-(void) syncSvrCfg;
-(void) checkOrder:(NSString*)orderID thOrderID:(NSString*)thOrderID;
-(void) onWorkSuccess:(WorkParams*) params;
-(void) onLoginSuccess:(bool)isRegister username:(NSString*)username;
-(NSString*) getPluginsDetails;

-(void) showMsgTip:(NSString*)msg;

-(bool) isSupportMethod:(NSString*) methodName;
-(void) onAntiAddictionData:(BridgeAntiData*)antiData;
-(void) onRealNameAuthenticationResult:(bool)isSupport isAuth:(bool)isAuth age:(int)age uid:(NSString*)uid other:(NSString*)other;

@end


@interface BridgeSDK(Extend)

//***********************应用生命周期的回调*******************//
//在应用对应的生命周期回调中调用
/**
 @brief - (BOOL)application:(UIApplication *)application handleOpenURL:(NSURL *)url;
 @brief - (BOOL)application:(UIApplication *)application openURL:(NSURL *)url sourceApplication:(nullable NSString *)sourceApplication annotation:(id)annotation
 @brief - (BOOL)application:(UIApplication *)app openURL:(NSURL *)url options:(NSDictionary<NSString*, id> *)options
 @brief 渠道处理应用跳转
 @result 错误码
 @note 必接
 */
- (BOOL)initApplication:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions;
- (void)startGame:(startGameSuccess)callback;
- (int)openURL:(NSURL *)url application:(UIApplication *)application;
- (int)openURL:(NSURL *)url sourceApplication:(NSString *)sourceApp application:(UIApplication *)application annotation:(id)annotation;
- (int)openURL:(NSURL *)url application:(UIApplication *)app options:(NSDictionary <NSString *, id>*)options;
/**
 @brief application:didRegisterForRemoteNotificationsWithDeviceToken:
 @brief 推送消息
 @result 错误码
 @note 必接
 */
- (int)application:(UIApplication*)application didRegisterForRemoteNotificationsWithDeviceToken:(NSData*)deviceToken;

/**
 @brief application:didFailToRegisterForRemoteNotificationsWithError:
 @brief 推送消息
 @result 错误码
 @note 必接
 */
- (int)application:(UIApplication*)application didFailToRegisterForRemoteNotificationsWithError:(NSError*)error;
- (int)application:(UIApplication *)application didReceiveRemoteNotification:(NSDictionary *)userInfo;
- (int)applicationWillResignActive:(UIApplication *)application;
- (int)applicationDidEnterBackground:(UIApplication *)application;
- (int)applicationWillEnterForeground:(UIApplication *)application;
- (int)applicationDidBecomeActive:(UIApplication *)application;
- (int)applicationWillTerminate:(UIApplication *)application;
- (NSUInteger)application:(UIApplication *)application supportedInterfaceOrientationsForWindow:(UIWindow *)window;
- (BOOL)application:(UIApplication *)application continueUserActivity:(NSUserActivity *)userActivity restorationHandler:(void(^)(NSArray *  restorableObjects))restorationHandler;
- (BOOL)application:(UIApplication *)application openURL:(NSURL *)url sourceApplication:(nullable NSString *)sourceApplication annotation:(id)annotation;
- (BOOL)application:(UIApplication *)app openURL:(NSURL *)url options:(NSDictionary<NSString*, id> *)options;

- (void)application:(UIApplication*)application didReceiveRemoteNotification:(NSDictionary *)userInfo fetchCompletionHandler:(void (^)(UIBackgroundFetchResult))completionHandler;

- (void)application:(UIApplication*)application didReceiveLocalNotification:(UILocalNotification *)notification;
@end
