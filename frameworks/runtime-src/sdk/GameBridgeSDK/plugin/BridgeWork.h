//
//  BridgeWork.h
//  BridgeSDK_IOS
//
//  Created by higame on 2018/8/9.
//  Copyright © 2018年 higame. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "IPlugin.h"

@interface BridgeWork : NSObject
{
    NSMutableArray* _cores;
}

+ (BridgeWork *)getInstance;
-(bool) isSupport:(NSString*)method;
-(void) work:(WorkParams*) data;
-(void) keepWork:(NSMutableArray*) works;
-(void) setWorkParams:(int)workType params:(NSString*)params;
-(void) callFunction:(int)funcType data:(NSString*)data;
-(NSMutableArray*) getPluginsDetails;
-(NSMutableArray*) getWorkTypes;
-(NSString*) getWorkTypeString;
@end
