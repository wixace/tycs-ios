//
//  BridgeGeneralcore.h
//  BridgeSDK_IOS
//
//  Created by higame on 2018/9/6.
//  Copyright © 2018年 higame. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "IPlugin.h"
#import "BridgeAntiData.h"

@interface BridgeAntiAddiction : NSObject
{
    NSMutableArray* _cores;
}

+ (BridgeAntiAddiction *)getInstance;
-(bool) isSupport:(NSString*)method;
-(void) callFunction:(int)funcType data:(NSString*)data;
-(bool) hasPlugin;
-(NSMutableArray*) getPluginsDetails;
-(void) onAntiAddictionData:(BridgeAntiData*)antiData;
-(void) onRealNameAuthenticationResult:(bool)isSupport isAuth:(bool)isAuth age:(int)age uid:(NSString*)uid other:(NSString*)other;
@end
