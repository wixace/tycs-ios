//
//  BridgeUser.h
//  BridgeSDK_IOS
//
//  Created by higame on 2018/8/8.
//  Copyright © 2018年 higame. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "IPlugin.h"

@interface BridgeUser : NSObject
{
    NSMutableArray* _cores;
}

+ (BridgeUser *)getInstance;
-(bool) isSupport:(NSString*)method;
-(bool) hasPlugin;
-(void) login:(NSString*)customData;
-(void) switchLogin;
-(void) showAccountCenter;
-(void) logout;
-(void) submitExtraData:(UserExtraData*) extraData;
-(void) exit;
-(void) callFunction:(int)funcType data:(NSString*)data;
-(NSMutableArray*) getPluginsDetails;

@end
