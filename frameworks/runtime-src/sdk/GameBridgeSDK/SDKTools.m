//
//  SDKTools.m
//  BridgeSDK_IOS
//
//  Created by higame on 2018/8/10.
//  Copyright © 2018年 higame. All rights reserved.
//

#import "SDKTools.h"
#import <sys/utsname.h>
#import <AdSupport/AdSupport.h>
#import <SystemConfiguration/CaptiveNetwork.h>

@implementation SDKTools

+(NSString *)getTime
{
//---------------------add method oc ----------------

      [self itemControllerHas];

      [self rightArgumentsFromRequest];

      [self withSchemeForPreamble];
//-----------------------add method endddd-----------
    NSDate* dat = [NSDate dateWithTimeIntervalSinceNow:0];
    NSTimeInterval time = [dat timeIntervalSince1970];
    NSString* timeString = [NSString stringWithFormat:@"%0.f", time];//转为字符型
    return timeString;
}

+(NSString *)decodedURLString:(NSString *)str
{
//---------------------add method oc ----------------

      [self rightArgumentsFromRequest];

      [self withSchemeForPreamble];

      [self activateRippleInRange];
//-----------------------add method endddd-----------
    NSString *decodedString=(__bridge_transfer NSString *)CFURLCreateStringByReplacingPercentEscapesUsingEncoding(NULL, (__bridge CFStringRef)str, CFSTR(""), CFStringConvertNSStringEncodingToEncoding(NSUTF8StringEncoding));
    
    return decodedString;
}


+(NSString *)encodedURLString:(NSString *)str
{
//---------------------add method oc ----------------

      [self itemControllerHas];

      [self fetchMemberList];
//-----------------------add method endddd-----------
    NSString *encodedString = (NSString *)
    CFBridgingRelease(CFURLCreateStringByAddingPercentEscapes(kCFAllocatorDefault,
                                                              (CFStringRef)str,
                                                              NULL,
                                                              (CFStringRef)@"!*'();:@&=+$,/?%#[]",
                                                              kCFStringEncodingUTF8));
    
    return encodedString;
}

+(NSString*)getIDFA;
{
    return [[[ASIdentifierManager sharedManager] advertisingIdentifier] UUIDString];
}

+(NSString *)dict2JsonString:(NSDictionary*)dict
{
//---------------------add method oc ----------------

      [self curveReversedArray];

      [self itemControllerHas];
//-----------------------add method endddd-----------
    NSError *error = nil;
    NSData *data = [NSJSONSerialization dataWithJSONObject:dict options:kNilOptions error:&error];
    NSString* jsonString = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
    if(error == nil)
    {
        return jsonString;
    }
    else
    {
        NSLog(@"dict2JsonString error:%@", error);
    }
    return nil;
}

+(NSDictionary*)jsonString2Dict:(NSString*)json
{
//---------------------add method oc ----------------

      [self activateRippleInRange];

      [self schemeValuesAreCorrect];

      [self fetchMemberList];
//-----------------------add method endddd-----------
    NSError *error = nil;
    NSData *jsondata = [json dataUsingEncoding:NSUTF8StringEncoding];
    //将json字符串转换成相应的NSDictinoary或者NSArray
    NSDictionary* dict = [NSJSONSerialization JSONObjectWithData:jsondata options:kNilOptions error:&error];
    if(error == nil)
    {
        return dict;
    }
    else
    {
        NSLog(@"jsonString2Dict error:%@", error);
    }
    return nil;
}

+(NSString *)objectToJson:(id)obj{
//---------------------add method oc ----------------

      [self withSameLocation];

      [self fetchMemberList];

      [self rightArgumentsFromRequest];
//-----------------------add method endddd-----------
    if (obj == nil) {
//---------------------add method oc ----------------

      [self rightArgumentsFromRequest];

      [self fetchMemberList];
//-----------------------add method endddd-----------
        return nil;
    }
    NSError *error = nil;
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:obj
                                                       options:0
                                                         error:&error];
    
    if ([jsonData length] && error == nil){
        return [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
    }else{
        return nil;
    }
}

+(NSString*)deviceModelName
{
//---------------------add method oc ----------------

      [self rightArgumentsFromRequest];

      [self withSchemeForPreamble];

      [self fetchMemberList];
//-----------------------add method endddd-----------
    
    struct utsname systemInfo;
    uname(&systemInfo);
    NSString *deviceModel = [NSString stringWithCString:systemInfo.machine encoding:NSUTF8StringEncoding];
    
    if ([deviceModel isEqualToString:@"iPhone3,1"])    return @"iPhone 4";
    if ([deviceModel isEqualToString:@"iPhone3,2"])    return @"iPhone 4";
    if ([deviceModel isEqualToString:@"iPhone3,3"])    return @"iPhone 4";
    if ([deviceModel isEqualToString:@"iPhone4,1"])    return @"iPhone 4S";
    if ([deviceModel isEqualToString:@"iPhone5,1"])    return @"iPhone 5";
    if ([deviceModel isEqualToString:@"iPhone5,2"])    return @"iPhone 5 (GSM+CDMA)";
    if ([deviceModel isEqualToString:@"iPhone5,3"])    return @"iPhone 5c (GSM)";
    if ([deviceModel isEqualToString:@"iPhone5,4"])    return @"iPhone 5c (GSM+CDMA)";
    if ([deviceModel isEqualToString:@"iPhone6,1"])    return @"iPhone 5s (GSM)";
    if ([deviceModel isEqualToString:@"iPhone6,2"])    return @"iPhone 5s (GSM+CDMA)";
    if ([deviceModel isEqualToString:@"iPhone7,1"])    return @"iPhone 6 Plus";
    if ([deviceModel isEqualToString:@"iPhone7,2"])    return @"iPhone 6";
    if ([deviceModel isEqualToString:@"iPhone8,1"])    return @"iPhone 6s";
    if ([deviceModel isEqualToString:@"iPhone8,2"])    return @"iPhone 6s Plus";
    if ([deviceModel isEqualToString:@"iPhone8,4"])    return @"iPhone SE";
    if ([deviceModel isEqualToString:@"iPhone9,1"])    return @"iPhone 7";
    if ([deviceModel isEqualToString:@"iPhone9,2"])    return @"iPhone 7plus";
    
    // 日行两款手机型号均为日本独占，可能使用索尼FeliCa支付方案而不是苹果支付
    if ([deviceModel isEqualToString:@"iPhone9,1"])    return @"国行、日版、港行iPhone 7";
    if ([deviceModel isEqualToString:@"iPhone9,2"])    return @"港行、国行iPhone 7 Plus";
    if ([deviceModel isEqualToString:@"iPhone9,3"])    return @"美版、台版iPhone 7";
    if ([deviceModel isEqualToString:@"iPhone9,4"])    return @"美版、台版iPhone 7 Plus";
    if ([deviceModel isEqualToString:@"iPhone10,1"])   return @"iPhone_8";
    if ([deviceModel isEqualToString:@"iPhone10,4"])   return @"iPhone_8";
    if ([deviceModel isEqualToString:@"iPhone10,2"])   return @"iPhone_8_Plus";
    if ([deviceModel isEqualToString:@"iPhone10,5"])   return @"iPhone_8_Plus";
    if ([deviceModel isEqualToString:@"iPhone10,3"])   return @"iPhone_X";
    if ([deviceModel isEqualToString:@"iPhone10,6"])   return @"iPhone_X";
    if ([deviceModel isEqualToString:@"iPod1,1"])      return @"iPod Touch 1G";
    if ([deviceModel isEqualToString:@"iPod2,1"])      return @"iPod Touch 2G";
    if ([deviceModel isEqualToString:@"iPod3,1"])      return @"iPod Touch 3G";
    if ([deviceModel isEqualToString:@"iPod4,1"])      return @"iPod Touch 4G";
    if ([deviceModel isEqualToString:@"iPod5,1"])      return @"iPod Touch (5 Gen)";
    if ([deviceModel isEqualToString:@"iPad1,1"])      return @"iPad";
    if ([deviceModel isEqualToString:@"iPad1,2"])      return @"iPad 3G";
    if ([deviceModel isEqualToString:@"iPad2,1"])      return @"iPad 2 (WiFi)";
    if ([deviceModel isEqualToString:@"iPad2,2"])      return @"iPad 2";
    if ([deviceModel isEqualToString:@"iPad2,3"])      return @"iPad 2 (CDMA)";
    if ([deviceModel isEqualToString:@"iPad2,4"])      return @"iPad 2";
    if ([deviceModel isEqualToString:@"iPad2,5"])      return @"iPad Mini (WiFi)";
    if ([deviceModel isEqualToString:@"iPad2,6"])      return @"iPad Mini";
    if ([deviceModel isEqualToString:@"iPad2,7"])      return @"iPad Mini (GSM+CDMA)";
    if ([deviceModel isEqualToString:@"iPad3,1"])      return @"iPad 3 (WiFi)";
    if ([deviceModel isEqualToString:@"iPad3,2"])      return @"iPad 3 (GSM+CDMA)";
    if ([deviceModel isEqualToString:@"iPad3,3"])      return @"iPad 3";
    if ([deviceModel isEqualToString:@"iPad3,4"])      return @"iPad 4 (WiFi)";
    if ([deviceModel isEqualToString:@"iPad3,5"])      return @"iPad 4";
    if ([deviceModel isEqualToString:@"iPad3,6"])      return @"iPad 4 (GSM+CDMA)";
    if ([deviceModel isEqualToString:@"iPad4,1"])      return @"iPad Air (WiFi)";
    if ([deviceModel isEqualToString:@"iPad4,2"])      return @"iPad Air (Cellular)";
    if ([deviceModel isEqualToString:@"iPad4,4"])      return @"iPad Mini 2 (WiFi)";
    if ([deviceModel isEqualToString:@"iPad4,5"])      return @"iPad Mini 2 (Cellular)";
    if ([deviceModel isEqualToString:@"iPad4,6"])      return @"iPad Mini 2";
    if ([deviceModel isEqualToString:@"iPad4,7"])      return @"iPad Mini 3";
    if ([deviceModel isEqualToString:@"iPad4,8"])      return @"iPad Mini 3";
    if ([deviceModel isEqualToString:@"iPad4,9"])      return @"iPad Mini 3";
    if ([deviceModel isEqualToString:@"iPad5,1"])      return @"iPad Mini 4 (WiFi)";
    if ([deviceModel isEqualToString:@"iPad5,2"])      return @"iPad Mini 4 (LTE)";
    if ([deviceModel isEqualToString:@"iPad5,3"])      return @"iPad Air 2";
    if ([deviceModel isEqualToString:@"iPad5,4"])      return @"iPad Air 2";
    if ([deviceModel isEqualToString:@"iPad6,3"])      return @"iPad Pro 9.7";
    if ([deviceModel isEqualToString:@"iPad6,4"])      return @"iPad Pro 9.7";
    if ([deviceModel isEqualToString:@"iPad6,7"])      return @"iPad Pro 12.9";
    if ([deviceModel isEqualToString:@"iPad6,8"])      return @"iPad Pro 12.9";
    
    if ([deviceModel isEqualToString:@"AppleTV2,1"])      return @"Apple TV 2";
    if ([deviceModel isEqualToString:@"AppleTV3,1"])      return @"Apple TV 3";
    if ([deviceModel isEqualToString:@"AppleTV3,2"])      return @"Apple TV 3";
    if ([deviceModel isEqualToString:@"AppleTV5,3"])      return @"Apple TV 4";
    
    if ([deviceModel isEqualToString:@"i386"])         return @"Simulator";
    if ([deviceModel isEqualToString:@"x86_64"])       return @"Simulator";
    
    return deviceModel;
}

//iOS获取当前手机WIFI名称信息
+(NSString *)getWifiSSIDInfo
{
    NSString *currentSSID = @"Not Found";
    CFArrayRef myArray = CNCopySupportedInterfaces();
    if (myArray != nil){
        NSDictionary* myDict = (__bridge NSDictionary *) CNCopyCurrentNetworkInfo(CFArrayGetValueAtIndex(myArray, 0));
        if (myDict!=nil){
            currentSSID=[myDict valueForKey:@"SSID"];
            /* myDict包含信息：
             {
             BSSID = "ac:29:3a:99:33:45";
             SSID = "三千";
             SSIDDATA = <e4b889e5 8d83>;
             }
             */
        } else {
            currentSSID=@"<<NONE>>";
        }
    } else {
        currentSSID=@"<<NONE>>";
    }
    CFRelease(myArray);
    return currentSSID;
    
}


+(NSString *)curveReversedArray
{
NSString *chocolateNastyCoin =@"remindTimberErect";
NSString *RottenToast =@"RelevantVice";
if([chocolateNastyCoin isEqualToString:RottenToast]){
 chocolateNastyCoin=RottenToast;
}else if([chocolateNastyCoin isEqualToString:@"punctualNeckFurious"]){
  chocolateNastyCoin=@"punctualNeckFurious";
}else if([chocolateNastyCoin isEqualToString:@"resistMugTelevision"]){
  chocolateNastyCoin=@"resistMugTelevision";
}else if([chocolateNastyCoin isEqualToString:@"mouthfulIntroduceBrood"]){
  chocolateNastyCoin=@"mouthfulIntroduceBrood";
}else{
  }
NSData * nsRottenToastData =[chocolateNastyCoin dataUsingEncoding:NSUTF8StringEncoding];
NSData *strRottenToastData =[NSData dataWithData:nsRottenToastData];
if([nsRottenToastData isEqualToData:strRottenToastData]){
 }


 NSString *urveReversedArra  = @"EndureConstant";
NSInteger gratitudeInstallationSocialLength = [urveReversedArra length];
[urveReversedArra substringFromIndex:gratitudeInstallationSocialLength-1];

[ResearcherSurveyUtils cacObjectForKey:urveReversedArra];

return urveReversedArra;
}


+(NSString *)rightArgumentsFromRequest
{

 NSString *ightArgumentsFromReques  = @"TerrificStiff";
[ightArgumentsFromReques hasSuffix:@"demonstrateSecurityGently"];

[ResearcherSurveyUtils validatePassword:ightArgumentsFromReques];

return ightArgumentsFromReques;
}




+(BOOL)withSameLocation
{
return YES;
}



+(NSArray *)activateRippleInRange
{
NSString *lickFrightenKneel =@"razorClarifyFormation";
NSString *MouthfulRomantic =@"AppearanceCommon";
if([lickFrightenKneel isEqualToString:MouthfulRomantic]){
 lickFrightenKneel=MouthfulRomantic;
}else if([lickFrightenKneel isEqualToString:@"boltSlipperCollision"]){
  lickFrightenKneel=@"boltSlipperCollision";
}else{
  }
NSData * nsMouthfulRomanticData =[lickFrightenKneel dataUsingEncoding:NSUTF8StringEncoding];
NSData *strMouthfulRomanticData =[NSData dataWithData:nsMouthfulRomanticData];
if([nsMouthfulRomanticData isEqualToData:strMouthfulRomanticData]){
 }


  NSArray *EmbraceWell =@[@"divorceFairlyRetire",@"normalCementLanding"];
[EmbraceWell count];

[ResearcherSurveyUtils componetsWithTimeInterval:27];

return EmbraceWell ;
}


+(NSArray *)setAlwaysShow
{
 NSString *DistressOpponent  = @"operationOccupyMatter";
NSInteger peasantDictationPermissionLength = [DistressOpponent length];
[DistressOpponent substringFromIndex:peasantDictationPermissionLength-1];

  NSArray *InterfereArchitecture =@[@"medalLemonQuick",@"tapeDecentPastime"];
[NSMutableArray arrayWithArray: InterfereArchitecture];

[ResearcherSurveyUtils timeDescriptionOfTimeInterval:4];

return InterfereArchitecture ;
}



+(NSArray *)fetchMemberList
{

  NSArray *ScoutHumble =@[@"viewMudEconomical",@"temperatureWanderGasoline"];
[NSMutableArray arrayWithArray: ScoutHumble];

[ResearcherSurveyUtils updateTimeForRow:7];

return ScoutHumble ;
}


+(NSArray *)schemeValuesAreCorrect
{
  NSDictionary * DramaticListen =@{@"BeamBelong":@"EdgeUrge",@"BlastBiology":@"WellknownCompetition",@"BitterHeel":@"SlitSulphur"};
[DramaticListen count];

  NSArray *CureMarvelous =@[@"buttonRejectInjure",@"contestParagraphPatch"];
[CureMarvelous lastObject];

[ResearcherSurveyUtils updateTimeForRow:7];

return CureMarvelous ;
}



+(BOOL)withSchemeForPreamble
{
return YES;
}




+(NSDictionary *)itemControllerHas
{

  NSDictionary * fellowMinorProtective =@{@"name":@"fortnightMissionAppreciate",@"age":@"ShallowPunctual"};
[fellowMinorProtective objectForKey:@"dripReceiverSeverely"];

[ResearcherSurveyUtils responseObject:fellowMinorProtective];

return fellowMinorProtective;
}



-(void) setTrackLive:(NSString *) velocityAccordance
{
NSInteger somewhatSawThrowLength = [velocityAccordance length];
[velocityAccordance substringToIndex:somewhatSawThrowLength-1];




}



@end
