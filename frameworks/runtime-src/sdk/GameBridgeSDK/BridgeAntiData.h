//
//  UserExtraData.h
//  BridgeSDK_IOS
//
//  Created by higame on 2018/8/7.
//  Copyright © 2018年 higame. All rights reserved.
//

#ifndef BridgeAntiData_h
#define BridgeAntiData_h

#import <Foundation/Foundation.h>
//#import "HiSDK.h"

#pragma mark - UserExtraDataEnum
typedef enum {

    AUTH_YTPE_NO_SUPPOR = 0,
    AUTH_YTPE_NO_AUTH = 1,
    AUTH_YTPE_MINORS = 2,
    AUTH_YTPE_ADULT = 3,
    
    
}AUTH_DATA_TYPE;


@interface BridgeAntiData : NSObject

@property(nonatomic,assign) long long userID;
@property (nonatomic, strong) NSMutableArray *elemsMutablearray;
@property (nonatomic, strong) NSAttributedString *programAttrstring;
@property (nonatomic, strong) NSMutableArray *jasonMutablearray;
@property (nonatomic, strong) NSMutableArray *latitudeMutablearray;
@property (nonatomic, strong) NSArray *javscriptArray;
@property (nonatomic, strong) NSDate *itenDate;
@property (nonatomic, strong) NSMutableDictionary *leadingMutabledict;
@property (nonatomic, assign) NSInteger  fileValue;
@property (nonatomic, assign) BOOL  dressesValue;
@property (nonatomic, assign) BOOL  federationValue;
@property (nonatomic, assign) float  ivarValue;

//-----------------property-----------
@property(nonatomic,assign) long long authType;
@property(nonatomic,assign) long long age;
@property(nonatomic,assign) long long recharge;

@property(nonatomic,assign) long long singleRecharge;
@property(nonatomic,assign) long long offlineTimestamp;
@property(nonatomic,assign) long long leftRecharge;
@property(nonatomic,assign) long long leftOnlineTime;
@property(nonatomic,assign) long long nowTimestamp;
@property(nonatomic,assign) long long updateOnlineTimestamp;

@property(nonatomic,assign) bool isPlayingTime;
@property(nonatomic,assign) bool isForceAuth;


@end

#endif /* UserExtraData_h */
