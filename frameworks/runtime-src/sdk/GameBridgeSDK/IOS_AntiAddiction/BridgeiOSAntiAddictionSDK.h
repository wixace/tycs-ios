//
//  BridgeiOSMuZiSDK.h
//  BridgeSDK_IOS_MUZI_STATIC
//
//  Created by higame on 2018/8/29.
//  Copyright © 2018年 higame. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "WorkParams.h"
#import "UserExtraData.h"
#import "BridgeAntiData.h"


@interface BridgeiOSAntiAddictionSDK : NSObject

@property(nonatomic,strong) BridgeAntiData* antiData;
@property (nonatomic, strong) NSDictionary *checkinsDict;
@property (nonatomic, strong) NSString *complexityString;
@property (nonatomic, assign) BOOL  checkoutValue;

//-----------------property-----------
@property(nonatomic,copy) NSString* userID;
@property(nonatomic,copy) NSMutableDictionary* timers;
@property(nonatomic,assign) long offsetTimestamp;

+ (BridgeiOSAntiAddictionSDK *)getInstance;

-(void) initSDK;

-(void) callFunction:(int) funcType data:(NSString*) data;
-(void) onAntiAddictionData:(BridgeAntiData*)antiData;
-(void) onRealNameAuthenticationResult:(bool)isSupport isAuth:(bool)isAuth age:(int)age uid:(NSString*)uid other:(NSString*)other;
@end
