//
//  KeychainUUID.h
//  BridgeSDK_IOS
//
//  Created by higame on 2018/8/23.
//  Copyright © 2018年 higame. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface KeychainUUID : NSObject
+(NSString *)getDeviceID;
@end
