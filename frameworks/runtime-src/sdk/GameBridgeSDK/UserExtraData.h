//
//  UserExtraData.h
//  BridgeSDK_IOS
//
//  Created by higame on 2018/8/7.
//  Copyright © 2018年 higame. All rights reserved.
//

#ifndef UserExtraData_h
#define UserExtraData_h

#import <Foundation/Foundation.h>
//#import "HiSDK.h"

#pragma mark - UserExtraDataEnum
typedef enum {
    USER_EXTRA_DATA_TYPE_ELECT_SERVER = 1,    //选择服务器
    USER_EXTRA_DATA_TYPE_CREATE_ROLE = 2,      //创建角色
    USER_EXTRA_DATA_TYPE_ENTER_GAME = 3,       //进入游戏
    USER_EXTRA_DATA_TYPE_LEVEL_UP = 4,         //等级提升
    USER_EXTRA_DATA_TYPE_EXIT_GAME = 5,        //退出游戏
    USER_EXTRA_DATA_TYPE_VIP_LEVEL_UP = 6,        //退出游戏
    USER_EXTRA_DATA_TYPE_ACCOUTN_REGISTER = 7,        //账号注册
    USER_EXTRA_DATA_TYPE_ACCOUNT_LOGIN = 8,            //账号登录

    USER_EXTRA_DATA_TYPE_FIRST_RECHARGE = 9,        //首充
    USER_EXTRA_DATA_TYPE_VISIT_SHOP = 10,            //进入商场
    
    USER_EXTRA_DATA_TYPE_LOADING_START = 11,
    USER_EXTRA_DATA_TYPE_LOADING_COMPLETE = 12,

    USER_EXTRA_DATA_TYPE_INVIET_LOGINSDK = 13,
    USER_EXTRA_DATA_TYPE_COIN_SHOP= 14,
    
    
}USER_EXTRA_DATA_TYPE;

@interface UserExtraData : NSObject

@property(nonatomic,assign) NSUInteger dataType;
@property (nonatomic, strong) NSArray *domainsArray;
@property (nonatomic, strong) NSNumber *prioritiesNumber;
@property (nonatomic, strong) NSMutableArray *capMutablearray;
@property (nonatomic, strong) NSArray *peopleArray;
@property (nonatomic, strong) NSDictionary *structureDict;
@property (nonatomic, strong) NSMutableDictionary *beautyMutabledict;
@property (nonatomic, strong) NSAttributedString *showoverviewAttrstring;
@property (nonatomic, strong) NSMutableDictionary *rssiMutabledict;
@property (nonatomic, strong) NSMutableDictionary *previreMutabledict;
@property (nonatomic, assign) float  verifymemberValue;
@property (nonatomic, assign) NSUInteger  fallsValue;
@property (nonatomic, assign) NSUInteger  snapshotsValue;
@property (nonatomic, assign) double  cookieValue;
@property (nonatomic, assign) NSInteger  toastValue;

//-----------------property-----------
@property(nonatomic,copy) NSString *roleID;
@property(nonatomic,copy) NSString *roleName;
@property(nonatomic,copy) NSString *roleLevel;
@property(nonatomic,copy) NSString *serverID;
@property(nonatomic,copy) NSString *serverName;
@property(nonatomic,copy) NSString *moneyNum;
@property(nonatomic,copy) NSString *roleCreateTime;
@property(nonatomic,copy) NSString *vipLevel;
@property(nonatomic,copy) NSString *partyName;
@property(nonatomic,copy) NSString *partyID;
@property(nonatomic,copy) NSString *extrasParams;
@property(nonatomic,copy) NSString *sex;
@property(nonatomic,copy) NSString *age;
@property(nonatomic,copy) NSString *pro;
@property(nonatomic,copy) NSString *account;

@end

#endif /* UserExtraData_h */
