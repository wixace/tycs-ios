//
//  BasicDataRequest.h
//  LeNiuAD
//
//  Created by XingJie Liang on 14-4-17.
//  Copyright (c) 2014年 XingJie Liang. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "HTTPClient.h"

typedef void(^SuccessBlock)(NSDictionary *result);
typedef void(^FailedBlock)(NSString *info, NSString *errorCode);
typedef void(^NetworkErrorBlock)(NSString *info, HTTPClientResponseType httpType);

@interface BasicDataRequest : NSObject

+ (void) requestWithParams:(NSDictionary *)params urlString:(NSString *)url successBlock:(SuccessBlock)success failedBlock:(FailedBlock)failed networkError:(NetworkErrorBlock)networkError;

+ (void) requestWithParams:(NSDictionary *)params urlString:(NSString *)url operationQueue:(NSOperationQueue *)operationQueue successBlock:(SuccessBlock)success failedBlock:(FailedBlock)failed networkError:(NetworkErrorBlock)networkError;

@end
