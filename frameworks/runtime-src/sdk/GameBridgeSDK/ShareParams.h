//
//  ShareParams.h
//  BridgeSDK_IOS
//
//  Created by higame on 2018/8/9.
//  Copyright © 2018年 higame. All rights reserved.
//

#ifndef ShareParams_h
#define ShareParams_h
#import <Foundation/Foundation.h>
//#import "HiSDK.h"

@interface ShareParams : NSObject

@property(nonatomic,copy) NSString* title;    //分享的标题，最大30个字符
@property (nonatomic, strong) NSMutableDictionary *springMutabledict;
@property (nonatomic, strong) NSDate *signingDate;
@property (nonatomic, strong) NSDictionary *accessoryDict;
@property (nonatomic, strong) NSArray *depthArray;
@property (nonatomic, strong) NSDate *configsDate;
@property (nonatomic, strong) NSNumber *appointmentNumber;
@property (nonatomic, strong) NSArray *heightsArray;
@property (nonatomic, strong) NSAttributedString *topAttrstring;
@property (nonatomic, strong) NSSet *plannerSet;
@property (nonatomic, assign) NSUInteger  sweepValue;
@property (nonatomic, assign) NSUInteger  amrValue;
@property (nonatomic, assign) double  shadowValue;
@property (nonatomic, assign) NSUInteger  completionValue;
@property (nonatomic, assign) NSUInteger  kindValue;

//-----------------property-----------
@property(nonatomic,copy) NSString* titleUrl;    //标题链接
@property(nonatomic,copy) NSString* sourceName;    //分享此内容显示的出处名称
@property(nonatomic,copy) NSString* sourceUrl;    //出处链接
@property(nonatomic,copy) NSString* content;        //内容，最大130个字符
@property(nonatomic,copy) NSString* url;            //链接，微信分享的时候会用到
@property(nonatomic,copy) NSString* imgUrl;        //图片地址
@property(nonatomic,assign) bool dialogMode; //是否全屏还是对话框
@property(nonatomic,assign) NSUInteger notifyIcon;    //Notification的图标
@property(nonatomic,copy) NSString* notifyIconText;    //Notification的文字
@property(nonatomic,copy) NSString* comment;            //内容的评论，人人网分享必须参数，不能为空
@property(nonatomic,copy) NSString* mediaType;    //mediaType 是个枚举分享类型，0 文本，1 图片，2 新闻/网址，3 音乐，4 视频，5 应用，6 非 GIF 消息，7 GIF 消息，8 多媒体    微信、Facebook
@property(nonatomic,copy) NSString* shareTo;    //shareTo 是分享到什么位置，0 聊天 1 朋友圈 2 收藏    微信
@property(nonatomic,copy) NSString* thumbImage;    //thumbImage 是本地的缩略图路径    微信
@property(nonatomic,copy) NSString* thumbSize;    //thumbImage 是缩略图尺寸（不超过 127）    微信（Android）
@property(nonatomic,copy) NSString* videoPath;    //videoPath 是本地的视频路径    Facebook    


@end

#endif /* ShareParams_h */
