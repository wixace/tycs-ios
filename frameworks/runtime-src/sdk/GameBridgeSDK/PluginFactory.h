//
//  BridgeUser.h
//  BridgeSDK_IOS
//
//  Created by higame on 2018/8/8.
//  Copyright © 2018年 higame. All rights reserved.
//

#import <Foundation/Foundation.h>

#import "SDKParams.h"
@interface PluginFactory : NSObject
{
    NSMutableArray* _pluginsConfig;
}


+(PluginFactory *) getInstance;

//-(bool) isSupportPlugin:(int)type;
//-(NSMutableArray*) getPluginsName:(int)type;
//-(NSMutableArray*) getPlugins:(int)type;
-(SDKParams*) getSDKParams;
//-(void) loadPluginInfo;

@end
