//
//  SDKParams.h
//  BridgeSDK_IOS
//
//  Created by higame on 2018/8/9.
//  Copyright © 2018年 higame. All rights reserved.
//

#import <Foundation/Foundation.h>
//#import "HiSDK.h"

@interface SDKParams : NSObject
{
    NSDictionary* _configs;
}
-(instancetype) initWithCfg:(NSDictionary*)cfg;
-(bool)contains:(NSString*)key;
-(NSString*)getString:(NSString*)key;
-(int) getInt:(NSString*)key;
-(float) getFloat:(NSString*)key;
-(double) getDouble:(NSString*)key;
-(long) getLong:(NSString*)key;
-(bool) getBoolean:(NSString*)key;
-(short) getShort:(NSString*)key;
-(Byte) getBtye:(NSString*)key;
-(NSString *)description;

@end
