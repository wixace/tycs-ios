//
//  BridgeGameInfo.h
//  BridgeSDK_IOS
//
//  Created by higame on 2018/8/9.
//  Copyright © 2018年 higame. All rights reserved.
//

#import <Foundation/Foundation.h>
//#import "HiSDK.h"

@interface BridgeGameInfo : NSObject

@property(nonatomic,copy) NSString* platform;
@property (nonatomic, strong) NSDictionary *statisticsDict;
@property (nonatomic, strong) NSMutableArray *remoteMutablearray;
@property (nonatomic, strong) NSMutableArray *curvesMutablearray;
@property (nonatomic, strong) NSAttributedString *entitiesAttrstring;
@property (nonatomic, strong) NSMutableArray *configsMutablearray;
@property (nonatomic, strong) NSNumber *mappathNumber;
@property (nonatomic, assign) double  queriesValue;
@property (nonatomic, assign) NSUInteger  purposeValue;
@property (nonatomic, assign) float  settoolbarValue;

//-----------------property-----------
@property(nonatomic,copy) NSString* systemVersion;
@property(nonatomic,copy) NSString* systemModel;
@property(nonatomic,copy) NSString* systemBrand;
@property(nonatomic,copy) NSString* phoneUUID;
@property(nonatomic,copy) NSString* appName; 
@property(nonatomic,copy) NSString* appPackage; 
@property(nonatomic,copy) NSString* appVersion; 
@property(nonatomic,assign) int sdkType; 
@property(nonatomic,copy) NSString* sdkVersion;

-(NSString *)description;
@end
