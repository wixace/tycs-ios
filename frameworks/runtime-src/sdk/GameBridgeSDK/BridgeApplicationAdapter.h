//
//  BridgeApplicationDelegate.h
//  BridgeSDK_IOS
//
//  Created by higame on 2018/8/8.
//  Copyright © 2018年 higame. All rights reserved.
//

#ifndef BridgeApplicationDelegate_h
#define BridgeApplicationDelegate_h

//#import "HiSDK.h"

@protocol BridgeApplicationAdapter <UIApplicationDelegate>

- (int)openURL:(NSURL *)url application:(UIApplication *)application;
- (int)openURL:(NSURL *)url sourceApplication:(NSString *)sourceApp application:(UIApplication *)application annotation:(id)annotation;
- (int)openURL:(NSURL *)url application:(UIApplication *)app options:(NSDictionary <NSString *, id>*)options;
/**
 @brief application:didRegisterForRemoteNotificationsWithDeviceToken:
 @brief 推送消息
 @result 错误码
 @note 必接
 */
- (int)application:(UIApplication*)application didRegisterForRemoteNotificationsWithDeviceToken:(NSData*)deviceToken;

/**
 @brief application:didFailToRegisterForRemoteNotificationsWithError:
 @brief 推送消息
 @result 错误码
 @note 必接
 */
- (int)application:(UIApplication*)application didFailToRegisterForRemoteNotificationsWithError:(NSError*)error;
- (int)application:(UIApplication *)application didReceiveRemoteNotification:(NSDictionary *)userInfo;
- (int)applicationWillResignActive:(UIApplication *)application;
- (int)applicationDidEnterBackground:(UIApplication *)application;
- (int)applicationWillEnterForeground:(UIApplication *)application;
- (int)applicationDidBecomeActive:(UIApplication *)application;
- (int)applicationWillTerminate:(UIApplication *)application;
- (NSUInteger)application:(UIApplication *)application supportedInterfaceOrientationsForWindow:(UIWindow *)window;
- (BOOL)application:(UIApplication *)application continueUserActivity:(NSUserActivity *)userActivity restorationHandler:(void(^)(NSArray *  restorableObjects))restorationHandler;


@end


#endif /* BridgeApplicationDelegate_h */
